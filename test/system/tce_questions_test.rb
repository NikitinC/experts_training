require "application_system_test_case"

class TceQuestionsTest < ApplicationSystemTestCase
  setup do
    @tce_question = tce_questions(:one)
  end

  test "visiting the index" do
    visit tce_questions_url
    assert_selector "h1", text: "Tce Questions"
  end

  test "creating a Tce question" do
    visit tce_questions_url
    click_on "New Tce Question"

    fill_in "Question description", with: @tce_question.question_description
    fill_in "Question", with: @tce_question.question_id
    fill_in "Question subject", with: @tce_question.question_subject_id
    click_on "Create Tce question"

    assert_text "Tce question was successfully created"
    click_on "Back"
  end

  test "updating a Tce question" do
    visit tce_questions_url
    click_on "Edit", match: :first

    fill_in "Question description", with: @tce_question.question_description
    fill_in "Question", with: @tce_question.question_id
    fill_in "Question subject", with: @tce_question.question_subject_id
    click_on "Update Tce question"

    assert_text "Tce question was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce question" do
    visit tce_questions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce question was successfully destroyed"
  end
end
