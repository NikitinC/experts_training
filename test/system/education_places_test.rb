require "application_system_test_case"

class EducationPlacesTest < ApplicationSystemTestCase
  setup do
    @education_place = education_places(:one)
  end

  test "visiting the index" do
    visit education_places_url
    assert_selector "h1", text: "Education Places"
  end

  test "creating a Education places" do
    visit education_places_url
    click_on "New Education Places"

    fill_in "Code", with: @education_place.code
    fill_in "Name", with: @education_place.name
    fill_in "Short name", with: @education_place.short_name
    fill_in "Sort by", with: @education_place.sort_by
    click_on "Create Education places"

    assert_text "Education places was successfully created"
    click_on "Back"
  end

  test "updating a Education places" do
    visit education_places_url
    click_on "Edit", match: :first

    fill_in "Code", with: @education_place.code
    fill_in "Name", with: @education_place.name
    fill_in "Short name", with: @education_place.short_name
    fill_in "Sort by", with: @education_place.sort_by
    click_on "Update Education places"

    assert_text "Education places was successfully updated"
    click_on "Back"
  end

  test "destroying a Education places" do
    visit education_places_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Education places was successfully destroyed"
  end
end
