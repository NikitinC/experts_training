require "application_system_test_case"

class RbdcSchoolFinanceTypesTest < ApplicationSystemTestCase
  setup do
    @rbdc_school_finance_type = rbdc_school_finance_types(:one)
  end

  test "visiting the index" do
    visit rbdc_school_finance_types_url
    assert_selector "h1", text: "Rbdc School Finance Types"
  end

  test "creating a Rbdc school finance type" do
    visit rbdc_school_finance_types_url
    click_on "New Rbdc School Finance Type"

    fill_in "Code", with: @rbdc_school_finance_type.code
    fill_in "Name", with: @rbdc_school_finance_type.name
    fill_in "Short name", with: @rbdc_school_finance_type.short_name
    fill_in "Sort by", with: @rbdc_school_finance_type.sort_by
    click_on "Create Rbdc school finance type"

    assert_text "Rbdc school finance type was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc school finance type" do
    visit rbdc_school_finance_types_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_school_finance_type.code
    fill_in "Name", with: @rbdc_school_finance_type.name
    fill_in "Short name", with: @rbdc_school_finance_type.short_name
    fill_in "Sort by", with: @rbdc_school_finance_type.sort_by
    click_on "Update Rbdc school finance type"

    assert_text "Rbdc school finance type was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc school finance type" do
    visit rbdc_school_finance_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc school finance type was successfully destroyed"
  end
end
