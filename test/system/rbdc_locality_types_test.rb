require "application_system_test_case"

class RbdcLocalityTypesTest < ApplicationSystemTestCase
  setup do
    @rbdc_locality_type = rbdc_locality_types(:one)
  end

  test "visiting the index" do
    visit rbdc_locality_types_url
    assert_selector "h1", text: "Rbdc Locality Types"
  end

  test "creating a Rbdc locality type" do
    visit rbdc_locality_types_url
    click_on "New Rbdc Locality Type"

    fill_in "Code", with: @rbdc_locality_type.code
    fill_in "Name", with: @rbdc_locality_type.name
    fill_in "Short name", with: @rbdc_locality_type.short_name
    fill_in "Sort by", with: @rbdc_locality_type.sort_by
    click_on "Create Rbdc locality type"

    assert_text "Rbdc locality type was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc locality type" do
    visit rbdc_locality_types_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_locality_type.code
    fill_in "Name", with: @rbdc_locality_type.name
    fill_in "Short name", with: @rbdc_locality_type.short_name
    fill_in "Sort by", with: @rbdc_locality_type.sort_by
    click_on "Update Rbdc locality type"

    assert_text "Rbdc locality type was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc locality type" do
    visit rbdc_locality_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc locality type was successfully destroyed"
  end
end
