require "application_system_test_case"

class RbdcSchoolTypesTest < ApplicationSystemTestCase
  setup do
    @rbdc_school_type = rbdc_school_types(:one)
  end

  test "visiting the index" do
    visit rbdc_school_types_url
    assert_selector "h1", text: "Rbdc School Types"
  end

  test "creating a Rbdc school type" do
    visit rbdc_school_types_url
    click_on "New Rbdc School Type"

    fill_in "Code", with: @rbdc_school_type.code
    fill_in "Name", with: @rbdc_school_type.name
    fill_in "Short name", with: @rbdc_school_type.short_name
    fill_in "Sort by", with: @rbdc_school_type.sort_by
    click_on "Create Rbdc school type"

    assert_text "Rbdc school type was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc school type" do
    visit rbdc_school_types_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_school_type.code
    fill_in "Name", with: @rbdc_school_type.name
    fill_in "Short name", with: @rbdc_school_type.short_name
    fill_in "Sort by", with: @rbdc_school_type.sort_by
    click_on "Update Rbdc school type"

    assert_text "Rbdc school type was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc school type" do
    visit rbdc_school_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc school type was successfully destroyed"
  end
end
