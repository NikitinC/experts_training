require "application_system_test_case"

class JobPlacesTest < ApplicationSystemTestCase
  setup do
    @job_place = job_places(:one)
  end

  test "visiting the index" do
    visit job_places_url
    assert_selector "h1", text: "Job Places"
  end

  test "creating a Job places" do
    visit job_places_url
    click_on "New Job Places"

    fill_in "Code", with: @job_place.code
    fill_in "Name", with: @job_place.name
    fill_in "Short name", with: @job_place.short_name
    fill_in "Sort by", with: @job_place.sort_by
    click_on "Create Job places"

    assert_text "Job places was successfully created"
    click_on "Back"
  end

  test "updating a Job places" do
    visit job_places_url
    click_on "Edit", match: :first

    fill_in "Code", with: @job_place.code
    fill_in "Name", with: @job_place.name
    fill_in "Short name", with: @job_place.short_name
    fill_in "Sort by", with: @job_place.sort_by
    click_on "Update Job places"

    assert_text "Job places was successfully updated"
    click_on "Back"
  end

  test "destroying a Job places" do
    visit job_places_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Job places was successfully destroyed"
  end
end
