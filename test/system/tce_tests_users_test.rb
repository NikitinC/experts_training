require "application_system_test_case"

class TceTestsUsersTest < ApplicationSystemTestCase
  setup do
    @tce_tests_user = tce_tests_users(:one)
  end

  test "visiting the index" do
    visit tce_tests_users_url
    assert_selector "h1", text: "Tce Tests Users"
  end

  test "creating a Tce tests user" do
    visit tce_tests_users_url
    click_on "New Tce Tests User"

    fill_in "Testuser comment", with: @tce_tests_user.testuser_comment
    fill_in "Testuser creation time", with: @tce_tests_user.testuser_creation_time
    fill_in "Testuser", with: @tce_tests_user.testuser_id
    fill_in "Testuser status", with: @tce_tests_user.testuser_status
    fill_in "Testuser test", with: @tce_tests_user.testuser_test_id
    fill_in "Testuser user", with: @tce_tests_user.testuser_user_id
    click_on "Create Tce tests user"

    assert_text "Tce tests user was successfully created"
    click_on "Back"
  end

  test "updating a Tce tests user" do
    visit tce_tests_users_url
    click_on "Edit", match: :first

    fill_in "Testuser comment", with: @tce_tests_user.testuser_comment
    fill_in "Testuser creation time", with: @tce_tests_user.testuser_creation_time
    fill_in "Testuser", with: @tce_tests_user.testuser_id
    fill_in "Testuser status", with: @tce_tests_user.testuser_status
    fill_in "Testuser test", with: @tce_tests_user.testuser_test_id
    fill_in "Testuser user", with: @tce_tests_user.testuser_user_id
    click_on "Update Tce tests user"

    assert_text "Tce tests user was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce tests user" do
    visit tce_tests_users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce tests user was successfully destroyed"
  end
end
