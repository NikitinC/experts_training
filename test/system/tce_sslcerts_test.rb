require "application_system_test_case"

class TceSslcertsTest < ApplicationSystemTestCase
  setup do
    @tce_sslcert = tce_sslcerts(:one)
  end

  test "visiting the index" do
    visit tce_sslcerts_url
    assert_selector "h1", text: "Tce Sslcerts"
  end

  test "creating a Tce sslcert" do
    visit tce_sslcerts_url
    click_on "New Tce Sslcert"

    check "Ssl enabled" if @tce_sslcert.ssl_enabled
    fill_in "Ssl end date", with: @tce_sslcert.ssl_end_date
    fill_in "Ssl hash", with: @tce_sslcert.ssl_hash
    fill_in "Ssl", with: @tce_sslcert.ssl_id
    fill_in "Ssl name", with: @tce_sslcert.ssl_name
    fill_in "Ssl user", with: @tce_sslcert.ssl_user_id
    click_on "Create Tce sslcert"

    assert_text "Tce sslcert was successfully created"
    click_on "Back"
  end

  test "updating a Tce sslcert" do
    visit tce_sslcerts_url
    click_on "Edit", match: :first

    check "Ssl enabled" if @tce_sslcert.ssl_enabled
    fill_in "Ssl end date", with: @tce_sslcert.ssl_end_date
    fill_in "Ssl hash", with: @tce_sslcert.ssl_hash
    fill_in "Ssl", with: @tce_sslcert.ssl_id
    fill_in "Ssl name", with: @tce_sslcert.ssl_name
    fill_in "Ssl user", with: @tce_sslcert.ssl_user_id
    click_on "Update Tce sslcert"

    assert_text "Tce sslcert was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce sslcert" do
    visit tce_sslcerts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce sslcert was successfully destroyed"
  end
end
