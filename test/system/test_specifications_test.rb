require "application_system_test_case"

class TestSpecificationsTest < ApplicationSystemTestCase
  setup do
    @test_specification = test_specifications(:one)
  end

  test "visiting the index" do
    visit test_specifications_url
    assert_selector "h1", text: "Test Specifications"
  end

  test "creating a Test specifications" do
    visit test_specifications_url
    click_on "New Test Specifications"

    fill_in "Examdate", with: @test_specification.examdate
    fill_in "Examdate end", with: @test_specification.examdate_end
    fill_in "Maxprimarymarc", with: @test_specification.maxprimarymarc
    fill_in "Maxprimarymark", with: @test_specification.maxprimarymark
    fill_in "Maxprimarymarka", with: @test_specification.maxprimarymarka
    fill_in "Maxprimarymarkb", with: @test_specification.maxprimarymarkb
    fill_in "Maxprimarymarkd", with: @test_specification.maxprimarymarkd
    fill_in "Name", with: @test_specification.name
    fill_in "Parallel", with: @test_specification.parallel
    fill_in "Subject", with: @test_specification.subject_id
    fill_in "Test type", with: @test_specification.exam_type_id
    fill_in "Testresultsa mask", with: @test_specification.testresultsa_mask
    fill_in "Testresultsa numtasks", with: @test_specification.testresultsa_numtasks
    fill_in "Testresultsb mask", with: @test_specification.testresultsb_mask
    fill_in "Testresultsb numtasks", with: @test_specification.testresultsb_numtasks
    fill_in "Testresultsc mask", with: @test_specification.testresultsc_mask
    fill_in "Testresultsc numtasks", with: @test_specification.testresultsc_numtasks
    fill_in "Testresultsd mask", with: @test_specification.testresultsd_mask
    fill_in "Testresultsd numtasks", with: @test_specification.testresultsd_numtasks
    click_on "Create Test specifications"

    assert_text "Test specifications was successfully created"
    click_on "Back"
  end

  test "updating a Test specifications" do
    visit test_specifications_url
    click_on "Edit", match: :first

    fill_in "Examdate", with: @test_specification.examdate
    fill_in "Examdate end", with: @test_specification.examdate_end
    fill_in "Maxprimarymarc", with: @test_specification.maxprimarymarc
    fill_in "Maxprimarymark", with: @test_specification.maxprimarymark
    fill_in "Maxprimarymarka", with: @test_specification.maxprimarymarka
    fill_in "Maxprimarymarkb", with: @test_specification.maxprimarymarkb
    fill_in "Maxprimarymarkd", with: @test_specification.maxprimarymarkd
    fill_in "Name", with: @test_specification.name
    fill_in "Parallel", with: @test_specification.parallel
    fill_in "Subject", with: @test_specification.subject_id
    fill_in "Test type", with: @test_specification.exam_type_id
    fill_in "Testresultsa mask", with: @test_specification.testresultsa_mask
    fill_in "Testresultsa numtasks", with: @test_specification.testresultsa_numtasks
    fill_in "Testresultsb mask", with: @test_specification.testresultsb_mask
    fill_in "Testresultsb numtasks", with: @test_specification.testresultsb_numtasks
    fill_in "Testresultsc mask", with: @test_specification.testresultsc_mask
    fill_in "Testresultsc numtasks", with: @test_specification.testresultsc_numtasks
    fill_in "Testresultsd mask", with: @test_specification.testresultsd_mask
    fill_in "Testresultsd numtasks", with: @test_specification.testresultsd_numtasks
    click_on "Update Test specifications"

    assert_text "Test specifications was successfully updated"
    click_on "Back"
  end

  test "destroying a Test specifications" do
    visit test_specifications_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Test specifications was successfully destroyed"
  end
end
