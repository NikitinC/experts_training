require "application_system_test_case"

class TceUserGroupsTest < ApplicationSystemTestCase
  setup do
    @tce_user_group = tce_user_groups(:one)
  end

  test "visiting the index" do
    visit tce_user_groups_url
    assert_selector "h1", text: "Tce User Groups"
  end

  test "creating a Tce user group" do
    visit tce_user_groups_url
    click_on "New Tce User Group"

    fill_in "Group", with: @tce_user_group.group_id
    fill_in "Group name", with: @tce_user_group.group_name
    click_on "Create Tce user group"

    assert_text "Tce user group was successfully created"
    click_on "Back"
  end

  test "updating a Tce user group" do
    visit tce_user_groups_url
    click_on "Edit", match: :first

    fill_in "Group", with: @tce_user_group.group_id
    fill_in "Group name", with: @tce_user_group.group_name
    click_on "Update Tce user group"

    assert_text "Tce user group was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce user group" do
    visit tce_user_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce user group was successfully destroyed"
  end
end
