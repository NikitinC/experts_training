require "application_system_test_case"

class DownloadCountsTest < ApplicationSystemTestCase
  setup do
    @download_count = download_counts(:one)
  end

  test "visiting the index" do
    visit download_counts_url
    assert_selector "h1", text: "Download Counts"
  end

  test "creating a Download count" do
    visit download_counts_url
    click_on "New Download Count"

    fill_in "Resource", with: @download_count.resource
    fill_in "User", with: @download_count.user_id
    click_on "Create Download count"

    assert_text "Download count was successfully created"
    click_on "Back"
  end

  test "updating a Download count" do
    visit download_counts_url
    click_on "Edit", match: :first

    fill_in "Resource", with: @download_count.resource
    fill_in "User", with: @download_count.user_id
    click_on "Update Download count"

    assert_text "Download count was successfully updated"
    click_on "Back"
  end

  test "destroying a Download count" do
    visit download_counts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Download count was successfully destroyed"
  end
end
