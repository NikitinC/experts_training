require "application_system_test_case"

class GroupsTest < ApplicationSystemTestCase
  setup do
    @group = groups(:one)
  end

  test "visiting the index" do
    visit groups_url
    assert_selector "h1", text: "Groups"
  end

  test "creating a Group" do
    visit groups_url
    click_on "New Group"

    fill_in "Guid", with: @group.guid
    fill_in "Letter", with: @group.letter
    fill_in "Number", with: @group.number
    fill_in "Ou", with: @group.ou_id
    fill_in "School class profile", with: @group.school_class_profile_id
    fill_in "Subgroup", with: @group.subgroup
    click_on "Create Group"

    assert_text "Group was successfully created"
    click_on "Back"
  end

  test "updating a Group" do
    visit groups_url
    click_on "Edit", match: :first

    fill_in "Guid", with: @group.guid
    fill_in "Letter", with: @group.letter
    fill_in "Number", with: @group.number
    fill_in "Ou", with: @group.ou_id
    fill_in "School class profile", with: @group.school_class_profile_id
    fill_in "Subgroup", with: @group.subgroup
    click_on "Update Group"

    assert_text "Group was successfully updated"
    click_on "Back"
  end

  test "destroying a Group" do
    visit groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Group was successfully destroyed"
  end
end
