require "application_system_test_case"

class AcademicDegreesTest < ApplicationSystemTestCase
  setup do
    @academic_degree = academic_degrees(:one)
  end

  test "visiting the index" do
    visit academic_degrees_url
    assert_selector "h1", text: "Academic Degres"
  end

  test "creating a Academic degres" do
    visit academic_degrees_url
    click_on "New Academic Degres"

    fill_in "Code", with: @academic_degree.code
    fill_in "Name", with: @academic_degree.name
    fill_in "Short name", with: @academic_degree.short_name
    fill_in "Sort by", with: @academic_degree.sort_by
    click_on "Create Academic degres"

    assert_text "Academic degres was successfully created"
    click_on "Back"
  end

  test "updating a Academic degres" do
    visit academic_degrees_url
    click_on "Edit", match: :first

    fill_in "Code", with: @academic_degree.code
    fill_in "Name", with: @academic_degree.name
    fill_in "Short name", with: @academic_degree.short_name
    fill_in "Sort by", with: @academic_degree.sort_by
    click_on "Update Academic degres"

    assert_text "Academic degres was successfully updated"
    click_on "Back"
  end

  test "destroying a Academic degres" do
    visit academic_degrees_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Academic degres was successfully destroyed"
  end
end
