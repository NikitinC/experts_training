require "application_system_test_case"

class DestroyReasonsTest < ApplicationSystemTestCase
  setup do
    @destroy_reason = destroy_reasons(:one)
  end

  test "visiting the index" do
    visit destroy_reasons_url
    assert_selector "h1", text: "Destroy Reasons"
  end

  test "creating a Destroy reason" do
    visit destroy_reasons_url
    click_on "New Destroy Reason"

    fill_in "Code", with: @destroy_reason.code
    fill_in "Deleted at", with: @destroy_reason.deleted_at
    fill_in "Fisgia11code", with: @destroy_reason.fisgia11code
    fill_in "Fisgia9code", with: @destroy_reason.fisgia9code
    fill_in "Name", with: @destroy_reason.name
    fill_in "Short name", with: @destroy_reason.short_name
    fill_in "Sort by", with: @destroy_reason.sort_by
    click_on "Create Destroy reason"

    assert_text "Destroy reason was successfully created"
    click_on "Back"
  end

  test "updating a Destroy reason" do
    visit destroy_reasons_url
    click_on "Edit", match: :first

    fill_in "Code", with: @destroy_reason.code
    fill_in "Deleted at", with: @destroy_reason.deleted_at
    fill_in "Fisgia11code", with: @destroy_reason.fisgia11code
    fill_in "Fisgia9code", with: @destroy_reason.fisgia9code
    fill_in "Name", with: @destroy_reason.name
    fill_in "Short name", with: @destroy_reason.short_name
    fill_in "Sort by", with: @destroy_reason.sort_by
    click_on "Update Destroy reason"

    assert_text "Destroy reason was successfully updated"
    click_on "Back"
  end

  test "destroying a Destroy reason" do
    visit destroy_reasons_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Destroy reason was successfully destroyed"
  end
end
