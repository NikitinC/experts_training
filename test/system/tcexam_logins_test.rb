require "application_system_test_case"

class TcexamLoginsTest < ApplicationSystemTestCase
  setup do
    @tcexam_login = tcexam_logins(:one)
  end

  test "visiting the index" do
    visit tcexam_logins_url
    assert_selector "h1", text: "Tcexam Logins"
  end

  test "creating a Tcexam login" do
    visit tcexam_logins_url
    click_on "New Tcexam Login"

    fill_in "User birthdate", with: @tcexam_login.user_birthdate
    fill_in "User birthplace", with: @tcexam_login.user_birthplace
    fill_in "User email", with: @tcexam_login.user_email
    fill_in "User firstname", with: @tcexam_login.user_firstname
    fill_in "User ip", with: @tcexam_login.user_ip
    fill_in "User lastname", with: @tcexam_login.user_lastname
    fill_in "User level", with: @tcexam_login.user_level
    fill_in "User name", with: @tcexam_login.user_name
    fill_in "User otpkey", with: @tcexam_login.user_otpkey
    fill_in "User password", with: @tcexam_login.user_password
    fill_in "User regdate", with: @tcexam_login.user_regdate
    fill_in "User regnumber", with: @tcexam_login.user_regnumber
    fill_in "User ssn", with: @tcexam_login.user_ssn
    fill_in "User verifycode", with: @tcexam_login.user_verifycode
    click_on "Create Tcexam login"

    assert_text "Tcexam login was successfully created"
    click_on "Back"
  end

  test "updating a Tcexam login" do
    visit tcexam_logins_url
    click_on "Edit", match: :first

    fill_in "User birthdate", with: @tcexam_login.user_birthdate
    fill_in "User birthplace", with: @tcexam_login.user_birthplace
    fill_in "User email", with: @tcexam_login.user_email
    fill_in "User firstname", with: @tcexam_login.user_firstname
    fill_in "User ip", with: @tcexam_login.user_ip
    fill_in "User lastname", with: @tcexam_login.user_lastname
    fill_in "User level", with: @tcexam_login.user_level
    fill_in "User name", with: @tcexam_login.user_name
    fill_in "User otpkey", with: @tcexam_login.user_otpkey
    fill_in "User password", with: @tcexam_login.user_password
    fill_in "User regdate", with: @tcexam_login.user_regdate
    fill_in "User regnumber", with: @tcexam_login.user_regnumber
    fill_in "User ssn", with: @tcexam_login.user_ssn
    fill_in "User verifycode", with: @tcexam_login.user_verifycode
    click_on "Update Tcexam login"

    assert_text "Tcexam login was successfully updated"
    click_on "Back"
  end

  test "destroying a Tcexam login" do
    visit tcexam_logins_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tcexam login was successfully destroyed"
  end
end
