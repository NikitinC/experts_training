require "application_system_test_case"

class TeacherCategoriesTest < ApplicationSystemTestCase
  setup do
    @teacher_categories = teacher_categories(:one)
  end

  test "visiting the index" do
    visit teacher_categories_url
    assert_selector "h1", text: "Teacher Categories"
  end

  test "creating a Teacher categories" do
    visit teacher_categories_url
    click_on "New Teacher Categories"

    fill_in "Code", with: @teacher_categories.code
    fill_in "Name", with: @teacher_categories.name
    fill_in "Short name", with: @teacher_categories.short_name
    fill_in "Sort by", with: @teacher_categories.sort_by
    click_on "Create Teacher categories"

    assert_text "Teacher categories was successfully created"
    click_on "Back"
  end

  test "updating a Teacher categories" do
    visit teacher_categories_url
    click_on "Edit", match: :first

    fill_in "Code", with: @teacher_categories.code
    fill_in "Name", with: @teacher_categories.name
    fill_in "Short name", with: @teacher_categories.short_name
    fill_in "Sort by", with: @teacher_categories.sort_by
    click_on "Update Teacher categories"

    assert_text "Teacher categories was successfully updated"
    click_on "Back"
  end

  test "destroying a Teacher categories" do
    visit teacher_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Teacher categories was successfully destroyed"
  end
end
