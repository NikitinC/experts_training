require "application_system_test_case"

class TceSubjectsTest < ApplicationSystemTestCase
  setup do
    @tce_subject = tce_subjects(:one)
  end

  test "visiting the index" do
    visit tce_subjects_url
    assert_selector "h1", text: "Tce Subjects"
  end

  test "creating a Tce subject" do
    visit tce_subjects_url
    click_on "New Tce Subject"

    fill_in "Subject description", with: @tce_subject.subject_description
    check "Subject enabled" if @tce_subject.subject_enabled
    fill_in "Subject", with: @tce_subject.subject_id
    fill_in "Subject module", with: @tce_subject.subject_module_id
    fill_in "Subject name", with: @tce_subject.name
    fill_in "Subject user", with: @tce_subject.subject_user_id
    click_on "Create Tce subject"

    assert_text "Tce subject was successfully created"
    click_on "Back"
  end

  test "updating a Tce subject" do
    visit tce_subjects_url
    click_on "Edit", match: :first

    fill_in "Subject description", with: @tce_subject.subject_description
    check "Subject enabled" if @tce_subject.subject_enabled
    fill_in "Subject", with: @tce_subject.subject_id
    fill_in "Subject module", with: @tce_subject.subject_module_id
    fill_in "Subject name", with: @tce_subject.name
    fill_in "Subject user", with: @tce_subject.subject_user_id
    click_on "Update Tce subject"

    assert_text "Tce subject was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce subject" do
    visit tce_subjects_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce subject was successfully destroyed"
  end
end
