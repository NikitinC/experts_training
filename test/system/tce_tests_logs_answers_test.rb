require "application_system_test_case"

class TceTestsLogsAnswersTest < ApplicationSystemTestCase
  setup do
    @tce_tests_logs_answer = tce_tests_logs_answers(:one)
  end

  test "visiting the index" do
    visit tce_tests_logs_answers_url
    assert_selector "h1", text: "Tce Tests Logs Answers"
  end

  test "creating a Tce tests logs answer" do
    visit tce_tests_logs_answers_url
    click_on "New Tce Tests Logs Answer"

    fill_in "Logansw answer", with: @tce_tests_logs_answer.logansw_answer_id
    fill_in "Logansw order", with: @tce_tests_logs_answer.logansw_order
    fill_in "Logansw position", with: @tce_tests_logs_answer.logansw_position
    fill_in "Logansw selected", with: @tce_tests_logs_answer.logansw_selected
    fill_in "Logansw testlog", with: @tce_tests_logs_answer.logansw_testlog_id
    click_on "Create Tce tests logs answer"

    assert_text "Tce tests logs answer was successfully created"
    click_on "Back"
  end

  test "updating a Tce tests logs answer" do
    visit tce_tests_logs_answers_url
    click_on "Edit", match: :first

    fill_in "Logansw answer", with: @tce_tests_logs_answer.logansw_answer_id
    fill_in "Logansw order", with: @tce_tests_logs_answer.logansw_order
    fill_in "Logansw position", with: @tce_tests_logs_answer.logansw_position
    fill_in "Logansw selected", with: @tce_tests_logs_answer.logansw_selected
    fill_in "Logansw testlog", with: @tce_tests_logs_answer.logansw_testlog_id
    click_on "Update Tce tests logs answer"

    assert_text "Tce tests logs answer was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce tests logs answer" do
    visit tce_tests_logs_answers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce tests logs answer was successfully destroyed"
  end
end
