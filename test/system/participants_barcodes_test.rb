require "application_system_test_case"

class ParticipantsBarcodesTest < ApplicationSystemTestCase
  setup do
    @participants_barcode = participants_barcodes(:one)
  end

  test "visiting the index" do
    visit participants_barcodes_url
    assert_selector "h1", text: "Participants Barcodes"
  end

  test "creating a Participants barcode" do
    visit participants_barcodes_url
    click_on "New Participants Barcode"

    fill_in "Barcode", with: @participants_barcode.barcode
    fill_in "Participant", with: @participants_barcode.participant_id
    fill_in "Subject", with: @participants_barcode.subject_id
    fill_in "Wave", with: @participants_barcode.wave_id
    click_on "Create Participants barcode"

    assert_text "Participants barcode was successfully created"
    click_on "Back"
  end

  test "updating a Participants barcode" do
    visit participants_barcodes_url
    click_on "Edit", match: :first

    fill_in "Barcode", with: @participants_barcode.barcode
    fill_in "Participant", with: @participants_barcode.participant_id
    fill_in "Subject", with: @participants_barcode.subject_id
    fill_in "Wave", with: @participants_barcode.wave_id
    click_on "Update Participants barcode"

    assert_text "Participants barcode was successfully updated"
    click_on "Back"
  end

  test "destroying a Participants barcode" do
    visit participants_barcodes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Participants barcode was successfully destroyed"
  end
end
