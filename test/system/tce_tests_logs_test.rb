require "application_system_test_case"

class TceTestsLogsTest < ApplicationSystemTestCase
  setup do
    @tce_tests_log = tce_tests_logs(:one)
  end

  test "visiting the index" do
    visit tce_tests_logs_url
    assert_selector "h1", text: "Tce Tests Logs"
  end

  test "creating a Tce tests log" do
    visit tce_tests_logs_url
    click_on "New Tce Tests Log"

    fill_in "Testlog answer text", with: @tce_tests_log.testlog_answer_text
    fill_in "Testlog change time", with: @tce_tests_log.testlog_change_time
    fill_in "Testlog comment", with: @tce_tests_log.testlog_comment
    fill_in "Testlog creation time", with: @tce_tests_log.testlog_creation_time
    fill_in "Testlog display time", with: @tce_tests_log.testlog_display_time
    fill_in "Testlog", with: @tce_tests_log.testlog_id
    fill_in "Testlog num answers", with: @tce_tests_log.testlog_num_answers
    fill_in "Testlog order", with: @tce_tests_log.testlog_order
    fill_in "Testlog question", with: @tce_tests_log.testlog_question_id
    fill_in "Testlog reaction time", with: @tce_tests_log.testlog_reaction_time
    fill_in "Testlog score", with: @tce_tests_log.testlog_score
    fill_in "Testlog testuser", with: @tce_tests_log.testlog_testuser_id
    fill_in "Testlog user ip", with: @tce_tests_log.testlog_user_ip
    click_on "Create Tce tests log"

    assert_text "Tce tests log was successfully created"
    click_on "Back"
  end

  test "updating a Tce tests log" do
    visit tce_tests_logs_url
    click_on "Edit", match: :first

    fill_in "Testlog answer text", with: @tce_tests_log.testlog_answer_text
    fill_in "Testlog change time", with: @tce_tests_log.testlog_change_time
    fill_in "Testlog comment", with: @tce_tests_log.testlog_comment
    fill_in "Testlog creation time", with: @tce_tests_log.testlog_creation_time
    fill_in "Testlog display time", with: @tce_tests_log.testlog_display_time
    fill_in "Testlog", with: @tce_tests_log.testlog_id
    fill_in "Testlog num answers", with: @tce_tests_log.testlog_num_answers
    fill_in "Testlog order", with: @tce_tests_log.testlog_order
    fill_in "Testlog question", with: @tce_tests_log.testlog_question_id
    fill_in "Testlog reaction time", with: @tce_tests_log.testlog_reaction_time
    fill_in "Testlog score", with: @tce_tests_log.testlog_score
    fill_in "Testlog testuser", with: @tce_tests_log.testlog_testuser_id
    fill_in "Testlog user ip", with: @tce_tests_log.testlog_user_ip
    click_on "Update Tce tests log"

    assert_text "Tce tests log was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce tests log" do
    visit tce_tests_logs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce tests log was successfully destroyed"
  end
end
