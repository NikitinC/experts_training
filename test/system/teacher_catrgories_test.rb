require "application_system_test_case"

class TeacherCatrgoriesTest < ApplicationSystemTestCase
  setup do
    @teacher_catrgory = teacher_catrgories(:one)
  end

  test "visiting the index" do
    visit teacher_catrgories_url
    assert_selector "h1", text: "Teacher Catrgories"
  end

  test "creating a Teacher catrgories" do
    visit teacher_catrgories_url
    click_on "New Teacher Catrgories"

    fill_in "Code", with: @teacher_catrgory.code
    fill_in "Name", with: @teacher_catrgory.name
    fill_in "Short name", with: @teacher_catrgory.short_name
    fill_in "Sort by", with: @teacher_catrgory.sort_by
    click_on "Create Teacher catrgories"

    assert_text "Teacher catrgories was successfully created"
    click_on "Back"
  end

  test "updating a Teacher catrgories" do
    visit teacher_catrgories_url
    click_on "Edit", match: :first

    fill_in "Code", with: @teacher_catrgory.code
    fill_in "Name", with: @teacher_catrgory.name
    fill_in "Short name", with: @teacher_catrgory.short_name
    fill_in "Sort by", with: @teacher_catrgory.sort_by
    click_on "Update Teacher catrgories"

    assert_text "Teacher catrgories was successfully updated"
    click_on "Back"
  end

  test "destroying a Teacher catrgories" do
    visit teacher_catrgories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Teacher catrgories was successfully destroyed"
  end
end
