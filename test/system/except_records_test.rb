require "application_system_test_case"

class ExceptRecordsTest < ApplicationSystemTestCase
  setup do
    @except_record = except_records(:one)
  end

  test "visiting the index" do
    visit except_records_url
    assert_selector "h1", text: "Except Records"
  end

  test "creating a Except record" do
    visit except_records_url
    click_on "New Except Record"

    fill_in "Code", with: @except_record.code
    fill_in "Fisgia11code", with: @except_record.fisgia11code
    fill_in "Fisgia9code", with: @except_record.fisgia9code
    fill_in "Name", with: @except_record.name
    fill_in "Short name", with: @except_record.short_name
    fill_in "Sort by", with: @except_record.sort_by
    click_on "Create Except record"

    assert_text "Except record was successfully created"
    click_on "Back"
  end

  test "updating a Except record" do
    visit except_records_url
    click_on "Edit", match: :first

    fill_in "Code", with: @except_record.code
    fill_in "Fisgia11code", with: @except_record.fisgia11code
    fill_in "Fisgia9code", with: @except_record.fisgia9code
    fill_in "Name", with: @except_record.name
    fill_in "Short name", with: @except_record.short_name
    fill_in "Sort by", with: @except_record.sort_by
    click_on "Update Except record"

    assert_text "Except record was successfully updated"
    click_on "Back"
  end

  test "destroying a Except record" do
    visit except_records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Except record was successfully destroyed"
  end
end
