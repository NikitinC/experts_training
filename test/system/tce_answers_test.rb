require "application_system_test_case"

class TceAnswersTest < ApplicationSystemTestCase
  setup do
    @tce_answer = tce_answers(:one)
  end

  test "visiting the index" do
    visit tce_answers_url
    assert_selector "h1", text: "Tce Answers"
  end

  test "creating a Tce answer" do
    visit tce_answers_url
    click_on "New Tce Answer"

    fill_in "Answer desription", with: @tce_answer.answer_description
    check "Answer enabled" if @tce_answer.answer_enabled
    fill_in "Answer explanation", with: @tce_answer.answer_explanation
    fill_in "Answer", with: @tce_answer.answer_id
    check "Answer isright" if @tce_answer.answer_isright
    fill_in "Answer keyboard key", with: @tce_answer.answer_keyboard_key
    fill_in "Answer position", with: @tce_answer.answer_position
    fill_in "Answer question", with: @tce_answer.answer_question_id
    click_on "Create Tce answer"

    assert_text "Tce answer was successfully created"
    click_on "Back"
  end

  test "updating a Tce answer" do
    visit tce_answers_url
    click_on "Edit", match: :first

    fill_in "Answer desription", with: @tce_answer.answer_description
    check "Answer enabled" if @tce_answer.answer_enabled
    fill_in "Answer explanation", with: @tce_answer.answer_explanation
    fill_in "Answer", with: @tce_answer.answer_id
    check "Answer isright" if @tce_answer.answer_isright
    fill_in "Answer keyboard key", with: @tce_answer.answer_keyboard_key
    fill_in "Answer position", with: @tce_answer.answer_position
    fill_in "Answer question", with: @tce_answer.answer_question_id
    click_on "Update Tce answer"

    assert_text "Tce answer was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce answer" do
    visit tce_answers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce answer was successfully destroyed"
  end
end
