require "application_system_test_case"

class EmployeesTest < ApplicationSystemTestCase
  setup do
    @employee = employees(:one)
  end

  test "visiting the index" do
    visit employees_url
    assert_selector "h1", text: "Employees"
  end

  test "creating a Employee" do
    visit employees_url
    click_on "New Employee"

    fill_in "Acedemic degree", with: @employee.academic_degree_id
    fill_in "Birthyear", with: @employee.birthyear
    fill_in "Document number", with: @employee.document_number
    fill_in "Document series", with: @employee.document_series
    fill_in "Document type", with: @employee.document_type_id
    fill_in "Education", with: @employee.education_id
    fill_in "Email", with: @employee.email
    check "Fg creativity 8" if @employee.fg_creativity_8
    check "Fg creativity 9" if @employee.fg_creativity_9
    check "Fg finance 8" if @employee.fg_finance_8
    check "Fg finance 9" if @employee.fg_finance_9
    check "Fg global 8" if @employee.fg_global_8
    check "Fg global 9" if @employee.fg_global_9
    check "Fg math 8" if @employee.fg_math_8
    check "Fg math 9" if @employee.fg_math_9
    check "Fg natural 8" if @employee.fg_natural_8
    check "Fg natural 9" if @employee.fg_natural_9
    check "Fg read 8" if @employee.fg_read_8
    check "Fg read 9" if @employee.fg_read_9
    check "Gender" if @employee.gender
    fill_in "Job place", with: @employee.job_place_id
    fill_in "Job places other", with: @employee.job_places_other
    fill_in "Name", with: @employee.name
    fill_in "Second name", with: @employee.second_name
    fill_in "Speciality", with: @employee.speciality
    fill_in "Stage", with: @employee.stage
    fill_in "Surname", with: @employee.surname
    fill_in "Teacher category", with: @employee.teacher_categories_id
    click_on "Create Employee"

    assert_text "Employee was successfully created"
    click_on "Back"
  end

  test "updating a Employee" do
    visit employees_url
    click_on "Edit", match: :first

    fill_in "Acedemic degree", with: @employee.academic_degree_id
    fill_in "Birthyear", with: @employee.birthyear
    fill_in "Document number", with: @employee.document_number
    fill_in "Document series", with: @employee.document_series
    fill_in "Document type", with: @employee.document_type_id
    fill_in "Education", with: @employee.education_id
    fill_in "Email", with: @employee.email
    check "Fg creativity 8" if @employee.fg_creativity_8
    check "Fg creativity 9" if @employee.fg_creativity_9
    check "Fg finance 8" if @employee.fg_finance_8
    check "Fg finance 9" if @employee.fg_finance_9
    check "Fg global 8" if @employee.fg_global_8
    check "Fg global 9" if @employee.fg_global_9
    check "Fg math 8" if @employee.fg_math_8
    check "Fg math 9" if @employee.fg_math_9
    check "Fg natural 8" if @employee.fg_natural_8
    check "Fg natural 9" if @employee.fg_natural_9
    check "Fg read 8" if @employee.fg_read_8
    check "Fg read 9" if @employee.fg_read_9
    check "Gender" if @employee.gender
    fill_in "Job place", with: @employee.job_place_id
    fill_in "Job places other", with: @employee.job_places_other
    fill_in "Name", with: @employee.name
    fill_in "Second name", with: @employee.second_name
    fill_in "Speciality", with: @employee.speciality
    fill_in "Stage", with: @employee.stage
    fill_in "Surname", with: @employee.surname
    fill_in "Teacher category", with: @employee.teacher_categories_id
    click_on "Update Employee"

    assert_text "Employee was successfully updated"
    click_on "Back"
  end

  test "destroying a Employee" do
    visit employees_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Employee was successfully destroyed"
  end
end
