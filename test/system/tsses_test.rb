require "application_system_test_case"

class TssesTest < ApplicationSystemTestCase
  setup do
    @tss = tsses(:one)
  end

  test "visiting the index" do
    visit tsses_url
    assert_selector "h1", text: "Tsses"
  end

  test "creating a Tss" do
    visit tsses_url
    click_on "New Tss"

    fill_in "Competention type", with: @tss.competention_type
    fill_in "Subject", with: @tss.subject_id
    fill_in "Task number", with: @tss.task_number
    fill_in "Test type", with: @tss.test_type
    click_on "Create Tss"

    assert_text "Tss was successfully created"
    click_on "Back"
  end

  test "updating a Tss" do
    visit tsses_url
    click_on "Edit", match: :first

    fill_in "Competention type", with: @tss.competention_type
    fill_in "Subject", with: @tss.subject_id
    fill_in "Task number", with: @tss.task_number
    fill_in "Test type", with: @tss.test_type
    click_on "Update Tss"

    assert_text "Tss was successfully updated"
    click_on "Back"
  end

  test "destroying a Tss" do
    visit tsses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tss was successfully destroyed"
  end
end
