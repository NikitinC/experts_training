require "application_system_test_case"

class RbdcSchoolKindsTest < ApplicationSystemTestCase
  setup do
    @rbdc_school_kind = rbdc_school_kinds(:one)
  end

  test "visiting the index" do
    visit rbdc_school_kinds_url
    assert_selector "h1", text: "Rbdc School Kinds"
  end

  test "creating a Rbdc school kind" do
    visit rbdc_school_kinds_url
    click_on "New Rbdc School Kind"

    fill_in "Code", with: @rbdc_school_kind.code
    fill_in "Name", with: @rbdc_school_kind.name
    fill_in "School type", with: @rbdc_school_kind.school_type_id
    fill_in "Sort by", with: @rbdc_school_kind.sort_by
    click_on "Create Rbdc school kind"

    assert_text "Rbdc school kind was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc school kind" do
    visit rbdc_school_kinds_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_school_kind.code
    fill_in "Name", with: @rbdc_school_kind.name
    fill_in "School type", with: @rbdc_school_kind.school_type_id
    fill_in "Sort by", with: @rbdc_school_kind.sort_by
    click_on "Update Rbdc school kind"

    assert_text "Rbdc school kind was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc school kind" do
    visit rbdc_school_kinds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc school kind was successfully destroyed"
  end
end
