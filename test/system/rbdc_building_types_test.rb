require "application_system_test_case"

class RbdcBuildingTypesTest < ApplicationSystemTestCase
  setup do
    @rbdc_building_type = rbdc_building_types(:one)
  end

  test "visiting the index" do
    visit rbdc_building_types_url
    assert_selector "h1", text: "Rbdc Building Types"
  end

  test "creating a Rbdc building type" do
    visit rbdc_building_types_url
    click_on "New Rbdc Building Type"

    fill_in "Code", with: @rbdc_building_type.code
    fill_in "Name", with: @rbdc_building_type.name
    fill_in "Short name", with: @rbdc_building_type.short_name
    fill_in "Sort by", with: @rbdc_building_type.sort_by
    click_on "Create Rbdc building type"

    assert_text "Rbdc building type was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc building type" do
    visit rbdc_building_types_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_building_type.code
    fill_in "Name", with: @rbdc_building_type.name
    fill_in "Short name", with: @rbdc_building_type.short_name
    fill_in "Sort by", with: @rbdc_building_type.sort_by
    click_on "Update Rbdc building type"

    assert_text "Rbdc building type was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc building type" do
    visit rbdc_building_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc building type was successfully destroyed"
  end
end
