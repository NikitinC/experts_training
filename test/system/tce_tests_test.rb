require "application_system_test_case"

class TceTestsTest < ApplicationSystemTestCase
  setup do
    @tce_test = tce_tests(:one)
  end

  test "visiting the index" do
    visit tce_tests_url
    assert_selector "h1", text: "Tce Tests"
  end

  test "creating a Tce test" do
    visit tce_tests_url
    click_on "New Tce Test"

    fill_in "Test answers order mode", with: @tce_test.test_answers_order_mode
    fill_in "Test begin time", with: @tce_test.test_begin_time
    check "Test comment enabled" if @tce_test.test_comment_enabled
    fill_in "Test description", with: @tce_test.test_description
    fill_in "Test duration time", with: @tce_test.test_duration_time
    fill_in "Test end time", with: @tce_test.test_end_time
    fill_in "Test", with: @tce_test.test_id
    fill_in "Test ip range", with: @tce_test.test_ip_range
    check "Test logout on timeout" if @tce_test.test_logout_on_timeout
    fill_in "Test max score", with: @tce_test.test_max_score
    check "Test mcma partial score" if @tce_test.test_mcma_partial_score
    check "Test mcma radio" if @tce_test.test_mcma_radio
    check "Test menu enabled" if @tce_test.test_menu_enabled
    fill_in "Test name", with: @tce_test.test_name
    check "Test noanswer enabled" if @tce_test.test_noanswer_enabled
    fill_in "Test password", with: @tce_test.test_password
    fill_in "Test questions order mode", with: @tce_test.test_questions_order_mode
    check "Test random answers order" if @tce_test.test_random_answers_order
    check "Test random answers select" if @tce_test.test_random_answers_select
    check "Test random questions order" if @tce_test.test_random_questions_order
    check "Test random questions select" if @tce_test.test_random_questions_select
    fill_in "Test repeatable", with: @tce_test.test_repeatable
    check "Test report to users" if @tce_test.test_report_to_users
    check "Test repratable" if @tce_test.test_repeatable
    check "Test results to user" if @tce_test.test_results_to_user
    fill_in "Test score right", with: @tce_test.test_score_right
    fill_in "Test score threshold", with: @tce_test.test_score_threshold
    fill_in "Test score unanswered", with: @tce_test.test_score_unanswered
    fill_in "Test score wrong", with: @tce_test.test_score_wrong
    fill_in "Test user", with: @tce_test.test_user_id
    click_on "Create Tce test"

    assert_text "Tce test was successfully created"
    click_on "Back"
  end

  test "updating a Tce test" do
    visit tce_tests_url
    click_on "Edit", match: :first

    fill_in "Test answers order mode", with: @tce_test.test_answers_order_mode
    fill_in "Test begin time", with: @tce_test.test_begin_time
    check "Test comment enabled" if @tce_test.test_comment_enabled
    fill_in "Test description", with: @tce_test.test_description
    fill_in "Test duration time", with: @tce_test.test_duration_time
    fill_in "Test end time", with: @tce_test.test_end_time
    fill_in "Test", with: @tce_test.test_id
    fill_in "Test ip range", with: @tce_test.test_ip_range
    check "Test logout on timeout" if @tce_test.test_logout_on_timeout
    fill_in "Test max score", with: @tce_test.test_max_score
    check "Test mcma partial score" if @tce_test.test_mcma_partial_score
    check "Test mcma radio" if @tce_test.test_mcma_radio
    check "Test menu enabled" if @tce_test.test_menu_enabled
    fill_in "Test name", with: @tce_test.test_name
    check "Test noanswer enabled" if @tce_test.test_noanswer_enabled
    fill_in "Test password", with: @tce_test.test_password
    fill_in "Test questions order mode", with: @tce_test.test_questions_order_mode
    check "Test random answers order" if @tce_test.test_random_answers_order
    check "Test random answers select" if @tce_test.test_random_answers_select
    check "Test random questions order" if @tce_test.test_random_questions_order
    check "Test random questions select" if @tce_test.test_random_questions_select
    fill_in "Test repeatable", with: @tce_test.test_repeatable
    check "Test report to users" if @tce_test.test_report_to_users
    check "Test repratable" if @tce_test.test_repeatable
    check "Test results to user" if @tce_test.test_results_to_user
    fill_in "Test score right", with: @tce_test.test_score_right
    fill_in "Test score threshold", with: @tce_test.test_score_threshold
    fill_in "Test score unanswered", with: @tce_test.test_score_unanswered
    fill_in "Test score wrong", with: @tce_test.test_score_wrong
    fill_in "Test user", with: @tce_test.test_user_id
    click_on "Update Tce test"

    assert_text "Tce test was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce test" do
    visit tce_tests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce test was successfully destroyed"
  end
end
