require "application_system_test_case"

class RegistrationPlacesTest < ApplicationSystemTestCase
  setup do
    @registration_place = registration_places(:one)
  end

  test "visiting the index" do
    visit registration_places_url
    assert_selector "h1", text: "Registration Places"
  end

  test "creating a Registration place" do
    visit registration_places_url
    click_on "New Registration Place"

    fill_in "Additional info", with: @registration_place.additional_info
    fill_in "Address", with: @registration_place.address
    fill_in "Ate id", with: @registration_place.ate_id_id
    fill_in "Name", with: @registration_place.name
    fill_in "Phones", with: @registration_place.phones
    fill_in "Site", with: @registration_place.site
    click_on "Create Registration place"

    assert_text "Registration place was successfully created"
    click_on "Back"
  end

  test "updating a Registration place" do
    visit registration_places_url
    click_on "Edit", match: :first

    fill_in "Additional info", with: @registration_place.additional_info
    fill_in "Address", with: @registration_place.address
    fill_in "Ate id", with: @registration_place.ate_id_id
    fill_in "Name", with: @registration_place.name
    fill_in "Phones", with: @registration_place.phones
    fill_in "Site", with: @registration_place.site
    click_on "Update Registration place"

    assert_text "Registration place was successfully updated"
    click_on "Back"
  end

  test "destroying a Registration place" do
    visit registration_places_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Registration place was successfully destroyed"
  end
end
