require "application_system_test_case"

class TceSessionsTest < ApplicationSystemTestCase
  setup do
    @tce_session = tce_sessions(:one)
  end

  test "visiting the index" do
    visit tce_sessions_url
    assert_selector "h1", text: "Tce Sessions"
  end

  test "creating a Tce session" do
    visit tce_sessions_url
    click_on "New Tce Session"

    fill_in "Cpsession", with: @tce_session.cpsession_id
    fill_in "Cpsessions data", with: @tce_session.cpsession_data
    fill_in "Spsessions expiry", with: @tce_session.cpsession_expiry
    click_on "Create Tce session"

    assert_text "Tce session was successfully created"
    click_on "Back"
  end

  test "updating a Tce session" do
    visit tce_sessions_url
    click_on "Edit", match: :first

    fill_in "Cpsession", with: @tce_session.cpsession_id
    fill_in "Cpsessions data", with: @tce_session.cpsession_data
    fill_in "Spsessions expiry", with: @tce_session.cpsession_expiry
    click_on "Update Tce session"

    assert_text "Tce session was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce session" do
    visit tce_sessions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce session was successfully destroyed"
  end
end
