require "application_system_test_case"

class SheduleTasksTest < ApplicationSystemTestCase
  setup do
    @shedule_task = shedule_tasks(:one)
  end

  test "visiting the index" do
    visit shedule_tasks_url
    assert_selector "h1", text: "Shedule Tasks"
  end

  test "creating a Shedule task" do
    visit shedule_tasks_url
    click_on "New Shedule Task"

    fill_in "Date", with: @shedule_task.date
    fill_in "Shedule", with: @shedule_task.shedule_id
    click_on "Create Shedule task"

    assert_text "Shedule task was successfully created"
    click_on "Back"
  end

  test "updating a Shedule task" do
    visit shedule_tasks_url
    click_on "Edit", match: :first

    fill_in "Date", with: @shedule_task.date
    fill_in "Shedule", with: @shedule_task.shedule_id
    click_on "Update Shedule task"

    assert_text "Shedule task was successfully updated"
    click_on "Back"
  end

  test "destroying a Shedule task" do
    visit shedule_tasks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Shedule task was successfully destroyed"
  end
end
