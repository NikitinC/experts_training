require "application_system_test_case"

class TceTestGroupsTest < ApplicationSystemTestCase
  setup do
    @tce_test_group = tce_testgroups(:one)
  end

  test "visiting the index" do
    visit tce_testgroups_url
    assert_selector "h1", text: "Tce Test Groups"
  end

  test "creating a Tce test group" do
    visit tce_testgroups_url
    click_on "New Tce Test Group"

    fill_in "Tstgrp group", with: @tce_test_group.tstgrp_group_id
    fill_in "Tstgrp test", with: @tce_test_group.tstgrp_test_id
    click_on "Create Tce test group"

    assert_text "Tce test group was successfully created"
    click_on "Back"
  end

  test "updating a Tce test group" do
    visit tce_testgroups_url
    click_on "Edit", match: :first

    fill_in "Tstgrp group", with: @tce_test_group.tstgrp_group_id
    fill_in "Tstgrp test", with: @tce_test_group.tstgrp_test_id
    click_on "Update Tce test group"

    assert_text "Tce test group was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce test group" do
    visit tce_testgroups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce test group was successfully destroyed"
  end
end
