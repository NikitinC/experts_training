require "application_system_test_case"

class DatExamsTest < ApplicationSystemTestCase
  setup do
    @dat_exam = dat_exams(:one)
  end

  test "visiting the index" do
    visit dat_exams_url
    assert_selector "h1", text: "Dat Exams"
  end

  test "creating a Dat exam" do
    visit dat_exams_url
    click_on "New Dat Exam"

    fill_in "Date", with: @dat_exam.date
    fill_in "Fiscode", with: @dat_exam.fiscode
    check "Is reserv" if @dat_exam.is_reserv
    fill_in "Subject code", with: @dat_exam.subject_code
    fill_in "Test type", with: @dat_exam.exam_type_id
    click_on "Create Dat exam"

    assert_text "Dat exam was successfully created"
    click_on "Back"
  end

  test "updating a Dat exam" do
    visit dat_exams_url
    click_on "Edit", match: :first

    fill_in "Date", with: @dat_exam.date
    fill_in "Fiscode", with: @dat_exam.fiscode
    check "Is reserv" if @dat_exam.is_reserv
    fill_in "Subject code", with: @dat_exam.subject_code
    fill_in "Test type", with: @dat_exam.exam_type_id
    click_on "Update Dat exam"

    assert_text "Dat exam was successfully updated"
    click_on "Back"
  end

  test "destroying a Dat exam" do
    visit dat_exams_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Dat exam was successfully destroyed"
  end
end
