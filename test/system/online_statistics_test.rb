require "application_system_test_case"

class OnlineStatisticsTest < ApplicationSystemTestCase
  setup do
    @online_statistic = online_statistics(:one)
  end

  test "visiting the index" do
    visit online_statistics_url
    assert_selector "h1", text: "Online Statistics"
  end

  test "creating a Online statistic" do
    visit online_statistics_url
    click_on "New Online Statistic"

    fill_in "Tcexam test", with: @online_statistic.tcexam_test_id
    click_on "Create Online statistic"

    assert_text "Online statistic was successfully created"
    click_on "Back"
  end

  test "updating a Online statistic" do
    visit online_statistics_url
    click_on "Edit", match: :first

    fill_in "Tcexam test", with: @online_statistic.tcexam_test_id
    click_on "Update Online statistic"

    assert_text "Online statistic was successfully updated"
    click_on "Back"
  end

  test "destroying a Online statistic" do
    visit online_statistics_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Online statistic was successfully destroyed"
  end
end
