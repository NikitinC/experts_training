require "application_system_test_case"

class RbdcStreetTypesTest < ApplicationSystemTestCase
  setup do
    @rbdc_street_type = rbdc_street_types(:one)
  end

  test "visiting the index" do
    visit rbdc_street_types_url
    assert_selector "h1", text: "Rbdc Street Types"
  end

  test "creating a Rbdc street type" do
    visit rbdc_street_types_url
    click_on "New Rbdc Street Type"

    fill_in "Code", with: @rbdc_street_type.code
    fill_in "Name", with: @rbdc_street_type.name
    fill_in "Short name", with: @rbdc_street_type.short_name
    fill_in "Sort by", with: @rbdc_street_type.sort_by
    click_on "Create Rbdc street type"

    assert_text "Rbdc street type was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc street type" do
    visit rbdc_street_types_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_street_type.code
    fill_in "Name", with: @rbdc_street_type.name
    fill_in "Short name", with: @rbdc_street_type.short_name
    fill_in "Sort by", with: @rbdc_street_type.sort_by
    click_on "Update Rbdc street type"

    assert_text "Rbdc street type was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc street type" do
    visit rbdc_street_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc street type was successfully destroyed"
  end
end
