require "application_system_test_case"

class TceModulesTest < ApplicationSystemTestCase
  setup do
    @tce_module = tce_modules(:one)
  end

  test "visiting the index" do
    visit tce_modules_url
    assert_selector "h1", text: "Tce Modules"
  end

  test "creating a Tce module" do
    visit tce_modules_url
    click_on "New Tce Module"

    check "Module enabled" if @tce_module.module_enabled
    fill_in "Module", with: @tce_module.module_id
    fill_in "Module name", with: @tce_module.module_name
    fill_in "Module user", with: @tce_module.module_user_id
    click_on "Create Tce module"

    assert_text "Tce module was successfully created"
    click_on "Back"
  end

  test "updating a Tce module" do
    visit tce_modules_url
    click_on "Edit", match: :first

    check "Module enabled" if @tce_module.module_enabled
    fill_in "Module", with: @tce_module.module_id
    fill_in "Module name", with: @tce_module.module_name
    fill_in "Module user", with: @tce_module.module_user_id
    click_on "Update Tce module"

    assert_text "Tce module was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce module" do
    visit tce_modules_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce module was successfully destroyed"
  end
end
