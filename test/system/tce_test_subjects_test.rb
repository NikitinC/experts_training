require "application_system_test_case"

class TceTestSubjectsTest < ApplicationSystemTestCase
  setup do
    @tce_test_subject = tce_test_subjects(:one)
  end

  test "visiting the index" do
    visit tce_test_subjects_url
    assert_selector "h1", text: "Tce Test Subjects"
  end

  test "creating a Tce test subject" do
    visit tce_test_subjects_url
    click_on "New Tce Test Subject"

    fill_in "Subjset subject", with: @tce_test_subject.subjset_subject_id
    fill_in "Subjset tsubset", with: @tce_test_subject.subjset_subject_id
    click_on "Create Tce test subject"

    assert_text "Tce test subject was successfully created"
    click_on "Back"
  end

  test "updating a Tce test subject" do
    visit tce_test_subjects_url
    click_on "Edit", match: :first

    fill_in "Subjset subject", with: @tce_test_subject.subjset_subject_id
    fill_in "Subjset tsubset", with: @tce_test_subject.subjset_subject_id
    click_on "Update Tce test subject"

    assert_text "Tce test subject was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce test subject" do
    visit tce_test_subjects_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce test subject was successfully destroyed"
  end
end
