require "application_system_test_case"

class RbdcTownTypesTest < ApplicationSystemTestCase
  setup do
    @rbdc_town_type = rbdc_town_types(:one)
  end

  test "visiting the index" do
    visit rbdc_town_types_url
    assert_selector "h1", text: "Rbdc Town Types"
  end

  test "creating a Rbdc town type" do
    visit rbdc_town_types_url
    click_on "New Rbdc Town Type"

    fill_in "Code", with: @rbdc_town_type.code
    fill_in "Name", with: @rbdc_town_type.name
    fill_in "Short name", with: @rbdc_town_type.short_name
    fill_in "Sort by", with: @rbdc_town_type.sort_by
    click_on "Create Rbdc town type"

    assert_text "Rbdc town type was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc town type" do
    visit rbdc_town_types_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_town_type.code
    fill_in "Name", with: @rbdc_town_type.name
    fill_in "Short name", with: @rbdc_town_type.short_name
    fill_in "Sort by", with: @rbdc_town_type.sort_by
    click_on "Update Rbdc town type"

    assert_text "Rbdc town type was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc town type" do
    visit rbdc_town_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc town type was successfully destroyed"
  end
end
