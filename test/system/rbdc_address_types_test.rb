require "application_system_test_case"

class RbdcAddressTypesTest < ApplicationSystemTestCase
  setup do
    @rbdc_address_type = rbdc_address_types(:one)
  end

  test "visiting the index" do
    visit rbdc_address_types_url
    assert_selector "h1", text: "Rbdc Address Types"
  end

  test "creating a Rbdc address type" do
    visit rbdc_address_types_url
    click_on "New Rbdc Address Type"

    fill_in "Code", with: @rbdc_address_type.code
    fill_in "Name", with: @rbdc_address_type.name
    fill_in "Sort by", with: @rbdc_address_type.sort_by
    click_on "Create Rbdc address type"

    assert_text "Rbdc address type was successfully created"
    click_on "Back"
  end

  test "updating a Rbdc address type" do
    visit rbdc_address_types_url
    click_on "Edit", match: :first

    fill_in "Code", with: @rbdc_address_type.code
    fill_in "Name", with: @rbdc_address_type.name
    fill_in "Sort by", with: @rbdc_address_type.sort_by
    click_on "Update Rbdc address type"

    assert_text "Rbdc address type was successfully updated"
    click_on "Back"
  end

  test "destroying a Rbdc address type" do
    visit rbdc_address_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rbdc address type was successfully destroyed"
  end
end
