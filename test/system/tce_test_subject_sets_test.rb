require "application_system_test_case"

class TceTestSubjectSetsTest < ApplicationSystemTestCase
  setup do
    @tce_test_subject_set = tce_test_subject_set(:one)
  end

  test "visiting the index" do
    visit tce_test_subject_set_url
    assert_selector "h1", text: "Tce Test Subject Sets"
  end

  test "creating a Tce test subject set" do
    visit tce_test_subject_set_url
    click_on "New Tce Test Subject Set"

    fill_in "Tsubset answers", with: @tce_test_subject_set.tsubset_answers
    fill_in "Tsubset difficulty", with: @tce_test_subject_set.tsubset_difficulty
    fill_in "Tsubset", with: @tce_test_subject_set.subjset_tsubset_id
    fill_in "Tsubset quantity", with: @tce_test_subject_set.tsubset_quantity
    fill_in "Tsubset test", with: @tce_test_subject_set.tsubset_test_id
    fill_in "Tsubset type", with: @tce_test_subject_set.tsubset_type
    click_on "Create Tce test subject set"

    assert_text "Tce test subject set was successfully created"
    click_on "Back"
  end

  test "updating a Tce test subject set" do
    visit tce_test_subject_set_url
    click_on "Edit", match: :first

    fill_in "Tsubset answers", with: @tce_test_subject_set.tsubset_answers
    fill_in "Tsubset difficulty", with: @tce_test_subject_set.tsubset_difficulty
    fill_in "Tsubset", with: @tce_test_subject_set.subjset_tsubset_id
    fill_in "Tsubset quantity", with: @tce_test_subject_set.tsubset_quantity
    fill_in "Tsubset test", with: @tce_test_subject_set.tsubset_test_id
    fill_in "Tsubset type", with: @tce_test_subject_set.tsubset_type
    click_on "Update Tce test subject set"

    assert_text "Tce test subject set was successfully updated"
    click_on "Back"
  end

  test "destroying a Tce test subject set" do
    visit tce_test_subject_set_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tce test subject set was successfully destroyed"
  end
end
