require "test_helper"

class RbdcLocalityTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_locality_type = rbdc_locality_types(:one)
  end

  test "should get index" do
    get rbdc_locality_types_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_locality_type_url
    assert_response :success
  end

  test "should create rbdc_locality_type" do
    assert_difference('RbdcLocalityType.count') do
      post rbdc_locality_types_url, params: { rbdc_locality_type: { code: @rbdc_locality_type.code, name: @rbdc_locality_type.name, short_name: @rbdc_locality_type.short_name, sort_by: @rbdc_locality_type.sort_by } }
    end

    assert_redirected_to rbdc_locality_type_url(RbdcLocalityType.last)
  end

  test "should show rbdc_locality_type" do
    get rbdc_locality_type_url(@rbdc_locality_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_locality_type_url(@rbdc_locality_type)
    assert_response :success
  end

  test "should update rbdc_locality_type" do
    patch rbdc_locality_type_url(@rbdc_locality_type), params: { rbdc_locality_type: { code: @rbdc_locality_type.code, name: @rbdc_locality_type.name, short_name: @rbdc_locality_type.short_name, sort_by: @rbdc_locality_type.sort_by } }
    assert_redirected_to rbdc_locality_type_url(@rbdc_locality_type)
  end

  test "should destroy rbdc_locality_type" do
    assert_difference('RbdcLocalityType.count', -1) do
      delete rbdc_locality_type_url(@rbdc_locality_type)
    end

    assert_redirected_to rbdc_locality_types_url
  end
end
