require "test_helper"

class TestSpecificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @test_specification = test_specifications(:one)
  end

  test "should get index" do
    get test_specifications_index_url
    assert_response :success
  end

  test "should get new" do
    get new_test_specification_url
    assert_response :success
  end

  test "should create test_specification" do
    assert_difference('TestSpecifications.count') do
      post test_specifications_index_url, params: { test_specification: { examdate: @test_specification.examdate, examdate_end: @test_specification.examdate_end, maxprimarymarc: @test_specification.maxprimarymarc, maxprimarymark: @test_specification.maxprimarymark, maxprimarymarka: @test_specification.maxprimarymarka, maxprimarymarkb: @test_specification.maxprimarymarkb, maxprimarymarkd: @test_specification.maxprimarymarkd, name: @test_specification.name, parallel: @test_specification.parallel, subject_id: @test_specification.subject_id, exam_type_id: @test_specification.exam_type_id, testresultsa_mask: @test_specification.testresultsa_mask, testresultsa_numtasks: @test_specification.testresultsa_numtasks, testresultsb_mask: @test_specification.testresultsb_mask, testresultsb_numtasks: @test_specification.testresultsb_numtasks, testresultsc_mask: @test_specification.testresultsc_mask, testresultsc_numtasks: @test_specification.testresultsc_numtasks, testresultsd_mask: @test_specification.testresultsd_mask, testresultsd_numtasks: @test_specification.testresultsd_numtasks } }
    end

    assert_redirected_to test_specification_url(TestSpecifications.last)
  end

  test "should show test_specification" do
    get test_specification_url(@test_specification)
    assert_response :success
  end

  test "should get edit" do
    get edit_test_specification_url(@test_specification)
    assert_response :success
  end

  test "should update test_specification" do
    patch test_specification_url(@test_specification), params: { test_specification: { examdate: @test_specification.examdate, examdate_end: @test_specification.examdate_end, maxprimarymarc: @test_specification.maxprimarymarc, maxprimarymark: @test_specification.maxprimarymark, maxprimarymarka: @test_specification.maxprimarymarka, maxprimarymarkb: @test_specification.maxprimarymarkb, maxprimarymarkd: @test_specification.maxprimarymarkd, name: @test_specification.name, parallel: @test_specification.parallel, subject_id: @test_specification.subject_id, exam_type_id: @test_specification.exam_type_id, testresultsa_mask: @test_specification.testresultsa_mask, testresultsa_numtasks: @test_specification.testresultsa_numtasks, testresultsb_mask: @test_specification.testresultsb_mask, testresultsb_numtasks: @test_specification.testresultsb_numtasks, testresultsc_mask: @test_specification.testresultsc_mask, testresultsc_numtasks: @test_specification.testresultsc_numtasks, testresultsd_mask: @test_specification.testresultsd_mask, testresultsd_numtasks: @test_specification.testresultsd_numtasks } }
    assert_redirected_to test_specification_url(@test_specification)
  end

  test "should destroy test_specification" do
    assert_difference('TestSpecifications.count', -1) do
      delete test_specification_url(@test_specification)
    end

    assert_redirected_to test_specifications_index_url
  end
end
