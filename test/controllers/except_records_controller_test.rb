require "test_helper"

class ExceptRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @except_record = except_records(:one)
  end

  test "should get index" do
    get except_records_url
    assert_response :success
  end

  test "should get new" do
    get new_except_record_url
    assert_response :success
  end

  test "should create except_record" do
    assert_difference('ExceptRecord.count') do
      post except_records_url, params: { except_record: { code: @except_record.code, fisgia11code: @except_record.fisgia11code, fisgia9code: @except_record.fisgia9code, name: @except_record.name, short_name: @except_record.short_name, sort_by: @except_record.sort_by } }
    end

    assert_redirected_to except_record_url(ExceptRecord.last)
  end

  test "should show except_record" do
    get except_record_url(@except_record)
    assert_response :success
  end

  test "should get edit" do
    get edit_except_record_url(@except_record)
    assert_response :success
  end

  test "should update except_record" do
    patch except_record_url(@except_record), params: { except_record: { code: @except_record.code, fisgia11code: @except_record.fisgia11code, fisgia9code: @except_record.fisgia9code, name: @except_record.name, short_name: @except_record.short_name, sort_by: @except_record.sort_by } }
    assert_redirected_to except_record_url(@except_record)
  end

  test "should destroy except_record" do
    assert_difference('ExceptRecord.count', -1) do
      delete except_record_url(@except_record)
    end

    assert_redirected_to except_records_url
  end
end
