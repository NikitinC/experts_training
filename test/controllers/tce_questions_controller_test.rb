require "test_helper"

class TceQuestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_question = tce_questions(:one)
  end

  test "should get index" do
    get tce_questions_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_question_url
    assert_response :success
  end

  test "should create tce_question" do
    assert_difference('TceQuestion.count') do
      post tce_questions_url, params: { tce_question: { question_description: @tce_question.question_description, question_id: @tce_question.question_id, question_subject_id: @tce_question.question_subject_id } }
    end

    assert_redirected_to tce_question_url(TceQuestion.last)
  end

  test "should show tce_question" do
    get tce_question_url(@tce_question)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_question_url(@tce_question)
    assert_response :success
  end

  test "should update tce_question" do
    patch tce_question_url(@tce_question), params: { tce_question: { question_description: @tce_question.question_description, question_id: @tce_question.question_id, question_subject_id: @tce_question.question_subject_id } }
    assert_redirected_to tce_question_url(@tce_question)
  end

  test "should destroy tce_question" do
    assert_difference('TceQuestion.count', -1) do
      delete tce_question_url(@tce_question)
    end

    assert_redirected_to tce_questions_url
  end
end
