require "test_helper"

class TeacherCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @teacher_categories = teacher_categories(:one)
  end

  test "should get index" do
    get teacher_categories_index_url
    assert_response :success
  end

  test "should get new" do
    get new_teacher_categories_url
    assert_response :success
  end

  test "should create teacher_categories" do
    assert_difference('TeacherCategories.count') do
      post teacher_categories_index_url, params: { teacher_categories: { code: @teacher_categories.code, name: @teacher_categories.name, short_name: @teacher_categories.short_name, sort_by: @teacher_categories.sort_by } }
    end

    assert_redirected_to teacher_categories_url(TeacherCategories.last)
  end

  test "should show teacher_categories" do
    get teacher_categories_url(@teacher_categories)
    assert_response :success
  end

  test "should get edit" do
    get edit_teacher_categories_url(@teacher_categories)
    assert_response :success
  end

  test "should update teacher_categories" do
    patch teacher_categories_url(@teacher_categories), params: { teacher_categories: { code: @teacher_categories.code, name: @teacher_categories.name, short_name: @teacher_categories.short_name, sort_by: @teacher_categories.sort_by } }
    assert_redirected_to teacher_categories_url(@teacher_categories)
  end

  test "should destroy teacher_categories" do
    assert_difference('TeacherCategories.count', -1) do
      delete teacher_categories_url(@teacher_categories)
    end

    assert_redirected_to teacher_categories_index_url
  end
end
