require "test_helper"

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get employees_url
    assert_response :success
  end

  test "should get new" do
    get new_employee_url
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post employees_url, params: { employee: { academic_degree_id: @employee.academic_degree_id, birthyear: @employee.birthyear, document_number: @employee.document_number, document_series: @employee.document_series, document_type_id: @employee.document_type_id, education_id: @employee.education_id, email: @employee.email, fg_creativity_8: @employee.fg_creativity_8, fg_creativity_9: @employee.fg_creativity_9, fg_finance_8: @employee.fg_finance_8, fg_finance_9: @employee.fg_finance_9, fg_global_8: @employee.fg_global_8, fg_global_9: @employee.fg_global_9, fg_math_8: @employee.fg_math_8, fg_math_9: @employee.fg_math_9, fg_natural_8: @employee.fg_natural_8, fg_natural_9: @employee.fg_natural_9, fg_read_8: @employee.fg_read_8, fg_read_9: @employee.fg_read_9, gender: @employee.gender, job_place_id: @employee.job_place_id, job_places_other: @employee.job_places_other, name: @employee.name, second_name: @employee.second_name, speciality: @employee.speciality, stage: @employee.stage, surname: @employee.surname, teacher_categories_id: @employee.teacher_categories_id } }
    end

    assert_redirected_to employee_url(Employee.last)
  end

  test "should show employee" do
    get employee_url(@employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_employee_url(@employee)
    assert_response :success
  end

  test "should update employee" do
    patch employee_url(@employee), params: { employee: { academic_degree_id: @employee.academic_degree_id, birthyear: @employee.birthyear, document_number: @employee.document_number, document_series: @employee.document_series, document_type_id: @employee.document_type_id, education_id: @employee.education_id, email: @employee.email, fg_creativity_8: @employee.fg_creativity_8, fg_creativity_9: @employee.fg_creativity_9, fg_finance_8: @employee.fg_finance_8, fg_finance_9: @employee.fg_finance_9, fg_global_8: @employee.fg_global_8, fg_global_9: @employee.fg_global_9, fg_math_8: @employee.fg_math_8, fg_math_9: @employee.fg_math_9, fg_natural_8: @employee.fg_natural_8, fg_natural_9: @employee.fg_natural_9, fg_read_8: @employee.fg_read_8, fg_read_9: @employee.fg_read_9, gender: @employee.gender, job_place_id: @employee.job_place_id, job_places_other: @employee.job_places_other, name: @employee.name, second_name: @employee.second_name, speciality: @employee.speciality, stage: @employee.stage, surname: @employee.surname, teacher_categories_id: @employee.teacher_categories_id } }
    assert_redirected_to employee_url(@employee)
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete employee_url(@employee)
    end

    assert_redirected_to employees_url
  end
end
