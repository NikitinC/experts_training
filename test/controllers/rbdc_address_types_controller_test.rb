require "test_helper"

class RbdcAddressTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_address_type = rbdc_address_types(:one)
  end

  test "should get index" do
    get rbdc_address_types_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_address_type_url
    assert_response :success
  end

  test "should create rbdc_address_type" do
    assert_difference('RbdcAddressType.count') do
      post rbdc_address_types_url, params: { rbdc_address_type: { code: @rbdc_address_type.code, name: @rbdc_address_type.name, sort_by: @rbdc_address_type.sort_by } }
    end

    assert_redirected_to rbdc_address_type_url(RbdcAddressType.last)
  end

  test "should show rbdc_address_type" do
    get rbdc_address_type_url(@rbdc_address_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_address_type_url(@rbdc_address_type)
    assert_response :success
  end

  test "should update rbdc_address_type" do
    patch rbdc_address_type_url(@rbdc_address_type), params: { rbdc_address_type: { code: @rbdc_address_type.code, name: @rbdc_address_type.name, sort_by: @rbdc_address_type.sort_by } }
    assert_redirected_to rbdc_address_type_url(@rbdc_address_type)
  end

  test "should destroy rbdc_address_type" do
    assert_difference('RbdcAddressType.count', -1) do
      delete rbdc_address_type_url(@rbdc_address_type)
    end

    assert_redirected_to rbdc_address_types_url
  end
end
