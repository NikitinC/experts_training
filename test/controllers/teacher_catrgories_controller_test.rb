require "test_helper"

class TeacherCatrgoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @teacher_catrgory = teacher_catrgories(:one)
  end

  test "should get index" do
    get teacher_catrgories_index_url
    assert_response :success
  end

  test "should get new" do
    get new_teacher_catrgory_url
    assert_response :success
  end

  test "should create teacher_catrgory" do
    assert_difference('TeacherCatrgories.count') do
      post teacher_catrgories_index_url, params: { teacher_catrgory: { code: @teacher_catrgory.code, name: @teacher_catrgory.name, short_name: @teacher_catrgory.short_name, sort_by: @teacher_catrgory.sort_by } }
    end

    assert_redirected_to teacher_catrgory_url(TeacherCatrgories.last)
  end

  test "should show teacher_catrgory" do
    get teacher_catrgory_url(@teacher_catrgory)
    assert_response :success
  end

  test "should get edit" do
    get edit_teacher_catrgory_url(@teacher_catrgory)
    assert_response :success
  end

  test "should update teacher_catrgory" do
    patch teacher_catrgory_url(@teacher_catrgory), params: { teacher_catrgory: { code: @teacher_catrgory.code, name: @teacher_catrgory.name, short_name: @teacher_catrgory.short_name, sort_by: @teacher_catrgory.sort_by } }
    assert_redirected_to teacher_catrgory_url(@teacher_catrgory)
  end

  test "should destroy teacher_catrgory" do
    assert_difference('TeacherCatrgories.count', -1) do
      delete teacher_catrgory_url(@teacher_catrgory)
    end

    assert_redirected_to teacher_catrgories_index_url
  end
end
