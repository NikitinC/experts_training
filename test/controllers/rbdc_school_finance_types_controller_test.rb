require "test_helper"

class RbdcSchoolFinanceTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_school_finance_type = rbdc_school_finance_types(:one)
  end

  test "should get index" do
    get rbdc_school_finance_types_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_school_finance_type_url
    assert_response :success
  end

  test "should create rbdc_school_finance_type" do
    assert_difference('RbdcSchoolFinanceType.count') do
      post rbdc_school_finance_types_url, params: { rbdc_school_finance_type: { code: @rbdc_school_finance_type.code, name: @rbdc_school_finance_type.name, short_name: @rbdc_school_finance_type.short_name, sort_by: @rbdc_school_finance_type.sort_by } }
    end

    assert_redirected_to rbdc_school_finance_type_url(RbdcSchoolFinanceType.last)
  end

  test "should show rbdc_school_finance_type" do
    get rbdc_school_finance_type_url(@rbdc_school_finance_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_school_finance_type_url(@rbdc_school_finance_type)
    assert_response :success
  end

  test "should update rbdc_school_finance_type" do
    patch rbdc_school_finance_type_url(@rbdc_school_finance_type), params: { rbdc_school_finance_type: { code: @rbdc_school_finance_type.code, name: @rbdc_school_finance_type.name, short_name: @rbdc_school_finance_type.short_name, sort_by: @rbdc_school_finance_type.sort_by } }
    assert_redirected_to rbdc_school_finance_type_url(@rbdc_school_finance_type)
  end

  test "should destroy rbdc_school_finance_type" do
    assert_difference('RbdcSchoolFinanceType.count', -1) do
      delete rbdc_school_finance_type_url(@rbdc_school_finance_type)
    end

    assert_redirected_to rbdc_school_finance_types_url
  end
end
