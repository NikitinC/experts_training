require "test_helper"

class TssesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tss = tsses(:one)
  end

  test "should get index" do
    get tsses_url
    assert_response :success
  end

  test "should get new" do
    get new_tss_url
    assert_response :success
  end

  test "should create tss" do
    assert_difference('Tss.count') do
      post tsses_url, params: { tss: { competention_type: @tss.competention_type, subject_id: @tss.subject_id, task_number: @tss.task_number, test_type: @tss.test_type } }
    end

    assert_redirected_to tss_url(Tss.last)
  end

  test "should show tss" do
    get tss_url(@tss)
    assert_response :success
  end

  test "should get edit" do
    get edit_tss_url(@tss)
    assert_response :success
  end

  test "should update tss" do
    patch tss_url(@tss), params: { tss: { competention_type: @tss.competention_type, subject_id: @tss.subject_id, task_number: @tss.task_number, test_type: @tss.test_type } }
    assert_redirected_to tss_url(@tss)
  end

  test "should destroy tss" do
    assert_difference('Tss.count', -1) do
      delete tss_url(@tss)
    end

    assert_redirected_to tsses_url
  end
end
