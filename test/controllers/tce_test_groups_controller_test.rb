require "test_helper"

class TceTestGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_test_group = tce_testgroups(:one)
  end

  test "should get index" do
    get tce_testgroups_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_test_group_url
    assert_response :success
  end

  test "should create tce_test_group" do
    assert_difference('TceTestGroup.count') do
      post tce_testgroups_url, params: { tce_test_group: { tstgrp_group_id: @tce_test_group.tstgrp_group_id, tstgrp_test_id: @tce_test_group.tstgrp_test_id } }
    end

    assert_redirected_to tce_test_group_url(TceTestGroup.last)
  end

  test "should show tce_test_group" do
    get tce_test_group_url(@tce_test_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_test_group_url(@tce_test_group)
    assert_response :success
  end

  test "should update tce_test_group" do
    patch tce_test_group_url(@tce_test_group), params: { tce_test_group: { tstgrp_group_id: @tce_test_group.tstgrp_group_id, tstgrp_test_id: @tce_test_group.tstgrp_test_id } }
    assert_redirected_to tce_test_group_url(@tce_test_group)
  end

  test "should destroy tce_test_group" do
    assert_difference('TceTestGroup.count', -1) do
      delete tce_test_group_url(@tce_test_group)
    end

    assert_redirected_to tce_testgroups_url
  end
end
