require "test_helper"

class DestroyReasonsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @destroy_reason = destroy_reasons(:one)
  end

  test "should get index" do
    get destroy_reasons_url
    assert_response :success
  end

  test "should get new" do
    get new_destroy_reason_url
    assert_response :success
  end

  test "should create destroy_reason" do
    assert_difference('DestroyReason.count') do
      post destroy_reasons_url, params: { destroy_reason: { code: @destroy_reason.code, deleted_at: @destroy_reason.deleted_at, fisgia11code: @destroy_reason.fisgia11code, fisgia9code: @destroy_reason.fisgia9code, name: @destroy_reason.name, short_name: @destroy_reason.short_name, sort_by: @destroy_reason.sort_by } }
    end

    assert_redirected_to destroy_reason_url(DestroyReason.last)
  end

  test "should show destroy_reason" do
    get destroy_reason_url(@destroy_reason)
    assert_response :success
  end

  test "should get edit" do
    get edit_destroy_reason_url(@destroy_reason)
    assert_response :success
  end

  test "should update destroy_reason" do
    patch destroy_reason_url(@destroy_reason), params: { destroy_reason: { code: @destroy_reason.code, deleted_at: @destroy_reason.deleted_at, fisgia11code: @destroy_reason.fisgia11code, fisgia9code: @destroy_reason.fisgia9code, name: @destroy_reason.name, short_name: @destroy_reason.short_name, sort_by: @destroy_reason.sort_by } }
    assert_redirected_to destroy_reason_url(@destroy_reason)
  end

  test "should destroy destroy_reason" do
    assert_difference('DestroyReason.count', -1) do
      delete destroy_reason_url(@destroy_reason)
    end

    assert_redirected_to destroy_reasons_url
  end
end
