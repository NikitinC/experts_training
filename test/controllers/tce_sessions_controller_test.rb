require "test_helper"

class TceSessionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_session = tce_sessions(:one)
  end

  test "should get index" do
    get tce_sessions_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_session_url
    assert_response :success
  end

  test "should create tce_session" do
    assert_difference('TceSession.count') do
      post tce_sessions_url, params: { tce_session: { cpsession_id: @tce_session.cpsession_id, cpsession_data: @tce_session.cpsession_data, cpsession_expiry: @tce_session.cpsession_expiry } }
    end

    assert_redirected_to tce_session_url(TceSession.last)
  end

  test "should show tce_session" do
    get tce_session_url(@tce_session)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_session_url(@tce_session)
    assert_response :success
  end

  test "should update tce_session" do
    patch tce_session_url(@tce_session), params: { tce_session: { cpsession_id: @tce_session.cpsession_id, cpsession_data: @tce_session.cpsession_data, cpsession_expiry: @tce_session.cpsession_expiry } }
    assert_redirected_to tce_session_url(@tce_session)
  end

  test "should destroy tce_session" do
    assert_difference('TceSession.count', -1) do
      delete tce_session_url(@tce_session)
    end

    assert_redirected_to tce_sessions_url
  end
end
