require "test_helper"

class RbdcStreetTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_street_type = rbdc_street_types(:one)
  end

  test "should get index" do
    get rbdc_street_types_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_street_type_url
    assert_response :success
  end

  test "should create rbdc_street_type" do
    assert_difference('RbdcStreetType.count') do
      post rbdc_street_types_url, params: { rbdc_street_type: { code: @rbdc_street_type.code, name: @rbdc_street_type.name, short_name: @rbdc_street_type.short_name, sort_by: @rbdc_street_type.sort_by } }
    end

    assert_redirected_to rbdc_street_type_url(RbdcStreetType.last)
  end

  test "should show rbdc_street_type" do
    get rbdc_street_type_url(@rbdc_street_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_street_type_url(@rbdc_street_type)
    assert_response :success
  end

  test "should update rbdc_street_type" do
    patch rbdc_street_type_url(@rbdc_street_type), params: { rbdc_street_type: { code: @rbdc_street_type.code, name: @rbdc_street_type.name, short_name: @rbdc_street_type.short_name, sort_by: @rbdc_street_type.sort_by } }
    assert_redirected_to rbdc_street_type_url(@rbdc_street_type)
  end

  test "should destroy rbdc_street_type" do
    assert_difference('RbdcStreetType.count', -1) do
      delete rbdc_street_type_url(@rbdc_street_type)
    end

    assert_redirected_to rbdc_street_types_url
  end
end
