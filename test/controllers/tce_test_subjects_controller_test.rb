require "test_helper"

class TceTestSubjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_test_subject = tce_test_subjects(:one)
  end

  test "should get index" do
    get tce_test_subjects_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_test_subject_url
    assert_response :success
  end

  test "should create tce_test_subject" do
    assert_difference('TceTestSubject.count') do
      post tce_test_subjects_url, params: { tce_test_subject: { subjset_subject_id: @tce_test_subject.subjset_subject_id, subjset_subject_id: @tce_test_subject.subjset_subject_id } }
    end

    assert_redirected_to tce_test_subject_url(TceTestSubject.last)
  end

  test "should show tce_test_subject" do
    get tce_test_subject_url(@tce_test_subject)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_test_subject_url(@tce_test_subject)
    assert_response :success
  end

  test "should update tce_test_subject" do
    patch tce_test_subject_url(@tce_test_subject), params: { tce_test_subject: { subjset_subject_id: @tce_test_subject.subjset_subject_id, subjset_subject_id: @tce_test_subject.subjset_subject_id } }
    assert_redirected_to tce_test_subject_url(@tce_test_subject)
  end

  test "should destroy tce_test_subject" do
    assert_difference('TceTestSubject.count', -1) do
      delete tce_test_subject_url(@tce_test_subject)
    end

    assert_redirected_to tce_test_subjects_url
  end
end
