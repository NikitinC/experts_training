require "test_helper"

class RegistrationPlacesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @registration_place = registration_places(:one)
  end

  test "should get index" do
    get registration_places_url
    assert_response :success
  end

  test "should get new" do
    get new_registration_place_url
    assert_response :success
  end

  test "should create registration_place" do
    assert_difference('RegistrationPlace.count') do
      post registration_places_url, params: { registration_place: { additional_info: @registration_place.additional_info, address: @registration_place.address, ate_id_id: @registration_place.ate_id_id, name: @registration_place.name, phones: @registration_place.phones, site: @registration_place.site } }
    end

    assert_redirected_to registration_place_url(RegistrationPlace.last)
  end

  test "should show registration_place" do
    get registration_place_url(@registration_place)
    assert_response :success
  end

  test "should get edit" do
    get edit_registration_place_url(@registration_place)
    assert_response :success
  end

  test "should update registration_place" do
    patch registration_place_url(@registration_place), params: { registration_place: { additional_info: @registration_place.additional_info, address: @registration_place.address, ate_id_id: @registration_place.ate_id_id, name: @registration_place.name, phones: @registration_place.phones, site: @registration_place.site } }
    assert_redirected_to registration_place_url(@registration_place)
  end

  test "should destroy registration_place" do
    assert_difference('RegistrationPlace.count', -1) do
      delete registration_place_url(@registration_place)
    end

    assert_redirected_to registration_places_url
  end
end
