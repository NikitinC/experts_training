require "test_helper"

class TceSslcertsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_sslcert = tce_sslcerts(:one)
  end

  test "should get index" do
    get tce_sslcerts_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_sslcert_url
    assert_response :success
  end

  test "should create tce_sslcert" do
    assert_difference('TceSslcert.count') do
      post tce_sslcerts_url, params: { tce_sslcert: { ssl_enabled: @tce_sslcert.ssl_enabled, ssl_end_date: @tce_sslcert.ssl_end_date, ssl_hash: @tce_sslcert.ssl_hash, ssl_id: @tce_sslcert.ssl_id, ssl_name: @tce_sslcert.ssl_name, ssl_user_id: @tce_sslcert.ssl_user_id } }
    end

    assert_redirected_to tce_sslcert_url(TceSslcert.last)
  end

  test "should show tce_sslcert" do
    get tce_sslcert_url(@tce_sslcert)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_sslcert_url(@tce_sslcert)
    assert_response :success
  end

  test "should update tce_sslcert" do
    patch tce_sslcert_url(@tce_sslcert), params: { tce_sslcert: { ssl_enabled: @tce_sslcert.ssl_enabled, ssl_end_date: @tce_sslcert.ssl_end_date, ssl_hash: @tce_sslcert.ssl_hash, ssl_id: @tce_sslcert.ssl_id, ssl_name: @tce_sslcert.ssl_name, ssl_user_id: @tce_sslcert.ssl_user_id } }
    assert_redirected_to tce_sslcert_url(@tce_sslcert)
  end

  test "should destroy tce_sslcert" do
    assert_difference('TceSslcert.count', -1) do
      delete tce_sslcert_url(@tce_sslcert)
    end

    assert_redirected_to tce_sslcerts_url
  end
end
