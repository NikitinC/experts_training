require "test_helper"

class TceUserGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_user_group = tce_user_groups(:one)
  end

  test "should get index" do
    get tce_user_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_user_group_url
    assert_response :success
  end

  test "should create tce_user_group" do
    assert_difference('TceUserGroup.count') do
      post tce_user_groups_url, params: { tce_user_group: { group_id: @tce_user_group.group_id, group_name: @tce_user_group.group_name } }
    end

    assert_redirected_to tce_user_group_url(TceUserGroup.last)
  end

  test "should show tce_user_group" do
    get tce_user_group_url(@tce_user_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_user_group_url(@tce_user_group)
    assert_response :success
  end

  test "should update tce_user_group" do
    patch tce_user_group_url(@tce_user_group), params: { tce_user_group: { group_id: @tce_user_group.group_id, group_name: @tce_user_group.group_name } }
    assert_redirected_to tce_user_group_url(@tce_user_group)
  end

  test "should destroy tce_user_group" do
    assert_difference('TceUserGroup.count', -1) do
      delete tce_user_group_url(@tce_user_group)
    end

    assert_redirected_to tce_user_groups_url
  end
end
