require "test_helper"

class TcexamLoginsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tcexam_login = tcexam_logins(:one)
  end

  test "should get index" do
    get tcexam_logins_url
    assert_response :success
  end

  test "should get new" do
    get new_tcexam_login_url
    assert_response :success
  end

  test "should create tcexam_login" do
    assert_difference('TcexamUser.count') do
      post tcexam_logins_url, params: { tcexam_login: { user_birthdate: @tcexam_login.user_birthdate, user_birthplace: @tcexam_login.user_birthplace, user_email: @tcexam_login.user_email, user_firstname: @tcexam_login.user_firstname, user_ip: @tcexam_login.user_ip, user_lastname: @tcexam_login.user_lastname, user_level: @tcexam_login.user_level, user_name: @tcexam_login.user_name, user_otpkey: @tcexam_login.user_otpkey, user_password: @tcexam_login.user_password, user_regdate: @tcexam_login.user_regdate, user_regnumber: @tcexam_login.user_regnumber, user_ssn: @tcexam_login.user_ssn, user_verifycode: @tcexam_login.user_verifycode } }
    end

    assert_redirected_to tcexam_login_url(TcexamUser.last)
  end

  test "should show tcexam_login" do
    get tcexam_login_url(@tcexam_login)
    assert_response :success
  end

  test "should get edit" do
    get edit_tcexam_login_url(@tcexam_login)
    assert_response :success
  end

  test "should update tcexam_login" do
    patch tcexam_login_url(@tcexam_login), params: { tcexam_login: { user_birthdate: @tcexam_login.user_birthdate, user_birthplace: @tcexam_login.user_birthplace, user_email: @tcexam_login.user_email, user_firstname: @tcexam_login.user_firstname, user_ip: @tcexam_login.user_ip, user_lastname: @tcexam_login.user_lastname, user_level: @tcexam_login.user_level, user_name: @tcexam_login.user_name, user_otpkey: @tcexam_login.user_otpkey, user_password: @tcexam_login.user_password, user_regdate: @tcexam_login.user_regdate, user_regnumber: @tcexam_login.user_regnumber, user_ssn: @tcexam_login.user_ssn, user_verifycode: @tcexam_login.user_verifycode } }
    assert_redirected_to tcexam_login_url(@tcexam_login)
  end

  test "should destroy tcexam_login" do
    assert_difference('TcexamUser.count', -1) do
      delete tcexam_login_url(@tcexam_login)
    end

    assert_redirected_to tcexam_logins_url
  end
end
