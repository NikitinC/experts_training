require "test_helper"

class JobPlacesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @job_place = job_places(:one)
  end

  test "should get index" do
    get job_places_index_url
    assert_response :success
  end

  test "should get new" do
    get new_job_place_url
    assert_response :success
  end

  test "should create job_place" do
    assert_difference('JobPlaces.count') do
      post job_places_index_url, params: { job_place: { code: @job_place.code, name: @job_place.name, short_name: @job_place.short_name, sort_by: @job_place.sort_by } }
    end

    assert_redirected_to job_place_url(JobPlaces.last)
  end

  test "should show job_place" do
    get job_place_url(@job_place)
    assert_response :success
  end

  test "should get edit" do
    get edit_job_place_url(@job_place)
    assert_response :success
  end

  test "should update job_place" do
    patch job_place_url(@job_place), params: { job_place: { code: @job_place.code, name: @job_place.name, short_name: @job_place.short_name, sort_by: @job_place.sort_by } }
    assert_redirected_to job_place_url(@job_place)
  end

  test "should destroy job_place" do
    assert_difference('JobPlaces.count', -1) do
      delete job_place_url(@job_place)
    end

    assert_redirected_to job_places_index_url
  end
end
