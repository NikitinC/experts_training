require "test_helper"

class OnlineStatisticsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @online_statistic = online_statistics(:one)
  end

  test "should get index" do
    get online_statistics_url
    assert_response :success
  end

  test "should get new" do
    get new_online_statistic_url
    assert_response :success
  end

  test "should create online_statistic" do
    assert_difference('OnlineStatistic.count') do
      post online_statistics_url, params: { online_statistic: { tcexam_test_id: @online_statistic.tcexam_test_id } }
    end

    assert_redirected_to online_statistic_url(OnlineStatistic.last)
  end

  test "should show online_statistic" do
    get online_statistic_url(@online_statistic)
    assert_response :success
  end

  test "should get edit" do
    get edit_online_statistic_url(@online_statistic)
    assert_response :success
  end

  test "should update online_statistic" do
    patch online_statistic_url(@online_statistic), params: { online_statistic: { tcexam_test_id: @online_statistic.tcexam_test_id } }
    assert_redirected_to online_statistic_url(@online_statistic)
  end

  test "should destroy online_statistic" do
    assert_difference('OnlineStatistic.count', -1) do
      delete online_statistic_url(@online_statistic)
    end

    assert_redirected_to online_statistics_url
  end
end
