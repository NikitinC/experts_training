require "test_helper"

class TceTestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_test = tce_tests(:one)
  end

  test "should get index" do
    get tce_tests_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_test_url
    assert_response :success
  end

  test "should create tce_test" do
    assert_difference('TceTest.count') do
      post tce_tests_url, params: { tce_test: { test_answers_order_mode: @tce_test.test_answers_order_mode, test_begin_time: @tce_test.test_begin_time, test_comment_enabled: @tce_test.test_comment_enabled, test_description: @tce_test.test_description, test_duration_time: @tce_test.test_duration_time, test_end_time: @tce_test.test_end_time, test_id: @tce_test.test_id, test_ip_range: @tce_test.test_ip_range, test_logout_on_timeout: @tce_test.test_logout_on_timeout, test_max_score: @tce_test.test_max_score, test_mcma_partial_score: @tce_test.test_mcma_partial_score, test_mcma_radio: @tce_test.test_mcma_radio, test_menu_enabled: @tce_test.test_menu_enabled, test_name: @tce_test.test_name, test_noanswer_enabled: @tce_test.test_noanswer_enabled, test_password: @tce_test.test_password, test_questions_order_mode: @tce_test.test_questions_order_mode, test_random_answers_order: @tce_test.test_random_answers_order, test_random_answers_select: @tce_test.test_random_answers_select, test_random_questions_order: @tce_test.test_random_questions_order, test_random_questions_select: @tce_test.test_random_questions_select, test_repeatable: @tce_test.test_repeatable, test_report_to_users: @tce_test.test_report_to_users, test_repeatable: @tce_test.test_repeatable, test_results_to_user: @tce_test.test_results_to_user, test_score_right: @tce_test.test_score_right, test_score_threshold: @tce_test.test_score_threshold, test_score_unanswered: @tce_test.test_score_unanswered, test_score_wrong: @tce_test.test_score_wrong, test_user_id: @tce_test.test_user_id } }
    end

    assert_redirected_to tce_test_url(TceTest.last)
  end

  test "should show tce_test" do
    get tce_test_url(@tce_test)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_test_url(@tce_test)
    assert_response :success
  end

  test "should update tce_test" do
    patch tce_test_url(@tce_test), params: { tce_test: { test_answers_order_mode: @tce_test.test_answers_order_mode, test_begin_time: @tce_test.test_begin_time, test_comment_enabled: @tce_test.test_comment_enabled, test_description: @tce_test.test_description, test_duration_time: @tce_test.test_duration_time, test_end_time: @tce_test.test_end_time, test_id: @tce_test.test_id, test_ip_range: @tce_test.test_ip_range, test_logout_on_timeout: @tce_test.test_logout_on_timeout, test_max_score: @tce_test.test_max_score, test_mcma_partial_score: @tce_test.test_mcma_partial_score, test_mcma_radio: @tce_test.test_mcma_radio, test_menu_enabled: @tce_test.test_menu_enabled, test_name: @tce_test.test_name, test_noanswer_enabled: @tce_test.test_noanswer_enabled, test_password: @tce_test.test_password, test_questions_order_mode: @tce_test.test_questions_order_mode, test_random_answers_order: @tce_test.test_random_answers_order, test_random_answers_select: @tce_test.test_random_answers_select, test_random_questions_order: @tce_test.test_random_questions_order, test_random_questions_select: @tce_test.test_random_questions_select, test_repeatable: @tce_test.test_repeatable, test_report_to_users: @tce_test.test_report_to_users, test_repeatable: @tce_test.test_repeatable, test_results_to_user: @tce_test.test_results_to_user, test_score_right: @tce_test.test_score_right, test_score_threshold: @tce_test.test_score_threshold, test_score_unanswered: @tce_test.test_score_unanswered, test_score_wrong: @tce_test.test_score_wrong, test_user_id: @tce_test.test_user_id } }
    assert_redirected_to tce_test_url(@tce_test)
  end

  test "should destroy tce_test" do
    assert_difference('TceTest.count', -1) do
      delete tce_test_url(@tce_test)
    end

    assert_redirected_to tce_tests_url
  end
end
