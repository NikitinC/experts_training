require "test_helper"

class DatExamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dat_exam = dat_exams(:one)
  end

  test "should get index" do
    get dat_exams_url
    assert_response :success
  end

  test "should get new" do
    get new_dat_exam_url
    assert_response :success
  end

  test "should create dat_exam" do
    assert_difference('DatExam.count') do
      post dat_exams_url, params: { dat_exam: { date: @dat_exam.date, fiscode: @dat_exam.fiscode, is_reserv: @dat_exam.is_reserv, subject_code: @dat_exam.subject_code, exam_type_id: @dat_exam.exam_type_id } }
    end

    assert_redirected_to dat_exam_url(DatExam.last)
  end

  test "should show dat_exam" do
    get dat_exam_url(@dat_exam)
    assert_response :success
  end

  test "should get edit" do
    get edit_dat_exam_url(@dat_exam)
    assert_response :success
  end

  test "should update dat_exam" do
    patch dat_exam_url(@dat_exam), params: { dat_exam: { date: @dat_exam.date, fiscode: @dat_exam.fiscode, is_reserv: @dat_exam.is_reserv, subject_code: @dat_exam.subject_code, exam_type_id: @dat_exam.exam_type_id } }
    assert_redirected_to dat_exam_url(@dat_exam)
  end

  test "should destroy dat_exam" do
    assert_difference('DatExam.count', -1) do
      delete dat_exam_url(@dat_exam)
    end

    assert_redirected_to dat_exams_url
  end
end
