require "test_helper"

class TceSubjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_subject = tce_subjects(:one)
  end

  test "should get index" do
    get tce_subjects_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_subject_url
    assert_response :success
  end

  test "should create tce_subject" do
    assert_difference('TceSubject.count') do
      post tce_subjects_url, params: { tce_subject: { subject_description: @tce_subject.subject_description, subject_enabled: @tce_subject.subject_enabled, subject_id: @tce_subject.subject_id, subject_module_id: @tce_subject.subject_module_id, subject_name: @tce_subject.name, subject_user_id: @tce_subject.subject_user_id } }
    end

    assert_redirected_to tce_subject_url(TceSubject.last)
  end

  test "should show tce_subject" do
    get tce_subject_url(@tce_subject)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_subject_url(@tce_subject)
    assert_response :success
  end

  test "should update tce_subject" do
    patch tce_subject_url(@tce_subject), params: { tce_subject: { subject_description: @tce_subject.subject_description, subject_enabled: @tce_subject.subject_enabled, subject_id: @tce_subject.subject_id, subject_module_id: @tce_subject.subject_module_id, subject_name: @tce_subject.name, subject_user_id: @tce_subject.subject_user_id } }
    assert_redirected_to tce_subject_url(@tce_subject)
  end

  test "should destroy tce_subject" do
    assert_difference('TceSubject.count', -1) do
      delete tce_subject_url(@tce_subject)
    end

    assert_redirected_to tce_subjects_url
  end
end
