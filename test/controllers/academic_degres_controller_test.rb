require "test_helper"

class AcademicDegreesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academic_degree = academic_degrees(:one)
  end

  test "should get index" do
    get academic_degrees_index_url
    assert_response :success
  end

  test "should get new" do
    get new_academic_degree_url
    assert_response :success
  end

  test "should create academic_degree" do
    assert_difference('AcademicDegrees.count') do
      post academic_degrees_index_url, params: { academic_degree: { code: @academic_degree.code, name: @academic_degree.name, short_name: @academic_degree.short_name, sort_by: @academic_degree.sort_by } }
    end

    assert_redirected_to academic_degree_url(AcademicDegrees.last)
  end

  test "should show academic_degree" do
    get academic_degree_url(@academic_degree)
    assert_response :success
  end

  test "should get edit" do
    get edit_academic_degree_url(@academic_degree)
    assert_response :success
  end

  test "should update academic_degree" do
    patch academic_degree_url(@academic_degree), params: { academic_degree: { code: @academic_degree.code, name: @academic_degree.name, short_name: @academic_degree.short_name, sort_by: @academic_degree.sort_by } }
    assert_redirected_to academic_degree_url(@academic_degree)
  end

  test "should destroy academic_degree" do
    assert_difference('AcademicDegrees.count', -1) do
      delete academic_degree_url(@academic_degree)
    end

    assert_redirected_to academic_degrees_index_url
  end
end
