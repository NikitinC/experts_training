require "test_helper"

class TceModulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_module = tce_modules(:one)
  end

  test "should get index" do
    get tce_modules_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_module_url
    assert_response :success
  end

  test "should create tce_module" do
    assert_difference('TceModule.count') do
      post tce_modules_url, params: { tce_module: { module_enabled: @tce_module.module_enabled, module_id: @tce_module.module_id, module_name: @tce_module.module_name, module_user_id: @tce_module.module_user_id } }
    end

    assert_redirected_to tce_module_url(TceModule.last)
  end

  test "should show tce_module" do
    get tce_module_url(@tce_module)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_module_url(@tce_module)
    assert_response :success
  end

  test "should update tce_module" do
    patch tce_module_url(@tce_module), params: { tce_module: { module_enabled: @tce_module.module_enabled, module_id: @tce_module.module_id, module_name: @tce_module.module_name, module_user_id: @tce_module.module_user_id } }
    assert_redirected_to tce_module_url(@tce_module)
  end

  test "should destroy tce_module" do
    assert_difference('TceModule.count', -1) do
      delete tce_module_url(@tce_module)
    end

    assert_redirected_to tce_modules_url
  end
end
