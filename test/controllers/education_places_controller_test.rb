require "test_helper"

class EducationPlacesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @education_place = education_places(:one)
  end

  test "should get index" do
    get education_places_index_url
    assert_response :success
  end

  test "should get new" do
    get new_education_place_url
    assert_response :success
  end

  test "should create education_place" do
    assert_difference('EducationPlaces.count') do
      post education_places_index_url, params: { education_place: { code: @education_place.code, name: @education_place.name, short_name: @education_place.short_name, sort_by: @education_place.sort_by } }
    end

    assert_redirected_to education_place_url(EducationPlaces.last)
  end

  test "should show education_place" do
    get education_place_url(@education_place)
    assert_response :success
  end

  test "should get edit" do
    get edit_education_place_url(@education_place)
    assert_response :success
  end

  test "should update education_place" do
    patch education_place_url(@education_place), params: { education_place: { code: @education_place.code, name: @education_place.name, short_name: @education_place.short_name, sort_by: @education_place.sort_by } }
    assert_redirected_to education_place_url(@education_place)
  end

  test "should destroy education_place" do
    assert_difference('EducationPlaces.count', -1) do
      delete education_place_url(@education_place)
    end

    assert_redirected_to education_places_index_url
  end
end
