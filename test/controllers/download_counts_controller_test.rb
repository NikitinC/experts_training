require "test_helper"

class DownloadCountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @download_count = download_counts(:one)
  end

  test "should get index" do
    get download_counts_url
    assert_response :success
  end

  test "should get new" do
    get new_download_count_url
    assert_response :success
  end

  test "should create download_count" do
    assert_difference('DownloadCount.count') do
      post download_counts_url, params: { download_count: { resource: @download_count.resource, user_id: @download_count.user_id } }
    end

    assert_redirected_to download_count_url(DownloadCount.last)
  end

  test "should show download_count" do
    get download_count_url(@download_count)
    assert_response :success
  end

  test "should get edit" do
    get edit_download_count_url(@download_count)
    assert_response :success
  end

  test "should update download_count" do
    patch download_count_url(@download_count), params: { download_count: { resource: @download_count.resource, user_id: @download_count.user_id } }
    assert_redirected_to download_count_url(@download_count)
  end

  test "should destroy download_count" do
    assert_difference('DownloadCount.count', -1) do
      delete download_count_url(@download_count)
    end

    assert_redirected_to download_counts_url
  end
end
