require "test_helper"

class TceTestsUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_tests_user = tce_tests_users(:one)
  end

  test "should get index" do
    get tce_tests_users_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_tests_user_url
    assert_response :success
  end

  test "should create tce_tests_user" do
    assert_difference('TceTestsUser.count') do
      post tce_tests_users_url, params: { tce_tests_user: { testuser_comment: @tce_tests_user.testuser_comment, testuser_creation_time: @tce_tests_user.testuser_creation_time, testuser_id: @tce_tests_user.testuser_id, testuser_status: @tce_tests_user.testuser_status, testuser_test_id: @tce_tests_user.testuser_test_id, testuser_user_id: @tce_tests_user.testuser_user_id } }
    end

    assert_redirected_to tce_tests_user_url(TceTestsUser.last)
  end

  test "should show tce_tests_user" do
    get tce_tests_user_url(@tce_tests_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_tests_user_url(@tce_tests_user)
    assert_response :success
  end

  test "should update tce_tests_user" do
    patch tce_tests_user_url(@tce_tests_user), params: { tce_tests_user: { testuser_comment: @tce_tests_user.testuser_comment, testuser_creation_time: @tce_tests_user.testuser_creation_time, testuser_id: @tce_tests_user.testuser_id, testuser_status: @tce_tests_user.testuser_status, testuser_test_id: @tce_tests_user.testuser_test_id, testuser_user_id: @tce_tests_user.testuser_user_id } }
    assert_redirected_to tce_tests_user_url(@tce_tests_user)
  end

  test "should destroy tce_tests_user" do
    assert_difference('TceTestsUser.count', -1) do
      delete tce_tests_user_url(@tce_tests_user)
    end

    assert_redirected_to tce_tests_users_url
  end
end
