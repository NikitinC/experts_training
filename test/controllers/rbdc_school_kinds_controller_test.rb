require "test_helper"

class RbdcSchoolKindsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_school_kind = rbdc_school_kinds(:one)
  end

  test "should get index" do
    get rbdc_school_kinds_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_school_kind_url
    assert_response :success
  end

  test "should create rbdc_school_kind" do
    assert_difference('RbdcSchoolKind.count') do
      post rbdc_school_kinds_url, params: { rbdc_school_kind: { code: @rbdc_school_kind.code, name: @rbdc_school_kind.name, school_type_id: @rbdc_school_kind.school_type_id, sort_by: @rbdc_school_kind.sort_by } }
    end

    assert_redirected_to rbdc_school_kind_url(RbdcSchoolKind.last)
  end

  test "should show rbdc_school_kind" do
    get rbdc_school_kind_url(@rbdc_school_kind)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_school_kind_url(@rbdc_school_kind)
    assert_response :success
  end

  test "should update rbdc_school_kind" do
    patch rbdc_school_kind_url(@rbdc_school_kind), params: { rbdc_school_kind: { code: @rbdc_school_kind.code, name: @rbdc_school_kind.name, school_type_id: @rbdc_school_kind.school_type_id, sort_by: @rbdc_school_kind.sort_by } }
    assert_redirected_to rbdc_school_kind_url(@rbdc_school_kind)
  end

  test "should destroy rbdc_school_kind" do
    assert_difference('RbdcSchoolKind.count', -1) do
      delete rbdc_school_kind_url(@rbdc_school_kind)
    end

    assert_redirected_to rbdc_school_kinds_url
  end
end
