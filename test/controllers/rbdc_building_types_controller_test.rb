require "test_helper"

class RbdcBuildingTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_building_type = rbdc_building_types(:one)
  end

  test "should get index" do
    get rbdc_building_types_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_building_type_url
    assert_response :success
  end

  test "should create rbdc_building_type" do
    assert_difference('RbdcBuildingType.count') do
      post rbdc_building_types_url, params: { rbdc_building_type: { code: @rbdc_building_type.code, name: @rbdc_building_type.name, short_name: @rbdc_building_type.short_name, sort_by: @rbdc_building_type.sort_by } }
    end

    assert_redirected_to rbdc_building_type_url(RbdcBuildingType.last)
  end

  test "should show rbdc_building_type" do
    get rbdc_building_type_url(@rbdc_building_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_building_type_url(@rbdc_building_type)
    assert_response :success
  end

  test "should update rbdc_building_type" do
    patch rbdc_building_type_url(@rbdc_building_type), params: { rbdc_building_type: { code: @rbdc_building_type.code, name: @rbdc_building_type.name, short_name: @rbdc_building_type.short_name, sort_by: @rbdc_building_type.sort_by } }
    assert_redirected_to rbdc_building_type_url(@rbdc_building_type)
  end

  test "should destroy rbdc_building_type" do
    assert_difference('RbdcBuildingType.count', -1) do
      delete rbdc_building_type_url(@rbdc_building_type)
    end

    assert_redirected_to rbdc_building_types_url
  end
end
