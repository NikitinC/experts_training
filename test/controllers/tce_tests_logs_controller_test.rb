require "test_helper"

class TceTestsLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_tests_log = tce_tests_logs(:one)
  end

  test "should get index" do
    get tce_tests_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_tests_log_url
    assert_response :success
  end

  test "should create tce_tests_log" do
    assert_difference('TceTestsLog.count') do
      post tce_tests_logs_url, params: { tce_tests_log: { testlog_answer_text: @tce_tests_log.testlog_answer_text, testlog_change_time: @tce_tests_log.testlog_change_time, testlog_comment: @tce_tests_log.testlog_comment, testlog_creation_time: @tce_tests_log.testlog_creation_time, testlog_display_time: @tce_tests_log.testlog_display_time, testlog_id: @tce_tests_log.testlog_id, testlog_num_answers: @tce_tests_log.testlog_num_answers, testlog_order: @tce_tests_log.testlog_order, testlog_question_id: @tce_tests_log.testlog_question_id, testlog_reaction_time: @tce_tests_log.testlog_reaction_time, testlog_score: @tce_tests_log.testlog_score, testlog_testuser_id: @tce_tests_log.testlog_testuser_id, testlog_user_ip: @tce_tests_log.testlog_user_ip } }
    end

    assert_redirected_to tce_tests_log_url(TceTestsLog.last)
  end

  test "should show tce_tests_log" do
    get tce_tests_log_url(@tce_tests_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_tests_log_url(@tce_tests_log)
    assert_response :success
  end

  test "should update tce_tests_log" do
    patch tce_tests_log_url(@tce_tests_log), params: { tce_tests_log: { testlog_answer_text: @tce_tests_log.testlog_answer_text, testlog_change_time: @tce_tests_log.testlog_change_time, testlog_comment: @tce_tests_log.testlog_comment, testlog_creation_time: @tce_tests_log.testlog_creation_time, testlog_display_time: @tce_tests_log.testlog_display_time, testlog_id: @tce_tests_log.testlog_id, testlog_num_answers: @tce_tests_log.testlog_num_answers, testlog_order: @tce_tests_log.testlog_order, testlog_question_id: @tce_tests_log.testlog_question_id, testlog_reaction_time: @tce_tests_log.testlog_reaction_time, testlog_score: @tce_tests_log.testlog_score, testlog_testuser_id: @tce_tests_log.testlog_testuser_id, testlog_user_ip: @tce_tests_log.testlog_user_ip } }
    assert_redirected_to tce_tests_log_url(@tce_tests_log)
  end

  test "should destroy tce_tests_log" do
    assert_difference('TceTestsLog.count', -1) do
      delete tce_tests_log_url(@tce_tests_log)
    end

    assert_redirected_to tce_tests_logs_url
  end
end
