require "test_helper"

class RbdcSchoolTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_school_type = rbdc_school_types(:one)
  end

  test "should get index" do
    get rbdc_school_types_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_school_type_url
    assert_response :success
  end

  test "should create rbdc_school_type" do
    assert_difference('RbdcSchoolType.count') do
      post rbdc_school_types_url, params: { rbdc_school_type: { code: @rbdc_school_type.code, name: @rbdc_school_type.name, short_name: @rbdc_school_type.short_name, sort_by: @rbdc_school_type.sort_by } }
    end

    assert_redirected_to rbdc_school_type_url(RbdcSchoolType.last)
  end

  test "should show rbdc_school_type" do
    get rbdc_school_type_url(@rbdc_school_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_school_type_url(@rbdc_school_type)
    assert_response :success
  end

  test "should update rbdc_school_type" do
    patch rbdc_school_type_url(@rbdc_school_type), params: { rbdc_school_type: { code: @rbdc_school_type.code, name: @rbdc_school_type.name, short_name: @rbdc_school_type.short_name, sort_by: @rbdc_school_type.sort_by } }
    assert_redirected_to rbdc_school_type_url(@rbdc_school_type)
  end

  test "should destroy rbdc_school_type" do
    assert_difference('RbdcSchoolType.count', -1) do
      delete rbdc_school_type_url(@rbdc_school_type)
    end

    assert_redirected_to rbdc_school_types_url
  end
end
