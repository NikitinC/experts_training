require "test_helper"

class ParticipantsBarcodesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @participants_barcode = participants_barcodes(:one)
  end

  test "should get index" do
    get participants_barcodes_url
    assert_response :success
  end

  test "should get new" do
    get new_participants_barcode_url
    assert_response :success
  end

  test "should create participants_barcode" do
    assert_difference('ParticipantsBarcode.count') do
      post participants_barcodes_url, params: { participants_barcode: { barcode: @participants_barcode.barcode, participant_id: @participants_barcode.participant_id, subject_id: @participants_barcode.subject_id, wave_id: @participants_barcode.wave_id } }
    end

    assert_redirected_to participants_barcode_url(ParticipantsBarcode.last)
  end

  test "should show participants_barcode" do
    get participants_barcode_url(@participants_barcode)
    assert_response :success
  end

  test "should get edit" do
    get edit_participants_barcode_url(@participants_barcode)
    assert_response :success
  end

  test "should update participants_barcode" do
    patch participants_barcode_url(@participants_barcode), params: { participants_barcode: { barcode: @participants_barcode.barcode, participant_id: @participants_barcode.participant_id, subject_id: @participants_barcode.subject_id, wave_id: @participants_barcode.wave_id } }
    assert_redirected_to participants_barcode_url(@participants_barcode)
  end

  test "should destroy participants_barcode" do
    assert_difference('ParticipantsBarcode.count', -1) do
      delete participants_barcode_url(@participants_barcode)
    end

    assert_redirected_to participants_barcodes_url
  end
end
