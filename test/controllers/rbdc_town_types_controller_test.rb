require "test_helper"

class RbdcTownTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rbdc_town_type = rbdc_town_types(:one)
  end

  test "should get index" do
    get rbdc_town_types_url
    assert_response :success
  end

  test "should get new" do
    get new_rbdc_town_type_url
    assert_response :success
  end

  test "should create rbdc_town_type" do
    assert_difference('RbdcTownType.count') do
      post rbdc_town_types_url, params: { rbdc_town_type: { code: @rbdc_town_type.code, name: @rbdc_town_type.name, short_name: @rbdc_town_type.short_name, sort_by: @rbdc_town_type.sort_by } }
    end

    assert_redirected_to rbdc_town_type_url(RbdcTownType.last)
  end

  test "should show rbdc_town_type" do
    get rbdc_town_type_url(@rbdc_town_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_rbdc_town_type_url(@rbdc_town_type)
    assert_response :success
  end

  test "should update rbdc_town_type" do
    patch rbdc_town_type_url(@rbdc_town_type), params: { rbdc_town_type: { code: @rbdc_town_type.code, name: @rbdc_town_type.name, short_name: @rbdc_town_type.short_name, sort_by: @rbdc_town_type.sort_by } }
    assert_redirected_to rbdc_town_type_url(@rbdc_town_type)
  end

  test "should destroy rbdc_town_type" do
    assert_difference('RbdcTownType.count', -1) do
      delete rbdc_town_type_url(@rbdc_town_type)
    end

    assert_redirected_to rbdc_town_types_url
  end
end
