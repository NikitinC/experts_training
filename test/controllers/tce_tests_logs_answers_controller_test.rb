require "test_helper"

class TceTestsLogsAnswersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_tests_logs_answer = tce_tests_logs_answers(:one)
  end

  test "should get index" do
    get tce_tests_logs_answers_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_tests_logs_answer_url
    assert_response :success
  end

  test "should create tce_tests_logs_answer" do
    assert_difference('TceTestsLogsAnswer.count') do
      post tce_tests_logs_answers_url, params: { tce_tests_logs_answer: { logansw_answer_id: @tce_tests_logs_answer.logansw_answer_id, logansw_order: @tce_tests_logs_answer.logansw_order, logansw_position: @tce_tests_logs_answer.logansw_position, logansw_selected: @tce_tests_logs_answer.logansw_selected, logansw_testlog_id: @tce_tests_logs_answer.logansw_testlog_id } }
    end

    assert_redirected_to tce_tests_logs_answer_url(TceTestsLogsAnswer.last)
  end

  test "should show tce_tests_logs_answer" do
    get tce_tests_logs_answer_url(@tce_tests_logs_answer)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_tests_logs_answer_url(@tce_tests_logs_answer)
    assert_response :success
  end

  test "should update tce_tests_logs_answer" do
    patch tce_tests_logs_answer_url(@tce_tests_logs_answer), params: { tce_tests_logs_answer: { logansw_answer_id: @tce_tests_logs_answer.logansw_answer_id, logansw_order: @tce_tests_logs_answer.logansw_order, logansw_position: @tce_tests_logs_answer.logansw_position, logansw_selected: @tce_tests_logs_answer.logansw_selected, logansw_testlog_id: @tce_tests_logs_answer.logansw_testlog_id } }
    assert_redirected_to tce_tests_logs_answer_url(@tce_tests_logs_answer)
  end

  test "should destroy tce_tests_logs_answer" do
    assert_difference('TceTestsLogsAnswer.count', -1) do
      delete tce_tests_logs_answer_url(@tce_tests_logs_answer)
    end

    assert_redirected_to tce_tests_logs_answers_url
  end
end
