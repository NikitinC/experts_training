require "test_helper"

class TceTestSubjectSetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_test_subject_set = tce_test_subject_set(:one)
  end

  test "should get index" do
    get tce_test_subject_set_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_test_subject_set_url
    assert_response :success
  end

  test "should create tce_test_subject_set" do
    assert_difference('TceTestSubjectSet.count') do
      post tce_test_subject_set_url, params: { tce_test_subject_set: { tsubset_answers: @tce_test_subject_set.tsubset_answers, tsubset_difficulty: @tce_test_subject_set.tsubset_difficulty, subjset_tsubset_id: @tce_test_subject_set.subjset_tsubset_id, tsubset_quantity: @tce_test_subject_set.tsubset_quantity, tsubset_test_id: @tce_test_subject_set.tsubset_test_id, tsubset_type: @tce_test_subject_set.tsubset_type } }
    end

    assert_redirected_to tce_test_subject_set_url(TceTestSubjectSet.last)
  end

  test "should show tce_test_subject_set" do
    get tce_test_subject_set_url(@tce_test_subject_set)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_test_subject_set_url(@tce_test_subject_set)
    assert_response :success
  end

  test "should update tce_test_subject_set" do
    patch tce_test_subject_set_url(@tce_test_subject_set), params: { tce_test_subject_set: { tsubset_answers: @tce_test_subject_set.tsubset_answers, tsubset_difficulty: @tce_test_subject_set.tsubset_difficulty, subjset_tsubset_id: @tce_test_subject_set.subjset_tsubset_id, tsubset_quantity: @tce_test_subject_set.tsubset_quantity, tsubset_test_id: @tce_test_subject_set.tsubset_test_id, tsubset_type: @tce_test_subject_set.tsubset_type } }
    assert_redirected_to tce_test_subject_set_url(@tce_test_subject_set)
  end

  test "should destroy tce_test_subject_set" do
    assert_difference('TceTestSubjectSet.count', -1) do
      delete tce_test_subject_set_url(@tce_test_subject_set)
    end

    assert_redirected_to tce_test_subject_set_url
  end
end
