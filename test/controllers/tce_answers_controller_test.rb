require "test_helper"

class TceAnswersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tce_answer = tce_answers(:one)
  end

  test "should get index" do
    get tce_answers_url
    assert_response :success
  end

  test "should get new" do
    get new_tce_answer_url
    assert_response :success
  end

  test "should create tce_answer" do
    assert_difference('TceAnswer.count') do
      post tce_answers_url, params: { tce_answer: { answer_description: @tce_answer.answer_description, answer_enabled: @tce_answer.answer_enabled, answer_explanation: @tce_answer.answer_explanation, answer_id: @tce_answer.answer_id, answer_isright: @tce_answer.answer_isright, answer_keyboard_key: @tce_answer.answer_keyboard_key, answer_position: @tce_answer.answer_position, answer_question_id: @tce_answer.answer_question_id } }
    end

    assert_redirected_to tce_answer_url(TceAnswer.last)
  end

  test "should show tce_answer" do
    get tce_answer_url(@tce_answer)
    assert_response :success
  end

  test "should get edit" do
    get edit_tce_answer_url(@tce_answer)
    assert_response :success
  end

  test "should update tce_answer" do
    patch tce_answer_url(@tce_answer), params: { tce_answer: { answer_description: @tce_answer.answer_description, answer_enabled: @tce_answer.answer_enabled, answer_explanation: @tce_answer.answer_explanation, answer_id: @tce_answer.answer_id, answer_isright: @tce_answer.answer_isright, answer_keyboard_key: @tce_answer.answer_keyboard_key, answer_position: @tce_answer.answer_position, answer_question_id: @tce_answer.answer_question_id } }
    assert_redirected_to tce_answer_url(@tce_answer)
  end

  test "should destroy tce_answer" do
    assert_difference('TceAnswer.count', -1) do
      delete tce_answer_url(@tce_answer)
    end

    assert_redirected_to tce_answers_url
  end
end
