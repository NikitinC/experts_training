require "test_helper"

class SheduleTasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @shedule_task = shedule_tasks(:one)
  end

  test "should get index" do
    get shedule_tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_shedule_task_url
    assert_response :success
  end

  test "should create shedule_task" do
    assert_difference('SheduleTask.count') do
      post shedule_tasks_url, params: { shedule_task: { date: @shedule_task.date, shedule_id: @shedule_task.shedule_id } }
    end

    assert_redirected_to shedule_task_url(SheduleTask.last)
  end

  test "should show shedule_task" do
    get shedule_task_url(@shedule_task)
    assert_response :success
  end

  test "should get edit" do
    get edit_shedule_task_url(@shedule_task)
    assert_response :success
  end

  test "should update shedule_task" do
    patch shedule_task_url(@shedule_task), params: { shedule_task: { date: @shedule_task.date, shedule_id: @shedule_task.shedule_id } }
    assert_redirected_to shedule_task_url(@shedule_task)
  end

  test "should destroy shedule_task" do
    assert_difference('SheduleTask.count', -1) do
      delete shedule_task_url(@shedule_task)
    end

    assert_redirected_to shedule_tasks_url
  end
end
