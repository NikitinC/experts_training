class CbtAnswerTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_answer_types_import = CbtAnswerTypesImport.new
  end

  def create
    @cbt_answer_types_import = CbtAnswerTypesImport.new(params[:cbt_answer_types_import])
    render :create
  end

end
