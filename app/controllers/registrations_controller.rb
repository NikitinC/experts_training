class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    # add custom create logic here
    @user = User.new(user_params)
    if @user.save
      # flash[:notice] = t("devise.registrations.update_needs_confirmation")
      redirect_to "/users/sign_in", :flash => { :notice => t("devise.registrations.update_needs_confirmation") }
    else
      render :action => 'new'
    end
  end

  def update
    super
  end

  def user_params
    params.require(:user).permit(:email, :first_name, :password, :password_confirmation)
  end

end