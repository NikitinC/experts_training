class SubjectsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @subjects = Subject.accessible_by(current_ability).paginate(page: params[:page]).order('sort_by ASC, code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @subjects = Subject.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('sort_by ASC, code ASC')
      end
    else
      if !params[:search].nil?
        @subjects = Subject.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('sort_by ASC, code ASC')
      end
    end
  end

  def export
    @subjects = Subject.accessible_by(current_ability).order('code ASC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'subjects_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @subject = Subject.new
    if !@subject.nil?
       render :edit
    else
       flash[:success] = t("subjects.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @subject = Subject.accessible_by(current_ability).find(params[:id])
    @previous = Subject.accessible_by(current_ability).where("id < " + @subject.id.to_s).order("id DESC").first
    @next = Subject.accessible_by(current_ability).where("id > " + @subject.id.to_s).order("id ASC").first
    @subjects = Subject.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @subject
      render :action => 'show'
    end
  end

  def create
    @subject = Subject.new(subject_params)
    if @subject.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @subject = Subject.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @subject }
    end
  end

  def modaledit
    @subject = Subject.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @subject }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    Subject.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @subject = Subject.accessible_by(current_ability).find(params[:id])
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to subjects_url, notice: t("subjects.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @subject = Subject.accessible_by(current_ability).find(params[:id])

    if @subject.update subject_params
      respond_to do |format|
        format.html {
          flash[:success] = t("subjects.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @subject.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("subjects." + er.attribute.to_s) + ': ' + t("subjects.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@subject) }
      end
    end

  end

  def subject_params
    params.require(:subject).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by, :protocol11_max_balls, :protocol11_col_names, :protocol9_max_balls, :protocol9_col_names, :norm_9, :norm_11)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@subject.versions.scoped.last), :method => :post)
  end

end
