class SiteInfosController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @site_info = SiteInfo.all
    if @site_info.count < 1
      @site_info = SiteInfo.new
    else
      @site_info = @site_info.first
    end
    @site_info.save
  end

  def export
    @site_infos = SiteInfo.accessible_by(current_ability).order('code ASC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'site_infos_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @site_info = SiteInfo.new
    if !@site_info.nil?
       render :edit
    else
       flash[:success] = t("site_infos.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @site_info = SiteInfo.accessible_by(current_ability).find(params[:id])
    @previous = SiteInfo.accessible_by(current_ability).where("id < " + @site_info.id.to_s).order("id DESC").first
    @next = SiteInfo.accessible_by(current_ability).where("id > " + @site_info.id.to_s).order("id ASC").first
    @site_infos = SiteInfo.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @site_info
      render :action => 'show'
    end
  end

  def create
    @site_info = SiteInfo.new(site_info_params)
    if @site_info.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @site_info = SiteInfo.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @site_info }
    end
  end

  def modaledit
    @site_info = SiteInfo.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @site_info }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    SiteInfo.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @site_info = SiteInfo.accessible_by(current_ability).find(params[:id])
    @site_info.destroy
    respond_to do |format|
      format.html { redirect_to site_infos_url, notice: t("site_infos.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @site_info = SiteInfo.accessible_by(current_ability).find(params[:id])

    if @site_info.update site_info_params
      respond_to do |format|
        format.html {
          flash[:success] = t("site_infos.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @site_info.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("site_infos." + er.attribute.to_s) + ': ' + t("site_infos.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@site_info) }
      end
    end

  end

  def site_info_params
    params.require(:site_info).permit(:is_close, :close_ips, :open_name, :close_name, :host,
                                      :email, :email_password,
                                      :copyright, :is_help, :is_condition, :support)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@site_info.versions.scoped.last), :method => :post)
  end

end
