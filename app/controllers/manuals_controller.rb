class ManualsController < ApplicationController
  before_action :set_manual, only: %i[ show edit update destroy ]
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!


  # GET /manuals or /manuals.json
  def index
    @manuals = Manual.all
  end

  # GET /manuals/1 or /manuals/1.json
  def show
  end

  # GET /manuals/new
  def new
    @manual = Manual.new
  end

  # GET /manuals/1/edit
  def edit
  end

  # POST /manuals or /manuals.json
  def create
    @manual = Manual.new(manual_params)

    respond_to do |format|
      if @manual.save
        format.html { redirect_to @manual, notice: "Руководство было успешно создано." }
        format.json { render :show, status: :created, location: @manual }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @manual.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /manuals/1 or /manuals/1.json
  def update
    respond_to do |format|
      if @manual.update(manual_params)
        format.html { redirect_to @manual, notice: "Руководство было успешно обновлено." }
        format.json { render :show, status: :ok, location: @manual }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @manual.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /manuals/1 or /manuals/1.json
  def destroy
    @manual.destroy
    respond_to do |format|
      format.html { redirect_to manuals_url, notice: "Руководство было успешно удалено." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_manual
      @manual = Manual.find(params[:id])
    end


    # Only allow a list of trusted parameters through.
    def manual_params
      params.require(:manual).permit(:model, :url, :sort_by, :manual_text)
    end
end
