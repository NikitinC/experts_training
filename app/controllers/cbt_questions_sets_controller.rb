class CbtQuestionsSetsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @cbt_questions_sets = CbtQuestionsSet.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')

    if current_user.admin?
      if !params[:search].nil?
        @cbt_questions_sets = CbtQuestionsSet.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    else
      if !params[:search].nil?
        @cbt_questions_sets = CbtQuestionsSet.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    end
  end

  def export
    @cbt_questions_sets = CbtQuestionsSet.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_questions_sets_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @sbt_subjects = CbtSubject.all.order("id")
    @cbt_questions_set = CbtQuestionsSet.new
    if !@cbt_questions_set.nil?
       render :edit
    else
       flash[:success] = t("cbt_questions_sets.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @sbt_subjects = CbtSubject.all.order("id")
    @cbt_questions_set = CbtQuestionsSet.accessible_by(current_ability).find(params[:id])
    @previous = CbtQuestionsSet.accessible_by(current_ability).where("id < " + @cbt_questions_set.id.to_s).order("id DESC").first
    @next = CbtQuestionsSet.accessible_by(current_ability).where("id > " + @cbt_questions_set.id.to_s).order("id ASC").first
    @cbt_questions_sets = CbtQuestionsSet.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_questions_set
      render :action => 'show'
    end
  end

  def create
    @sbt_subjects = CbtSubject.all.order("id")
    @cbt_questions_set = CbtQuestionsSet.new(cbt_questions_set_params)
    if @cbt_questions_set.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @sbt_subjects = CbtSubject.all.order("id")
    @cbt_questions_set = CbtQuestionsSet.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_questions_set }
    end
  end

  def modaledit
    @cbt_questions_set = CbtQuestionsSet.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_questions_set }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtQuestionsSet.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_questions_set = CbtQuestionsSet.accessible_by(current_ability).find(params[:id])
    @cbt_questions_set.destroy
    respond_to do |format|
      format.html { redirect_to cbt_questions_sets_url, notice: t("cbt_questions_sets.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_questions_set = CbtQuestionsSet.accessible_by(current_ability).find(params[:id])

    if @cbt_questions_set.update cbt_questions_set_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_questions_sets.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_questions_set.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_questions_sets." + er.attribute.to_s) + ': ' + t("cbt_questions_sets.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_questions_set) }
      end
    end

  end

  def cbt_questions_set_params
    params.require(:cbt_questions_set).permit(:name, :description, :shuffle, :cbt_subject_id, :deleted_at,
                                              :help_pdf_file_cache, :remove_help_pdf_file, :help_pdf_file, # справочные материалы
                                              :description_text)
  end

  private

  def fill_directories
    @cbt_subjects = CbtSubject.all.order("id")
  end


  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_questions_set.versions.scoped.last), :method => :post)
  end

end
