class CbtSubjectsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_subjects_import = CbtSubjectsImport.new
  end

  def create
    @cbt_subjects_import = CbtSubjectsImport.new(params[:cbt_subjects_import])
    render :create
  end

end
