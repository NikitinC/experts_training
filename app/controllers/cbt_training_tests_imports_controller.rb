class CbtTrainingTestsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_training_tests_import = CbtTrainingTestsImport.new
  end

  def create
    @cbt_training_tests_import = CbtTrainingTestsImport.new(params[:cbt_training_tests_import])
    render :create
  end

end
