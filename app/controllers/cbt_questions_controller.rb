class CbtQuestionsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  # def index
  #   @cbt_subject = CbtSubject
  #   @cbt_answer_type = CbtAnswerType
  #   @cbt_question_type = CbtQuestionType
  #   @cbt_questions = CbtQuestion.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
  #   if current_user.admin?
  #     if !params[:search].nil?
  #       @cbt_questions = CbtQuestion.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
  #     end
  #   else
  #     if !params[:search].nil?
  #       @cbt_questions = CbtQuestion.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
  #     end
  #   end
  # end

  def index
    @cbt_questions = CbtQuestion.accessible_by(current_ability)

    @filterrific = initialize_filterrific(
      @cbt_questions,
      params[:filterrific],
      select_options: {
        with_subject_id: CbtSubject.options_for_select,
        with_test_id: CbtTest.options_for_select,
        with_training_id: CbtTraining.options_for_select,
      },
      persistence_id: "shared_key",
      default_filter_params: {},
      available_filters: [:search_query, :with_subject_id, :no_question_sets, :with_test_id, :with_training_id],
      sanitize_params: true,
      ) || return

    begin
      @cbt_questions = @filterrific.find.page(params[:page])
    rescue
      @cbt_questions = @filterrific.find.paginate(page: 1)
    end

    respond_to do |format|
      format.html
      format.js
    end

  end

  def preview
    @cbt_question = CbtQuestion.accessible_by(current_ability).find(params[:id])
  end
  def export
    @cbt_questions = CbtQuestion.accessible_by(current_ability)

    @filterrific = initialize_filterrific(
      @cbt_questions,
      params[:filterrific],
      select_options: {
        with_subject_id: CbtSubject.options_for_select,
        with_test_id: CbtTest.options_for_select,
        with_training_id: CbtTraining.options_for_select,
      },
      persistence_id: "shared_key",
      default_filter_params: {},
      available_filters: [:search_query, :with_subject_id, :no_question_sets, :with_test_id, :with_training_id],
      sanitize_params: true,
      ) || return

    @cbt_questions = @filterrific.find

    # render xlsx: 'cbt_questions_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/cbt_questions/export.xlsx.axlsx'
    # render "export.xlsx.axlsx"

    # @cbt_questions = CbtQuestion.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_questions_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end

  end


  def new
    @cbt_subject = CbtSubject
    @cbt_answer_type = CbtAnswerType
    @cbt_question_type = CbtQuestionType

    @cbt_question = CbtQuestion.new
    if !@cbt_question.nil?
       render :edit
    else
       flash[:success] = t("cbt_questions.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @cbt_subject = CbtSubject
    @cbt_answer_type = CbtAnswerType
    @cbt_question_type = CbtQuestionType

    @cbt_question = CbtQuestion.accessible_by(current_ability).find(params[:id])
    @previous = CbtQuestion.accessible_by(current_ability).where("id < " + @cbt_question.id.to_s).order("id DESC").first
    @next = CbtQuestion.accessible_by(current_ability).where("id > " + @cbt_question.id.to_s).order("id ASC").first
    @cbt_questions = CbtQuestion.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_question
      render :action => 'show'
    end
  end

  def create
    @cbt_question = CbtQuestion.new(cbt_question_params)
    if @cbt_question.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_question = CbtQuestion.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_question }
    end
  end

  def modaledit
    @cbt_question = CbtQuestion.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_question }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtQuestion.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_question = CbtQuestion.accessible_by(current_ability).find(params[:id])
    @cbt_question.destroy
    respond_to do |format|
      format.html { redirect_to cbt_questions_url, notice: t("cbt_questions.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_question = CbtQuestion.accessible_by(current_ability).find(params[:id])

    if @cbt_question.update cbt_question_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_questions.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_question.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_questions." + er.attribute.to_s) + ': ' + t("cbt_questions.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_question) }
      end
    end

  end

  def cbt_question_params
    params.require(:cbt_question).permit(:name, :cbt_subject_id, :cbt_answer_type_id, :cbt_question_type_id, :settings, :primary_mark,
                                         :correct_answer, :partial_mark, :next_cbt_question_id, :deleted_at,
                                         :pdf_file_cache, :remove_pdf_file, :pdf_file, # - это PDF
                                         :help_pdf_file_cache, :remove_help_pdf_file, :help_pdf_file, # справочные материалы
                                         :shuffle, #перемешивание для вариантов ответов
                                         :k_nums, #номера критериев (когда не по порядку)
                                         :question_text # - приложенный Trix-текст
                                      )
  end

  private

  #@cbt_subjects, @cbt_answer_types, @cbt_question_types
  def fill_directories
    @cbt_subjects = CbtSubject.all.order("id")
    @cbt_answer_types = CbtAnswerType.all.order("id")
    @cbt_question_types = CbtQuestionType.all.order("id")
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_question.versions.scoped.last), :method => :post)
  end

end
