class CbtTestsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @cbt_tests = CbtTest.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_tests = CbtTest.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    else
      if !params[:search].nil?
        @cbt_tests = CbtTest.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    end
  end

  def export
    @cbt_tests = CbtTest.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_tests_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @cbt_test = CbtTest.new
    if !@cbt_test.nil?
       render :edit
    else
       flash[:success] = t("cbt_tests.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @cbt_test = CbtTest.accessible_by(current_ability).find(params[:id])
    @previous = CbtTest.accessible_by(current_ability).where("id < " + @cbt_test.id.to_s).order("id DESC").first
    @next = CbtTest.accessible_by(current_ability).where("id > " + @cbt_test.id.to_s).order("id ASC").first
    @cbt_tests = CbtTest.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_test
      render :action => 'show'
    end
  end

  def create
    @cbt_test = CbtTest.new(cbt_test_params)
    if @cbt_test.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_test = CbtTest.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_test }
    end
  end

  def modaledit
    @cbt_test = CbtTest.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_test }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtTest.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_test = CbtTest.accessible_by(current_ability).find(params[:id])
    @cbt_test.destroy
    respond_to do |format|
      format.html { redirect_to cbt_tests_url, notice: t("cbt_tests.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_test = CbtTest.accessible_by(current_ability).find(params[:id])

    if @cbt_test.update cbt_test_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_tests.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_test.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_tests." + er.attribute.to_s) + ': ' + t("cbt_tests.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_test) }
      end
    end

  end

  def cbt_test_params
    params.require(:cbt_test).permit(:name, :description, :active, :shuffle, :time_limit,
                                     :explore_correct_answers, :explore_correct_answers_after_stopped,
                                     :manual_cache, :remove_manual, :manual, # - это PDF с инструкциями
                                     :attempts_number, :started_at, :stopped_at, :manual_text)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_test.versions.scoped.last), :method => :post)
  end

end
