class CbtUserTestsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @cbt_user_tests = CbtUserTest.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_user_tests = CbtUserTest.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    else
      if !params[:search].nil?
        @cbt_user_tests = CbtUserTest.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    end
  end

  def export
    @cbt_user_tests = CbtUserTest.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_user_tests_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @cbt_user_test = CbtUserTest.new
    if !@cbt_user_test.nil?
       render :edit
    else
       flash[:success] = t("cbt_user_tests.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @cbt_user_test = CbtUserTest.accessible_by(current_ability).find(params[:id])
    @previous = CbtUserTest.accessible_by(current_ability).where("id < " + @cbt_user_test.id.to_s).order("id DESC").first
    @next = CbtUserTest.accessible_by(current_ability).where("id > " + @cbt_user_test.id.to_s).order("id ASC").first
    @cbt_user_tests = CbtUserTest.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_user_test
      render :action => 'show'
    end
  end

  def create
    @cbt_user_test = CbtUserTest.new(cbt_user_test_params)
    if @cbt_user_test.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_user_test = CbtUserTest.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_user_test }
    end
  end

  def modaledit
    @cbt_user_test = CbtUserTest.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_user_test }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtUserTest.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_user_test = CbtUserTest.accessible_by(current_ability).find(params[:id])
    @cbt_user_test.destroy
    respond_to do |format|
      format.html { redirect_to cbt_user_tests_url, notice: t("cbt_user_tests.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_user_test = CbtUserTest.accessible_by(current_ability).find(params[:id])

    if @cbt_user_test.update cbt_user_test_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_user_tests.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_user_test.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_user_tests." + er.attribute.to_s) + ': ' + t("cbt_user_tests.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_user_test) }
      end
    end

  end

  def cbt_user_test_params
    params.require(:cbt_user_test).permit(:cbt_test_id, :user_id, :start_time, :end_time, :primary_mark, :mark100, :description)
  end

  private

  def fill_directories
    @cbt_tests = CbtTest.all.order("id")
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_user_test.versions.scoped.last), :method => :post)
  end

end
