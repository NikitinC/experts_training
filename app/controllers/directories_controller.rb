class DirectoriesController < ApplicationController
  before_action :fill_class_name

  def index
    if request.xhr?
      render :text => @items.map { |k| {name: k.name, key: k.code, role: k.role, fiscode: k.fiscode} }.to_json
    else
      @items = @items.paginate(:page => params[:page], :per_page => 20)
    end
  end

  def new
    render :partial => "form"
  end

  def create
    if @item.save
      render :layout => false
    else
      render :partial => "form"
    end
  end

  def edit
    render :partial => "form"
  end

  def update
    if @item.update_attributes(params[@class_name])
      render :layout => false
    else
      render :partial => "form", :layout => false
    end
  end

  def destroy
    @item.destroy
    redirect_to :action => :index
  end

  private

  def fill_class_name
    @class_name = self.class.name.underscore.gsub("_controller", "").singularize
  end
end
