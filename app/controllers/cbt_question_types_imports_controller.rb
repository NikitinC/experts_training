class CbtQuestionTypesImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_question_types_import = CbtQuestionTypesImport.new
  end

  def create
    @cbt_question_types_import = CbtQuestionTypesImport.new(params[:cbt_question_types_import])
    render :create
  end

end
