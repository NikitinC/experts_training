class ExpertsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @experts_import = ExpertsImport.new
  end

  def create
    @experts_import = ExpertsImport.new(params[:experts_import])
    render :create
  end

end
