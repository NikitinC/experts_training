class CbtTrainingsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @cbt_trainings = CbtTraining.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_trainings = CbtTraining.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    else
      if !params[:search].nil?
        @cbt_trainings = CbtTraining.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    end
  end

  def addtests
    @cbt_training = CbtTraining.accessible_by(current_ability).find(params[:id])
    @cbt_tests = CbtTest.all - CbtTest.where(:id => CbtTrainingTest.all.map(&:cbt_test_id))
  end
  def export
    @cbt_trainings = CbtTraining.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_trainings_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @cbt_training = CbtTraining.new
    if !@cbt_training.nil?
       render :edit
    else
       flash[:success] = t("cbt_trainings.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @cbt_training = CbtTraining.accessible_by(current_ability).find(params[:id])
    @previous = CbtTraining.accessible_by(current_ability).where("id < " + @cbt_training.id.to_s).order("id DESC").first
    @next = CbtTraining.accessible_by(current_ability).where("id > " + @cbt_training.id.to_s).order("id ASC").first
    @cbt_trainings = CbtTraining.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_training
      render :action => 'show'
    end
  end

  def create
    @cbt_training = CbtTraining.new(cbt_training_params)
    if @cbt_training.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_training = CbtTraining.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_training }
    end
  end

  def modaledit
    @cbt_training = CbtTraining.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_training }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtTraining.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_training = CbtTraining.accessible_by(current_ability).find(params[:id])
    @cbt_training.destroy
    respond_to do |format|
      format.html { redirect_to cbt_trainings_url, notice: t("cbt_trainings.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_training = CbtTraining.accessible_by(current_ability).find(params[:id])

    if @cbt_training.update cbt_training_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_trainings.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_training.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_trainings." + er.attribute.to_s) + ': ' + t("cbt_trainings.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_training) }
      end
    end

  end

  def cbt_training_params
    params.require(:cbt_training).permit(:name, :description, :active, :shuffle, :time_limit,
                                     :explore_correct_answers, :explore_correct_answers_after_stopped,
                                     :title_image_cache, :remove_title_image, :title_image,
                                     :attempts_number, :started_at, :stopped_at, :manual_text,
                                     :for_students, :for_employees, :for_experts, :cbt_certificate_id,
                                     :subject_id, :cbt_subject_id, :for_observers,
                                     :certificate, :certificate_geometry, :show_status)
  end

  private

  def fill_directories
    @cbt_subjects = CbtSubject.all.order("id")
    @subjects = Subject.all.order("code")
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_training.versions.scoped.last), :method => :post)
  end

end
