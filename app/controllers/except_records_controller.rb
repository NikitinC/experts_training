class ExceptRecordsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @except_records = ExceptRecord.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @except_records = ExceptRecord.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @except_records = ExceptRecord.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @except_records = ExceptRecord.accessible_by(current_ability).order('code ASC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'except_records_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @except_record = ExceptRecord.new
    if !@except_record.nil?
       render :edit
    else
       flash[:success] = t("except_records.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @except_record = ExceptRecord.accessible_by(current_ability).find(params[:id])
    @previous = ExceptRecord.accessible_by(current_ability).where("id < " + @except_record.id.to_s).order("id DESC").first
    @next = ExceptRecord.accessible_by(current_ability).where("id > " + @except_record.id.to_s).order("id ASC").first
    @except_records = ExceptRecord.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @except_record
      render :action => 'show'
    end
  end

  def create
    @except_record = ExceptRecord.new(except_record_params)
    if @except_record.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @except_record = ExceptRecord.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @except_record }
    end
  end

  def modaledit
    @except_record = ExceptRecord.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @except_record }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    ExceptRecord.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @except_record = ExceptRecord.accessible_by(current_ability).find(params[:id])
    @except_record.destroy
    respond_to do |format|
      format.html { redirect_to except_records_url, notice: t("except_records.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @except_record = ExceptRecord.accessible_by(current_ability).find(params[:id])

    if @except_record.update except_record_params
      respond_to do |format|
        format.html {
          flash[:success] = t("except_records.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @except_record.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("except_records." + er.attribute.to_s) + ': ' + t("except_records.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@except_record) }
      end
    end

  end

  def except_record_params
    params.require(:except_record).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code, :fisgia11code, :sort_by)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@except_record.versions.scoped.last), :method => :post)
  end

end
