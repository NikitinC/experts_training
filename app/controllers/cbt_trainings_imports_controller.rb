class CbtTrainingsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_trainings_import = CbtTrainingsImport.new
  end

  def create
    @cbt_trainings_import = CbtTrainingsImport.new(params[:cbt_trainings_import])
    render :create
  end

end
