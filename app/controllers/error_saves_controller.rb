class ErrorSavesController < ApplicationController

  def error_save_params
    params.require(:error_save).permit(:user_id, :referer, :url, :exception)
  end

end
