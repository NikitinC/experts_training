class CbtQuestionTypesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @cbt_question_types = CbtQuestionType.accessible_by(current_ability).paginate(page: params[:page]).order('created_at DESC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_question_types = CbtQuestionType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('created_at DESC')
      end
    else
      if !params[:search].nil?
        @cbt_question_types = CbtQuestionType.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('created_at DESC')
      end
    end
  end

  def export
    @cbt_question_types = CbtQuestionType.accessible_by(current_ability).order('created_at DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_question_types_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @cbt_question_type = CbtQuestionType.new
    if !@cbt_question_type.nil?
      render :edit
    else
      flash[:success] = t("cbt_question_types.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @cbt_question_type = CbtQuestionType.accessible_by(current_ability).find(params[:id])
    @previous = CbtQuestionType.accessible_by(current_ability).where("id < " + @cbt_question_type.id.to_s).order("id DESC").first
    @next = CbtQuestionType.accessible_by(current_ability).where("id > " + @cbt_question_type.id.to_s).order("id ASC").first
    @cbt_question_types = CbtQuestionType.accessible_by(current_ability).all.order('created_at DESC')
    if can? :view, @cbt_question_type
      render :action => 'show'
    end
  end

  def create
    @cbt_question_type = CbtQuestionType.new(cbt_question_type_params)
    if @cbt_question_type.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_question_type = CbtQuestionType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_question_type }
    end
  end

  def modaledit
    @cbt_question_type = CbtQuestionType.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_question_type }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtQuestionType.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_question_type = CbtQuestionType.accessible_by(current_ability).find(params[:id])
    @cbt_question_type.destroy
    respond_to do |format|
      format.html { redirect_to cbt_question_types_url, notice: t("cbt_question_types.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_question_type = CbtQuestionType.accessible_by(current_ability).find(params[:id])

    if @cbt_question_type.update cbt_question_type_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_question_types.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_question_type.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_question_types." + er.attribute.to_s) + ': ' + t("cbt_question_types.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_question_type) }
      end
    end

  end

  def cbt_question_type_params
    params.require(:cbt_question_type).permit(:name, :deleted_at)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_question_type.versions.scoped.last), :method => :post)
  end

end
