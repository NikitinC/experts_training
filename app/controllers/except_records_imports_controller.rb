class ExceptRecordsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @except_records_import = ExceptRecordsImport.new
  end

  def create
    @except_records_import = ExceptRecordsImport.new(params[:except_records_import])
    render :create
  end

end
