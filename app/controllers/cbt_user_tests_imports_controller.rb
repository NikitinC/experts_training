class CbtUserTestsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_user_tests_import = CbtUserTestsImport.new
  end

  def create
    @cbt_user_tests_import = CbtUserTestsImport.new(params[:cbt_user_tests_import])
    render :create
  end

end
