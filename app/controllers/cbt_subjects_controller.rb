class CbtSubjectsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @cbt_subjects = CbtSubject.accessible_by(current_ability).paginate(page: params[:page]).order('code ASC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_subjects = CbtSubject.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    else
      if !params[:search].nil?
        @cbt_subjects = CbtSubject.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('code ASC')
      end
    end
  end

  def export
    @cbt_subjects = CbtSubject.accessible_by(current_ability).order('code ASC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_subjects_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
    
  end

  def new
    @cbt_subject = CbtSubject.new
    if !@cbt_subject.nil?
       render :edit
    else
       flash[:success] = t("cbt_subjects.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @cbt_subject = CbtSubject.accessible_by(current_ability).find(params[:id])
    @previous = CbtSubject.accessible_by(current_ability).where("id < " + @cbt_subject.id.to_s).order("id DESC").first
    @next = CbtSubject.accessible_by(current_ability).where("id > " + @cbt_subject.id.to_s).order("id ASC").first
    @cbt_subjects = CbtSubject.accessible_by(current_ability).all.order('code ASC')
    if can? :view, @cbt_subject
      render :action => 'show'
    end
  end

  def create
    @cbt_subject = CbtSubject.new(cbt_subject_params)
    if @cbt_subject.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_subject = CbtSubject.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_subject }
    end
  end

  def modaledit
    @cbt_subject = CbtSubject.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_subject }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtSubject.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_subject = CbtSubject.accessible_by(current_ability).find(params[:id])
    @cbt_subject.destroy
    respond_to do |format|
      format.html { redirect_to cbt_subjects_url, notice: t("cbt_subjects.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_subject = CbtSubject.accessible_by(current_ability).find(params[:id])

    if @cbt_subject.update cbt_subject_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_subjects.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_subject.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_subjects." + er.attribute.to_s) + ': ' + t("cbt_subjects.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_subject) }
      end
    end

  end

  def cbt_subject_params
    params.require(:cbt_subject).permit(:code, :name, :created_at, :updated_at, :short_name, :fisgia9code,
                                        :fisgia11code, :sort_by,
                                        :border_osn, :border_osn_error,
                                        :border_st, :border_st_error,
                                        :border_ved, :border_ved_error)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_subject.versions.scoped.last), :method => :post)
  end

end
