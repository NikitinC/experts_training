class CbtUserTestQuestionsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_user_test_questions_import = CbtUserTestQuestionsImport.new
  end

  def create
    @cbt_user_test_questions_import = CbtUserTestQuestionsImport.new(params[:cbt_user_test_questions_import])
    render :create
  end

end
