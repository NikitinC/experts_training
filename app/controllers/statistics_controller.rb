class StatisticsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    begin
      params_page = "0#{params[:page]}".to_i + 1
    rescue
      params_page = 2
    end
    params_page = params_page-1
    if params_page < 1
      params_page = 1
    end

    @statistics = Statistic.accessible_by(current_ability).paginate(page: params_page).order('id ASC')

    if current_user.admin?
      if !params[:search].nil?
        @statistics = Statistic.accessible_by(current_ability).search(params[:search]).paginate(page: params_page).order('id ASC')
      end
    else
      if !params[:search].nil?
        @statistics = Statistic.accessible_by(current_ability).search(params[:search]).paginate(page: params_page).order('id ASC')
      end
    end
  end

  def export
    @statistic = Statistic.accessible_by(current_ability).find(params[:id])

    if current_user.admin?
      sql = @statistic.sql_string_region
    elsif current_user.mouo_role?
      sql = @statistic.sql_string_mouo
    elsif current_user.ous_role?
      sql = @statistic.sql_sting_ou
    elsif current_user.mouo_role? and user.target_id == 41
      sql = @statistic.sql_string_ekb
    end

    sql = sql.gsub('ТАРГЕТ', "#{current_user.target_id}")

    @rows = ActiveRecord::Base.connection.execute(sql)
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{"st_#{@statistic.short_name}_" + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @statistic = Statistic.new
    if !@statistic.nil?
       render :edit
    else
       flash[:success] = t("statistics.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @statistic = Statistic.accessible_by(current_ability).find(params[:id])

    if current_user.admin?
      sql = @statistic.sql_string_region
    elsif current_user.mouo_role?
      sql = @statistic.sql_string_mouo
    elsif current_user.ous_role?
      sql = @statistic.sql_sting_ou
    elsif current_user.mouo_role? and user.target_id == 41
      sql = @statistic.sql_string_ekb
    end

    sql = sql.gsub('ТАРГЕТ', "#{current_user.target_id}")
    @rows = ActiveRecord::Base.connection.execute(sql)

    if can? :view, @statistic
      render :action => 'show'
    end
  end

  def create
    @statistic = Statistic.new(statistic_params)
    if @statistic.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @statistic = Statistic.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @statistic }
    end
  end

  def modaledit
    @statistic = Statistic.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @statistic }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    Statistic.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @statistic = Statistic.accessible_by(current_ability).find(params[:id])
    @statistic.destroy
    respond_to do |format|
      format.html { redirect_to "/statistics?page=#{params[:page]}", notice: t("statistics.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
    # redirect_to "/statistics?page=#{params[:statistic][:page]}"
  end

  def update
    @statistic = Statistic.accessible_by(current_ability).find(params[:id])

    if @statistic.update statistic_params
      respond_to do |format|
        format.html {
          flash[:success] = t("statistics.notice.updated")
          if "#{params[:statistic][:page]}" != ''
            redirect_to "/statistics?page=#{params[:statistic][:page]}"
          else
            redirect_to :action => 'index'
          end
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @statistic.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("statistics." + er.attribute.to_s) + ': ' + t("statistics.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@statistic) }
      end
    end

  end

  def statistic_params
    params.require(:statistic).permit(:name, :short_name, :sql_string_region, :sql_string_mouo, :sql_sting_ou, :sql_string_ekb, :description, :is_active, :object_columns, :object_columns_width)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@statistic.versions.scoped.last), :method => :post)
  end

end
