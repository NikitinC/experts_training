class CbtTestQuestionSetsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @cbt_test_question_sets = CbtTestQuestionSet.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_test_question_sets = CbtTestQuestionSet.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    else
      if !params[:search].nil?
        @cbt_test_question_sets = CbtTestQuestionSet.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    end
  end

  def export
    @cbt_test_question_sets = CbtTestQuestionSet.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_test_question_sets_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @cbt_question_sets = CbtQuestionsSet.all
    @cbt_questions = CbtQuestion.all

    @cbt_test_question_set = CbtTestQuestionSet.new
    if !@cbt_test_question_set.nil?
      render :edit
    else
      flash[:success] = t("cbt_test_question_sets.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @cbt_question_sets = CbtQuestionSet.all
    @cbt_questions = CbtQuestion.all

    @cbt_test_question_set = CbtTestQuestionSet.accessible_by(current_ability).find(params[:id])
    @previous = CbtTestQuestionSet.accessible_by(current_ability).where("id < " + @cbt_test_question_set.id.to_s).order("id DESC").first
    @next = CbtTestQuestionSet.accessible_by(current_ability).where("id > " + @cbt_test_question_set.id.to_s).order("id ASC").first
    @cbt_test_question_sets = CbtTestQuestionSet.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_test_question_set
      render :action => 'show'
    end
  end

  def create
    @cbt_test_question_set = CbtTestQuestionSet.new(cbt_test_question_set_params)
    if @cbt_test_question_set.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_question_sets = CbtQuestionsSet.all.order("id")
    @cbt_questions = CbtQuestion.all
    @cbt_test_question_set = CbtTestQuestionSet.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_test_question_set }
    end
  end

  def modaledit
    @cbt_question_sets = CbtQuestionSet.all
    @cbt_questions = CbtQuestion.all

    @cbt_test_question_set = CbtTestQuestionSet.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_test_question_set }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtTestQuestionSet.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_test_question_set = CbtTestQuestionSet.accessible_by(current_ability).find(params[:id])
    @cbt_test_question_set.destroy
    respond_to do |format|
      format.html { redirect_to cbt_test_question_sets_url, notice: t("cbt_test_question_sets.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_test_question_set = CbtTestQuestionSet.accessible_by(current_ability).find(params[:id])

    if @cbt_test_question_set.update cbt_test_question_set_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_test_question_sets.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_test_question_set.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_test_question_sets." + er.attribute.to_s) + ': ' + t("cbt_test_question_sets.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_test_question_set) }
      end
    end

  end

  def cbt_test_question_set_params
    params.require(:cbt_test_question_set).permit(:cbt_questions_set_id, :cbt_test_id, :sort_by, :deleted_at)
  end

  private

  def fill_directories
    # @cbt_question_set = CbtQuestionsSet.all.order("id")
    @cbt_question_sets = CbtQuestionsSet.all.order("id")
    @cbt_tests = CbtTest.all.order("id")
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_test_question_set.versions.scoped.last), :method => :post)
  end

end
