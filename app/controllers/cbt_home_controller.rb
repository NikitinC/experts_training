class CbtHomeController < ApplicationController

  def index
    @test = CbtTest.accessible_by(current_ability).all.order("created_at DESC")
    @trainings = CbtTraining.accessible_by(current_ability).all.order("created_at DESC")
  end

  def test
    @test = CbtTest.find(params[:id])
  end

  def training
    @training = CbtTraining.find(params[:id])
    @training_tests = CbtTrainingTest.where(:cbt_training_id => @training.id).order("sort_by")
  end

  def get_user_test
    @test = CbtTest.find(params[:id])
    user_test_now = CbtUserTest.where(:cbt_test_id => @test.id, :user_id => current_user.id).where("end_time is NULL").first

    if user_test_now.nil?

      # Здесь надо проверить, имею ли я право создавать? Или уже время кончилось
      user_test_now = CbtUserTest.new
      user_test_now.cbt_test_id = @test.id
      user_test_now.user_id = current_user.id
      user_test_now.start_time = Time.current
      user_test_now.save

    end

    if CbtUserTestQuestion.where(:cbt_user_test_id => user_test_now.id).count < 1
      # А отсюда, раз можно создавать, я начинаю создавать все необходимые questions
      cbt_test_question_sets = CbtTestQuestionSet.where(:cbt_test_id => @test.id).order("sort_by")
      i = 1
      cbt_test_question_sets.each do |ctqs|
        qs = CbtQuestionsSet.find(ctqs.cbt_questions_set_id)
        qsq = CbtQuestionSetQuestion.where(:cbt_questions_set_id => qs.id)
        if qsq.count > 0
          # puts("qsq: #{qsq.inspect}")
          q = CbtQuestion.find_by(id: qsq[rand(qsq.count)-1].cbt_question_id)
          # puts("q: #{q.inspect}")
          qq = CbtUserTestQuestion.new
          # puts("qq: #{qq.inspect}")
          qq.cbt_user_test_id = user_test_now.id
          qq.cbt_question_id = q.id
          if !ctqs.sort_by.nil?
            qq.sort_by = ctqs.sort_by
          else
            qq.sort_by = i
          end
          qq.save
          i = i + 1
        end
      end

    end

    @user_test = user_test_now
    @user_test_questions = CbtUserTestQuestion.where(:cbt_user_test_id => user_test_now.id).order("sort_by ASC")

  end

  def get_user_test_question
    @cbt_user_test_question = CbtUserTestQuestion.find(params[:id])
    if @cbt_user_test_question.start_time.nil?
      @cbt_user_test_question.start_time = Time.current
      @cbt_user_test_question.save
    end
    @test = CbtTest.find(@cbt_user_test_question.cbt_user_test.cbt_test.id)
    @cbt_user_test_questions = CbtUserTestQuestion.where(:cbt_user_test_id => @cbt_user_test_question.cbt_user_test_id).order("sort_by ASC")
    @cbt_question = CbtQuestion.where(:id=>@cbt_user_test_question.cbt_question_id)
  end

  def get_user_training_results

    if !current_user.nil?
      @cbt_training = CbtTraining.find(params[:id])

      filename = @cbt_training.certificate

      if !current_user.nil? and !@cbt_training.nil?
        destination = "#{Rails.public_path}/uploads/cbt_certificates/cbt_cert_#{@cbt_training.id}_#{current_user.id}.pdf"
      else
        flash[:error] = "Вы не авторизованы для просмотра данной страницы"
        redirect_to "/users/sign_in"
      end

      begin
        if File.exist?("#{Rails.public_path}#{destination}")
          r = IO.popen("rm #{destination}", "r+")
        end
      rescue
        puts('nothing')
      end

      geom = @cbt_training.certificate_geometry

      # 'КОМУ',320,350,'center',18,'BeerMoney','#BCA669'+'СТАТУС',360,215,'center',18,'Arial','#BCA669'+'ПРЕДМЕТ',350,290,'center',18,'Arial','#000000'+'БАЛЛ',350,255,'center',18,'Arial','#000000'

      expert = Expert.where(:online_login => current_user.first_name).first

      if !expert.nil?
        fio_komu = Petrovich(lastname: "#{expert.surname}",
                             firstname: "#{expert.name}",
                             middlename: "#{expert.second_name}",).dative.to_s
        fio = "#{expert.surname} #{expert.name} #{expert.second_name}"
      else
        fio_komu = current_user.first_name
        fio = current_user.first_name
      end

      geom = geom.gsub('КОМУ', fio_komu)
      geom = geom.gsub('КТО', fio)

      subject = @cbt_training.subject

      geom = geom.gsub('ПРЕДМЕТ', subject.name)

      cbt_training_tests = CbtTrainingTest.where(:cbt_training_id => @cbt_training.id, :mandatory => true)

      cbt_tests = CbtTest.where(:id => cbt_training_tests.map(&:cbt_test_id))

      mm100 = []
      max_mark100 = nil

      i = -1
      cbt_tests.each do |ct|
        puts("here is cbt_tests #{ct.inspect}")
        i = i + 1
        puts("the I is #{i}")
        cbt_user_tests = CbtUserTest.where(:cbt_test_id => ct.id, :user_id => current_user.id).where("end_time is NOT NULL").where("mark100 is NOT NULL")
        mm100[i] = cbt_user_tests.map(&:mark100).max
        puts("here is mm100 #{mm100.inspect}")
      end


      begin
        max_mark100 = mm100.sum(0.0) / mm100.size
      rescue
        max_mark100 = 0
      end



      begin
        max_description = cbt_user_tests.where(:mark100 => max_mark100).order("description ASC").first.description
        max_description = max_description.gsub(' расхождений на 2 и более баллов по критериям', '')
        max_description = "#{max_description.to_i}"
      rescue
        max_description = '0'
      end

      if max_mark100.nil?
        max_mark100 = 0
      end

      if max_description.nil?
        max_description = '100'
      end

      if max_mark100 > 100
        max_mark100 = 100
      end

      max_description = max_description.to_i

      geom = geom.gsub('БАЛЛ', max_mark100.round(1).to_s)

      status = 'Порог не преодолён'

      begin
        if max_mark100 >= @cbt_training.cbt_subject.border_osn and max_description <= @cbt_training.cbt_subject.border_osn_error
          status = 'Основной эксперт'
        elsif max_mark100 >= @cbt_training.cbt_subject.border_st and max_description <= @cbt_training.cbt_subject.border_st_error
          status = 'Старший эксперт'
        elsif max_mark100 >= @cbt_training.cbt_subject.border_ved and max_description <= @cbt_training.cbt_subject.border_ved_error
          status = 'Ведущий эксперт'
        end
      rescue
        status = 'Порог не преодолён'
      end

      if @cbt_training.show_status == true
        geom = geom.gsub('СТАТУС', status)
      else
        geom = geom.gsub('СТАТУС', '')
      end

      # 89
      # puts("#{@cbt_training.certificate}")
      if File.exist?("public/#{@cbt_training.certificate}")
        r = IO.popen("python3 pdftextadd.py public/#{@cbt_training.certificate} #{destination} #{geom}", "r+")
        Process.wait(r.pid)
      else
        r = IO.popen("python3 pdftextadd.py certificate.pdf #{destination} #{geom}", "r+")
        Process.wait(r.pid)
      end
      if File.exist?(destination)
        send_file(destination, :type => "application/pdf", :disposition => 'attachment', :filename => "Cертификат № #{@cbt_training.id}-#{current_user.id}.pdf")
      else
        r = IO.popen("python3 pdftextadd.py certificate.pdf #{destination} #{geom}", "r+")
        Process.wait(r.pid)
        send_file(destination, :type => "application/pdf", :disposition => 'attachment', :filename => "Cертификат № #{@cbt_training.id}-#{current_user.id}.pdf")
      end

      # print("python3 pdftextadd.py public/#{@cbt_training.certificate} #{destination} #{geom}")

      # python3 pdftextadd.py cert.pdf cbt_cert_75_348594.pdf 'sw118034',320,350,'center',18,'BeerMoney','#BCA669'+'',360,215,'center',18,'Arial','#BCA669'+'Испанский язык (ГВЭ)',350,290,'center',18,'Arial','#000000'+'100.0',350,255,'center',18,'Arial','#000000'

      # puts("python3 pdftextadd.py public/#{@cbt_training.certificate} #{destination} #{geom}")


    else
      if user_signed_in?
        flash[:error] = "Вы не авторизованы для просмотра данной страницы"
        session[:user_return_to] = nil
        redirect_to root_url
      else
        flash[:error] = "Для перехода к данной странице сначала надо войти в систему."
        session[:user_return_to] = request.url
        redirect_to "/users/sign_in"
      end
    end
  end

  def get_user_test_results
    @user_test = CbtUserTest.find(params[:id])
    @test = CbtTest.find(@user_test.cbt_test_id)
    @user_test_questions = CbtUserTestQuestion.where(:cbt_user_test_id => @user_test.id).order("sort_by ASC")
    if @user_test.end_time.nil?
      @user_test.end_time = Time.current
      @user_test.save
    end
    na2ibolee = 0
    @user_test_questions.each do |utq|
      if utq.end_time.nil?
        utq.end_time = @user_test.end_time
      end
      q = utq.cbt_question
      if q.cbt_answer_type.name == 'Отметки по критериям'
        if utq.user_answer.nil?
          utq.primary_mark = 0
          utq.save
        end
        if utq.primary_mark.nil?
          if "#{utq.user_answer}" == "#{q.correct_answer}"
            utq.primary_mark = q.primary_mark
            utq.save
          end
          if utq.primary_mark.nil? and (q.partial_mark == true) and !utq.user_answer.nil?
            ua = utq.user_answer.split('#')
            ca = q.correct_answer.split('#')
            cc = ca.count
            i = 0
            m = 0
            s = 0
            ua.each do |a|
              # puts "stroka 93"
              # puts("a = #{a}, ca[i] = #{ca[i]}")
              if a == ca[i]
                m = m + (q.primary_mark/cc).round(2)
              end
              if !a.nil? and ca[i].nil?
                if (a.gsub('X', '0').to_f - ca[i].gsub('X', '0').to_f).abs >= 2
                  s = s + 1
                end
              end
              i = i + 1
            end
            utq.primary_mark = m
            # puts ("stroka 104 now s is #{s.to_s}")
            na2ibolee+=s
            utq.comment = s.to_s
            utq.save
            # puts ("#{utq.inspect}")
          end
        end
        if utq.comment.nil?
          ca = q.correct_answer.split('#')

          if !utq.user_answer.nil?
            ua = utq.user_answer.split('#')
          else
            ua = 'X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X#X'[0..((ca.count-1)*2)].split('#')
          end
          # puts "stroka 115"
          # puts ("ua = #{ua.inspect}, ca = #{ca.inspect}")
          s = 0
          i = 0
          ua.each do |a|
            # puts ("a = #{a}, ca[i] = #{ca[i]}")
            # puts ("a.to_f = #{a.gsub('X', '0').to_f}, ca[i].to_f = #{ca[i].gsub('X', '0').to_f}")
            if (a.gsub('X', '0').to_f - ca[i].gsub('X', '0').to_f).abs >= 2
              s = s + 1
            end
            i = i + 1
          end
          # puts ("stroka 129 now s is #{s.to_s}")
          utq.comment = s.to_s
          utq.save
        end
      elsif q.cbt_answer_type.name == 'Выбор нескольких вариантов из многих'
        utq.primary_mark = 0
        correct_answers = 0
        not_correct_answers = 0
        "#{utq.user_answer}".split('#').each do |u|
          if "#{q.correct_answer}".split('#').include? u
            correct_answers+=1
          else
            not_correct_answers+=1
          end
        end
        if !q.primary_mark.nil?
          ves = q.primary_mark/(correct_answers+not_correct_answers)
        else
          q.primary_mark = 0
          ves = 0
        end
        correct_answers_num = "#{q.correct_answer}".split('#').count
        # puts("correct_answers: #{correct_answers}; not_correct_answers: #{not_correct_answers}, correct_answers_num: #{correct_answers_num}")
        if q.partial_mark == true
          utq.primary_mark = correct_answers*ves.round(2)
        else
          if correct_answers_num == correct_answers
            utq.primary_mark = q.primary_mark
          end
        end
        utq.save
      elsif q.cbt_answer_type.name == 'Выбор одного варианта из многих'
        utq.primary_mark = 0
        if q.correct_answer == utq.user_answer
          utq.primary_mark = q.primary_mark
        end
        utq.save
      elsif q.cbt_answer_type.name == 'Свободный краткий ответ'
        utq.primary_mark = 0
        if "#{q.correct_answer}".split('#').include? utq.user_answer
          utq.primary_mark = q.primary_mark
        end
        utq.save
      end

      @user_test.primary_mark = @user_test_questions.sum(:primary_mark)
      @user_test.mark100 = @user_test.primary_mark*100/(CbtQuestion.where(:id=>@user_test_questions.map(&:cbt_question_id)).sum(:primary_mark))
      @user_test.description = "#{na2ibolee} расхождений на 2 и более баллов по критериям"

      @user_test.save
    end
  end

end
