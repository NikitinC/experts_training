class ExpertsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :set_expert, only: %i[ show edit update destroy ]
  before_action :fill_directories, only: %i[ new show edit update destroy create ]


  def index
    @experts = Expert.accessible_by(current_ability).paginate(page: params[:page]).order('surname ASC, name ASC, second_name ASC, id ASC')

    @filterrific = initialize_filterrific(
      @experts,
      params[:filterrific],
      select_options: {
      },
      persistence_id: "shared_key",
      default_filter_params: {},
      available_filters: [:search_query, :with_subject_id],
      sanitize_params: true,
      ) || return

    @experts = @filterrific.find.page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end

  end

  def recreate
    # nothing
  end

  # def export
  #   @experts = Expert.accessible_by(current_ability).order('surname ASC, name ASC, second_name ASC, id ASC')
  #   render xlsx: 'experts_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/experts/export.xlsx.axlsx'
  # end

  def export
    @experts = Expert.accessible_by(current_ability).order('surname ASC, name ASC, second_name ASC, code ASC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'expert_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @expert = Expert.new
    if !@expert.nil?
      render :edit
    else
      flash[:success] = t("experts.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @previous = Expert.accessible_by(current_ability).where("id < " + @expert.id.to_s).order("id DESC").first
    @next = Expert.accessible_by(current_ability).where("id > " + @expert.id.to_s).order("id ASC").first
    @experts = Expert.accessible_by(current_ability).all.order('code ASC')

    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @expert = Expert.new(expert_params)
    @expert.guid = "{#{SecureRandom.uuid}}"
    if @expert.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    # @school_workers = SchoolWorker.accessible_by(current_ability).order("second_name, first_name, middle_name, document_number")
    respond_to do |format|
      format.html
      format.json { respond_modal_with @expert }
    end
  end

  def modaledit
    @expert = Expert.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @expert }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    Expert.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    # @expert = Expert.accessible_by(current_ability).find(params[:id])
    @expert.destroy
    respond_to do |format|
      format.html { redirect_to experts_url, notice: t("experts.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update

    if @expert.update expert_params
      respond_to do |format|
        format.html {
          flash[:success] = t("experts.notice.updated")
          redirect_to :action => 'show'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @expert.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("experts." + er.attribute.to_s) + ': ' + t("experts.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@expert) }
      end
    end

  end

  def expert_params
    params.require(:expert).permit(:surname, :name, :second_name, :document_series, :document_number, :birthday,
                                   :gender, :code, :education_code, :scientific_degree_code, :qualification,
                                   :ou_id, :job_place, :job_position, :stage, :took_part_in_OGE_previous_year,
                                   :took_part_in_OGE_appeal_comission, :subject1_subject_code, :subject1_category_code,
                                   :subject1_third, :subject1_voice, :subject2_subject_code, :subject2_category_code,
                                   :subject2_third, :subject2_voice, :subject3_subject_code, :subject3_category_code,
                                   :subject3_third, :subject3_voice, :user, :email, :guid, :diploma_surname,
                                   :diploma_series, :diploma_number, :diploma_institute, :diploma_scan,
                                   :diploma_fio_change_scan, :diploma_foreign_translate_scan, :bank_requizites_bank,
                                   :bank_requizites_address, :bank_requizites_inn, :bank_requizites_kpp,
                                   :bank_requizites_kor, :bank_requizites_bik, :bank_requizites_beneficiary_account,
                                   :bank_requizites_beneficiar, :bank_requizites_scan, :inn, :inn_scan, :snils_scan,
                                   :passport_registration, :passport_fio_scan, :passport_registration_scan,
                                   :compensation_declaration_scan, :compensation_number, :agreement_pdn_education_scan,
                                   :compensation_agreement_pdn_scan, :phone, :snils, :passport_country,
                                   :passport_postcode, :passport_region, :passport_rajon, :passport_town_type,
                                   :passport_town, :passport_street_type, :passport_street, :passport_building_type,
                                   :passport_building, :passport_building_part, :passport_flat, :passport_get_date,
                                   :passport_get_place, :passport_department_code, :account_number_ubrir,
                                   :account_number_sberbank, :statement_start_date, :statement_end_date,
                                   :document_type_code, :hidden, :subject1_qualify_category, :subject1_comission_stage,
                                   :subject1_comission_job, :subject1_level_up_year, :subject2_qualify_category,
                                   :subject2_comission_stage, :subject2_comission_job, :subject2_level_up_year,
                                   :subject3_qualify_category, :subject3_comission_stage, :subject3_comission_job,
                                   :subject3_level_up_year, :user_id, :deleted_at,
                                   :online_login, :online_password, :is_ege, :subject1_ege_qualify_category,
                                   :subject1_ege_comission_stage, :subject1_ege_comission_job,
                                   :subject1_ege_level_up_year, :subject1_ege_subject_code,
                                   :subject1_ege_category_code, :subject1_ege_third, :subject1_ege_voice,
                                   :subject2_ege_qualify_category, :subject2_ege_comission_stage,
                                   :subject2_ege_comission_job, :subject2_ege_level_up_year, :subject2_ege_subject_code,
                                   :subject2_ege_category_code, :subject2_ege_third, :subject2_ege_voice,
                                   :school_worker_id)
  end

  private

  def fill_directories
    @subjects = Subject.order("sort_by")
  end

  def set_expert
    @expert = Expert.accessible_by(current_ability).find(params[:id])
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@expert.versions.scoped.last), :method => :post)
  end

end

