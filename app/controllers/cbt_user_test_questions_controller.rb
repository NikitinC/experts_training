class CbtUserTestQuestionsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @cbt_user_test_questions = CbtUserTestQuestion.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_user_test_questions = CbtUserTestQuestion.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    else
      if !params[:search].nil?
        @cbt_user_test_questions = CbtUserTestQuestion.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    end
  end

  def export
    @cbt_user_test_questions = CbtUserTestQuestion.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_user_test_questions_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @cbt_user_test_question = CbtUserTestQuestion.new
    if !@cbt_user_test_question.nil?
       render :edit
    else
       flash[:success] = t("cbt_user_test_questions.errors.cant_create_new")
       redirect_to :action => 'index'
    end
  end

  def show
    @cbt_user_test_question = CbtUserTestQuestion.accessible_by(current_ability).find(params[:id])
    @previous = CbtUserTestQuestion.accessible_by(current_ability).where("id < " + @cbt_user_test_question.id.to_s).order("id DESC").first
    @next = CbtUserTestQuestion.accessible_by(current_ability).where("id > " + @cbt_user_test_question.id.to_s).order("id ASC").first
    @cbt_user_test_questions = CbtUserTestQuestion.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_user_test_question
      render :action => 'show'
    end
  end

  def create
    @cbt_user_test_question = CbtUserTestQuestion.new(cbt_user_test_question_params)
    if @cbt_user_test_question.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_user_test_question = CbtUserTestQuestion.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_user_test_question }
    end
  end

  def modaledit
    @cbt_user_test_question = CbtUserTestQuestion.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_user_test_question }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtUserTestQuestion.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_user_test_question = CbtUserTestQuestion.accessible_by(current_ability).find(params[:id])
    @cbt_user_test_question.destroy
    respond_to do |format|
      format.html { redirect_to cbt_user_test_questions_url, notice: t("cbt_user_test_questions.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_user_test_question = CbtUserTestQuestion.accessible_by(current_ability).find(params[:id])

    if !params[:mark_k].nil?
      i = 0
      @cbt_user_test_question.user_answer = ''

      params[:mark_k].each do |m|
        i = i + 1
        if @cbt_user_test_question.cbt_question.cbt_answer_type.name == 'Отметки по критериям'
          if i > 1
            @cbt_user_test_question.user_answer += '#'
          end
          ball = m[1]
          # puts(m[1])
          if !ball.nil?
            ball = ball.gsub(',', '.').gsub(' ', '')
            if !(begin !!Float(ball) rescue false end)
              ball = 'X'
            end
          end
          ball = 'X' if ball.nil?
          ball = 'X' if ball == ''
          # puts (ball)
          # puts (@mark.balls_list)
          @cbt_user_test_question.user_answer+= ball.to_s
        elsif @cbt_user_test_question.cbt_question.cbt_answer_type.name == 'Выбор одного варианта из многих'
          @cbt_user_test_question.user_answer = m[1]
        elsif @cbt_user_test_question.cbt_question.cbt_answer_type.name == 'Выбор нескольких вариантов из многих'
          if m[1] == '1'
            if @cbt_user_test_question.user_answer != ''
              @cbt_user_test_question.user_answer += '#'
            end
            @cbt_user_test_question.user_answer += m[0]
          end
        elsif @cbt_user_test_question.cbt_question.cbt_answer_type.name == 'Свободный краткий ответ'
          # puts("here is m: #{m.inspect}")
          # puts("here is m[0]: #{m[0]}")
          # puts("here is m[1]: #{m[1]}")
          @cbt_user_test_question.user_answer = m[1]
        else
          @cbt_user_test_question.user_answer = 'Не понятен тип ответа'
        end

      end
    else
      @cbt_user_test_question.user_answer = nil
    end

    @cbt_user_test_question.end_time = Time.now.zone

    if @cbt_user_test_question.update cbt_user_test_question_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_user_test_questions.notice.updated")
          redirect_to "/cbt_home/get_user_test_question/#{@cbt_user_test_question.id}"
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_user_test_question.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_user_test_questions." + er.attribute.to_s) + ': ' + t("cbt_user_test_questions.errors." + er.type.to_s)) + "; "
          end
          redirect_to "/cbt_home/get_user_test_question/#{@cbt_user_test_question.id}"
        }
        format.json { respond_with_bip(@cbt_user_test_question) }
      end
    end

  end

  def cbt_user_test_question_params
    params.require(:cbt_user_test_question).permit(:cbt_user_test_id, :cbt_question_id, :start_time, :end_time, :user_answer, :primary_mark, :marked, :sort_by)
  end

  private
  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_user_test_question.versions.scoped.last), :method => :post)
  end

end
