class CbtQuestionsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_questions_import = CbtQuestionsImport.new
  end

  def create
    @cbt_questions_import = CbtQuestionsImport.new(params[:cbt_questions_import])
    render :create
  end

end
