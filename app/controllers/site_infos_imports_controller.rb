class SiteInfosImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @site_infos_import = SiteInfosImport.new
  end

  def create
    @site_infos_import = SiteInfosImport.new(params[:site_infos_import])
    render :create
  end

end
