class CbtQuestionSetQuestionsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_question_set_questions_import = CbtQuestionSetQuestionsImport.new
  end

  def create
    @cbt_question_set_questions_import = CbtQuestionSetQuestionsImport.new(params[:cbt_question_set_questions_import])
    render :create
  end

end
