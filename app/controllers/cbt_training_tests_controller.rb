class CbtTrainingTestsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @cbt_training_tests = CbtTrainingTest.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')
    if current_user.admin?
      if !params[:search].nil?
        @cbt_training_tests = CbtTrainingTest.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    else
      if !params[:search].nil?
        @cbt_training_tests = CbtTrainingTest.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id DESC')
      end
    end
  end

  def export
    @cbt_training_tests = CbtTrainingTest.accessible_by(current_ability).order('id DESC')
    respond_to do |format|
      format.xlsx{
        response.headers['Content-Disposition'] = "attachment; filename=#{'cbt_training_tests_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.') + '.xlsx'}"
      }
    end
  end

  def new
    @cbt_training_test = CbtTrainingTest.new
    if !@cbt_training_test.nil?
      render :edit
    else
      flash[:success] = t("cbt_training_tests.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @cbt_training_test = CbtTrainingTest.accessible_by(current_ability).find(params[:id])
    @previous = CbtTrainingTest.accessible_by(current_ability).where("id < " + @cbt_training_test.id.to_s).order("id DESC").first
    @next = CbtTrainingTest.accessible_by(current_ability).where("id > " + @cbt_training_test.id.to_s).order("id ASC").first
    @cbt_training_tests = CbtTrainingTest.accessible_by(current_ability).all.order('id DESC')
    if can? :view, @cbt_training_test
      render :action => 'show'
    end
  end

  def create
    @cbt_training_test = CbtTrainingTest.new(cbt_training_test_params)
    if @cbt_training_test.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @cbt_question_sets = CbtQuestionsSet.all.order("id")
    @cbt_questions = CbtQuestion.all
    @cbt_training_test = CbtTrainingTest.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html
      format.json { respond_modal_with @cbt_training_test }
    end
  end

  def modaledit

    @cbt_training_test = CbtTrainingTest.accessible_by(current_ability).find(params[:id])
    respond_to do |format|
      format.html { render :partial => 'form' }
      format.json { respond_modal_with @cbt_training_test }
    end
  end

  def delete
    # flash[:success] = t("ates.notice.updated")
    CbtTrainingTest.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @cbt_training_test = CbtTrainingTest.accessible_by(current_ability).find(params[:id])
    @cbt_training_test.destroy
    respond_to do |format|
      format.html { redirect_to cbt_training_tests_url, notice: t("cbt_training_tests.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @cbt_training_test = CbtTrainingTest.accessible_by(current_ability).find(params[:id])

    if @cbt_training_test.update cbt_training_test_params
      respond_to do |format|
        format.html {
          flash[:success] = t("cbt_training_tests.notice.updated")
          redirect_to :action => 'index'
        }
        format.json { render :json => { :status => 200 }}
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = ""
          @cbt_training_test.errors.each do |er|
            flash[:error] = ((flash[:error] || '') + t("cbt_training_tests." + er.attribute.to_s) + ': ' + t("cbt_training_tests.errors." + er.type.to_s)) + "; "
          end
          render :action => 'edit'
        }
        format.json { respond_with_bip(@cbt_training_test) }
      end
    end

  end

  def cbt_training_test_params
    params.require(:cbt_training_test).permit(:cbt_training_id, :cbt_test_id, :sort_by, :mandatory, :deleted_at)
  end

  private

  def fill_directories
    # @cbt_question_set = CbtQuestionsSet.all.order("id")
    @cbt_question_sets = CbtQuestionsSet.all.order("id")
    @cbt_tests = CbtTest.all.order("id")
    @cbt_trainings = CbtTraining.all.order("id")
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@cbt_training_test.versions.scoped.last), :method => :post)
  end

end
