class HomeController < ApplicationController

  def index
  end

  def show_result
  end

  def protocol
    @online_expertize = OnlineExpertize.find(params[:id])
    @exam = Exam.where(:exam_type_id => @online_expertize.exam_type_id).where("fisgia9code = #{@online_expertize.exam_global_id} or fisgia11code = #{@online_expertize.exam_global_id}").first
    @subject = @exam.subject

    respond_to do |format|
      format.html
      format.js
      format.pdf do
        if ExamType.find(@exam.exam_type_id).name[0..2]=='ЕГЭ'
          if [29, 30, 31, 33, 34].include? @subject.fisgia11code or [29, 30, 31, 33, 34].include? @subject.fisgia9code
            render pdf: "Протокол онлайн-проверки №#{@online_expertize.protocol_id}",
                   page_size: 'A4',
                   template: "/online_expertizes/generatepdf_voice.html.haml",
                   layout: "protocol_voice.html",
                   orientation: "Portrait",
                   lowquality: false,
                   zoom: 1,
                   dpi: 200,
                   margin:  {top: 0, bottom: 0, left: 0, right: 0}
            # page_height: 210,
            # page_width: 297
          else
            render pdf: "Протокол онлайн-проверки №#{@online_expertize.protocol_id}",
                   page_size: 'A4',
                   template: "/online_expertizes/generatepdf.html.haml",
                   layout: "protocol.html",
                   orientation: "Landscape",
                   lowquality: false,
                   zoom: 1,
                   dpi: 200,
                   margin:  {top: 0, bottom: 0, left: 0, right: 0}
            # page_height: 210,
            # page_width: 297

          end
        else #Отсюда 9-е классы
        if [29, 30, 31, 33, 34].include? @subject.fisgia11code or [29, 30, 31, 33, 34].include? @subject.fisgia9code
          render pdf: "Протокол онлайн-проверки №#{@online_expertize.protocol_id}",
                 page_size: 'A4',
                 template: "/online_expertizes/generatepdf9_voice.html.haml",
                 layout: "protocol9_voice.html",
                 orientation: "Portrait",
                 lowquality: false,
                 zoom: 1,
                 dpi: 200,
                 margin:  {top: 0, bottom: 0, left: 0, right: 0}
          # page_height: 210,
          # page_width: 297

        else
          render pdf: "Протокол онлайн-проверки №#{@online_expertize.protocol_id}",
                 page_size: 'A4',
                 template: "/online_expertizes/generatepdf9.html.haml",
                 layout: "protocol9.html",
                 orientation: "Landscape",
                 lowquality: false,
                 zoom: 1,
                 dpi: 200,
                 margin:  {top: 0, bottom: 0, left: 0, right: 0}
          # page_height: 210,
          # page_width: 297

        end
        end

      end
    end
  end


  def vvv
  end

  def appeal
    @appeal = Appeal.where(:result_id => params[:id])
    result = Result.where(:id => params[:id]).first
    student = Student.where(:guid => result.identifier).first
    subject = Subject.where(:fisgia11code => result.subject_code).first if student.is_ege_or_gve11
    subject = Subject.where(:fisgia9code => result.subject_code).first if student.is_oge_or_gve9
    @exam = Exam.where(:date=>result.examdate, :subject_code => subject.code).first
    # ГАОУ ДПО СО «ИРО»
    # exam = Exam.where(:date => xlsx.sheet(0).row(i)[4], :subject_code => subject.map(&:code))
    # exam_student = ExamStudent.where(:student_id => student.id, :exam_id => exam.map(&:id)).first
    # exam = Exam.where(:date => result.examdate, :subject_id =>)
    if @appeal.count == 0
      @appeal = Appeal.new
      @appeal.result_id = params[:id]
      @appeal.appeal_type_id = 2
      @appeal.appeal_condition_id = 1
      @appeal.exam_type_id = @exam.exam_type_id
      @appeal.subject_id = subject.id
      @appeal.identifier = result.identifier
      @appeal.result_id = result.id
      @appeal.examdate = result.examdate
      mouo_id = Mouo.where(:code => student.group.ou.ate.code).first.id
      begin
        if student.is_oge_or_gve9
          # puts("###############")
          # puts(AppealStation.where(:mouo_id=>mouo_id).where(:is_gia9 => true).first.id)
          @appeal.appeal_station_id = AppealStation.where(:mouo_id=>mouo_id).where(:is_gia9 => true).first.id
        end
        if student.is_ege_or_gve11
          @appeal.appeal_station_id = AppealStation.where(:mouo_id=>mouo_id).where(:is_gia11 => true).first.id
        end
      rescue
        @appeal.appeal_station_id = 1
      end

      # puts("###############")
      @appeal.appeal_time = nil
      @appeal.appeal_link = nil
      @appeal.presence = true
      @appeal.user_id = current_user.id
    else
      @appeal = @appeal.first
    end

  end

  def show_results_list
  end

  def terms
  end

  def privacy
  end
end
