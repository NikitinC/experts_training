class CbtTestQuestionSetsImportsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    redirect_to :action => 'new'
  end

  def new
    @cbt_test_question_sets_import = CbtTestQuestionSetsImport.new
  end

  def create
    @cbt_test_question_sets_import = CbtTestQuestionSetsImport.new(params[:cbt_test_question_sets_import])
    render :create
  end

end
