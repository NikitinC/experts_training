class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :masquerade_user!
  before_action :set_paper_trail_whodunnit

  # rescue_from Exception, :with => :render_not_found
  # rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found
  # rescue_from StandardError, :with => :render_error
  #
  # rescue_from AbstractController::ActionNotFound, :with => :render_not_found
  # rescue_from ActionController::RoutingError, :with => :render_not_found
  #Redirects to login for secure resources
  rescue_from CanCan::AccessDenied do |exception|

    if user_signed_in?
      flash[:error] = "Вы не авторизованы для просмотра данной страницы"
      session[:user_return_to] = nil
      redirect_to root_url

    else
      flash[:error] = "Для перехода к данной странице сначала надо войти в систему."
      session[:user_return_to] = request.url
      redirect_to "/users/sign_in"
    end

  end
  # rescue_from CanCan::AccessDenied do |exception|
  #   er = ErrorSave.new
  #   begin
  #     er.user_id = current_user.id
  #   rescue
  #     er.user_id = nil
  #   end
  #   er.referer = request.referer
  #   er.url = request.url
  #   er.exception = exception
  #   er.save
  #   redirect_to main_app.root_url, :alert => "Вы не имеете прав на выполнение запрошенных действий."
  # end

  def render_not_found(exception)
    er = ErrorSave.new
    begin
      er.user_id = current_user.id
    rescue
      er.user_id = nil
    end
    er.referer = request.referer
    er.url = request.url
    er.exception = exception
    er.save
    render :template => "layouts/errors/404",
           :layout => 'errors/404',
           :status => 404
  end

  def render_error(exception)
    er = ErrorSave.new
    begin
      er.user_id = current_user.id
    rescue
      er.user_id = nil
    end
    er.referer = request.referer
    er.url = request.url
    er.exception = exception
    er.save
    @exception = exception
    render :template => "layouts/errors/500",
           :layout => 'errors/500',
           :status => 500
  end


  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :avatar])
    end

end
