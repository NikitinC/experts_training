class ApplicationMailer < ActionMailer::Base
  default from: "#{SiteInfo.find(1).email}"
  layout 'mailer'
end
