module DependenceListHelper
  def dependence_list items, options={}
    model_name = options[:model] && options[:model].to_s.underscore || items.first && items.first.class.name.underscore || nil
    partials = options.delete(:partials)
    #without_destroy_reason = options.delete(:without_destroy_reason)
    resource_parents = [*options.delete(:resource_parents)]
    new_item = model_name.camelize.constantize.new
    unless partials
      raise("Render Error: You must define partials")
    end
    capture_haml do
      haml_tag "div.dependence-list", "data-edit-button-selector" => options[:edit_button_selector],
               "data-save-button-selector" => options[:save_button_selector],
               "data-cancel-button-selector" => options[:cancel_button_selector],
               "data-index-url" => url_for([*resource_parents, new_item]),
               "data-cannot-create" => can?(:create, model_name.camelize.constantize) ? nil : "true",
               "data-new-line-url" => url_for([:new, *resource_parents, model_name.to_sym]) do
        haml_tag "table.table.table-bordered" do
          haml_tag "thead" do
            haml_tag "tr" do
              haml_concat render(:partial => partials[:head])
            end
          end
          haml_tag "tbody" do
            items.each do |item|
              begin
                html_class = item.try(:valid_cache) ? nil : "invalid-tr";
              rescue;
              end
              haml_tag "tr.show-line", :class => html_class do
                haml_concat render(:partial => partials[:show].is_a?(Hash) ? partials[:show][:path] : partials[:show],
                                   :locals => {:item => item}.merge(partials[:show].is_a?(Hash) ? partials[:show][:locals] || {} : {}))
              end
              if can? :update, item
                haml_tag "tr.edit-line", "data-url" => url_for(item), "data-method" => "put" do
                  haml_concat render(:partial => partials[:edit].is_a?(Hash) ? partials[:edit][:path] : partials[:edit],
                                     :locals => {:item => item}.merge(partials[:edit].is_a?(Hash) ? partials[:edit][:locals] || {} : {}))
                  haml_tag "td.under-pressure.buttons-td" do
                    #if without_destroy_reason
                    #  haml_concat(link_to(send("#{item.class.name.underscore}_path", item), :method => :delete,
                    #                      :class => 'btn btn-mini btn-danger') do
                    #    content_tag(:i, "", :class => "#{options[:icon]} icon-white")
                    #  end)
                    #else
                    # кнопка удаления для экзаменов!!! пришлось выключить в 2017 году
                    # в 2018 нашёл вот такой выход: если дестрой разрешен - показывать
                    if can? :destroy, item
                        haml_concat destroy_for_reason_btn(item, {:class => "destroy-btn"})
                    end
                    #end
                  end
                  if current_user.has_role? [:admin, :rcoi]
                    haml_tag "td" do
                      haml_concat(rlog_button(item, {:no_button_text => true}))
                    end
                  end
                end
              else
                haml_tag "tr.edit-line", "data-url" => false do
                  haml_concat render(:partial => partials[:show].is_a?(Hash) ? partials[:show][:path] : partials[:show],
                                     :locals => {:item => item}.merge(partials[:show].is_a?(Hash) ? partials[:show][:locals] || {} : {}))
                  haml_tag "td.under-pressure"
                end
              end
            end
            haml_tag "tr.new-line", "data-url" => url_for(new_item), "data-method" => "post" do
              haml_tag "td.under-pressure.buttons-td" do
                haml_tag :a, :class => "btn btn-danger btn-mini destroy-btn" do
                  haml_tag :i, "", :class => "icon-trash icon-white"
                end
              end
            end
          end
        end
        haml_tag ".add-button-container" do
          haml_concat link_to(t("buttons.add"), "#", class: "btn btn-mini btn-primary add-btn")
        end
        haml_tag ".clear"
      end
    end
  end
end