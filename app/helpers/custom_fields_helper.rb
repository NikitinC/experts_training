module CustomFieldsHelper
  def render_custom_fields f, field, custom_fields
    custom_fields.map do |key, value|
      keyword = f.object.class.name.underscore
      field_name = "#{field}[#{key}]"
      label = I18n.t("activerecord.attributes.#{keyword}.#{field}.#{key}")
      case value
        when :integer
          capture_haml do
            haml_concat control_group f, field_name, :text_field_tag, "", "data-mask" => "9?999999999", :label => label
          end
        when :string
          capture_haml do
            haml_concat control_group f, field_name, :text_field_tag, "", :label => label
          end
        else
          klass = value.to_s.camelize.constantize
          arr = klass.all
          capture_haml do
            haml_concat control_group f, field_name, :select_tag, options_from_collection_for_select(arr, "code", "name"), :label => label
          end
      end
    end.join("").html_safe
  end

  def render_custom_values object, field_name, custom_fields
    return "" if object.nil?
    custom_values = object.send field_name
    capture_haml do
      haml_tag :div do
        custom_values.map do |key, value|
          label = I18n.t("activerecord.attributes.#{object.class.name.underscore}.#{field_name}.#{key}")
          type = custom_fields[key.to_sym]

          case type
            when :integer
              name = value
            when :string
              name = value
            else
              klass = type.to_s.camelize.constantize
              obj = klass.find_by_code value
              name = obj.to_s
          end

          haml_tag :div, label + ": " + name
        end
      end
    end
  end
end
