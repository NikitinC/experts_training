module HeavySelectHelper
  def generate_heavy_select label, field_name, options = {}, &block
    capture_haml do
      haml_tag :div, :class => "heavy-select", "data-field-name" => field_name,
               "data-parent-selector" => options[:parent_selector] do
        haml_tag :a, "", :class => "filter-chosen-object"
        haml_tag :a, label, :class => "filter-link btn btn-mini"
        haml_tag :a, t("buttons.clear"), :class => "filter-clear btn btn-mini btn-danger"
        haml_tag :div, :class => "filter-container" do
          haml_tag(:div, "", :class => "filter-triangle")
          haml_tag :div, :class => "filter-popup" do
            if block_given?
              haml_tag :div, :class => "filter-form", &block
            end
            haml_tag(:div, "", :class => "filter-results")
            haml_tag(:a, t("buttons.close_icon"), :class => "close filter-close-btn")
          end
        end
        haml_tag(:div, "", :class => "clear")
      end
    end
  end
end