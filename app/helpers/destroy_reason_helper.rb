module DestroyReasonHelper
  def destroy_for_reason_btn target, options = {}
    options[:class] ||= ""
    options[:icon] ||= "icon-trash"
    if target.is_a? Symbol
      target_type = target.to_s.camelize
    else
      target_id = target.id
      target_type = target.class.name
    end
    capture_haml do
      a = link_to destroy_reasons_path("target_type" => target_type, "target_id" => target_id), {:class => "btn btn-mini btn-danger modal-link #{options[:class]}",
                                                                                                 "data-modal-class" => "destroy-reason-modal", :title => t("buttons.delete"), "data-modal-params" => "#{target_type}#{target_id}"} do
        raw [content_tag(:i, "", :class => "#{options[:icon]} icon-white"), ""].join(" ")
      end
      haml_concat a
    end
  end

  def dependent_association associations
    associations.map do |a|
      t("activerecord.collection.#{a.to_s.underscore}").mb_chars.downcase.to_s
    end.join(",")
  end
end