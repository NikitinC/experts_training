# encoding: utf-8

module BootstrapHelper
  def control_group f, field_name, input_type = nil, *options, &block
    opts = options.last.is_a?(Hash) ? options.last : {}
    hint = opts.delete(:hint)
    label = opts.delete(:label)
    label_field_name = [field_name].flatten.first
    input_field_name = [field_name].flatten.last
    form_tag_helper = Object.new.extend(ActionView::Helpers::FormTagHelper).extend(ActionView::Helpers::TagHelper)

    content_tag :div, :class => "control-group" do
      raw([
              (
              label ? content_tag(:label, :class => "control-label") do
                label
              end : f.label(label_field_name, :class => "control-label")
              ),

              if block_given?
                content_tag :div, :class => "controls", &block
              else
                content_tag(:div, :class => "controls") do
                  field = if f.respond_to?(input_type)
                            f.send(input_type, input_field_name, *options)
                          else
                            form_tag_helper.send(input_type, input_field_name, *options)
                          end
                  raw([
                          field,
                          hint && form_tag_helper.content_tag(:p, hint, :class => "help-block", "data-help-for" => input_field_name)
                      ].join(" "))
                end
              end,
          ].join(" "))
    end
  end

  def word_files_upload_group f, field_name
    control_group f, field_name, :image_tag do
      haml_tag :div, :class => "picture-uploader" do
        url = f.object.send(field_name.to_s).to_s
        picture = TxtFileUploader::FILE_EXTENTIONS.include?(File.extname(url).downcase)
        haml_tag "div.clear"
        haml_concat f.file_field field_name.to_sym, :class => "file-uploader"
        haml_concat f.hidden_field (field_name.to_s + "_cache").to_sym
        haml_tag :br
        haml_concat f.check_box ("remove_" + field_name.to_s).to_sym, :class => "remove-picture"
        haml_tag :span, t("Удалить загруженный файл")
      end
    end
  end

  def excel_files_upload_group f, field_name
    control_group f, field_name, :image_tag do
      haml_tag :div, :class => "picture-uploader" do
        url = f.object.send(field_name.to_s).to_s
        picture = CsvFileUploader::FILE_EXTENTIONS.include?(File.extname(url).downcase)
        haml_tag "div.clear"
        haml_concat f.file_field field_name.to_sym, :class => "file-uploader"
        haml_concat f.hidden_field (field_name.to_s + "_cache").to_sym
        haml_tag :br
        haml_concat f.check_box ("remove_" + field_name.to_s).to_sym, :class => "remove-picture"
        haml_tag :span, t("Удалить загруженный файл")
      end
    end
  end

  def excel_files_upload_ou f, field_name
    control_group f, field_name, :image_tag do
      haml_tag :div, :class => "picture-uploader" do
        url = f.object.send(field_name.to_s).to_s
        picture = CsvFileUploader::FILE_EXTENTIONS.include?(File.extname(url).downcase)
        haml_tag "div.clear"
        haml_concat f.file_field field_name.to_sym, :class => "file-uploader"
        haml_concat f.hidden_field (field_name.to_s + "_cache").to_sym
        haml_tag :br
        haml_concat f.check_box ("remove_" + field_name.to_s).to_sym, :class => "remove-picture"
        haml_tag :span, t("Удалить загруженный файл")
      end
    end
  end

  def picture_upload_group2 f, field_name
    control_group f, ' ', :image_tag do
      haml_tag :div, :class => "picture-uploader" do
        url = f.object.send(field_name.to_s).to_s
        picture = ScanUploader::PICTURE_EXTENTIONS.include?(File.extname(url).downcase)
        haml_concat picture ? link_to(image_tag(f.object.send(CGI.unescape(field_name).to_s + "_url", :preview), :class => "pull-left"), url, :class => "fancybox") : link_to(CGI.unescape(File.basename(url)), url) if f.object.send(field_name.to_s + "_url")
        haml_tag "div.clear"
        haml_concat f.check_box ("remove_" + field_name.to_s).to_sym, :class => "remove-picture"
        haml_tag :span, t("buttons.delete")
        haml_tag "br"
        haml_concat f.file_field field_name.to_sym, :class => "file-uploader"
        haml_concat f.hidden_field (field_name.to_s + "_cache").to_sym
      end
    end
  end

  def picture_upload_group f, field_name
    control_group f, field_name, :image_tag do
      haml_tag :div, :class => "picture-uploader" do
        url = f.object.send(field_name.to_s).to_s
        picture = ScanUploader::PICTURE_EXTENTIONS.include?(File.extname(url).downcase)
        haml_concat picture ? link_to(image_tag(f.object.send(field_name.to_s + "_url", :preview), :class => "pull-left"), url, :class => "fancybox") : link_to(File.basename(url), url) if f.object.send(field_name.to_s + "_url")
        haml_tag "div.clear"
        haml_concat f.file_field field_name.to_sym, :class => "file-uploader"
        haml_concat f.hidden_field (field_name.to_s + "_cache").to_sym
        haml_concat f.check_box ("remove_" + field_name.to_s).to_sym, :class => "remove-picture"
        haml_tag :span, t("buttons.delete")
      end
    end
  end

  def zip_files_upload_group f, field_name
    control_group f, field_name, :image_tag do
      haml_tag :div, :class => "picture-uploader" do
        url = f.object.send(field_name.to_s).to_s
        picture = ZipUploader::FILE_EXTENTIONS.include?(File.extname(url).downcase)
        haml_tag "div.clear"
        haml_concat f.file_field field_name.to_sym, :class => "file-uploader"
        haml_concat f.hidden_field (field_name.to_s + "_cache").to_sym
        haml_tag :br
        haml_concat f.check_box ("remove_" + field_name.to_s).to_sym, :class => "remove-picture"
        haml_tag :span, t("Удалить загруженный файл")
      end
    end
  end

  def data_row object, attribute_name, &block
    i18n_text = t("activerecord.attributes.#{object.class.name.underscore}.#{attribute_name.to_s}")
    capture_haml do
      haml_tag "tr" do
        haml_tag "td.static3", i18n_text
        if block_given?
          haml_tag "td.strong", yield
        else
          haml_tag "td.strong", object.send(attribute_name.to_sym)
        end
      end
    end
  end

  def bool_data_row item, field
    data_row(item, field) { bool_to_word item.send(field) }
  end

  def header_row value = ''
    capture_haml do
      haml_tag :tr, :class => 'header' do
        haml_tag :td, {:class => 'strong', :colspan => 2} do
          haml_tag :h4, value
        end
      end
    end
  end

  def control_check_box f, name
    capture_haml do
      haml_tag :div, :class => "control-group" do
        haml_tag :div, :class => "controls" do
          haml_tag :label, :class => "checkbox" do
            haml_concat f.check_box(name)
            haml_concat I18n.t("activerecord.attributes.#{f.object.class.name.underscore}.#{name}")
          end
        end
      end
    end
  end

  def control_header value = ''
    capture_haml do
      haml_tag :div, :class => 'control-group header' do
        haml_tag :div, :class => 'controls' do
          haml_tag :h4, value
        end
      end
    end
  end

  def control_well(message)
    capture_haml do
      haml_tag :div, class: 'control-group' do
        haml_tag :div, class: 'controls' do
          haml_tag(:div, class: 'well') { haml_concat message }
        end
      end
    end
  end


  def render_scan object, attribute_name
    i18n_text = t("activerecord.attributes.#{object.class.name.underscore}.#{attribute_name.to_s}")
    capture_haml do
      haml_tag "tr" do
        haml_tag "td.static3", i18n_text
        url = object.send(attribute_name.to_sym).to_s
        picture = ScanUploader::PICTURE_EXTENTIONS.include?(File.extname(url).downcase)
        if object.send(attribute_name.to_s + "_url")
          haml_tag "td.strong" do
            haml_tag "div", link_to(image_tag(object.send(attribute_name.to_s + "_url", :preview).to_s), url, :class => "fancybox")
            haml_tag "div", link_to(t("messages.download"), url)
          end
        else
          haml_tag "td.strong", t("messages.file_not_load")
        end
      end
    end
  end


  def render_scan2 object, attribute_name
    i18n_text = t("activerecord.attributes.#{object.class.name.underscore}.#{attribute_name.to_s}")
    url = object.send(attribute_name.to_sym).to_s
    picture = ScanUploader::PICTURE_EXTENTIONS.include?(File.extname(url).downcase)
    if object.send(attribute_name.to_s + "_url")
      link_to(image_tag(object.send(attribute_name.to_s + "_url", :preview).to_s), url, :class => "fancybox")
      link_to(t("messages.download"), url)
    else
      t("messages.file_not_load")
    end
  end

  def directory_select(f, name, directories, options = {})
    control_group f, name, :select,
                  options_from_collection_for_select(directories, :code, :name, :selected => f.object.send(name)),
                  {:include_blank => true}, {:class => "simple-chosen", "data-placeholder" => t("helpers.placeholder.default")}.merge(options)
  end

  def bool_select(f, name, type = :word, options = {})
    control_group f, name, :select, bool_select_array(type), {include_blank: true, selected: f.object.send(name)},
                  {class: "simple-chosen", data: {placeholder: t("helpers.placeholder.default")}.merge(options)}
  end

  def submit_button(f)
    f.submit f.object.new_record? ? t("buttons.create") : t("buttons.save"), :class => "btn btn-primary btn-with-tooltip",
             "data-original-title" => t("messages.tooltips.save"), "data-placement" => "bottom"
  end
end