module ValidFilterHelper
  def render_valid_filter filter
    capture_haml do
      haml_tag :div, :class => "control-group" do
        haml_concat check_box_tag("filter[valid_cache]", false, filter[:valid_cache], "data-autosubmit" => "change")
        haml_tag :span, I18n.t("views.layouts.valid_cache_checkbox_label")
      end
    end unless filter.nil?
  end
end