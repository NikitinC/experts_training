module ApplicationHelper
  def bootstrap_class_for(flash_type)
    {
      success: "alert-success",
      error: "alert-danger",
      alert: "alert-warning",
      notice: "alert-info"
    }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end
  
  def avatar_path(object, options = {})
    size = options[:size] || 180
    default_image = options[:default] || "mp"
    base_url =  "https://secure.gravatar.com/avatar"
    base_url_params = "?s=#{size}&d=#{default_image}"
    
    if object.respond_to?(:avatar) && object.avatar.attached? && object.avatar.variable?
      object.avatar.variant(resize_to_fill: [size, size, { gravity: 'Center' }])
    elsif object.respond_to?(:email) && object.email
      gravatar_id = Digest::MD5::hexdigest(object.email.downcase)
      "#{base_url}/#{gravatar_id}#{base_url_params}"
    else
      "#{base_url}/00000000000000000000000000000000#{base_url_params}"
    end
  end


  # Cells 3 style cell render call.
  def render_cell(name, state, *args)
    cell(name).(state, *args)
  end

  deprecate :render_cell


  def bool_to_word bool, type = :word
    !bool.nil? ? I18n.t("helpers.bool_to.#{type}.if_#{bool}") : ""
  end

  def bool_select_array type = :word
    [true, false].map { |b|
      [bool_to_word(b, type), b]
    }
  end

  def ln date
    #TODO: delete this method, replace it to helper date_field, and read http://api.rubyonrails.org/classes/ActionView/Helpers/DateHelper.html
    date.nil? ? nil : l(date)
  end

  def error_explanation target, save_draft = false
    if target.errors.any?
      capture_haml do
        haml_tag "ul.error-messages" do
          target.errors.full_messages.uniq.each do |msg|
            haml_tag "li", msg
          end
          haml_tag "a", t("buttons.save_draft"), :href => "#", :class => "btn btn-inline", "data-chained-button" => "input[name=save_draft]" if save_draft && current_user
        end
      end
    end
  end

  def show_if_no_valid target
    unless target.valid_cache?
      capture_haml do
        haml_tag "div.warning" do
          haml_tag :span, t("messages.validation.if_no_valid")
        end
      end
    end
  end

  def button_fix
    content_tag :span, " "
  end

  def render_title(target)
    controller = params[:controller]
    action = params[:action]

    target && target.to_s || t("titles.#{controller}.#{action}", :default => t("titles.default"))
  end

  def render_checkbox_filter filter, name, text
    capture_haml do
      haml_tag :div, :class => "control-group" do
        haml_concat check_box_tag("filter[#{name.to_s}]", true, filter[name], "data-autosubmit" => "change")
        haml_tag :span, text
      end
    end unless filter.nil?
  end

end
