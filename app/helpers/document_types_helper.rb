module DocumentTypesHelper

  def document_type_select_options document_types, current_document_type_code
    capture_haml do
      document_types.each do |dt|
        haml_tag :option, dt.name,
                 :value => dt.code,
                 :data => {"series-regexp" => dt.series_mask,
                           "number-regexp" => dt.number_mask,
                           "series-example" => dt.series_example,
                           "number-example" => dt.number_example},
                 :selected => dt.code == current_document_type_code  ? "selected" : nil
      end
    end
  end

end
