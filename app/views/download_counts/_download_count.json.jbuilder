json.extract! download_count, :id, :user_id, :resource, :created_at, :updated_at
json.url download_count_url(download_count, format: :json)
