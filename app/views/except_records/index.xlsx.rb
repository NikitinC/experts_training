def create_spreadsheet relation
  p = Axlsx::Package.new
  ExceptRecord.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_except_record")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_except_record")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.except_record.code"),
                          I18n.t("except_records.tables_headers.name"),
                          I18n.t("except_records.tables_headers.chief_fio"),
                          I18n.t("except_records.tables_headers.chief_phone"),
                          I18n.t("except_records.tables_headers.chief_email"),
                          I18n.t("except_records.tables_headers.information_exchange_fio"),
                          I18n.t("except_records.tables_headers.information_exchange_phone"),
                          I18n.t("except_records.tables_headers.information_exchange_email"),
                          I18n.t('except_records.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('except_records.tables_headers.specialist_final_certification_phone'),
                          I18n.t('except_records.tables_headers.specialist_final_certification_email'),
                          I18n.t('except_records.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('except_records.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |except_record|
          sheet.add_row [
                            except_record.code,
                            except_record.name,
                            except_record.chief_full_name,
                            except_record.chief_phone,
                            except_record.chief_email,
                            except_record.information_exchange_specialist_full_name,
                            except_record.information_exchange_specialist_phone,
                            except_record.information_exchange_specialist_email,
                            except_record.specialist_final_certification_full_name,
                            except_record.specialist_final_certification_phone,
                            except_record.specialist_final_certification_email,
                            except_record.exam_materials_specialist_full_name,
                            except_record.exam_materials_specialist_post,
                            except_record.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end