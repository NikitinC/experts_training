json.extract! except_record, :id, :code, :name, :short_name, :fisgia9code, :fisgia11code, :sort_by, :created_at, :updated_at
json.url except_record_url(except_record, format: :json)
