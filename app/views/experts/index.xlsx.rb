def create_spreadsheet relation
  p = Axlsx::Package.new
  Expert.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_expert")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_expert")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.expert.code"),
                          I18n.t("experts.tables_headers.name"),
                          I18n.t("experts.tables_headers.chief_fio"),
                          I18n.t("experts.tables_headers.chief_phone"),
                          I18n.t("experts.tables_headers.chief_email"),
                          I18n.t("experts.tables_headers.information_exchange_fio"),
                          I18n.t("experts.tables_headers.information_exchange_phone"),
                          I18n.t("experts.tables_headers.information_exchange_email"),
                          I18n.t('experts.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('experts.tables_headers.specialist_final_certification_phone'),
                          I18n.t('experts.tables_headers.specialist_final_certification_email'),
                          I18n.t('experts.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('experts.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |expert|
          sheet.add_row [
                            expert.code,
                            expert.name,
                            expert.chief_full_name,
                            expert.chief_phone,
                            expert.chief_email,
                            expert.information_exchange_specialist_full_name,
                            expert.information_exchange_specialist_phone,
                            expert.information_exchange_specialist_email,
                            expert.specialist_final_certification_full_name,
                            expert.specialist_final_certification_phone,
                            expert.specialist_final_certification_email,
                            expert.exam_materials_specialist_full_name,
                            expert.exam_materials_specialist_post,
                            expert.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end