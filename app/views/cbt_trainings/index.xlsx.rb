def create_spreadsheet relation
  p = Axlsx::Package.new
  CbtTraining.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_cbt_training")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_cbt_training")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.cbt_trainings.code"),
                          I18n.t("cbt_trainings.tables_headers.name"),
                          I18n.t("cbt_trainings.tables_headers.chief_fio"),
                          I18n.t("cbt_trainings.tables_headers.chief_phone"),
                          I18n.t("cbt_trainings.tables_headers.chief_email"),
                          I18n.t("cbt_trainings.tables_headers.information_exchange_fio"),
                          I18n.t("cbt_trainings.tables_headers.information_exchange_phone"),
                          I18n.t("cbt_trainings.tables_headers.information_exchange_email"),
                          I18n.t('cbt_trainings.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('cbt_trainings.tables_headers.specialist_final_certification_phone'),
                          I18n.t('cbt_trainings.tables_headers.specialist_final_certification_email'),
                          I18n.t('cbt_trainings.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('cbt_trainings.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |cbt_training|
          sheet.add_row [
                            cbt_training.code,
                            cbt_training.name,
                            cbt_training.chief_full_name,
                            cbt_training.chief_phone,
                            cbt_training.chief_email,
                            cbt_training.information_exchange_specialist_full_name,
                            cbt_training.information_exchange_specialist_phone,
                            cbt_training.information_exchange_specialist_email,
                            cbt_training.specialist_final_certification_full_name,
                            cbt_training.specialist_final_certification_phone,
                            cbt_training.specialist_final_certification_email,
                            cbt_training.exam_materials_specialist_full_name,
                            cbt_training.exam_materials_specialist_post,
                            cbt_training.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end