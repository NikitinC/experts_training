def create_spreadsheet relation
  p = Axlsx::Package.new
  CbtSubject.uncached do
    p.workbook do |wb|
      styles = wb.styles
      title = styles.add_style(:sz => 15, :b => true)
      header = styles.add_style(:b => true, :border => Axlsx::STYLE_THIN_BORDER)
      default = styles.add_style(:border => Axlsx::STYLE_THIN_BORDER)
      wb.add_worksheet(name: I18n.t("spreadsheets.list_cbt_subject")) do |sheet|
        sheet.add_row [I18n.t("spreadsheets.list_cbt_subject")], :style => title
        sheet.add_row ["#{I18n.t("spreadsheets.export_date")}:#{Russian::l (Time.new)}"]
        sheet.add_row [
                          I18n.t("activerecord.attributes.cbt_subject.code"),
                          I18n.t("cbt_subjects.tables_headers.name"),
                          I18n.t("cbt_subjects.tables_headers.chief_fio"),
                          I18n.t("cbt_subjects.tables_headers.chief_phone"),
                          I18n.t("cbt_subjects.tables_headers.chief_email"),
                          I18n.t("cbt_subjects.tables_headers.information_exchange_fio"),
                          I18n.t("cbt_subjects.tables_headers.information_exchange_phone"),
                          I18n.t("cbt_subjects.tables_headers.information_exchange_email"),
                          I18n.t('cbt_subjects.tables_headers.specialist_final_certification_full_name'),
                          I18n.t('cbt_subjects.tables_headers.specialist_final_certification_phone'),
                          I18n.t('cbt_subjects.tables_headers.specialist_final_certification_email'),
                          I18n.t('cbt_subjects.tables_headers.exam_materials_specialist_full_name'),
                          I18n.t('cbt_subjects.tables_headers.exam_materials_specialist_post'),
                          I18n.t("activerecord.attributes.user.login"),
                      ], :style => header
        relation.find_each do |cbt_subject|
          sheet.add_row [
                            cbt_subject.code,
                            cbt_subject.name,
                            cbt_subject.chief_full_name,
                            cbt_subject.chief_phone,
                            cbt_subject.chief_email,
                            cbt_subject.information_exchange_specialist_full_name,
                            cbt_subject.information_exchange_specialist_phone,
                            cbt_subject.information_exchange_specialist_email,
                            cbt_subject.specialist_final_certification_full_name,
                            cbt_subject.specialist_final_certification_phone,
                            cbt_subject.specialist_final_certification_email,
                            cbt_subject.exam_materials_specialist_full_name,
                            cbt_subject.exam_materials_specialist_post,
                            cbt_subject.users.map { |u| u.login }.join(" ,")
                        ], :style => default
        end
        sheet.merge_cells("A1:D1")
      end
    end
  end
  p
end