json.extract! manual, :id, :manual_text, :model, :url, :sort_by
json.url manual_url(manual, format: :json)
