class CbtQuestionsImport
  include ActiveModel::Model
  require 'roo'
  #
  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_cbt_questions
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      cbt_question = Item.find_by_id(row["id"]) || Item.new
      cbt_question.attributes = row.to_hash
      cbt_question
    end
  end

  def imported_cbt_questions
    @imported_cbt_questions ||= load_imported_cbt_questions
  end

  def save
    if imported_cbt_questions.map(&:valid?).all?
      imported_cbt_questions.each(&:save!)
      true
    else
      imported_cbt_questions.each_with_index do |cbt_question, index|
        cbt_question.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

  def importing(file, current_user)
    result = ''
    if 1 == 1
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)

        if xlsx.sheets.count == 1

          # Импорт вопросов
          attribute_list = ['-', 'name', '-', '-', '-', '-', '-', 'primary_mark', 'correct_answer',
                            'settings', 'k_nums', 'next_cbt_question_id',
                            '-', '-', 'question_text', '-', '-']
          if xlsx.sheet(0).row(1) == ['ID', 'Наименование вопроса', 'Предмет', 'Тип ответа', 'Тип вопроса',
                                      'Частичное оценивание', 'Перемешивание', 'Первичный балл',
                                      'Верный ответ', 'Настройки вопроса и ответа', 'Нумерация критериев', 'ID следующего вопроса',
                                      'PDF-файл', 'Справочные материалы (Файл для скачивания)',
                                      'Полноформатное описание', 'Набор вопросов', 'Тесты']
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              sj = CbtSubject.where(:name => "#{xlsx.sheet(0).row(i)[2]}".squish).first
              if !sj.nil?
                j = 0
                m = CbtQuestion.where("cast(id as bigint) = #{xlsx.sheet(0).row(i)[0].to_i}").first
                sozdali = false
                if m.nil?
                  m = CbtQuestion.where("name = '#{xlsx.sheet(0).row(i)[1]}' and cbt_subject_id = #{sj.id}").first
                end
                if m.nil?
                  m = CbtQuestion.new
                  if "0#{xlsx.sheet(0).row(i)[0].to_i}".to_i > 0
                    m.id = "0#{xlsx.sheet(0).row(i)[0].to_i}".to_i
                  end
                  sozdali = true
                end

                #  1 - id; 2 - subject; 3 - answer_type; 4 - question_type; 5 - partial_mark; 6 - shuffle;
                # 14 - question_sets; 15 - tests

                m.cbt_subject_id = sj.id
                m.cbt_answer_type_id = CbtAnswerType.where(:name => "#{xlsx.sheet(0).row(i)[3]}".squish).first.id
                m.cbt_question_type_id = CbtQuestionType.where(:name => "#{xlsx.sheet(0).row(i)[4]}".squish).first.id
                if "#{xlsx.sheet(0).row(i)[5]}".squish.upcase == 'ДА'
                  m.partial_mark = true
                elsif "#{xlsx.sheet(0).row(i)[5]}".squish.upcase == 'НЕТ'
                  m.partial_mark = true
                end
                if "#{xlsx.sheet(0).row(i)[6]}".squish.upcase == 'ДА'
                  m.shuffle = true
                elsif "#{xlsx.sheet(0).row(i)[6]}".squish.upcase == 'НЕТ'
                  m.shuffle = true
                end

                cbt_questions_set = CbtQuestionsSet.where(:name => "#{xlsx.sheet(0).row(i)[15]}".squish, :cbt_subject_id => m.cbt_subject_id).first
                if cbt_questions_set.nil?
                  cbt_questions_set = CbtQuestionsSet.new
                  cbt_questions_set.name = "#{xlsx.sheet(0).row(i)[15]}".squish
                  cbt_questions_set.cbt_subject_id = m.cbt_subject_id
                  cbt_questions_set.save
                end

                cbt_test = CbtTest.where(:name => "#{xlsx.sheet(0).row(i)[16]}".squish).first
                if cbt_test.nil?
                  cbt_test = CbtTest.new
                  cbt_test.name = "#{xlsx.sheet(0).row(i)[16]}".squish
                  cbt_test.save
                end

                while j<attribute_list.count
                  if m.has_attribute?(attribute_list[j].to_sym)
                    m.set_property(attribute_list[j], xlsx.sheet(0).row(i)[j])
                  end
                  j = j + 1
                end


                m.question_text = m.simple_format("#{xlsx.sheet(0).row(i)[14]}")

                # m.pdf_file = "asdasd.pdf"
                # m.help_pdf_file = File.extname("#{xlsx.sheet(0).row(i)[12]}").downcase


                if 1 == 1
                  if m.save

                    sql = "update cbt_questions set pdf_file = '#{xlsx.sheet(0).row(i)[12]}', help_pdf_file = '#{xlsx.sheet(0).row(i)[13]}'  where id = #{m.id}"
                    ActiveRecord::Base.connection.execute (sql)

                    cbt_question_set_question = CbtQuestionSetQuestion.where(:cbt_questions_set_id => cbt_questions_set.id, :cbt_question_id => m.id).first
                    if cbt_question_set_question.nil?
                      cbt_question_set_question = CbtQuestionSetQuestion.new
                      cbt_question_set_question.cbt_questions_set_id = cbt_questions_set.id
                      cbt_question_set_question.cbt_question_id = m.id
                      cbt_question_set_question.save
                    end

                    cbt_test_question_set = CbtTestQuestionSet.where(:cbt_test_id => cbt_test.id, :cbt_questions_set_id => cbt_questions_set.id).first
                    if cbt_test_question_set.nil?
                      cbt_test_question_set = CbtTestQuestionSet.new
                      cbt_test_question_set.cbt_test_id = cbt_test.id
                      cbt_test_question_set.cbt_questions_set_id = cbt_questions_set.id
                      cbt_test_question_set.save
                    end

                    if sozdali == true
                      result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[1]}</td><td>Удалось, создали</td></tr>"
                    else
                      result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[1]}</td><td>Удалось, обновили #{m.name}</td></tr>"
                    end
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось: "
                    m.errors.full_messages.each do |message|
                      result = result + "<li>#{message}</li>"
                    end
                    result = result + "</td></tr>"
                  end

                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
                end
              else
                result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось найти указанный предмет тестирования #{xlsx.sheet(0).row(i)[2].squish}</td></tr>"
              end
            end

          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
      return ("Файл не определён")
      end
    else
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на #{SiteInfo.find(1).support} с приложением файла.")
    end
  end


end