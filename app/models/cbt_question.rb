class CbtQuestion < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  validates  :name,
             :presence => true

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations

  belongs_to  :cbt_subject
  belongs_to  :cbt_answer_type
  belongs_to  :cbt_question_type

  has_rich_text :question_text

  def self.search(search)
    if search
      where('name ?', "%#{search}%")
    end
  end

  def to_s
    name
  end

  def simple_format( text )
    return text if ( text =~ /(<table.*>)/ ) # return text unchanged

    start_tag = '<p>'
    text = text.to_s.dup
    text.gsub!(/\r\n?/, "\n")    # \r\n and \r => \n
    text.gsub!(/\n\n+/, "</p>\n\n#{start_tag}")  # 2+ newline  => paragraph
    text.gsub!(/([^\n]\n)(?=[^\n])/, '\1<br />')  # 1 newline   => br
    text.insert 0, start_tag
    text << "</p>"
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end


  [:pdf_file, :help_pdf_file].each do |scan_field|
    mount_uploader scan_field, ScansUploader
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20


  def formatted_body
    innertext = Richtext::CodeBlocks::HtmlService.render(self.question_text.to_s).html_safe
    innertext = Richtext::CodeBlocks::HtmlService.latex(innertext).html_safe
    return innertext
  end


  filterrific(
    available_filters: [
      :search_query,
      :with_subject_id,
      :with_test_id,
      :with_training_id,
      :no_question_sets
    ]
  )


  scope :search_query, ->(query) {
    # Searches the students table on the 'first_name' and 'last_name' columns.
    # Matches using LIKE, automatically appends '%' to each term.
    # LIKE is case INsensitive with MySQL, however it is case
    # sensitive with PostGreSQL. To make it work in both worlds,
    # we downcase everything.
    return nil  if query.blank?

    query = query.to_s
    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)

    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      ("%" + e.tr("*", "%") + "%").gsub(/%+/, "%")
    }
    num_or_conditions = 1
    where(
      terms.map {
        or_clauses = [
          "LOWER(cbt_questions.name) LIKE ?"
        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten,
      )
      .order("length(cbt_questions.name) asc")
  }

  scope :with_subject_id, ->(subject_ids) {
    where(cbt_questions: {cbt_subject_id: [*subject_ids]})
  }

  scope :with_test_id, ->(test_ids) {
    where("id in (SELECT cbt_question_id FROM cbt_question_set_questions cqsq JOIN cbt_test_question_sets tqs ON tqs.cbt_questions_set_id = cqsq.cbt_questions_set_id where tqs.deleted_at is NULL and tqs.deleted_at is NULL and tqs.cbt_test_id in (#{test_ids}))")
  }

  scope :with_training_id, ->(training_id) {
    where("id in (SELECT cbt_question_id FROM cbt_question_set_questions cqsq JOIN cbt_test_question_sets tqs ON tqs.cbt_questions_set_id = cqsq.cbt_questions_set_id join cbt_training_tests ctt ON ctt.cbt_test_id = tqs.cbt_test_id where tqs.deleted_at is NULL and tqs.deleted_at is NULL and ctt.deleted_at is NULL and ctt.cbt_training_id in (#{training_id}))")
  }

  scope :no_question_sets, ->(no_question_sets) {
    if no_question_sets.to_i == 1
      where("id not in (SELECT cbt_question_id FROM public.cbt_question_set_questions where deleted_at is NULL)")
    end
  }

end
