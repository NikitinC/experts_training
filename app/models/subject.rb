class Subject < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  validates  :code,
             :name,
             :short_name,
             :presence => true

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations

  before_save do
    self.fiscode = self.fisgia11code if !fisgia11code.nil?
    self.fiscode = self.fisgia9code if !fisgia9code.nil?
  end

  def subject_name
    name
  end


  def self.search(search)
    if search
      where('name || cast(code as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  def to_s
    short_name
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  def self.options_for_select
    order("sort_by").map { |e| ["#{e.name}", e.code] }
  end

  def self.options_for_select_id
    order("sort_by").map { |e| ["#{e.name}", e.id] }
  end
  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
