class CbtQuestionsSet < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  validates  :name,
             :presence => true

  #=============================VALIDATION END=================================

  belongs_to :cbt_subject

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations

  has_rich_text :description_text

  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%")
    end
  end

  def formatted_body
    innertext = Richtext::CodeBlocks::HtmlService.render(self.description_text.to_s).html_safe
    rescue innertext = Richtext::CodeBlocks::HtmlService.latex(innertext).html_safe
    innertext
  end

  [:help_pdf_file].each do |scan_field|
    mount_uploader scan_field, ScansUploader
  end


  def to_s
    name
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
