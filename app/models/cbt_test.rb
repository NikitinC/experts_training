class CbtTest < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  validates  :name,
             :presence => true

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations


  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%")
    end
  end

  def to_s
    name
  end



  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  def simple_format( text )
    return text if ( text =~ /(<table.*>)/ ) # return text unchanged

    start_tag = '<p>'
    text = text.to_s.dup
    text.gsub!(/\r\n?/, "\n")    # \r\n and \r => \n
    text.gsub!(/\n\n+/, "</p>\n\n#{start_tag}")  # 2+ newline  => paragraph
    text.gsub!(/([^\n]\n)(?=[^\n])/, '\1<br />')  # 1 newline   => br
    text.insert 0, start_tag
    text << "</p>"
  end

  def self.options_for_select
    order("started_at").map { |e| ["#{e.name}", e.id] }
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

  [:manual].each do |scan_field|
    mount_uploader scan_field, ScansUploader
  end

  has_rich_text :manual_text

  def formatted_body
    innertext = Richtext::CodeBlocks::HtmlService.render(self.manual_text.to_s).html_safe
    innertext = Richtext::CodeBlocks::HtmlService.latex(innertext).html_safe
    return innertext
  end

end
