class CbtUserTest < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  #============================VALIDATION START================================


  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations
  belongs_to :cbt_test
  belongs_to :user

  def self.search(search)
    if search
      where('cast(id as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  def to_s
    "#{self.cbt_test.to_s} #{self.user.first_name}"
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

end
