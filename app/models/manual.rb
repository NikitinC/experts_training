class Manual < ApplicationRecord
  has_rich_text :manual_text

  def to_s
    "#{self.model}:#{self.sort_by}"
  end

  def self.search(search)
    if search
      where('model LIKE ?', "%#{search}%")
    end
  end

  def formatted_body
    innertext = Richtext::CodeBlocks::HtmlService.render(self.manual_text.to_s).html_safe
    # rescue innertext = Richtext::CodeBlocks::HtmlService.latex(innertext).html_safe
    innertext
  end

end
