class CbtTraining < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  after_save :recreate_delayed_versions!

  def recreate_delayed_versions!
    title_image.recreate_versions!(:thumb,:preview)
  end


  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  validates  :name, :title_image,
             :presence => true

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations
  belongs_to :cbt_subject
  belongs_to :subject

  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%")
    end
  end

  def to_s
    name
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20

  [:title_image, :certificate].each do |scan_field|
    mount_uploader scan_field, ScansUploader
  end

  has_rich_text :manual_text

  def formatted_body
    innertext = Richtext::CodeBlocks::HtmlService.render(self.manual_text.to_s).html_safe
    innertext = Richtext::CodeBlocks::HtmlService.latex(innertext).html_safe
    return innertext
  end

  def self.options_for_select
    order("started_at").map { |e| ["#{e.name}", e.id] }
  end


end
