class ExpertsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_experts
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      expert = Item.find_by_id(row["id"]) || Item.new
      expert.attributes = row.to_hash
      expert
    end
  end

  def imported_experts
    @imported_experts ||= load_imported_experts
  end

  def save
    if imported_experts.map(&:valid?).all?
      imported_experts.each(&:save!)
      true
    else
      imported_experts.each_with_index do |expert, index|
        expert.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

  def stophone(s)
    s = s.gsub(/[^0-9]/, '')
    if s.length > 10
      s = s[-10..-1] || s
    end
    s = "+7(#{s[0..2]})#{s[3..5]}-#{s[6..9]}"
    return s
  end

  def importing(file, current_user)
    result = ''
    if 1 == 1
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)

        if xlsx.sheets.count == 2

          # Импорт экспертов
          attribute_list = ['surname', 'name', 'second_name', 'code', '-', '-', '-', '-', '-', '-', '-']
          if xlsx.sheet(0).row(1) == ['Фамилия', 'Имя', 'Отчество', 'Код эксперта', 'Работа на ЕГЭ (Да/Нет)', 'ОГЭ. Предмет №1', 'ОГЭ. Предмет №2', 'ОГЭ. Предмет №3', 'ЕГЭ. Предмет №1', 'ЕГЭ. Предмет №2', 'Привлекается к работе в конфликтной комиссии (Да/Нет)'] or xlsx.sheet(0).row(1) == ['Фамилия', 'Имя', 'Отчество', 'Код эксперта', 'Работа на ЕГЭ (Да/Нет)', 'ОГЭ. Предмет №1', 'ОГЭ. Предмет №2', 'ОГЭ. Предмет №3', 'ЕГЭ. Предмет №1', 'ЕГЭ. Предмет №2', 'Привлекается к работе в конфликтной комиссии (Да/Нет)', 'Логин', 'Пароль']
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              j = 0
              m = Expert.where("cast(code as bigint) = #{xlsx.sheet(0).row(i)[0].to_i}").first
              sozdali = false
              if m.nil?
                m = Expert.new
                sozdali = true
              end

              # is_ege
              if "#{xlsx.sheet(0).row(i)[4]}".upcase == 'ДА'
                m.is_ege = true
              else
                m.is_ege = false
              end
              # subject1_subject_code
              sj = Subject.where("upper(name) = '#{xlsx.sheet(0).row(i)[5]}'".upcase).first
              if !sj.nil?
                m.subject1_subject_code = sj.code
              else
                m.subject1_subject_code = nil
              end
              # subject2_subject_code
              sj = Subject.where("upper(name) = '#{xlsx.sheet(0).row(i)[6]}'".upcase).first
              if !sj.nil?
                m.subject2_subject_code = sj.code
              else
                m.subject2_subject_code = nil
              end
              # subject3_subject_code
              sj = Subject.where("upper(name) = '#{xlsx.sheet(0).row(i)[7]}'".upcase).first
              if !sj.nil?
                m.subject3_subject_code = sj.code
              else
                m.subject3_subject_code = nil
              end
              # subject1_ege_subject_code
              sj = Subject.where("upper(name) = '#{xlsx.sheet(0).row(i)[8]}'".upcase).first
              if !sj.nil?
                m.subject1_ege_subject_code = sj.code
              else
                m.subject1_ege_subject_code = nil
              end
              # subject2_ege_subject_code
              sj = Subject.where("upper(name) = '#{xlsx.sheet(0).row(i)[9]}'".upcase).first
              if !sj.nil?
                m.subject2_ege_subject_code = sj.code
              else
                m.subject2_ege_subject_code = nil
              end
              # expert_took_part_in_OGE_appeal_comission
              if "#{xlsx.sheet(0).row(i)[10].to_i}".upcase == 'ДА'
                m.took_part_in_OGE_appeal_comission = true
              else
                m.took_part_in_OGE_appeal_comission = false
              end

              while j<attribute_list.count
                if m.has_attribute?(attribute_list[j].to_sym)
                  m.set_property(attribute_list[j], xlsx.sheet(0).row(i)[j])
                end
                j = j + 1
              end

              begin
                if m.save
                  if sozdali == true
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, создали</td></tr>"
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, обновили</td></tr>"
                  end
                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось: "
                  m.errors.full_messages.each do |message|
                    result = result + "<li>#{message}</li>"
                  end
                  result = result + "</td></tr>"
                end

              rescue
                result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
              end

            end

          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
      return ("Файл не определён")
      end
    else
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на #{SiteInfo.find(1).support} с приложением файла.")
    end
  end


end