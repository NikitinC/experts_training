class StatisticsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_statistics
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      statistic = Item.find_by_id(row["id"]) || Item.new
      statistic.attributes = row.to_hash
      statistic
    end
  end

  def imported_statistics
    @imported_statistics ||= load_imported_statistics
  end

  def save
    if imported_statistics.map(&:valid?).all?
      imported_statistics.each(&:save!)
      true
    else
      imported_statistics.each_with_index do |statistic, index|
        statistic.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

  def stophone(s)
    s = s.gsub(/[^0-9]/, '')
    if s.length > 10
      s = s[-10..-1] || s
    end
    s = "+7(#{s[0..2]})#{s[3..5]}-#{s[6..9]}"
    return s
  end

  def importing(file, current_user)
    result = ''
    if 1 == 1
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)

        if xlsx.sheets.count == 1

          # Импорт онлайн-протоколоы
          attribute_list = ['name', 'short_name', 'sql_string_region', 'sql_string_mouo', 'sql_sting_ou', 'sql_string_ekb', 'description', 'is_active', 'object_columns', 'object_columns_width']
          if xlsx.sheet(0).row(1) == ['Код', 'Наименование', 'Краткое наименование', 'SQL-строка для региона', 'SQL-строка для муниципалитета', 'SQL-строка для школы', 'SQL-строка для Екб', 'Описание', 'Активно', 'Наименования столбцов', 'Ширина столбцов']
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              j = 0
              m = Statistic.where("cast(code as bigint) = #{xlsx.sheet(0).row(i)[0].to_i}").first
              sozdali = false
              if m.nil?
                m = Statistic.new
                sozdali = true
              end

              while j<attribute_list.count
                if m.has_attribute?(attribute_list[j].to_sym)
                  m.set_property(attribute_list[j], xlsx.sheet(0).row(i)[j])
                end
                j = j + 1
              end

              begin
                if m.save
                  if sozdali == true
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, создали</td></tr>"
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, обновили</td></tr>"
                  end
                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось: "
                  m.errors.full_messages.each do |message|
                    result = result + "<li>#{message}</li>"
                  end
                  result = result + "</td></tr>"
                end

              rescue
                result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
              end

            end

          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
      return ("Файл не определён")
      end
    else
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на #{SiteInfo.find(1).support} с приложением файла.")
    end
  end


end