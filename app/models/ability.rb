# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

    if user.admin?
      can :manage, :all
    else
      cannot :manage, :all
    end

    if user.expert_role? and !user.admin?
      exp = Expert.find(user.target_id)
      cannot :manage, :all
      subject_codes = []
      subject_codes << 0
      subject_codes << exp.subject1_subject_code if !exp.subject1_subject_code.nil?
      subject_codes << exp.subject2_subject_code if !exp.subject2_subject_code.nil?
      subject_codes << exp.subject3_subject_code if !exp.subject3_subject_code.nil?
      subject_codes << exp.subject1_ege_subject_code if !exp.subject1_ege_subject_code.nil?
      subject_codes << exp.subject2_ege_subject_code if !exp.subject2_ege_subject_code.nil?
      # puts ("here is subject_codes: #{subject_codes.inspect}")
      can [:manage], CbtTraining, :subject_id => Subject.where("code in (#{subject_codes.join(', ')})").map(&:id)
      can [:manage], CbtUserTestQuestion, :cbt_user_test_id => CbtUserTest.where(:user_id => user.id).map(&:id)
    end

  end

end



