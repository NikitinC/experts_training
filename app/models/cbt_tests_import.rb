class CbtTestsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_cbt_tests
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(5)
    (6..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      cbt_test = Item.find_by_id(row["id"]) || Item.new
      cbt_test.attributes = row.to_hash
      cbt_test
    end
  end

  def imported_cbt_tests
    @imported_cbt_tests ||= load_imported_cbt_tests
  end

  def save
    if imported_cbt_tests.map(&:valid?).all?
      imported_cbt_tests.each(&:save!)
      true
    else
      imported_cbt_tests.each_with_index do |cbt_test, index|
        cbt_test.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

  def stophone(s)
    s = s.gsub(/[^0-9]/, '')
    if s.length > 10
      s = s[-10..-1] || s
    end
    s = "+7(#{s[0..2]})#{s[3..5]}-#{s[6..9]}"
    return s
  end

  def importing(file, current_user)
    result = ''
    if 1 == 1
      if !file.nil?
        xlsx = Roo::Spreadsheet.open(file.tempfile)

        if xlsx.sheets.count == 1

          # Импорт онлайн-протоколоы
          attribute_list = ['-', 'name', 'description', 'manual', 'active', 'shuffle', 'time_limit', 'explore_correct_answers',
                            'explore_correct_answers', 'attempts_number', 'started_at', 'stopped_at', 'manual_text',
                            '-']
          if xlsx.sheet(0).row(1) == ['ID', 'Наименование', 'Краткое описание', 'Инструкции', 'Активный',
                                      'Перемешивать вопросы местами', 'Ограничение по времени',
                                      'Показывать в конце теста правильные/неправильные ответы',
                                      'Показывать правильные/неправильные ответы после закрытия теста',
                                      'Количество попыток', 'Дата-время открытия', 'Дата-время закрытия',
                                      'Полноформатное описание', 'Список тренингов']
            # 4 - active; 5 - shuffle; 7 - explore_correct_answers; 8 - explore_correct_answers_after_stopped;
            i = 1
            while i<xlsx.sheet(0).last_row
              i = i + 1
              j = 0
              m = CbtTest.where("cast(id as bigint) = #{xlsx.sheet(0).row(i)[0].to_i}").first
              sozdali = false

              if m.nil?
                m = CbtTest.new
                sozdali = true
              end

              while j<attribute_list.count
                if m.has_attribute?(attribute_list[j].to_sym)
                  if "#{xlsx.sheet(0).row(i)[j]}".squish.upcase == 'ДА'
                    m.set_property(attribute_list[j], true)
                  elsif "#{xlsx.sheet(0).row(i)[j]}".squish.upcase == 'НЕТ'
                    m.set_property(attribute_list[j], false)
                  else
                    m.set_property(attribute_list[j], xlsx.sheet(0).row(i)[j])
                  end
                end
                j = j + 1
              end

              m.manual_text = m.simple_format("#{xlsx.sheet(0).row(i)[12]}")

              if 1 == 1
                if m.save
                  sql = "update cbt_tests set manual = '#{xlsx.sheet(0).row(i)[3]}' where id = #{m.id}"
                  ActiveRecord::Base.connection.execute (sql)
                  if sozdali == true
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, создали</td></tr>"
                  else
                    result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Удалось, обновили</td></tr>"
                  end
                else
                  result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось: "
                  m.errors.full_messages.each do |message|
                    result = result + "<li>#{message}</li>"
                  end
                  result = result + "</td></tr>"
                end

              else
                result = result + "<tr><td>#{i-1}</td><td>#{xlsx.sheet(0).row(i)[2]}</td><td>Не удалось на уровне базы!!: #{m}</td></tr>"
              end

            end

          end
          result = "<table class='table-stripped'><thead><th>Строка №</th><th> - - Запись - -</th><th>Результат загрузки</th>#{result}</table>"
          return result
        end
      return ("Файл не определён")
      end
    else
      return ("Файл вызвал общую ошибку загрузки на уровне базы данных. Сделайте заявку на #{SiteInfo.find(1).support} с приложением файла.")
    end
  end


end