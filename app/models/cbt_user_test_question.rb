class CbtUserTestQuestion < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations
  belongs_to :cbt_user_test
  belongs_to :cbt_question

  def self.search(search)
    if search
      where('cast(id as varchar) LIKE ?', "%#{search}%")
    end
  end

  def to_s
    name
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 1

  # set per_page globally
  WillPaginate.per_page = 1

end
