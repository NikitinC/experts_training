module FreeRussianValidator
  extend ActiveSupport::Concern

  class FreeRussianValidator < ActiveModel::EachValidator

    def validate_each(record, attribute, value)
      return if ExceptRecord.all.map(&:name).include? value

      unless value =~ /\A[а-яА-ЯёЁ0-9№\-\s\.'"\/]+\z/
        record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name"))
      end
    end
  end


end
