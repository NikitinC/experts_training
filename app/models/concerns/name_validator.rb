# coding: utf-8

module NameValidator
  extend ActiveSupport::Concern

  class NameValidator < ActiveModel::EachValidator

    def validate_each(record, attribute, value)
      return if ExceptRecord.all.map(&:name).include? value

      unless value =~ /\A[a-zA-Zа-яА-ЯёЁ\-\s\.']+\z/
        record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name"))
      end
    end
  end

end
