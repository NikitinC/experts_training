# coding: utf-8

module NamePartValidator
  extend ActiveSupport::Concern

  class NamePartValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      return if value == nil
      return if ExceptRecord.all.map(&:name).include? value

      words = value.split
      return record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name")) if
        (words.length < 2)

      words.each do |word|
        next if %w(уулу кызы оглы Уулу Кызы Оглы Кизи кизи).include? word
        parts = word.split '-'
        return record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name")) if
          (parts.length > 2) ||
            (parts[0] !~ /\A[А-ЯЁ][а-яё]+\z/) ||
            (parts.length == 2 && parts[1] !~ /\A[А-ЯЁ]?[а-яё]+\z/)
      end
    end
  end

end

