class Expert < ApplicationRecord
  acts_as_paranoid
  has_paper_trail

  include NamePartValidator

  #============================VALIDATION START================================
  # name, code, created_at, updated_at, short_name, fisgia9code, fisgia11code
  # validates  :ou_id,
  #            :presence => true
  #
  # SNILS_FORMAT_REGEXP = /\A(?:\d{3})(?:-\d{3}){2} \d\d\z/
  # validates :snils, format: {with: SNILS_FORMAT_REGEXP}, allow_blank: true
  # validates :snils, allow_blank: true, uniqueness: { scope: :deleted_at }, snils: true
  #
  # validates :document_number, format: {with: ->(object) { Regexp.new(object.document_type.number_mask) }}, if: :document_type
  # validates :document_series, format: {with: ->(object) { Regexp.new(object.document_type.series_mask) }}, if: :document_type
  #
  validates :surname, :name, :name_part => true
  validates :surname, :name_part => true, allow_blank: true

  # validates :document_number, uniqueness: {scope: [:deleted_at, :document_series, :document_type_code]}, unless: :hidden
  # validates :birthday, date: {before: Proc.new { Date.today }, after: Proc.new { Date.today - 100.year }}

  #=============================VALIDATION END=================================

  # has_many :ous
  # has_many :shedule_tasks
  # has_many :monitoring_objectivity_ates
  # has_many :appeal_stations

  # belongs_to :ou, optional: true
  # belongs_to :expert, optional: false
  # belongs_to :scientific_degrees, :foreign_key => :scientific_degree_code, :primary_key =>  :code, optional: true
  # belongs_to :expert_post, :foreign_key => :expert_post_code, :primary_key =>  :code, optional: true
  # belongs_to :expert_qualification, :foreign_key => :subject1_qualify_category, :primary_key =>  :code, optional: true
  # belongs_to :comission_job, :foreign_key => :subject1_comission_job, :primary_key =>  :code, optional: true
  # belongs_to :employee_category, :foreign_key => :subject1_category_code, :primary_key =>  :code, optional: true
  belongs_to :subject, :foreign_key => :subject1_subject_code, :primary_key =>  :code, optional: true
  # belongs_to :station_posts, :foreign_key => :expert_post_code, :primary_key =>  :code

  # belongs_to :human_settlement_type, :foreign_key => :passport_town_type, :primary_key =>  :code, optional: true
  # belongs_to :street_type, :foreign_key => :passport_street_type, :primary_key =>  :code, optional: true
  # belongs_to :building_type, :foreign_key => :passport_building_type, :primary_key =>  :code, optional: true
  #
  # belongs_to :document_type, :foreign_key => :document_type_code, :primary_key =>  :code
  # belongs_to :education, optional: true
  # # belongs_to :station_post, optional: true
  # belongs_to :station_post, :foreign_key => :station_post_code, :primary_key =>  :code, optional: true
  # belongs_to :station_post, :foreign_key => :station_post_code_oge, :primary_key =>  :code, optional: true

  # has_many :expert_ous, :inverse_of => :expert
  # accepts_nested_attributes_for :expert_ous

  # belongs_to :station_post_oge, optional: true

  # belongs_to :mouo, :foreign_key => :owner_mouo_id, :primary_key =>  :id, optional: true

  # has_many :expert_ous, :inverse_of => :expert
  # accepts_nested_attributes_for :expert_ous
  # has_many :exam_station_experts, :dependent => :destroy

  # [:diploma_scan, :diploma_fio_change_scan, :diploma_foreign_translate_scan, :inn_scan, :snils_scan,
  #  :bank_requizites_scan, :passport_fio_scan, :passport_registration_scan,
  #  :compensation_declaration_scan, :agreement_pdn_education_scan,
  #  :compensation_agreement_pdn_scan].each do |scan_field|
  #   mount_uploader scan_field, ScanUploader
  # end


  # def self.search(search)
  #   if search
  #     where('name || cast(surname as varchar(5)) LIKE ?', "%#{search}%")
  #   end
  # end


  def to_s
    "#{self.surname.to_s} #{self.name.to_s} #{self.surname.to_s}".strip
  end

  def set_property(prop_name, prop_value)
    self.send("#{prop_name}=",prop_value)
    # puts("#{prop_name} : #{prop_value}")
    # puts self.inspect
  end

  self.per_page = 20

  # set per_page globally
  WillPaginate.per_page = 20


  filterrific(
    default_filter_params: { experts_sorted_by: 'Фамилия_asc' },
    available_filters: [
      :experts_sorted_by,
      :search_query,
      :with_ou_id,
      :with_mouo_id,
      :with_subject_id
    ]
  )


  scope :search_query, ->(query) {
    # Searches the experts table on the 'name' and 'last_name' columns.
    # Matches using LIKE, automatically appends '%' to each term.
    # LIKE is case INsensitive with MySQL, however it is case
    # sensitive with PostGreSQL. To make it work in both worlds,
    # we downcase everything.
    return nil  if query.blank?

    query = query.to_s
    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)

    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.tr("*", "%") + "%").gsub(/%+/, "%")
    }
    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conditions = 12
    where(
      terms.map {
        or_clauses = [
          "LOWER(experts.surname) LIKE ?",
          "LOWER(experts.name) LIKE ?",
          "LOWER(experts.second_name) LIKE ?",
          "LOWER(experts.surname) || ' ' || LOWER(experts.name) || ' ' || LOWER(experts.second_name) LIKE ?",
          "LOWER(experts.surname) || ' ' || LOWER(experts.second_name) || ' ' || LOWER(experts.name) LIKE ?",
          "LOWER(experts.name) || ' ' || LOWER(experts.surname) || ' ' || LOWER(experts.second_name) LIKE ?",
          "LOWER(experts.name) || ' ' || LOWER(experts.second_name) || ' ' || LOWER(experts.surname) LIKE ?",
          "LOWER(experts.second_name) || ' ' || LOWER(experts.surname) || ' ' || LOWER(experts.name) LIKE ?",
          "LOWER(experts.second_name) || ' ' || LOWER(experts.name) || ' ' || LOWER(experts.surname) LIKE ?",
          "LOWER(experts.document_series) || ' ' || LOWER(experts.document_number) LIKE ?",
          "LOWER(experts.document_number) || ' ' || LOWER(experts.document_series) LIKE ?",
          "LOWER(experts.snils) LIKE ?"

        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }

  scope :experts_sorted_by, ->(sort_option) {
    # Sorts experts by sort_option
    direction = /desc$/.match?(sort_option) ? "desc" : "asc"
    case sort_option.to_s
    when /^Фамилия/
      order("lower(experts.surname) #{direction}")
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :with_ou_id, ->(ou_ids) {
    # Filters school_classes with any of the given school_profile_ids
    where(ou_id: [*ou_ids])
  }

  scope :with_mouo_id, ->(mouo_ids) {
    # Filters school_classes with any of the given school_profile_ids
    where(ous: {mouo_id: [*mouo_ids]}).joins("join ous ON ous.id = experts.ou_id")
  }

  scope :with_subject_id, ->(with_subject_id) {
    # Filters school_classes with any of the given school_profile_ids
    # where(expert_ous: {subject_id: [*subject_ids]}).joins("join expert_ous eo ON eo.expert_id = experts.id join ous ON ous.id = eo.ou_id")
    where("1 = 1")
  }

  scope :created_at_gte, ->(reference_time) {
    where("experts.created_at >= ?", reference_time)
  }

  # This method provides select options for the `sorted_by` filter select input.
  # It is called in the controller as part of `initialize_filterrific`.
  def self.options_for_sorted_by
    [
      ["Фамилия (a-z)", "Фамилия_asc"],
    ]
  end

  def birthday_formatted_date
    if !self.birthday.nil?
      return self.birthday.strftime("%d.%m.%Y")
    else
      return "01.01.1955"
    end
  end




end
