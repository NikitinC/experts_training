class ScanUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  PICTURE_EXTENTIONS = %w(pdf tiff tif jpg jpeg png)

  def extension_white_list
    PICTURE_EXTENTIONS.map{|x| x.gsub('.', '')}
  end

  configure do |config|
    config.remove_previously_stored_files_after_update = false
  end

  def store_dir
    id = model.id.to_s.rjust 9, '0'
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{id[0..2]}/#{id[3..5]}/#{id[6..8]}"
  end

  # process :convert => :tif, :if => :is_picture?

  # version :preview do
  #   begin
  #     process :resize_to_limit => [200, 200], :convert => :png
  #   rescue
  #     puts ("ok")
  #   end
  # end

  def remove!
    with_callbacks(:remove) do
      @file = nil
      @cache_id = nil
    end
  end

  # def filename
  #   if @filename
  #     extension = is_picture?(@filename) ? ".tif" : File.extname(@filename)
  #     random_token = Digest::SHA2.hexdigest(Guid.new.to_s).first(20)
  #     ivar = "@#{mounted_as}_secure_token"
  #     token = model.instance_variable_get(ivar)
  #     token ||= model.instance_variable_set(ivar, random_token)
  #     "#{token}#{extension}"
  #   end
  # end

  def filename

    if !file.nil?
      if File.exist?("#{Rails.public_path}/#{self.store_dir}/#{File.basename(file.path)}")
        return "#{File.basename(file.path)}"
      else
        return "#{File.basename(file.path)}" if original_filename.present?
      end
    else
      return nil
    end

  end

  protected

  def is_picture? x
    file_name = x.is_a?(String) ? x : x.filename
    ScanUploader::PICTURE_EXTENTIONS.include?(File.extname(file_name).downcase)
  end
end
