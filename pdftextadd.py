# -*- coding: utf-8 -*-
# Внешний интерфейс для генерации сертификата
# Для работы требует установленный Arial.ttf, либо его в исполняемой папке
# Получает: 
# python3 pdftextadd.py bura_rcoko.pdf destination.pdf 'Никитину Сергею Васильевичу',320,350,'center',18,'BeerMoney','#BCA669'+'Старший эксперт',360,215,'center',18,'Arial','#BCA669'+'Информатика и ИКТ',350,290,'center',18,'Arial','#000000'+'75',350,255,'center',18,'Arial','#000000'
#  path - это исходный PDF, на который надо накладывать текст
#  destination - как назвать итоговый PDF с уже наложенным текстом
#  Далее следует текст в виде массива через запятую:
#  Текст, x, y, 'расположение center или left или right', размер шрифта, ttf-шрифт

from reportlab.lib.colors import HexColor
from PyPDF2 import PdfWriter, PdfReader
from reportlab.lib.pagesizes import A4, landscape
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.pdfbase.pdfmetrics import stringWidth
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from sys import argv


def get_info(path):
    with open(path, 'rb') as f:
        pdf = PdfReader(f)
        info = pdf.getDocumentInfo()
        number_of_pages = pdf.getNumPages()
        print(info)

packet = io.BytesIO()
# новый канвас PDF with Reportlab
can = canvas.Canvas(packet, pagesize=landscape(A4))

for a in argv[3].split('+'):
    aa = a.split(',')
    pdfmetrics.registerFont(TTFont(aa[5],aa[5] + '.ttf'))
    can.setFont(aa[5], float(aa[4]))
    can.setFillColor(HexColor(aa[6]))
    text_width = stringWidth(aa[1], aa[5], float(aa[4]))
    if aa[3] == 'left':
        can.drawString(float(aa[1]), float(aa[2]), aa[0])
    if aa[3] == 'center':
        can.drawString(float(aa[1])-text_width/2.0, float(aa[2]), aa[0])
    if aa[3] == 'right':
        can.drawString(float(aa[1])-text_width, float(aa[2]), aa[0])   
can.save()
packet.seek(0)

# create a new PDF with Reportlab
new_pdf = PdfReader(packet)
# read your existing PDF
existing_pdf = PdfReader(open(argv[1], "rb"))
output = PdfWriter()
# add the "watermark" (which is the new pdf) on the existing page
page = existing_pdf.pages[0]
page.merge_page(new_pdf.pages[0])
output.add_page(page)
# finally, write "output" to a real file
outputStream = open(argv[2], "wb")
output.write(outputStream)
outputStream.close()
