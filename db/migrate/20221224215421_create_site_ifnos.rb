class CreateSiteIfnos < ActiveRecord::Migration[7.0]
  def change
    create_table :site_infos do |t|
      t.boolean :is_close
      t.string :close_ips
      t.string :open_name
      t.string :close_name
      t.string :host
      t.string :email
      t.string :email_password
      t.string :copyright
      t.string :support
      t.string :region_name
      t.boolean :is_help
      t.boolean :is_condition
      t.timestamps
    end
  end
end
