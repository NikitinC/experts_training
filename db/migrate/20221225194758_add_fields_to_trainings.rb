class AddFieldsToTrainings < ActiveRecord::Migration[7.0]
  def change
    add_column :cbt_trainings, :for_observers, :boolean
  end
end

# Тренинг для экспертов ЕГЭ по русскому языку (по материалам 2022 года)