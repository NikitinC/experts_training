class AddCertificateToCbtTrainings < ActiveRecord::Migration[7.0]
  def change
    add_column :cbt_trainings, :certificate, :string
    add_column :cbt_trainings, :certificate_geometry, :text
  end
end
