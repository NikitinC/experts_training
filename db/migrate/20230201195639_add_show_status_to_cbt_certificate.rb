class AddShowStatusToCbtCertificate < ActiveRecord::Migration[7.0]
  def change
    add_column :cbt_trainings, :show_status, :boolean
  end
end
