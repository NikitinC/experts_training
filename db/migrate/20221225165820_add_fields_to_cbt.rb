class AddFieldsToCbt < ActiveRecord::Migration[7.0]
  def change
    add_column :cbt_questions, :shuffle, :boolean
    add_column :cbt_subjects, :border_osn, :integer
    add_column :cbt_subjects, :border_osn_error, :integer
    add_column :cbt_subjects, :border_st, :integer
    add_column :cbt_subjects, :border_st_error, :integer
    add_column :cbt_subjects, :border_ved, :integer
    add_column :cbt_subjects, :border_ved_error, :integer
    add_column :cbt_questions, :k_nums, :string
  end
end

# КИ по Русскому языку ЕГЭ 2022
# КИ РЯ ЕГЭ 2022