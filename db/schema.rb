# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_12_25_194758) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "dblink"
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "announcements", force: :cascade do |t|
    t.datetime "published_at", precision: nil
    t.string "announcement_type"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cbt_answer_types", force: :cascade do |t|
    t.string "name"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_cbt_answer_types_on_id", unique: true
  end

  create_table "cbt_question_set_questions", force: :cascade do |t|
    t.bigint "cbt_questions_set_id"
    t.bigint "cbt_question_id"
    t.time "time_limit"
    t.boolean "show_correct_answer"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cbt_question_id"], name: "index_cbt_question_set_questions_on_cbt_question_id"
    t.index ["cbt_questions_set_id"], name: "index_cbt_question_set_questions_on_cbt_questions_set_id"
    t.index ["id"], name: "index_cbt_question_set_questions_on_id", unique: true
  end

  create_table "cbt_question_types", force: :cascade do |t|
    t.string "name"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_cbt_question_types_on_id", unique: true
  end

  create_table "cbt_questions", force: :cascade do |t|
    t.string "name"
    t.bigint "cbt_subject_id"
    t.bigint "cbt_answer_type_id"
    t.bigint "cbt_question_type_id"
    t.text "settings"
    t.float "primary_mark"
    t.text "correct_answer"
    t.boolean "partial_mark"
    t.integer "next_cbt_question_id"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pdf_file"
    t.string "help_pdf_file"
    t.boolean "shuffle"
    t.string "k_nums"
    t.index ["cbt_answer_type_id"], name: "index_cbt_questions_on_cbt_answer_type_id"
    t.index ["cbt_question_type_id"], name: "index_cbt_questions_on_cbt_question_type_id"
    t.index ["cbt_subject_id"], name: "index_cbt_questions_on_cbt_subject_id"
    t.index ["id"], name: "index_cbt_questions_on_id", unique: true
  end

  create_table "cbt_questions_sets", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.boolean "shuffle"
    t.bigint "cbt_subject_id"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "help_pdf_file"
    t.index ["cbt_subject_id"], name: "index_cbt_questions_sets_on_cbt_subject_id"
    t.index ["id"], name: "index_cbt_questions_sets_on_id", unique: true
  end

  create_table "cbt_subjects", force: :cascade do |t|
    t.integer "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at", precision: nil
    t.integer "fiscode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "border_osn"
    t.integer "border_osn_error"
    t.integer "border_st"
    t.integer "border_st_error"
    t.integer "border_ved"
    t.integer "border_ved_error"
    t.index ["code"], name: "index_cbt_subjects_on_code"
  end

  create_table "cbt_test_question_sets", force: :cascade do |t|
    t.string "name"
    t.bigint "cbt_test_id"
    t.bigint "cbt_questions_set_id"
    t.integer "sort_by"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cbt_questions_set_id"], name: "index_cbt_test_question_sets_on_cbt_questions_set_id"
    t.index ["cbt_test_id"], name: "index_cbt_test_question_sets_on_cbt_test_id"
    t.index ["id"], name: "index_cbt_test_question_sets_on_id", unique: true
  end

  create_table "cbt_tests", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "manual"
    t.boolean "active"
    t.boolean "shuffle"
    t.time "time_limit"
    t.boolean "explore_correct_answers"
    t.boolean "explore_correct_answers_after_stopped"
    t.integer "attempts_number"
    t.datetime "started_at", precision: nil
    t.datetime "stopped_at", precision: nil
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_cbt_tests_on_id", unique: true
  end

  create_table "cbt_training_tests", force: :cascade do |t|
    t.bigint "cbt_test_id", null: false
    t.bigint "cbt_training_id", null: false
    t.integer "sort_by"
    t.boolean "mandatory"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cbt_test_id"], name: "index_cbt_training_tests_on_cbt_test_id"
    t.index ["cbt_training_id"], name: "index_cbt_training_tests_on_cbt_training_id"
  end

  create_table "cbt_trainings", force: :cascade do |t|
    t.string "name"
    t.bigint "cbt_subject_id", null: false
    t.bigint "subject_id", null: false
    t.string "title_image"
    t.boolean "active"
    t.boolean "for_students"
    t.boolean "for_employees"
    t.boolean "for_experts"
    t.integer "cbt_certificate_id"
    t.datetime "started_at", precision: nil
    t.datetime "stopped_at", precision: nil
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "for_observers"
    t.index ["cbt_subject_id"], name: "index_cbt_trainings_on_cbt_subject_id"
    t.index ["subject_id"], name: "index_cbt_trainings_on_subject_id"
  end

  create_table "cbt_user_test_questions", force: :cascade do |t|
    t.bigint "cbt_user_test_id"
    t.bigint "cbt_question_id"
    t.datetime "start_time", precision: nil
    t.datetime "end_time", precision: nil
    t.text "user_answer"
    t.float "primary_mark"
    t.boolean "marked"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sort_by"
    t.string "comment"
    t.index ["cbt_question_id"], name: "index_cbt_user_test_questions_on_cbt_question_id"
    t.index ["cbt_user_test_id"], name: "index_cbt_user_test_questions_on_cbt_user_test_id"
    t.index ["id"], name: "index_cbt_user_test_questions_on_id", unique: true
  end

  create_table "cbt_user_tests", force: :cascade do |t|
    t.bigint "cbt_test_id"
    t.bigint "user_id"
    t.datetime "start_time", precision: nil
    t.datetime "end_time", precision: nil
    t.float "primary_mark"
    t.float "mark100"
    t.text "description"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cbt_test_id"], name: "index_cbt_user_tests_on_cbt_test_id"
    t.index ["id"], name: "index_cbt_user_tests_on_id", unique: true
    t.index ["user_id"], name: "index_cbt_user_tests_on_user_id"
  end

  create_table "except_records", force: :cascade do |t|
    t.integer "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "experts", force: :cascade do |t|
    t.string "surname"
    t.string "name"
    t.string "second_name"
    t.string "document_series"
    t.string "document_number"
    t.integer "birthyear"
    t.boolean "gender"
    t.integer "code"
    t.integer "education_code"
    t.integer "scientific_degree_code"
    t.string "qualification"
    t.integer "ou_id"
    t.string "job_place"
    t.string "job_position"
    t.integer "stage"
    t.boolean "took_part_in_OGE_previous_year"
    t.boolean "took_part_in_OGE_appeal_comission"
    t.integer "subject1_subject_code"
    t.integer "subject1_category_code"
    t.boolean "subject1_third"
    t.boolean "subject1_voice"
    t.integer "subject2_subject_code"
    t.integer "subject2_category_code"
    t.boolean "subject2_third"
    t.boolean "subject2_voice"
    t.integer "subject3_subject_code"
    t.integer "subject3_category_code"
    t.boolean "subject3_third"
    t.boolean "subject3_voice"
    t.integer "user"
    t.string "email"
    t.string "guid", limit: 42
    t.text "diploma_surname"
    t.text "diploma_series"
    t.text "diploma_number"
    t.text "diploma_institute"
    t.text "diploma_scan"
    t.text "diploma_fio_change_scan"
    t.text "diploma_foreign_translate_scan"
    t.text "bank_requizites_bank"
    t.text "bank_requizites_address"
    t.text "bank_requizites_inn"
    t.text "bank_requizites_kpp"
    t.text "bank_requizites_kor"
    t.text "bank_requizites_bik"
    t.text "bank_requizites_beneficiary_account"
    t.text "bank_requizites_beneficiar"
    t.text "bank_requizites_scan"
    t.text "inn"
    t.text "inn_scan"
    t.text "snils_scan"
    t.text "passport_registration"
    t.text "passport_fio_scan"
    t.text "passport_registration_scan"
    t.text "compensation_declaration_scan"
    t.text "compensation_number"
    t.text "agreement_pdn_education_scan"
    t.text "compensation_agreement_pdn_scan"
    t.text "phone"
    t.text "snils"
    t.text "passport_country"
    t.text "passport_postcode"
    t.integer "passport_region"
    t.text "passport_rajon"
    t.integer "passport_town_type"
    t.text "passport_town"
    t.integer "passport_street_type"
    t.text "passport_street"
    t.integer "passport_building_type"
    t.text "passport_building"
    t.text "passport_building_part"
    t.text "passport_flat"
    t.text "passport_get_date"
    t.text "passport_get_place"
    t.text "passport_department_code"
    t.text "account_number_ubrir"
    t.text "account_number_sberbank"
    t.text "statement_start_date"
    t.text "statement_end_date"
    t.integer "document_type_code"
    t.string "hidden"
    t.integer "subject1_qualify_category"
    t.integer "subject1_comission_stage"
    t.integer "subject1_comission_job"
    t.integer "subject1_level_up_year"
    t.integer "subject2_qualify_category"
    t.integer "subject2_comission_stage"
    t.integer "subject2_comission_job"
    t.integer "subject2_level_up_year"
    t.integer "subject3_qualify_category"
    t.integer "subject3_comission_stage"
    t.integer "subject3_comission_job"
    t.integer "subject3_level_up_year"
    t.integer "user_id"
    t.datetime "deleted_at", precision: nil
    t.date "birthday"
    t.string "online_login"
    t.string "online_password"
    t.boolean "is_ege"
    t.integer "subject1_ege_qualify_category"
    t.integer "subject1_ege_comission_stage"
    t.integer "subject1_ege_comission_job"
    t.integer "subject1_ege_level_up_year"
    t.integer "subject1_ege_subject_code"
    t.integer "subject1_ege_category_code"
    t.boolean "subject1_ege_third"
    t.boolean "subject1_ege_voice"
    t.integer "subject2_ege_qualify_category"
    t.integer "subject2_ege_comission_stage"
    t.integer "subject2_ege_comission_job"
    t.integer "subject2_ege_level_up_year"
    t.integer "subject2_ege_subject_code"
    t.integer "subject2_ege_category_code"
    t.boolean "subject2_ege_third"
    t.boolean "subject2_ege_voice"
    t.integer "school_worker_id"
    t.index ["id"], name: "index_experts_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at", precision: nil
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "manuals", force: :cascade do |t|
    t.string "model"
    t.string "url"
    t.integer "sort_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.string "recipient_type", null: false
    t.bigint "recipient_id", null: false
    t.string "type", null: false
    t.jsonb "params"
    t.datetime "read_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["read_at"], name: "index_notifications_on_read_at"
    t.index ["recipient_type", "recipient_id"], name: "index_notifications_on_recipient"
  end

  create_table "services", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "provider"
    t.string "uid"
    t.string "access_token"
    t.string "access_token_secret"
    t.string "refresh_token"
    t.datetime "expires_at", precision: nil
    t.text "auth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_services_on_user_id"
  end

  create_table "site_infos", force: :cascade do |t|
    t.boolean "is_close"
    t.string "close_ips"
    t.string "open_name"
    t.string "close_name"
    t.string "host"
    t.string "email"
    t.string "email_password"
    t.string "copyright"
    t.string "support"
    t.string "region_name"
    t.boolean "is_help"
    t.boolean "is_condition"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "station_additional_features", force: :cascade do |t|
    t.integer "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at", precision: nil
  end

  create_table "statistics", force: :cascade do |t|
    t.string "name"
    t.string "short_name"
    t.text "sql_string_region"
    t.text "sql_string_mouo"
    t.text "sql_sting_ou"
    t.text "sql_string_ekb"
    t.text "description"
    t.boolean "is_active"
    t.string "object_columns"
    t.string "object_columns_width"
    t.datetime "deleted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.integer "code"
    t.string "name"
    t.string "short_name"
    t.integer "fisgia9code"
    t.integer "fisgia11code"
    t.integer "sort_by"
    t.datetime "deleted_at", precision: nil
    t.integer "fiscode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_subjects_on_code"
  end

  create_table "tables", force: :cascade do |t|
    t.integer "columns", default: 1
    t.integer "rows", default: 1
    t.json "data", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.string "first_name"
    t.string "last_name"
    t.datetime "announcements_last_read_at", precision: nil
    t.boolean "admin", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "target_type"
    t.integer "target_id"
    t.boolean "superadmin_role", default: false
    t.boolean "mouo_role", default: false
    t.boolean "ous_role", default: false
    t.boolean "expert_role", default: false
    t.boolean "dpo_role", default: false
    t.boolean "blank_role", default: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at", precision: nil
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "cbt_training_tests", "cbt_tests"
  add_foreign_key "cbt_training_tests", "cbt_trainings"
  add_foreign_key "cbt_trainings", "cbt_subjects"
  add_foreign_key "cbt_trainings", "subjects"
  add_foreign_key "services", "users"
end
