get '/isi_monitorings/export', to: 'isi_monitorings#export'
get '/isi_monitorings/modaledit/:id', to: 'isi_monitorings#modaledit'
resources :isi_monitorings
resources :isi_monitorings_imports, only: [:new, :create, :index]