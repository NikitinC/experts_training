require 'sidekiq/web'
# root 'pages#home'
# skip_before_action :authenticate_user!, :only => [:index]

Rails.application.routes.draw do
  resources :destroy_reasons
  # resources :expert_qualifications
  # resources :comission_jobs
  # resources :exam_students
  # resources :registration_places
  resources :except_records
  resources :groups
  resources :download_counts
  resources :shedule_tasks
  resources :shedules
  resources :manuals
  # resources :ates


  concern :downloadable do
    resources :download_counts, only: :create
  end


  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  namespace :admin do
    resources :users
    resources :announcements
    resources :notifications
    resources :services
    root to: "users#index"
  end

  get '/20bdd15b60814bc7b9d87eee7af08cfe/:id', to: 'home#protocol'
  get '/privacy', to: 'home#privacy'
  get '/mouo_statistics', to: 'home#mouo_statistics'
  get '/help', to: 'home#help'
  get '/h5p', to: 'home#h5p'
  get '/terms', to: 'home#terms'
  get '/mssql', to: 'home#mssql'
  get '/recreateemployees', to: 'home#recreateemployees'

  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  # resources :destroy_reasons, only: :index
  get '/destroy_reasons/export', to: 'destroy_reasons#export'
  get '/destroy_reasons/modaledit/:id', to: 'destroy_reasons#modaledit'
  resources :destroy_reasons
  resources :destroy_reasons_imports, only: [:new, :create, :index]

  resources :notifications, only: [:index]
  resources :announcements, only: [:index]

  resources :show_blanks, only: [:index, :show]
  # resources :statistics, only: [:index, :show]
  resources :youtube, only: :show
  resources :tables, only: [:show, :create, :update]

  get '/ates/export', to: 'ates#export'
  get '/ates/modaledit/:id', to: 'ates#modaledit'
  resources :ates
  resources :ates_imports, only: [:new, :create, :index]

  get '/business_entity_types/export', to: 'business_entity_types#export'
  get '/business_entity_types/modaledit/:id', to: 'business_entity_types#modaledit'
  resources :business_entity_types
  resources :business_entity_types_imports, only: [:new, :create, :index]

  get '/human_settlement_types/export', to: 'human_settlement_types#export'
  get '/human_settlement_types/modaledit/:id', to: 'human_settlement_types#modaledit'
  resources :human_settlement_types
  resources :human_settlement_types_imports, only: [:new, :create, :index]

  get '/town_types/export', to: 'town_types#export'
  get '/town_types/modaledit/:id', to: 'town_types#modaledit'
  resources :town_types
  resources :town_types_imports, only: [:new, :create, :index]

  get '/street_types/export', to: 'street_types#export'
  get '/street_types/modaledit/:id', to: 'street_types#modaledit'
  resources :street_types
  resources :street_types_imports, only: [:new, :create, :index]

  get '/building_types/export', to: 'building_types#export'
  get '/building_types/modaledit/:id', to: 'building_types#modaledit'
  resources :building_types
  resources :building_types_imports, only: [:new, :create, :index]

  get '/mouos/export', to: 'mouos#export'
  get '/mouos/modaledit/:id', to: 'mouos#modaledit'
  get '/mouos/create_logins_for_all_mouos', to: 'mouos#create_logins_for_all_mouos'
  resources :mouos
  resources :mouos_imports, only: [:new, :create, :index]

  get '/registration_places/export', to: 'registration_places#export'
  get '/registration_places/modaledit/:id', to: 'registration_places#modaledit'
  resources :registration_places
  resources :registration_places_imports, only: [:new, :create, :index]

  get '/ou_types/export', to: 'ou_types#export'
  get '/ou_types/modaledit/:id', to: 'ou_types#modaledit'
  resources :ou_types
  resources :ou_types_imports, only: [:new, :create, :index]

  get '/ou_kinds/export', to: 'ou_kinds#export'
  get '/ou_kinds/modaledit/:id', to: 'ou_kinds#modaledit'
  resources :ou_kinds
  resources :ou_kinds_imports, only: [:new, :create, :index]

  get '/ous/export', to: 'ous#export'
  get '/ous/create_logins_for_all_ous', to: 'ous#create_logins_for_all_ous'
  get '/ous/get_ous_by_mouo', to: 'ous#get_ous_by_mouo'
  get '/ous/modaledit/:id', to: 'ous#modaledit'
  resources :ous
  resources :ous_imports, only: [:new, :create, :index]

  get '/school_class_profiles/export', to: 'school_class_profiles#export'
  get '/school_class_profiles/modaledit/:id', to: 'school_class_profiles#modaledit'
  resources :school_class_profiles
  resources :school_class_profiles_imports, only: [:new, :create, :index]

  get '/groups/export', to: 'groups#export'
  get '/groups/modaledit/:id', to: 'groups#modaledit'
  resources :groups
  resources :groups_imports, only: [:new, :create, :index]

  get '/citizenships/export', to: 'citizenships#export'
  get '/citizenships/modaledit/:id', to: 'citizenships#modaledit'
  resources :citizenships
  resources :citizenships_imports, only: [:new, :create, :index]

  get '/document_types/export', to: 'document_types#export'
  get '/document_types/modaledit/:id', to: 'document_types#modaledit'
  resources :document_types
  resources :document_types_imports, only: [:new, :create, :index]

  get '/education_document_types/export', to: 'education_document_types#export'
  get '/education_document_types/modaledit/:id', to: 'education_document_types#modaledit'
  resources :education_document_types
  resources :education_document_types_imports, only: [:new, :create, :index]

  get '/limited_facilities_groups/export', to: 'limited_facilities_groups#export'
  get '/limited_facilities_groups/modaledit/:id', to: 'limited_facilities_groups#modaledit'
  resources :limited_facilities_groups
  resources :limited_facilities_groups_imports, only: [:new, :create, :index]

  get '/regions/export', to: 'regions#export'
  get '/regions/modaledit/:id', to: 'regions#modaledit'
  resources :regions
  resources :regions_imports, only: [:new, :create, :index]

  get '/gek_issues/export', to: 'gek_issues#export'
  get '/gek_issues/modaledit/:id', to: 'gek_issues#modaledit'
  resources :gek_issues
  resources :gek_issues_imports, only: [:new, :create, :index]

  get '/study_forms/export', to: 'study_forms#export'
  get '/study_forms/modaledit/:id', to: 'study_forms#modaledit'
  resources :study_forms
  resources :study_forms_imports, only: [:new, :create, :index]

  get '/attestation_forms/export', to: 'attestation_forms#export'
  get '/attestation_forms/modaledit/:id', to: 'attestation_forms#modaledit'
  resources :attestation_forms
  resources :attestation_forms_imports, only: [:new, :create, :index]

  get '/special_seatings/export', to: 'special_seatings#export'
  get '/special_seatings/modaledit/:id', to: 'special_seatings#modaledit'
  resources :special_seatings
  resources :special_seatings_imports, only: [:new, :create, :index]

  get '/ege_participant_categories/export', to: 'ege_participant_categories#export'
  get '/ege_participant_categories/modaledit/:id', to: 'ege_participant_categories#modaledit'
  resources :ege_participant_categories
  resources :ege_participant_categories_imports, only: [:new, :create, :index]

  get '/exam_students/exam_students_vyverka', to: 'exam_students#exam_students_vyverka'
  resources :exam_students

  get '/students/set_gia_admission_to_all', to: 'students#set_gia_admission_to_all'
  get '/students/export', to: 'students#export'
  get '/students/modaledit/:id', to: 'students#modaledit'
  get '/students/name_norming', to: 'students#name_norming'
  get '/student/exam_students', to: 'exam_students#index'
  get 'new_student_exam_student', to: 'exam_students#new'
  get '/students/create_logins_for_all_students', to: 'students#create_logins_for_all_students'
  resources :students
  resources :students_imports, only: [:new, :create, :index]
  post 'students/:id', to: 'students#show'

  get '/composition_types/export', to: 'composition_types#export'
  get '/composition_types/modaledit/:id', to: 'composition_types#modaledit'
  resources :composition_types
  resources :composition_types_imports, only: [:new, :create, :index]

  get '/compositions/export', to: 'compositions#export'
  get '/compositions/modaledit/:id', to: 'compositions#modaledit'
  resources :compositions
  resources :compositions_imports, only: [:new, :create, :index]

  get '/subjects/export', to: 'subjects#export'
  get '/subjects/modaledit/:id', to: 'subjects#modaledit'
  resources :subjects
  resources :subjects_imports, only: [:new, :create, :index]

  get '/exams/export', to: 'exams#export'
  get '/exams/modaledit/:id', to: 'exams#modaledit'
  resources :exams
  resources :exams_imports, only: [:new, :create, :index]

  get '/exam_waves/export', to: 'exam_waves#export'
  get '/exam_waves/modaledit/:id', to: 'exam_waves#modaledit'
  resources :exam_waves
  resources :exam_waves_imports, only: [:new, :create, :index]

  get '/exam_types/export', to: 'exam_types#export'
  get '/exam_types/modaledit/:id', to: 'exam_types#modaledit'
  resources :exam_types
  resources :exam_types_imports, only: [:new, :create, :index]

  get '/except_records/export', to: 'except_records#export'
  get '/except_records/modaledit/:id', to: 'except_records#modaledit'
  resources :except_records
  resources :except_records_imports, only: [:new, :create, :index]

  get '/ege_statements/new_ege', to: 'ege_statements#new_ege'
  get '/ege_statements/new_oge', to: 'ege_statements#new_oge'
  get '/ege_statements/new_gve9', to: 'ege_statements#new_gve9'
  get '/ege_statements/new_gve11', to: 'ege_statements#new_gve11'
  get '/ege_statements/export', to: 'ege_statements#export'
  get '/ege_statements/modaledit/:id', to: 'ege_statements#modaledit'
  get '/ege_statements/:id/import', to: 'ege_statements#import'
  get '/ege_statements/:id/import_create_student', to: 'ege_statements#import_create_student'
  get '/ege_statements/import_create_exams', to: 'ege_statements#import_create_exams'
  get '/ege_statements/import_stop_edit', to: 'ege_statements#import_stop_edit'


  resources :ege_statements
  resources :ege_statements_imports, only: [:new, :create, :index]

  get '/lks_ege_statements/:id/import', to: 'lks_ege_statements#import'
  get '/lks_ege_statements/:id/import_create_student', to: 'lks_ege_statements#import_create_student'

  # get '/lks_ege_statements/edit_student', to: 'lks_ege_statements#edit_student'
  # get '/lks_ege_statements/update_student', to: 'lks_ege_statements#update_student'
  # get '/lks_ege_statements/create_student', to: 'lks_ege_statements#create_student'
  get '/lks_ege_statements/import_create_exams', to: 'lks_ege_statements#import_create_exams'
  get '/lks_ege_statements/import_stop_edit', to: 'lks_ege_statements#import_stop_edit'
  resources :lks_ege_statements
  # resources :lks_ege_statements [:new, :create, :index, :import]


  get '/gve11_statements/export', to: 'gve11_statements#export'
  get '/gve11_statements/modaledit/:id', to: 'gve11_statements#modaledit'
  resources :gve11_statements
  resources :gve11_statements_imports, only: [:new, :create, :index]

  get '/gve9_statements/export', to: 'gve9_statements#export'
  get '/gve9_statements/modaledit/:id', to: 'gve9_statements#modaledit'
  resources :gve9_statements
  resources :gve9_statements_imports, only: [:new, :create, :index]

  get '/oge_statements/export', to: 'oge_statements#export'
  get '/oge_statements/modaledit/:id', to: 'oge_statements#modaledit'
  resources :oge_statements
  resources :oge_statements_imports, only: [:new, :create, :index]
  
  get '/students_mass_compositions/export_isi_vyverka', to: 'students_mass_compositions#export_isi_vyverka'
  get '/students_mass_compositions/export_is_01', to: 'students_mass_compositions#export_is_01'
  get '/students_mass_compositions/export_is_02', to: 'students_mass_compositions#export_is_02'
  get '/students_mass_compositions/export_is_03', to: 'students_mass_compositions#export_is_03'
  get '/students_mass_compositions/export_is_04', to: 'students_mass_compositions#export_is_04'
  get '/students_mass_compositions/export_is_05', to: 'students_mass_compositions#export_is_05'
  get '/students_mass_compositions/export_is_06', to: 'students_mass_compositions#export_is_06'
  get '/students_mass_compositions/export_is_07', to: 'students_mass_compositions#export_is_07'
  get '/students_mass_compositions/export_is_08', to: 'students_mass_compositions#export_is_08'
  get '/students_mass_compositions/export_is_09', to: 'students_mass_compositions#export_is_09'


  get '/students_mass_compositions/export', to: 'students_mass_compositions#export'
  get '/students_mass_compositions/set_list', to: 'students_mass_compositions#set_list'
  get '/students_mass_compositions/delete_list', to: 'students_mass_compositions#delete_list'
  get '/students_mass_compositions/places_to_list', to: 'students_mass_compositions#places_to_list'
  get '/students_mass_compositions/place_to_own_school', to: 'students_mass_compositions#place_to_own_school'
  resources :students_mass_compositions, only: [:index]



  get '/students_mass_exams/export_exams_vyverka', to: 'students_mass_exams#export_exams_vyverka'
  get '/students_mass_exams/recreate_exams', to: 'students_mass_exams#recreate_exams'

  get '/students_mass_exams/export', to: 'students_mass_exams#export'
  get '/students_mass_exams/set_list', to: 'students_mass_exams#set_list'
  get '/students_mass_exams/delete_list', to: 'students_mass_exams#delete_list'
  get '/students_mass_exams/places_to_list', to: 'students_mass_exams#places_to_list'
  get '/students_mass_exams/place_to_own_school', to: 'students_mass_exams#place_to_own_school'
  resources :students_mass_exams, only: [:index]

  get '/timesheets/export', to: 'timesheets#export'
  resources :timesheets
  resources :school_worker_timesheets
  resources :school_worker_timesheet_kks

  get '/stations/export', to: 'stations#export'
  get '/stations/:id/timesheet', to: 'stations#timesheet'
  get '/stations/:id/timesheet_dop', to: 'stations#timesheet_dop'
  get '/stations/:id/station_workers_export', to: 'stations#station_workers_export'
  get '/stations/:id/station_students_export', to: 'stations#station_students_export'
  get '/stations/modaledit/:id', to: 'stations#modaledit'
  get '/stations/:id/set_auditoriums', to: 'stations#set_auditoriums'
  get '/stations/:id/set_station_workers', to: 'stations#set_station_workers'
  resources :stations
  resources :stations_imports, only: [:new, :create, :index]

  get '/classrooms/export', to: 'classrooms#export'
  get '/classrooms/modaledit/:id', to: 'classrooms#modaledit'
  resources :classrooms
  resources :classrooms_imports, only: [:new, :create, :index]

  get '/station_types/export', to: 'station_types#export'
  get '/station_types/modaledit/:id', to: 'station_types#modaledit'
  resources :station_types
  resources :station_types_imports, only: [:new, :create, :index]

  get '/station_additional_features/export', to: 'station_additional_features#export'
  get '/station_additional_features/modaledit/:id', to: 'station_additional_features#modaledit'
  resources :station_additional_features
  resources :station_additional_features_imports, only: [:new, :create, :index]

  get '/cctvs/export', to: 'cctvs#export'
  get '/cctvs/modaledit/:id', to: 'cctvs#modaledit'
  resources :cctvs
  resources :cctvs_imports, only: [:new, :create, :index]

  get '/employee_categories/export', to: 'employee_categories#export'
  get '/employee_categories/modaledit/:id', to: 'employee_categories#modaledit'
  resources :employee_categories
  resources :employee_categories_imports, only: [:new, :create, :index]


  get '/employee_posts/export', to: 'employee_posts#export'
  get '/employee_posts/modaledit/:id', to: 'employee_posts#modaledit'
  resources :employee_posts
  resources :employee_posts_imports, only: [:new, :create, :index]

  get '/station_posts/export', to: 'station_posts#export'
  get '/station_posts/modaledit/:id', to: 'station_posts#modaledit'
  resources :station_posts
  resources :station_posts_imports, only: [:new, :create, :index]

  get '/educations/export', to: 'educations#export'
  get '/educations/modaledit/:id', to: 'educations#modaledit'
  resources :educations
  resources :educations_imports, only: [:new, :create, :index]

  get '/employees/export', to: 'employees#export'
  get '/employees/modaledit/:id', to: 'employees#modaledit'
  get '/employees/subject_rus', to: 'employees#subject_rus'
  resources :employees do
    resources :employee_ous, only: [:index, :new]
    resources :exam_station_employees, only: [:index, :new]
    member do
      put :recover
    end
  end
  # resources :employees
  resources :employees_imports, only: [:new, :create, :index]
  resources :employee_ous, only: [:update, :destroy, :create]

  get '/school_workers/export', to: 'school_workers#export'
  get '/school_workers/:id/docs', to: 'school_workers#docs'
  get '/school_workers/recreate', to: 'school_workers#recreate'
  resources :school_workers
  resources :school_workers_imports, only: [:new, :create, :index]


  get '/experts/export', to: 'experts#export'
  get '/experts/modaledit/:id', to: 'experts#modaledit'
  get '/experts/subject_rus', to: 'experts#subject_rus'
  get '/experts/create_users_for_experts', to: 'experts#create_users_for_experts'
  resources :experts
  resources :experts_imports, only: [:new, :create, :index]

  get '/study_levels/export', to: 'study_levels#export'
  get '/study_levels/modaledit/:id', to: 'study_levels#modaledit'
  resources :study_levels
  resources :study_levels_imports, only: [:new, :create, :index]



  get '/employees/export', to: 'employees#export'
  get '/employees/modaledit/:id', to: 'employees#modaledit'
  resources :employees
  resources :employees_imports, only: [:new, :create, :index]
  post 'employees/:id', to: 'employees#show'


  get '/scientific_degrees/export', to: 'scientific_degrees#export'
  get '/scientific_degrees/modaledit/:id', to: 'scientific_degrees#modaledit'
  resources :scientific_degrees
  resources :scientific_degrees_imports, only: [:new, :create, :index]

  get '/comission_jobs/export', to: 'comission_jobs#export'
  get '/comission_jobs/modaledit/:id', to: 'comission_jobs#modaledit'
  resources :comission_jobs
  resources :comission_jobs_imports, only: [:new, :create, :index]

  get '/expert_qualifications/export', to: 'expert_qualifications#export'
  get '/expert_qualifications/modaledit/:id', to: 'expert_qualifications#modaledit'
  resources :expert_qualifications
  resources :expert_qualifications_imports, only: [:new, :create, :index]

  get 'analytics', to: 'analytics#index'
  get '/analytics/export', to: 'analytics#export'
  get 'analytics/rcoi/(:alert_type)', to: 'analytics#rcoi_statistics'
  get 'analytics/mouo/(:alert_type)', to: 'analytics#mouo_statistics'

  get '/results/sverka', to: 'results#sverka'
  get '/results/no_blanks', to: 'results#no_blanks'
  get '/results/export', to: 'results#export'
  get '/results/modaledit/:id', to: 'results#modaledit'
  resources :results
  resources :results_imports, only: [:new, :create, :index]

  get '/limit_seatings/export', to: 'limit_seatings#export'
  get '/limit_seatings/modaledit/:id', to: 'limit_seatings#modaledit'
  resources :limit_seatings
  resources :limit_seatings_imports, only: [:new, :create, :index]

  get '/transfers/export', to: 'transfers#export'
  post '/transfers/:id', to: 'transfers#confirm'
  get '/transfers/modaledit/:id', to: 'transfers#modaledit'
  resources :transfers
  resources :transfers_imports, only: [:new, :create, :index]

  get '/analytic_tasks/export', to: 'analytic_tasks#export'
  get '/analytic_tasks/modaledit/:id', to: 'analytic_tasks#modaledit'
  resources :analytic_tasks
  resources :analytic_tasks_imports, only: [:new, :create, :index]

  get '/statistics/export', to: 'statistics#export'
  get '/statistics/saogia9', to: 'statistics#saogia9'
  get '/statistics/:id/export', to: 'statistics#export'
  resources :statistics
  resources :statistics_imports, only: [:new, :create, :index]

  get '/downloads/:filename', to: 'downloads#file', :constraints => { :filename => /[^\/]+/ }
  resources :downloads, only: [:index, :file]


  # CBTs
  #
  # cbt_answer_types
  # cbt_subjects
  # cbt_question_types
  # cbt_questions_sets
  # cbt_questions
  # cbt_tests 
  # cbt_test_question_sets
  # cbt_question_set_questions
  # cbt_user_tests
  # cbt_user_test_questions
  #

  get '/cbt_answer_types/export', to: 'cbt_answer_types#export'
  get '/cbt_answer_types/modaledit/:id', to: 'cbt_answer_types#modaledit'
  resources :cbt_answer_types
  resources :cbt_answer_types_imports, only: [:new, :create, :index]
  
  get '/cbt_subjects/export', to: 'cbt_subjects#export'
  get '/cbt_subjects/modaledit/:id', to: 'cbt_subjects#modaledit'
  resources :cbt_subjects
  resources :cbt_subjects_imports, only: [:new, :create, :index]
  
  get '/cbt_question_types/export', to: 'cbt_question_types#export'
  get '/cbt_question_types/modaledit/:id', to: 'cbt_question_types#modaledit'
  resources :cbt_question_types
  resources :cbt_question_types_imports, only: [:new, :create, :index]
  
  get '/cbt_questions_sets/export', to: 'cbt_questions_sets#export'
  get '/cbt_questions_sets/modaledit/:id', to: 'cbt_questions_sets#modaledit'
  resources :cbt_questions_sets
  resources :cbt_questions_sets_imports, only: [:new, :create, :index]
  
  get '/cbt_questions/export', to: 'cbt_questions#export'
  get '/cbt_questions/modaledit/:id', to: 'cbt_questions#modaledit'
  get '/cbt_questions/:id/preview', to: 'cbt_questions#preview'
  resources :cbt_questions
  resources :cbt_questions_imports, only: [:new, :create, :index]
  
  get '/cbt_tests/export', to: 'cbt_tests#export'
  get '/cbt_tests/modaledit/:id', to: 'cbt_tests#modaledit'
  resources :cbt_tests
  resources :cbt_tests_imports, only: [:new, :create, :index]
  
  get '/cbt_test_question_sets/export', to: 'cbt_test_question_sets#export'
  get '/cbt_test_question_sets/modaledit/:id', to: 'cbt_test_question_sets#modaledit'
  resources :cbt_test_question_sets
  resources :cbt_test_question_sets_imports, only: [:new, :create, :index]
  
  get '/cbt_question_set_questions/export', to: 'cbt_question_set_questions#export'
  get '/cbt_question_set_questions/modaledit/:id', to: 'cbt_question_set_questions#modaledit'
  resources :cbt_question_set_questions
  resources :cbt_question_set_questions_imports, only: [:new, :create, :index]

  get '/cbt_user_tests/export', to: 'cbt_user_tests#export'
  get '/cbt_user_tests/modaledit/:id', to: 'cbt_user_tests#modaledit'
  resources :cbt_user_tests
  resources :cbt_user_tests_imports, only: [:new, :create, :index]

  get '/cbt_user_test_questions/export', to: 'cbt_user_test_questions#export'
  get '/cbt_user_test_questions/modaledit/:id', to: 'cbt_user_test_questions#modaledit'
  resources :cbt_user_test_questions
  resources :cbt_user_test_questions_imports, only: [:new, :create, :index]

  get '/cbt_training_tests/export', to: 'cbt_training_tests#export'
  get '/cbt_training_tests/modaledit/:id', to: 'cbt_training_tests#modaledit'
  resources :cbt_training_tests
  resources :cbt_training_tests_imports, only: [:new, :create, :index]

  get '/cbt_trainings/export', to: 'cbt_trainings#export'
  get '/cbt_trainings/:id/addtests', to: 'cbt_trainings#addtests'
  get '/cbt_trainings/modaledit/:id', to: 'cbt_trainings#modaledit'
  resources :cbt_trainings
  resources :cbt_trainings_imports, only: [:new, :create, :index]

  resources :user_permissions

  get '/cbt_home/test/:id', to: 'cbt_home#test'
  get '/cbt_home/training/:id', to: 'cbt_home#training'
  get '/cbt_home/tests/', to: 'cbt_home#tests'
  get '/cbt_home/trainings/', to: 'cbt_home#trainings'
  get '/cbt_home/get_user_test/:id', to: 'cbt_home#get_user_test'
  get '/cbt_home/get_user_test_question/:id', to: 'cbt_home#get_user_test_question'
  get '/cbt_home/get_user_test_results/:id', to: 'cbt_home#get_user_test_results'
  get '/cbt_home/get_user_training_results/:id', to: 'cbt_home#get_user_training_results'
  resources :cbt_home, only: [:index]

  get '/interviews/export', to: 'interviews#export'
  get '/interviews/list', to: 'interviews#list'
  get '/interviews/modaledit/:id', to: 'interviews#modaledit'
  resources :interviews
  resources :interviews_imports, only: [:new, :create, :index]

  get '/ppois/export', to: 'ppois#export'
  get '/ppois/modaledit/:id', to: 'ppois#modaledit'
  resources :ppois
  resources :ppois_imports, only: [:new, :create, :index]

  get '/textbooks/export', to: 'textbooks#export'
  get '/textbooks/modaledit/:id', to: 'textbooks#modaledit'
  resources :textbooks
  resources :textbooks_imports, only: [:new, :create, :index]

  get '/task_specifications/export', to: 'task_specifications#export'
  resources :task_specifications
  resources :task_specifications_imports, only: [:new, :create]

  get '/task_recomendations/export', to: 'task_recomendations#export'
  resources :task_recomendations
  resources :task_recomendations_imports, only: [:new, :create]

  get '/answers/export', to: 'answers#export'
  get '/answers/sverka', to: 'answers#sverka'
  resources :answers
  resources :answers_imports, only: [:new, :create]

  get '/whatismyip', to: 'home#whatismyip'

  get '/process_conditions/export', to: 'process_conditions#export'
  resources :process_conditions
  resources :process_conditions_imports, only: [:new, :create]

  get '/show_result/:id', to: 'home#show_result'
  get '/appeal/:id', to: 'home#appeal'
  get '/vvv', to: 'home#vvv'

  get '/appeals/export', to: 'appeals#export'
  resources :appeals
  resources :appeals_imports, only: [:new, :create]
  resources :appeals_imports_uvs, only: [:index, :new, :create]

  get '/appeal_conditions/export', to: 'appeal_conditions#export'
  resources :appeal_conditions
  resources :appeal_conditions_imports, only: [:new, :create]

  get '/appeal_types/export', to: 'appeal_types#export'
  resources :appeal_types
  resources :appeal_types_imports, only: [:new, :create]

  get '/appeal_stations/export', to: 'appeal_stations#export'
  get '/appeal_stations/:id/timesheet', to: 'appeal_stations#timesheet'
  get '/appeal_stations/:id/timesheet_gia9', to: 'appeal_stations#timesheet_gia9'
  resources :appeal_stations
  resources :appeal_stations_imports, only: [:new, :create]

  get '/verification_conditions/export', to: 'verification_conditions#export'
  get '/verification_conditions/modaledit/:id', to: 'verification_conditions#modaledit'
  resources :verification_conditions
  resources :verification_conditions_imports, only: [:new, :create, :index]

  get '/verifications/export', to: 'verifications#export'
  get '/verifications/modaledit/:id', to: 'verifications#modaledit'
  resources :verifications
  resources :verifications_imports, only: [:new, :create, :index]

  get '/observers/export', to: 'observers#export'
  resources :observers

  get '/online_expertizes/get_protocol/:id', to: 'online_expertizes#get_protocol'
  get '/online_expertizes/protocols_ended', to: 'online_expertizes#protocols_ended'
  get '/online_expertizes/protocols', to: 'online_expertizes#protocols'
  get '/online_expertizes/export', to: 'online_expertizes#export'
  get '/online_expertizes/modaledit/:id', to: 'online_expertizes#modaledit'
  get '/online_expertizes/sverka', to: 'online_expertizes#sverka'
  resources :online_expertizes
  resources :online_expertizes_imports, only: [:new, :create, :index]

  get '/online_expertize_statuses/export', to: 'online_expertize_statuses#export'
  get '/online_expertize_statuses/modaledit/:id', to: 'online_expertize_statuses#modaledit'
  resources :online_expertize_statuses
  resources :online_expertize_statuses_imports, only: [:new, :create, :index]

  get '/conflict_comission_posts/export', to: 'conflict_comission_posts#export'
  resources :conflict_comission_posts

  get '/conflict_comission_workers/export', to: 'conflict_comission_workers#export'
  resources :conflict_comission_workers

  get '/timesheet_kks/export', to: 'timesheet_kks#export'
  resources :timesheet_kks

  get '/group_textbooks/export', to: 'group_textbooks#export'
  resources :group_textbooks

  get '/ikt_barcodes/export', to: 'ikt_barcodes#export'
  resources :ikt_barcodes
  resources :ikt_barcodes_imports, only: [:new, :create, :index]

  get '/students_subjects/export', to: 'students_subjects#export'
  get '/students_subjects/modaledit/:id', to: 'students_subjects#modaledit'
  resources :students_subjects
  resources :students_subjects_imports, only: [:new, :create, :index]

  get '/isi_monitorings/export', to: 'isi_monitorings#export'
  get '/isi_monitorings/modaledit/:id', to: 'isi_monitorings#modaledit'
  resources :isi_monitorings
  resources :isi_monitorings_imports, only: [:new, :create, :index]

  resources :statements
  resources :statements_imports, only: [:new, :create, :index]
  get '/statements/:id/cmp', to: 'statements#cmp'
  get '/statements/:id/receive', to: 'statements#receive'

  resources :show_blanks, only: [:index, :show]
  get '/show_blank/:id', to: 'home#show_blank'

  resources :site_infos

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks", :registrations => "registrations"}
  # devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}
  # root :to => redirect("/users/sign_in")
  # devise_for :users, :controllers => {}

  root :to => 'home#index'

  get '*path' => redirect('/')
  #
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
