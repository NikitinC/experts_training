Warden::Strategies.add(:student_auth) do
  def valid?
    # code here to check whether to try and authenticate using this strategy;
    return true
  end

  def authenticate!
    # code here for doing authentication;
    # if successful, call
    begin
      # puts("params_first_name: #{params['user']['first_name']}")
      first_name = params['user']['first_name']
      users = User.where(:first_name => first_name)
      # puts (params[:user][:password])
      f = false
      users.each do |user|
        # puts("user_id: #{user.inspect}")
        # puts(user.valid_password?(params[:user][:password]))
        if user.valid_password?(params[:user][:password])
          f = true
          success!(user)
        end
      end
      if f == false
        fail!('Пользователь с указанными данными не найден')
      end
    rescue
      fail!('Требуется аутентификация')
    end

    # puts("params: #{params.inspect}")
    # fail!('не удалось')
    # success!(resource) # where resource is the whatever you've authenticated, e.g. user;
    # if fail, call
    # fail!(message) # where message is the failure message
  end
end
