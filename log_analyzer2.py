import re

def process_log_file(file_path):
    with open(file_path, 'r') as file:
        log_content = file.read()

    # Split the log content into individual error entries
    error_entries = re.split(r'={30,}', log_content)

    processed_entries = []

    for entry in error_entries:
        # Remove the unique identifier [6750b2b1-98ef-4073-83fc-e3179b04dd50]
        entry = re.sub(r'\[([\w-]+)\]', '', entry)

        # Remove the timestamp [2023-05-24T16:31:55.838078 #2728122]
        entry = re.sub(r'\[\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+ #\d+\]', '', entry)

        # Skip the first 3 lines of each entry
        entry_lines = entry.split('\n')[3:]
        entry = '\n'.join(entry_lines)

        processed_entries.append(entry.strip())

    # Group the processed error entries and count the occurrences
    error_counts = {}
    for entry in processed_entries:
        if entry in error_counts:
            error_counts[entry] += 1
        else:
            error_counts[entry] = 1

    return error_counts

# Usage example
log_file_path = 'log.txt'
errors = process_log_file(log_file_path)

# Print the grouped errors and their counts
for error, count in errors.items():
    print(f"Error: {error}\nCount: {count}\n{'='*30}")
