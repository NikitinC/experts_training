source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.2'

gem 'listen', '~> 3.3'
# # Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
# gem 'rails', '~> 6.1.3'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails', '~> 7.0.4'
# Use postgresql asjquery the database for Active Record
gem 'pg', '~> 1.1'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

gem 'liquid'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false
gem 'recaptcha', require: 'recaptcha/rails'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end


group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'administrate', github: 'excid3/administrate', branch: 'jumpstart'
gem 'bootstrap', '~> 4.5'
gem 'devise', '~> 4.7', '>= 4.7.1'
gem 'devise-bootstrapped', github: 'excid3/devise-bootstrapped', branch: 'bootstrap4'
gem 'devise_masquerade', '~> 1.2'
gem 'font-awesome-sass', '~> 5.13'
gem 'friendly_id', '~> 5.3'
gem 'image_processing'
gem 'mini_magick', '~> 4.10', '>= 4.10.1'
gem 'name_of_person', '~> 1.1'
gem 'noticed', '~> 1.2'
gem 'omniauth-facebook', '~> 6.0'
gem 'omniauth-github', '~> 1.4'
gem 'omniauth-twitter', '~> 1.4'
gem 'pundit', '~> 2.1'
gem 'redis', '~> 4.2', '>= 4.2.2'
gem 'sidekiq', '~> 6.0', '>= 6.0.3'
gem 'sitemap_generator', '~> 6.1', '>= 6.1.2'
gem 'whenever', require: false

gem "haml-rails", "~> 2.0"
# gem 'rails_admin', github: 'sferik/rails_admin'
gem 'rails_admin', '~> 3.0'

gem 'cancancan'
gem "rails_admin_import", "~> 2.2"
gem "roo"
gem "roo-xls"
gem 'will_paginate'
gem 'jquery-rails'
gem 'pg_search'
gem 'bootstrap-icons-helper'
gem 'filterrific'
gem 'caxlsx'
gem 'caxlsx_rails'

#apt-get install libreoffice -qq
gem 'libreconv'
gem 'poppler'

gem 'jquery_mask_rails'

# apt-get -y install imagemagick
gem "activestorage-office-previewer"

gem "best_in_place", git: "https://github.com/mmotherwell/best_in_place"
# gem "best_in_place", git: "https://github.com/josefsj3m/best_in_place.git"

gem "chartkick"
# yarn add chartkick chart.js

# gem 'pdfjs_viewer-rails'
gem 'pdfjs_rails'

# gem "bootstrap-table-rails"

gem 'bootstrap-datepicker-rails'

gem 'capistrano', '~> 3.11'
gem 'capistrano-rails', '~> 1.4'
gem 'capistrano-passenger', '~> 0.2.0'
gem 'capistrano-rbenv', '~> 2.1', '>= 2.1.4'

gem 'katex', '~> 0.8.0'

gem 'acts_as_paranoid'
gem 'paper_trail'
gem 'composite_primary_keys'

gem 'dropzonejs-rails'
gem 'carrierwave'

gem 'axlsx_styler'

gem 'barby'
gem 'chunky_png'
gem 'resque'
# gem 'barkick'

gem "hotwire-rails", "~> 0.1.3"
# gem 'validates_russian'
gem 'date_validator'
gem 'snils', require: 'snils/rails'

gem 'ed25519'
gem 'bcrypt_pbkdf'


# gem 'newrelic_rpm'
# gem 'newrelic-infinite_tracing'

gem 'activerecord-import'
# gem 'tiny_tds'

# gem 'mp3_player'

# gem 'simple_form'


gem 'wicked_pdf', '=1.4.0'
# gem 'wkhtmltopdf-binary'


gem 'net-smtp', require: false
gem 'net-imap', require: false
gem 'net-pop', require: false

gem "petrovich"
gem 'phonelib'