module DraftManager
  def self.included base
    base.class_exec do
      extend BaseClassMethods
    end
  end

  module BaseClassMethods
    def has_drafts
      include DraftManager::InstanceMethods
      extend DraftManager::ClassMethods
      attr_accessor :draft_id
      attr_accessible :draft_id
      after_create :clear_draft
    end
  end

  module InstanceMethods
    def save_draft user
      return false unless self.new_record?
      draft = Draft.find_by_id(self.draft_id) || Draft.new
      attrs = self.attributes.clone.delete_if { |key, value| self.class.accessible_attributes.deny?(key) }
      cache_attrs = {}
      attrs.each { |attr, val| cache_attrs[attr + "_cache"] = self.send(attr + "_cache") if self.send(attr).is_a?(CarrierWave::Uploader::Base) }
      draft.data = attrs.merge(cache_attrs)
      draft.name = self.to_s
      draft.target_type = self.class.name
      draft.user_id = user.id
      res = draft.save
      self.draft_id = draft.id if res
      res
    end

    private

    def clear_draft
      draft = Draft.find_by_id self.draft_id
      draft.destroy if draft
    end
  end

  module ClassMethods
    def from_draft data
      draft = data.is_a?(Draft) ? data : Draft.find(data)
      target = self.new draft.data
      target.draft_id = draft.id
      target.valid?
      target
    end

    def drafts user
      Draft.where(:user_id => user.id, :target_type => self.name)
    end
  end

end