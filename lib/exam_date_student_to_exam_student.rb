def start
  file = File.open('log/ExamDateStudent_to_ExamStudent.log', 'w')
  count = ExamDateStudent.count
  errors = 0
  offset = 0
  step = 100
  (count / step + 1).times do
    puts "Start package #{offset + 1} - #{offset + step}"
    file.puts "Start package #{offset + 1} - #{offset + step} \n"
    ExamDateStudent.limit(step).offset(offset).each_with_index do |eds, i|
      exam_name = eds.exams.first
      exams = []
      exams = Exam.where(:subject_code => eds.get_exam_code(eds.exams.first), :date => eds.exam_date.date) if (!exam_name.blank?)
      if (exams.any?)
        exam = exams.first
        puts "Exam_params ========================= :subject_code => " + eds.get_exam_code(eds.exams.first).to_s + ", :date => " + eds.exam_date.date.to_s + ", id " + eds.id.to_s
        file.puts "Exam_params ========================= :subject_code => " + eds.get_exam_code(eds.exams.first).to_s + ", :date => " + eds.exam_date.date.to_s + ", eds id = " + eds.id.to_s + "\n"

        e = ExamStudent.new(:exam_id => exam.id,
                            :points_of_hundred => eds.points_of_hundred,
                            :option_number => eds.option_number,
                            :points_of_five => eds.points_of_five, :rating => eds.rating,
                            :mask_a => eds.mask_a, :mask_b => eds.mask_b, :mask_c => eds.mask_c,
                            :primary_point => eds.primary_point, :variant => eds.variant)
        e.student_id = eds.student_id

        if e.save
          puts "Done #{offset + i + 1} / #{count}"
          file.puts "Done #{offset + i + 1 } / #{count} \n"
        else
          puts "Error #{e.errors.inspect}"
          file.puts "Error #{e.errors.inspect} \n"
          errors += 1
        end
      end
    end

    offset += 100
  end

  puts "Errors: #{errors}"
  file.puts "Errors: #{errors}"
end