module ActiveRecordLogger
  @target_models = []

  def self.target_models
    @target_models
  end

  def self.included(base)
    base.class_exec do
      extend ClassMethods
    end

    base.class.class_exec do
      attr_reader :rlog_fields
      attr_reader :recoverable
    end
  end

  module InstanceMethods
    private

    def prepare_rlog_item
      self.log = RlogItem.new
      self.log.user = ActiveRecord::Base.current_user
      self.log.ip = ActiveRecord::Base.current_request ? ActiveRecord::Base.current_request.remote_ip : "rails console"
      self.log.record = self
      self.log.name = self.to_s
      @changes = ActiveSupport::HashWithIndifferentAccess.new
      self.changes.each do |k, v|
        @changes[k] = [v.first] if self.class.rlog_fields.include?(k.to_sym) and v[0] != v[1]
      end
    end

    def save_rlog_item
      if @changes.present? #|| self.log.hack
        @changes.each do |k, v|
          @changes[k].push self.send(k)
        end
        self.log.record_changes = @changes
        self.log.save
      end
    end

    #def permission_check
    #  if ActiveRecord::Base.current_user && !Ability.new(ActiveRecord::Base.current_user).can?(:update, self)
    #    self.log.hack = true
    #  end
    #end
  end

  module ClassMethods
    def log_changes(options = {})
      @recoverable = options[:recoverable].nil? ? true : options[:recoverable]
      include ActiveRecordLogger::InstanceMethods
      before_save :prepare_rlog_item
      #after_save :permission_check
      after_save :save_rlog_item

      has_many :rlog_items, :as => :record
      attr_accessor :log

      ActiveRecordLogger.target_models.push self.name unless ActiveRecordLogger.target_models.include? self.name

      fields = self.attribute_names.map(&:to_sym)
      fields -= options[:expect].map(&:to_sym) if options[:expect]
      fields &= options[:only].map(&:to_sym) if options[:only]

      @rlog_fields = fields.map(&:to_sym)
    end
  end
end