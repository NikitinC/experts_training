class ActiveRecord::DestroyWithoutReason < Exception
end

module Moderable
  @target_models = []

  def self.target_models
    @target_models
  end

  def self.included base
    base.send :extend, ClassMethods
  end

  module ClassMethods
    def acts_as_moderable
      #if self.table_exists? && ActiveRecord::Base.connection.column_exists?(self.table_name, :hidden)
      cattr_accessor :dependent_options
      cattr_accessor :is_force
      attr_accessible :awake
      attr_reader :awake

      define_method :awake= do |v|
        @awake = v
        self.hidden = nil if @awake == "true"
      end

      self.dependent_options = {}
      self.is_force = []
      include Moderable::InstanceMethods

      self.instance_eval do
        def self.has_many(name, options={}, &extension)
          dependent = options.delete(:dependent)
          self.dependent_options[name] = dependent
          self.is_force.push name if dependent.to_s.include?("safe_destroy")
          super(name, options, &extension)
        end

        def self.has_one(name, options={})
          dependent = options.delete(:dependent)
          self.dependent_options[name] = dependent
          self.is_force.push name if dependent.to_s.include?("safe_destroy")
          super(name, options)
        end
      end
      default_scope where(:hidden => nil)
      scope :were_hidden, lambda { |hidden| unscoped { where(:hidden => hidden) } }
      Moderable.target_models.push self.name unless Moderable.target_models.include? self.name
      self.send :extend, Moderable::ModerableClassMethods
      #end
    end
  end

  module ModerableClassMethods
    def find_on_moderable_by_id id
      self.unscoped.find_by_id id
    end
  end

  module InstanceMethods
    def destroy(reason, force = false, target = nil)
      return false unless run_callbacks :destroy

      raise "Target not found" if reason == :recursive && target == nil #TODO: Right Exception!!!
      raise "Target is not needed" if reason != :recursive && target != nil #TODO: Right Exception!!!

      if reason.is_a? Integer or reason.is_a? String and !reason.blank?
        reason = DestroyReason.where(:code => reason).first
        reason_code = reason.code
      elsif reason.is_a? DestroyReason
        reason_code = reason.code
      elsif reason.is_a?(Symbol) && (reason == :recursive || reason == :permission_denied)
        reason_code = reason
      else
        reason = nil
      end
      raise ActiveRecord::DestroyWithoutReason.new unless reason

      self.hidden = "#{reason_code}:#{target.try :id}:#{target.try(:class).try(:name)}"

      transaction_procedures = []

      self.class.dependent_options.each do |name, key|
        dependent, options = [*key].first
        case dependent
          when :restrict
            transaction_procedures.push moderable_restrict(name, options)
          when :destroy
            transaction_procedures.push moderable_destroy(name, options)
          when :delete
            transaction_procedures.push moderable_delete(name, options)
          when :nullify
            transaction_procedures.push moderable_nullify(name, options)
          when :safe_destroy
            if force
              transaction_procedures.push moderable_destroy(name, options)
            else
              res = self.send(name)
              raise ActiveRecord::DeleteRestrictionError.new(name) if res && res.any?
            end
          when nil
          else
            raise "undefined dependment type #{key}"
        end
      end

      ActiveRecord::Base.transaction do
        transaction_procedures.compact.each do |proc|
          proc.() if proc.is_a? Proc
        end
        self.save(:validate => false)
      end
    end

    def recovery
      self.hidden = nil
      self.save
    end

    def killer
      unless self.hidden.blank?
        recursive, id, type = self.hidden.split(":")
        if recursive == "recursive"
          res = type.camelize.constantize.unscoped.find_by_id id
        end
        res
      end
    end

    private

    def moderable_restrict name, options
      raise ActiveRecord::DeleteRestrictionError.new(name) unless self.send(name).empty?
      nil
    end

    def moderable_destroy name, options
      -> do
        [*self.send(name)].each do |child|
          child.destroy(:recursive, options == :force, self)
        end
      end
    end

    def moderable_delete name, options
      raise "Moderable: dependent => :delete was not implemented"
      nil
    end

    def moderable_nullify name, options
      raise "Moderable: dependent => :nullify was not implemented"
      nil
    end
  end
end
