require 'unicode'

module Faker
  class RussianAddress < Base
    flexible :russian_address

    class << self
      def street_name
        %w(Восточная Ленина Малышева).sample
      end

      def city
        %w(Москва Питер Екатеринбург).sample
      end
    end
  end

  PhoneNumber.instance_eval do
    def typical_phone
      "(343) 2212123"
    end

    def student_phone
      "+7 (343) #{get_number 3}-#{get_number 2}-#{get_number 2}"
    end

    private
    def get_number digit
      rand(10**digit).to_s.rjust(digit, '0')
    end
  end

  Lorem.instance_eval do
    def russian_sentence_without_dot
      Unicode::capitalize(Faker::Lorem.sentence.chomp('.'))
    end
  end

  Internet.instance_eval do
    def simple_email
      names = %w(iraniandebris temperaturechangtse schemabiotic hopelessdeficient physicaljvm tailarete serverepsilon moisttwelve focussummer venogramcourses castrationmint awkproviding champagnequokka starvelingpochard trainjdk pickledhocuspocus clichechough poochblanc neuronleg rawsaa imagineedit tearfulposterior tenuouscause racialempty endscombustion)
      [names.sample, 'example.'+ %w[org com net].shuffle.first].join('@')
    end
  end

end