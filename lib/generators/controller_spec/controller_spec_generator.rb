class ControllerSpecGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  def create_rspec_controller_file
    template "controller_spec.erb", "spec/controllers/#{file_name.underscore}_controller_spec.rb"
  end

end
