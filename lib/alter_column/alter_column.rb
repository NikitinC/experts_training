# used to alter columns in postgresql
module AlterColumn
  def alter_column(table_name, column_name, new_type, mapping, default = nil)
    drop_default = %Q{ALTER TABLE #{table_name} ALTER COLUMN #{column_name} DROP DEFAULT;}
    execute(drop_default)
    # puts drop_default

    def prepare_value v
      return "NULL" if v.nil?
      return v if v.is_a?(FalseClass) || v.is_a?(TrueClass)
      "'" + "#{v}" + "'"
    end

    def prepare_key k
      return "ISNULL" if k.nil?
      return "= #{k}" if k.is_a?(FalseClass) || k.is_a?(TrueClass)
      "='" + "#{k}" + "'"
    end

    base = %Q{ALTER TABLE #{table_name} ALTER COLUMN #{column_name} TYPE #{new_type} }
    if mapping.kind_of?(Hash)
      contains_else = mapping.has_key?("else")
      else_mapping = mapping.delete("else")


      when_mapping = mapping.map do |k, v|
        "when #{column_name} #{prepare_key(k)} then #{prepare_value(v)}"
      end.join("\n")

      base += %Q{ USING CASE #{when_mapping} } unless when_mapping.blank?
      base += %Q{ ELSE #{else_mapping} } unless contains_else.blank?
      base += %Q{ END } if !when_mapping.blank? or !contains_else.blank?
    elsif mapping.kind_of?(String)
      base += mapping
    end
    base += ";"


    # puts base
    execute(base);

    unless default.blank?
      set_default = %Q{ALTER TABLE #{table_name} ALTER COLUMN #{column_name} SET DEFAULT #{prepare_value(default)};}
      execute(set_default)
      # puts set_default
    end
  end

  module_function :alter_column
end
