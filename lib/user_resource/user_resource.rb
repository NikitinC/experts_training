module UserResource
  def self.included base
    base.class_exec do
      extend BaseClassMethods
    end
  end

  module BaseClassMethods
    def acts_as_user_resource
      has_one :user, as: :target, autosave: true #TODO, :dependent => :safe_destroy
      has_many :users, as: :target, autosave: true #TODO, :dependent => :safe_destroy
    end
  end
end