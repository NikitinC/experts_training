module Filtering
  def self.included base
    base.class_exec do
      extend BaseClassMethods
    end
  end

  module BaseClassMethods
    def define_filtering &block
      extend ClassMethods
      @filtering_order = {}
      filtering_order = @filtering_order

      @filtering_fields = {}
      filtering_fields = @filtering_fields
      obj = Object.new
      klass = self

      obj.instance_eval do
        @filtering_fields = filtering_fields
        @filtering_order = filtering_order
        @klass = klass
      end
      obj.class_eval do

        def prepare_order name
          "#{@klass.table_name}.#{name.to_s}"
        end

        def where name, order = nil
          @filtering_order[name.to_sym] = prepare_order(order) unless order.nil?
          @filtering_fields[name.to_sym] = :where
        end

        def method_missing method_name, *args
          name = args[0]
          order = args[1]
          @filtering_order[name.to_sym] = prepare_order(order) unless order.nil?
          @filtering_fields[name.to_sym] = method_name.to_sym
        end

        def field name, order = nil, &block
          @filtering_order[name.to_sym] = prepare_order(order) unless order.nil?
          @filtering_fields[name.to_sym] = Proc.new { |slf, value| yield(slf, value) }
        end
      end

      obj.instance_eval &block
    end

  end

  module ClassMethods
    def filtering(filter = {})
      res = self.scoped
      order_values = res.order_values
      fields = @filtering_fields.keys
      filter.each do |key, value|
        next if value.blank? || !fields.include?(key.to_sym)
        res = res.scope_factory(key, value) || res
      end
      res.order_values = order_values
      target_filtering_order = @filtering_order.clone.delete_if { |k, v| filter[k].blank? }.values.join(", ")

      if target_filtering_order.blank?
        res
      else
        res.reorder.order(target_filtering_order)
      end
    end

    def scope_factory(key, value)
      key = key.to_sym
      type = @filtering_fields[key]

      case type
        when :where
          self.where key => value
        when Proc
          self.instance_eval do
            type.call(self, value)
          end
        when nil
          # self
          raise "filtering error: undefined filtering type - #{key}"
        else
          self.send type.to_sym, value
      end
    end
  end
end