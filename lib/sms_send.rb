require "net/http"
require "uri"

def send_sms(phone, message)
  if Rails.env.test?
    puts "test sms send"
    return 0
  end

  uri = URI.parse("http://smsc.ru/sys/send.php")
  login = "mystand"
  password = "146f4b483c846f5af5c0c2c392c3f866"
  phone.gsub!(/-|\(|\)|\s/, '')

  http = Net::HTTP.new(uri.host, uri.port)
  http.open_timeout = http.read_timeout = 5
  begin
    body = http.get("#{uri.path}?login=#{login}&psw=#{password}&phones=#{phone}&mes=#{message}&charset=utf-8").body
  rescue Exception => e
    {error: e.message}
  end
end