namespace :import do

  task :major_results, [:path, :exam_type_id, :min_points, :method] => :environment do |t, args|
    time_replace_rules = [/(?:[^0-9])+/, '.']
    path = args[:path]
    exam_type_id = args[:exam_type_id].try :to_i
    min_points = args[:min_points].try :to_i
    method = args[:method]

    raise "Not specified path" if path.nil?
    raise "Not specified min_points" if min_points.nil?
    raise "Not specified ExamType" if exam_type_id.nil?
    raise "Not specified method(force or update)" if method.nil? || !["force", "update"].include?(method)
    ExamType.find(exam_type_id)

    input = File.new(path, 'r')
    logs = File.new File.join(Rails.root, "log", "import.results.#{Time.now.to_s.gsub(*time_replace_rules)}.log"), "w"

    line_number = 0
    complete = 0

    input.each do |line, number|
      line_number += 1
      info = line.split(';')
      if info.length != 14
        logs.puts("Bad line: #{line_number}")
        next
      end

      print "#{line_number} "

      student = Student.where(:document_number => info[8], :document_series => info[7]).first
      unless student
        logs.puts("Unknown student at #{line_number} line (document)")
        next
      end

      exam_student = student.exam_students.includes(:exam).where(:exams => {:exam_type_id => exam_type_id, :subject_code => info[1].to_i}).first
      unless exam_student
        logs.puts("Could not find ExamStudent for student at #{line_number} line")
        next
      end
      next if method == "update" && exam_student.points_of_hundred.present?
      unless exam_student.exam_station.try(:station).try(:code) == info[2].rjust(4, "0")
        logs.puts("Bad station in ExamStudent for student at #{line_number} line")
        next
      end

      exam_student.mask_a = info[9]
      exam_student.mask_b = info[10]
      exam_student.mask_c = info[11]
      exam_student.points_of_hundred = info[13].to_i
      exam_student.min_point = min_points

      unless exam_student.save
        logs.puts "Could not save info from #{line_number} line: #{exam_student.errors.messages}"
      else
        complete += 1
      end


    end

    logs.puts "Complete: #{complete}/#{line_number}"
    logs.puts "Errors: #{line_number - complete}/#{line_number}"

    input.close()
    logs.close()
  end

end




