namespace :import do

  task :results_by_exam_id, [:path, :exam_id, :min_points, :method] => :environment do |t, args|
    time_replace_rules = [/(?:[^0-9])+/, '.']
    path = args[:path]
    exam_id = args[:exam_id].try :to_i
    min_points = args[:min_points].try :to_i
    method = args[:method]

    raise "Not specified path" if path.nil?
    raise "Not specified min_points" if min_points.nil?
    raise "Not specified exam id" if exam_id.nil?
    raise "Not specified method(force or update)" if method.nil? || !["force", "update"].include?(method)
    Exam.find(exam_id)

    input = File.new(path, 'r')
    logs = File.new File.join(Rails.root, "log", "import.results.id#{exam_id}.#{Time.now.to_s.gsub(*time_replace_rules)}.log"), "w"

    line_number = 0
    complete = 0

    input.each do |line, number|
      line_number += 1
      info = line.split(';')
      if info.length != 14
        logs.puts("Bad line: #{line_number}")
        next
      end

      print "#{line_number} "

      student = Student.where(:document_number => info[8], :document_series => info[7]).first
      unless student
        logs.puts("Unknown student at #{line_number} line (document)")
        next
      end

      exam_student = student.exam_students.where(:exam_id => exam_id).first
      unless exam_student
        logs.puts("Could not find ExamStudent for student at #{line_number} line")
        next
      end
      next if method == "update" && exam_student.points_of_hundred.present?
      #unless exam_student.exam_station.try(:station).try(:code) == info[2].rjust(4, "0")
      #  logs.puts("Bad station in ExamStudent for student at #{line_number} line")
      #  next
      #end

      exam_student.mask_a = info[9]
      exam_student.mask_b = info[10]
      exam_student.mask_c = info[11]
      exam_student.points_of_hundred = info[13].to_i
      exam_student.min_point = min_points

      unless exam_student.save
        logs.puts "Could not save info from #{line_number} line: #{exam_student.errors.messages}"
      else
        complete += 1
      end
    end

    logs.puts "Complete: #{complete}/#{line_number}"
    logs.puts "Errors: #{line_number - complete}/#{line_number}"

    input.close()
    logs.close()
  end

end




