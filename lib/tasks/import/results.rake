namespace :import do

  task :results, [:path, :exam_type_id] => :environment do |t, args|
    time_replace_rules = [/(?:[^0-9])+/, '.']
    path = args[:path]
    exam_type_id = args[:exam_type_id].try :to_i

    raise "Not specified path" if path.nil?
    raise "Not specified ExamType" if exam_type_id.nil?
    ExamType.find(exam_type_id)

    input = File.new(path, 'r')
    logs = File.new File.join(Rails.root, "log", "import.results.#{Time.now.to_s.gsub(*time_replace_rules)}.log"), "w"

    line_number = 0
    complete = 0

    input.each do |line, number|
      line_number += 1
      info = line.split('#')
      if info.length != 32
        logs.puts("Bad line: #{line_number}")
        next
      end

      print "#{line_number} "

      student = Student.where(:guid => info[29].downcase).first
      unless student
        logs.puts("Unknown student at #{line_number} line (unknown guid)")
        next
      end
      unless student.document_series == info[5]
        logs.puts("Unknown student at #{line_number} line (Do not match document series)")
        next
      end
      unless student.document_number == info[6]
        logs.puts("Unknown student at #{line_number} line (Do not match document number)")
        next
      end

      unless info[1].to_i == 66
        logs.puts("Bad region at #{line_number} line")
        next
      end

      exam_student = student.exam_students.includes(:exam).where(:exams => {:exam_type_id => exam_type_id, :subject_code => info[28].to_i}).first
      unless exam_student
        logs.puts("Could not find ExamStudent for student at #{line_number} line")
        next
      end
      unless exam_student.exam_station.try(:station).try(:code) == info[24]
        logs.puts("Bad station in ExamStudent for student at #{line_number} line")
        next
      end

      exam_student.variant = info[8]
      exam_student.mask_a = info[10]
      exam_student.mask_b = info[11]
      exam_student.mask_c = info[12]
      exam_student.points_of_hundred = info[20].to_i
      exam_student.rating = info[21]
      exam_student.points_of_five = info[22].to_i
      exam_student.classroom_name = info[26].to_i
      exam_student.blank_number = info[30]
      exam_student.min_point = info[31].to_i

      unless exam_student.save
        logs.puts "Could not save info from #{line_number} line: #{exam_student.errors.messages}"
      else
        complete += 1
      end


    end

    logs.puts "Complete: #{complete}/#{line_number}"
    logs.puts "Errors: #{line_number - complete}/#{line_number}"

    input.close()
    logs.close()
  end

end

#0 Номер
#1 Регион
#2 Фамилия
#3 Имя
#4 Отчество
#5 Серия документа
#6 Номер документа
#7 Пол
#8 Номер варианта
#9 Старый номер варианта
#10 Маска ответов на задания типа А
#11 Маска ответов на задания типа В
#12 Маска ответов на задания типа С
#13 Маска ответов на задания типа D
#14 Число верно выполненных заданий
#15 Процент верно выполненных заданий
#16 Количество верно выполненных заданий типа А
#17 Количество верно выполненных заданий типа В
#18 Количество верно выполненных заданий типа С
#19 Количество верно выполненных заданий типа D
#20 Итоговый балл
#21 Рейтинг
#22 Оценка
#23 Код АТЕ
#24 Код ППЭ
#25 Код ОУ
#26 Аудитория
#27 Класс
#28 Предмет
#29 ID в региональной базе
#30 Номер бланка
#31 Минимальный балл

=begin
45
66
Патрушева
Алёна
Вадимовна
6509
729450
Ж
901


+++++--+------
0(2)0(2)0(3)0(3)0(4)0(4)

6
18
0
6
0
0
28
0
5
25
2501
250103
0003
11 А
2
{D902A0B0-FBCC-012F-61EB-001B7838B0DE}
2330254917228
24
=end


