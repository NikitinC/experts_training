namespace :dt do
  task :init => [:environment] do
    require File.join(Rails.root, "config", "document_templates")
  end
end