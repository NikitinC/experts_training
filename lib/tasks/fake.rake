namespace :fake do
  task :before => [:environment] do
    require File.join(Rails.root, "lib/fake/fake")
  end

  task :all => [:before] do
    srand(Time.now.to_i)
    fake_all
  end

  task :exam_types, [:n] => [:before] do |t, args|
    fake_exam_types args[:n].present? ? args[:n].to_i : 1
  end

  task :destroy => [:before] do
    Student.unscoped.destroy_all
    Employee.unscoped.destroy_all
    Group.unscoped.destroy_all
    Ou.unscoped.destroy_all
    Ate.unscoped.destroy_all
    Mouo.unscoped.destroy_all
    Station.unscoped.destroy_all
    ExamStudent.unscoped.destroy_all
    Exam.unscoped.destroy_all
    ExamType.unscoped.destroy_all
  end

  task :users => [:before] do
    rcou = User.create :login => "rcoi", :target_type => "Rcoi", :password => "123qwe", :email => "test@test.te"
    [:mouo, :ou].each do |name|
      u = User.create :login => name,
                      :target => name.camelize.constantize.first,
                      :password => "123qwe", :email => "#{name}@test.te"

    end
  end

  task :students, [:n, :ou_id] => [:before] do |t, args|
    ou = Ou.find(args[:ou_id].to_i)
    group = fake_groups(1, ou).first
    fake_students (args[:n].to_i || 10), group
  end

  task :public_observers, [:n, :mouo_id] => [:before] do |t, args|
    mouo = Mouo.find(args[:mouo_id].to_i)
    fake_public_observers args[:n].to_i || 10, mouo
  end

  task :employee, [:n, :ou_id] => [:before] do |t, args|
    ou = Ou.find(args[:ou_id].to_i)
    fake_employees (args[:n].to_i || 10), ou
  end

  task :appeal => [:before] do
    fake_appeal
  end

end