namespace :clear do
  task :students, [:group_number] => :environment do |t, args|

    group_number = args[:group_number]
    fail "Invalid exam_type_id argument" if group_number.blank?

    relation = Student.includes(group: {ou: :mouo}).where(groups: {number: group_number.to_i})
    .where("mouos.code not in ('#{Mouo::GOU_MOUO_CODES.join("','")}')")

    i = 0
    count = relation.count
    relation.find_each do |item|
      i += 1
      item.destroy('106', true)
      puts "Students. Group: #{group_number}. #{i}/#{count}"
    end

  end
end


