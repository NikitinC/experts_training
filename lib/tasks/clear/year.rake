namespace :clear do
  task :year => :environment do


    puts "Clear exam_students & exam_stations"
    exam_type_ids = ExamType.all.map(&:id)
    Rake::Task['clear:exam_students'].invoke exam_type_ids
    Rake::Task['clear:exam_stations'].invoke exam_type_ids

    puts "Clear students"
    Rake::Task['clear:students'].invoke 11

    puts "Update groups"
    Rake::Task['clear:update_groups'].invoke

  end
end
