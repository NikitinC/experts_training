namespace :clear do
  task :exam_stations, [:exam_type_id] => :environment do |t, args|

    exam_type_arg = args[:exam_type_id]
    raise 'Invalid exam_type_id argument' if exam_type_arg.blank?
    exam_type_arg = exam_type_arg.to_i unless exam_type_arg.is_a? Array

    ActiveRecord::Base.uncached do
      relation = ExamStation.includes(:exam).where(exams: {exam_type_id: exam_type_arg})
      count = relation.count

      i = 0
      relation.find_each do |exam_station|
        i += 1

        # children_count = exam_station.exam_students.count
        # j = 0
        exam_station.exam_students.find_each do |child|
          # j += 1
          child.destroy '266', true
          # puts "#{i}/#{count}, #{j}/#{children_count}"
        end

        exam_station.destroy '196', true
        puts "ExamStations. ExamType: #{exam_type_arg}, #{i}/#{count}"
      end
    end

  end
end
