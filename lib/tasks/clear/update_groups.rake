namespace :clear do
  task :update_groups, :environment do
    ActiveRecord::Base.uncached do

      relation = Group.includes(ou: :mouo).where("mouos.code not in ('#{Mouo::GOU_MOUO_CODES.join("','")}')")

      i = 0
      count = relation.count

      relation.find_each do |group|
        i += 1
        group.number += 1
        group.save validate: false
        puts "Update groups: #{i}/#{count}"
      end

    end
  end
end
