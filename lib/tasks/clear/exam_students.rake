namespace :clear do
  task :exam_students, [:exam_type_id] => :environment do |t, args|

    exam_type_arg = args[:exam_type_id]
    raise 'Invalid exam_type_id argument' if exam_type_arg.blank?
    exam_type_arg = exam_type_arg.to_i unless exam_type_arg.is_a? Array

    ActiveRecord::Base.uncached do
      students_rel = Student.includes(:group).where(groups: {number: [9, 11]})
      students_count = students_rel.count
      i = 0

      students_rel.find_each do |student|
        i += 1
        rel = student.exam_students.includes(:exam).where(exams: {exam_type_id: exam_type_arg})
        count = rel.count

        rel.each_with_index do |exam_student, index|
          exam_student.destroy '266'
          puts "ExamStudents. ExamType #{exam_type_arg}. #{i}/#{students_count} #{index + 1}/#{count}"
        end
      end
    end

  end
end
