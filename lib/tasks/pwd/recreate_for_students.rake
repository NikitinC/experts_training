#coding: utf-8
namespace :pwd do

  # Пересоздает пароли для учеников, кроме тех кто зарегистрировался сам
  #
  # Usage:
  #   rake pwd:recreate_for_students[EXAM_TYPE_ID]
  #
  # Example:
  #   rake pwd:recreate_for_students[5]
  #
  # Options:
  #   EXAM_TYPE_ID - id типа экзамена для участников которого мы пересоздаем пароли
  #
  task :recreate_for_students, [:exam_type_id] => :prepare do |t, args|
    TIME_TEMPLATE = '%Y_%m_%d_%H_%M_%S'

    raise 'Invalid Arguments' if args[:exam_type_id].nil?
    exam_type_id = args[:exam_type_id].to_i

    absolute_passwords_folder = File.join Rails.root, 'tmp', "pwds_#{Time.now.strftime(TIME_TEMPLATE)}"
    errors_directory_path = File.join Rails.root, "log", 'pwd'

    FileUtils.mkdir_p errors_directory_path
    FileUtils.mkdir_p absolute_passwords_folder

    errors_file = File.new File.join(errors_directory_path, "errors_#{Time.now.strftime(TIME_TEMPLATE)}.log"), "w"

    ActiveRecord::Base.uncached do
      mouo_size = Mouo.count
      mouo_i = 0
      Mouo.find_each do |mouo|
        mouo_i += 1
        absolute_mouo_folder = File.join absolute_passwords_folder, I18n.transliterate(mouo.to_s).gsub(/\W+/, '_')

        ous_relation = mouo.ous.where("ous.code not like '%000000'")
        ou_size = ous_relation.count
        ou_i = 0

        ous_relation.find_each do |ou|
          ou_i += 1
          absolute_ou_folder = File.join absolute_mouo_folder, ou.code

          group_relation = ou.groups
          group_size = group_relation.count
          group_i = 0

          group_relation.find_each do |group|
            group_i += 1

            students = group.students.joins(exam_students: :exam).where(exam_students: {hidden: nil}, exams: {exam_type_id: exam_type_id}).uniq
            students = students.where self_registration: false
            students_size = students.count #TODO НЕВЕРНЫЙ count из за uniq
            students_i = 0

            if students.any?
              puts "group #{group.id} #{group.to_s}"
              FileUtils.mkdir_p absolute_ou_folder unless File.exists? absolute_ou_folder

              group_filename = File.join absolute_ou_folder, "#{I18n.transliterate(group.to_s).gsub(/\W+/, '_')}.csv"
              group_file = File.new group_filename, 'w:windows-1251'
              group_file.puts "ID в РБД; ФИО; Логин; Пароль"

              students.find_each do |student|
                students_i += 1

                existing_user = student.user
                existing_user.destroy('180') if existing_user

                user = User.new
                user.target = student
                user.created_at = '2021-01-01'
                user.login = user.generate_login
                user.email = "#{user.login}@irro.ru"
                password = user.password = user.generate_password
                if user.save
                  out_array = [
                      student.id,
                      student.full_name,
                      user.login,
                      password
                  ]
                  puts "#{mouo_i}/#{mouo_size} #{ou_i}/#{ou_size} #{group_i}/#{group_size} #{students_i}/#{students_size}"
                else
                  puts "!-------! #{user.errors.messages}"
                  out_array = [
                      student.id,
                      student.full_name,
                      "cant create user: #{user.errors.messages}"
                  ]
                  errors_file.puts "#{student.id}\n#{user.errors.messages}"
                end


                group_file.puts out_array.join(';')
              end
              group_file.close
            end

          end
        end
      end
    end
    errors_file.close
  end
end
