#coding: utf-8
namespace :pwd do

  task :recreate_for_ous, [:folder_name] => :prepare do |t, args|
    time_transliterate_rules = [/([^-0-9]|\s)+/, '_']
    transliterate_rules = [/\W+/, '_']
    passwords_folder = args[:folder_name] || 'pwds_' + Time.now.to_s.gsub(*time_transliterate_rules)

    absolute_passwords_folder = File.join Rails.root, "tmp", passwords_folder
    errors_file = File.new File.join(Rails.root, "log", "error_pwds_file_#{Time.now.to_s.gsub(*time_transliterate_rules)}.log"), "w"
    ous_path = File.join absolute_passwords_folder, "ous"
    %x{mkdir -p "#{ous_path}"} unless File.exists? ous_path

    ActiveRecord::Base.uncached do
      mouo_size = Mouo.count
      Mouo.all.each_with_index do |mouo, mouo_index|

        ou_filename = File.join ous_path, "#{I18n.transliterate(mouo.to_s).gsub(*transliterate_rules)}.csv"
        ou_file = File.new(ou_filename, 'w:windows-1251')
        ou_file.puts "ID в РБД; Код; Название; Логин; Пароль"

        ou_size = mouo.ous.count
        ou_i = 0
        mouo.ous.find_each do |ou|
          ou_i += 1

          User.where(target_id: ou.id, target_type: "Ou").each do |user|
            password = user.password = user.generate_password
            #user.must_send_email = "1"

            if user.save(validate: false)
              out_array = [
                  ou.id,
                  ou.code,
                  ou.short_name,
                  user.login,
                  password
              ]
              puts "Mouo: #{mouo_index + 1}/#{mouo_size}; Ou: #{ou_i}/#{ou_size}"
              ou_file.puts out_array.join(";")
            end
          end #each user
        end #ou
      end
    end #ActiveRecord::Base.uncached
  end
end
