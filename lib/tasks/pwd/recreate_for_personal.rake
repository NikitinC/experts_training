namespace :pwd do
  task recreate_for_personal: :prepare do
    time_transliterate_rules = [/([^-0-9]|\s)+/, '_']
    folder = 'pwds_' + Time.now.to_s.gsub(*time_transliterate_rules)

    Rake::Task['pwd:recreate_for_adcoi'].invoke(folder)
    Rake::Task['pwd:recreate_for_mouos'].invoke(folder)
    Rake::Task['pwd:recreate_for_ous'].invoke(folder)
    puts 'recreate_for_personal done.'
  end
end
