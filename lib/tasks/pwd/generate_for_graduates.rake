#coding: utf-8
namespace :pwd do

  task generate_for_graduates: :prepare do
    transliterate_rules = [/[^-0-9]|\s/, '-']
    passwords_folder = "pwds"
    absolute_passwords_folder = File.join Rails.root, "tmp", passwords_folder, "graduates"

    errors_directory_path = File.join Rails.root, "log", 'pwd'
    FileUtils.mkdir_p errors_directory_path
    errors_file = File.new File.join(errors_directory_path, "errors_#{Time.now.to_s.gsub(*transliterate_rules)}.log"), "w"

    ActiveRecord::Base.uncached do

      ou_size = Ou.where("ous.code like '%0000'").count
      ou_i = 0
      Ou.where("ous.code like '%0000'").find_each do |ou|
        ou_i += 1
        absolute_ou_filename = File.join absolute_passwords_folder, "#{I18n.transliterate(ou.to_s).gsub(*transliterate_rules)}.csv"


        students = ou.students.joins(:exam_students).where(:exam_students => {:hidden => nil, :exam_id => federal_exam_codes.keys}).reorder("students.id").uniq
        students_size = students.map(&:id).count
        students_i = 0

        if students.any?
          %x{mkdir -p "#{absolute_passwords_folder}"} unless File.exists? absolute_passwords_folder
          ou_file = File.new(absolute_ou_filename, "w")
          ou_file.puts "ID в РБД; ФИО; Логин; Пароль".encode("windows-1251")

          students.find_each do |student|
            students_i += 1
            user = User.new
            user.target_type = "Student"
            user.target_id = student.id
            user.login = user.generate_login
            user.email = "#{user.login}@irro.ru"
            password = user.password = user.generate_password
            if user.save
              out_array = [student.id,
                           student.full_name,
                           user.login,
                           password]
              puts "#{ou_i}/#{ou_size}  #{students_i}/#{students_size}"
            else
              puts "!-------! #{user.errors.messages}"
              out_array = [student.id,
                           student.full_name,
                           "cant create user: #{user.errors.messages}"]
              errors_file.puts "#{student.id}\n#{user.errors.messages}"
            end
            ou_file.puts out_array.join(";").encode("windows-1251")
          end #students
        end #students.any?
      end #ou
    end #ActiveRecord::Base.uncached
  end
end
