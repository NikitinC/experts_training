#coding: utf-8
namespace :pwd do

  task :recreate_for_adcoi, [:folder_name] => :prepare do |t, args|
    transliterate_rules = [/([^-0-9]|\s)+/, '_']
    passwords_folder = args[:folder_name] || 'pwds_' + Time.now.to_s.gsub(*time_transliterate_rules)

    absolute_passwords_folder = File.join Rails.root, "tmp", passwords_folder
    %x{mkdir -p "#{absolute_passwords_folder}"} unless File.exists? absolute_passwords_folder
    out_filename = File.join absolute_passwords_folder, 'rcoi-admin.csv'
    errors_file = File.new File.join(Rails.root, "log", "error_pwds_file_#{Time.now.to_s.gsub(*transliterate_rules)}.log"), "w"

    out_file = File.new(out_filename, 'w:windows-1251')
    out_file.puts "Логин; Пароль"

    ActiveRecord::Base.uncached do

      User.where(:target_type => ["Rcoi", "Admin"]).each do |user|
        password = user.password = user.generate_password

        if user.save
          out_array = [
                       user.login,
                       password]
        else
          puts "!-------! #{user.errors.messages}"
          out_array = [
                       "cant change user: #{user.errors.messages}"]
          errors_file.puts "#{user.errors.messages}"
        end
        out_file.puts out_array.join(";")
      end #each user
    end #ActiveRecord::Base.uncached
  end
end
