#coding: utf-8
namespace :pwd do

  task :generate_for, [:model, :folder] => :prepare do |t, args|
    model_name = args[:model].underscore
    model = args[:model].camelize.constantize

    time_transliterate_rules = [/([^0-9]|\s)+/, '_']
    transliterate_rules = [/\W+/, '_']
    passwords_folder = File.join('tmp', 'pwd', (args[:folder] || 'pwds_g_' + Time.now.to_s.gsub(*time_transliterate_rules)))

    absolute_passwords_folder = File.join Rails.root, passwords_folder
    errors_file = File.new File.join(Rails.root, 'log', "error_pwds_file_#{Time.now.to_s.gsub(*time_transliterate_rules)}.log"), 'w'

    %x{mkdir -p "#{absolute_passwords_folder}"} unless File.exists? absolute_passwords_folder
    model_file_path = File.join absolute_passwords_folder, "#{model_name.pluralize}.csv"
    model_file = File.new model_file_path, 'w:windows-1251'

    ActiveRecord::Base.uncached do
      count = model.count
      i = 0
      model.find_each do |item|
        i += 1
        User.where(:target_id => item.id, :target_type => model.name).find_each do |user|
          password = User.generate_password 8

          out_array = [
              item.id,
              item.code,
              item.to_s,
              user.id,
              user.login,
              password
          ]
          puts "Item: #{i}/#{count}"
          model_file.puts out_array.join(';')
        end
      end
    end
  end
end