require 'csv'

namespace :pwd do
  desc 'Import passwords'

  task :apply_login_pwd_list => :environment do
    CSV.foreach("/home/r0ot/logins.csv", :encoding => 'cp1251:utf-8', :headers => true, :col_sep => "#") do |row|
      begin
        puts (row['ou'])
        login = row['ou']
        password = row['password']
        user = User.where(:login => login).where("hidden IS NULL").first
        puts "login: #{login}; password: #{password}; user: #{user.email}"
        begin
          user.password = password
        rescue NoMethodError
          puts(login, password)
        end
        user.save :validate => false
        next

      end
    end

  end

end
