namespace :pwd do
  task :recreate_for_student_by_ou, [:ou_code] => :prepare do |t, args|
    ou_code = args[:ou_code]

    ou = Ou.where(code: ou_code).first

    ou.students.reorder.joins(:exam_students).where(exam_students: {hidden: nil, exam_id: federal_exam_codes.keys}).uniq.each do
      user = student.user
      if user
        password = user.password = user.generate_password
        if user.save
          out_array = [student.id,
                       student.full_name,
                       user.login,
                       password]
        else
          out_array = ["fatal save error for:"]
        end
      else
        out_array = ["fatal error"]
      end
      puts out_array.join(";").encode("windows-1251")
    end
  end
end
