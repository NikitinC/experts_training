namespace :pwd do
  task prepare: :environment do
    @config = {
        federal_exam_codes: APP_CONFIG['federal_exam_codes']
    }

    def federal_exam_codes
      @config[:federal_exam_codes]
    end
  end
end