#coding: utf-8
namespace :pwd do

  task :recreate_for_mouos, [:folder_name] => :prepare do |t, args|
    transliterate_rules = [/([^-0-9]|\s)+/, '_']
    passwords_folder = args[:folder_name] || 'pwds_' + Time.now.to_s.gsub(*transliterate_rules)

    absolute_passwords_folder = File.join Rails.root, "tmp", passwords_folder
    %x{mkdir -p "#{absolute_passwords_folder}"} unless File.exists? absolute_passwords_folder
    mouo_filename = File.join absolute_passwords_folder, "mouo.csv"
    errors_file = File.new File.join(Rails.root, "log", "error_pwds_file_#{Time.now.to_s.gsub(*transliterate_rules)}.log"), "w"

    out_file = File.new(mouo_filename, 'w:windows-1251')
    out_file.puts "ID в РБД; Код; Название; Логин; Пароль"

    ActiveRecord::Base.uncached do

      size = Mouo.count
      i = 0
      Mouo.find_each do |object|
        i += 1

        User.where(target_id: object.id, target_type: "Mouo").each do |user|
          password = user.password = user.generate_password

          if user.save validate: false
            out_array = [
                object.id,
                object.code,
                object.name,
                user.login,
                password
            ]
            puts "Mouo: #{i}/#{size}"
            out_file.puts out_array.join(';')
          end
        end

      end
    end
  end
end
