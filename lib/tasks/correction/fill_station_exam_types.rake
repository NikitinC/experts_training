#coding: utf-8
namespace :correction do

  task :fill_station_exam_types => :environment do
    @config = {
        ege_exam_type_ids: [5,6,10]
    }

    ActiveRecord::Base.uncached do
      Station.find_each do |station|
        station.exam_type_ids = @config[:ege_exam_type_ids]
        station.save :validate => false
      end
    end
  end
end
