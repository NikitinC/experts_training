namespace :employee_ou do

  task :prepare => [:environment] do
  end

  task :create_for_all => [:prepare] do

    size = Employee.count
    filled = 0
    completed = 0
    count = 0

    Employee.reorder(:id).find_each do |employee|
      count += 1
      if employee.employee_ous.blank?
        document = {:document_number => employee.document_number, :document_series => employee.document_series}
        names = {:first_name => employee.first_name, :second_name => employee.second_name, :middle_name => employee.middle_name}
        duplicate_employees = Employee.where(document.merge(names)).where("id != ?", employee.id)
        employee_ou = EmployeeOu.new

        target_duplicates_employees = duplicate_employees.select do |duplicate_employee|
          duplicate_employee.employee_ous.any?
        end

        if target_duplicates_employees.any?
          employee_ou.employee_id = duplicate_employees.first.id
          employee_ou.pluralist = true
        else
          employee_ou.employee_id = employee.id
          employee_ou.pluralist = false
        end

        employee_ou.ou_id = employee.ou_id
        employee_ou.employee_post_code = employee.employee_post_code
        employee_ou.employee_ou_subjects = employee.subjects.map(&:code)
        employee_ou.valid_cache = employee_ou.valid?
        employee_ou.save(:validate => false)

        puts "#{count} / #{size} completed"
        completed += 1
      else
        puts "#{count} / #{size} was filled"
        filled += 1
      end

      puts "FINISH. #{completed} - completed, #{filled} - was filled"
    end

  end

end
