#coding: utf-8
namespace :correction do

  #удаляем категории
  #выполняем сиды
  #выполняем таск
  task :ege_participant_categories_2014 => :environment do
    @config = {
        #from_codes => to_code
        #[1] => 1,
        #[2, 3, 12, 14] => 3,
        2 => 3,
        12 => 3,
        14 => 3,
        #[8] => 8,
        #[4, 5, 6, 7] => 4,
        5 => 4,
        6 => 4,
        7 => 4
    }

    ActiveRecord::Base.uncached do
      count = Student.count
      index = 0

      config_keys = @config.keys
      Student.find_each do |student|
        index += 1

        code = student.ege_participant_category_code
        if config_keys.include?(code)
          student.ege_participant_category_code = @config[code]
          student.save :validate => false
        end

        puts "complete #{index}/#{count}"
      end
    end
  end
end
