#coding: utf-8
namespace :correction do

  task :study_forms_2014 => :environment do
    StudyForm.destroy_all
    Rake::Task['db:seed'].invoke

    @config = {
        #from_codes => to_code
        3 => 7,
        6 => 7,
    }

    ActiveRecord::Base.uncached do
      relation = Student.where(:study_form_code => @config.keys)
      count = relation.count
      index = 0

      config_keys = @config.keys
      relation.find_each do |student|
        index += 1

        code = student.study_form_code
        if config_keys.include?(code)
          student.study_form_code = @config[code]
          student.save :validate => false
        end

        puts "complete #{index}/#{count}"
      end
    end
  end
end
