namespace :correction do

  # rake correction:exam[3,8,2014.3.21]
  task :exam_date, [:exam_type_id, :exam_code, :date] => :environment do |t, args|
    exam_type_id = args[:exam_type_id].to_i
    exam_code = args[:exam_code].to_i
    date = Date.new *(args[:date].split('.').map(&:to_i))

    exams = Exam.where(exam_type_id: exam_type_id, code: exam_code)

    raise "Error: script has found #{exams.count} exams" if exams.count != 1

    exam = exams.first
    exam.date = date
    if exam.save
      puts 'Successful changed'
    else
      puts 'Validation errors:'
      puts exam.errors.messages
    end
  end
end
