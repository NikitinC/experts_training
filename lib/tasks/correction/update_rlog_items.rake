task "rlog:update_names" => :environment do
  RlogItem.find_each do |rlog_item|
    target = rlog_item.unscoped_record
    if target
      rlog_item.name = target.to_s
      rlog_item.save
    end
  end;nil
end