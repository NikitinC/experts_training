#coding: utf-8
namespace :correction do

  #удаляем категории
  #выполняем сиды
  #выполняем таск
  task :human_settlement_types_2014, [:path] => :environment do |t, args|
    path = args[:path]
    file = File.new File.join(Rails.root, path)

    codes = {
        'село' => 9,
        'посёлок' => 6,
        'город' => 1,
        'поселок' => 6,
        'поселок городского типа' => 7,
        'деревня' => 2,
        'рабочий посёлок' => 64,
        'посёлок городского типа' => 7,
        'городок' => 26
    }

    ActiveRecord::Base.uncached do
      ActiveRecord::Base.transaction do
        # HumanSettlementType.destroy_all
        # require File.join(Rails.root, 'db/seeds/directories/human_settlement_types.rb')

        file.each_line do |line|
          values = line.split ';'
          guid = values[0]
          human_settlement_type_name = values[5]

          human_settlement_type_code = codes[human_settlement_type_name]

          if human_settlement_type_code
            Ou.where(address_guid: guid).update_all(human_settlement_type_code: human_settlement_type_code)
            Ou.where(legal_address_guid: guid).update_all(legal_human_settlement_type_code: human_settlement_type_code)
          end
        end
      end
    end
  end
end