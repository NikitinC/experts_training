require 'csv'

namespace :db do
  desc 'Import Directories'

  task :examimport => :environment do
    CSV.foreach(Rails.root + "db/exams.csv", :encoding => 'cp1251:utf-8', :headers => true, :col_sep => ";") do |row|
      begin
        puts "Грузим:" + row['date']
        Exam.create!(row.to_hash)
      rescue
        puts "Не удалось загрузить: " + row['date'].to_s + ' : ' + row['subject_code'].to_s + ' : ' + row['exam_type_id'].to_s
      end
    end

  end

end
