namespace :cache do
  task :valid, [:model] => [:environment] do |t, args|
    all = args[:model] || [:appeals, :ates, :classrooms, :employees, :groups, :mouos, :ous, :stations, :students, :users]
    [*all].each do |word|
      model = word.to_s.singularize.camelize.constantize
      count = model.count
      index = 0
      model.where(:valid_cache => true).reorder(:id).find_each do |a|
        index += 1
        puts "#{model.name} id:#{a.id} - #{index}/#{count}"
        a.save(:validate => false)
      end
      puts "finished,  model.where(:valid_cache => true).size => #{model.where(:valid_cache => true).size}"
    end
  end

  task :revalidate, [:model] => [:environment] do |t, args|
    all = args[:model] || [:appeals, :ates, :classrooms, :employees, :groups, :mouos, :ous, :stations, :students, :users]
    [*all].each do |word|
      model = word.to_s.singularize.camelize.constantize
      count = model.count
      index = 0
      model.reorder(:id).find_each do |a|
        index += 1
        puts "#{model.name} id:#{a.id} - #{index}/#{count}"
        a.save(:validate => false)
      end
      puts "finished,  model.where(:valid_cache => true).size => #{model.where(:valid_cache => true).size}"
    end
  end
end