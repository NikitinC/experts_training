# coding: utf-8
namespace :dump do

  task :export, [:model] => [:prepare] do |t, args|
    default_config = {
        :target_file_path => File.join(Rails.root, "tmp", "dumps"),
        :log_file_path => File.join(Rails.root, "log", "dump_export_errors"),
    }
    default_attributes = {
        :created_at => false,
        :updated_at => false,
        :hidden => false,
        :occupancy => false
    }

    begin
      Dir::mkdir(default_config[:target_file_path])
    rescue Exception => e
      unless File.exists?(default_config[:target_file_path])
        raise Exception.new "Can`t create destination folder: #{default_config[:target_file_path]}"
      end
    end

    begin
      Dir::mkdir(default_config[:log_file_path])
    rescue Exception => e
      unless File.exists?(default_config[:log_file_path])
        raise Exception.new "Can`t create destination folder: #{default_config[:log_file_path]}"
      end
    end

    config_size = export_dump_config.size
    config_count = 0

    export_dump_config.each do |config|
      next if args[:model] and args[:model].camelize != config[:model_name].camelize

      config = (default_config).merge(config)
      config[:attributes] = (config[:attributes] || {}).merge(default_attributes)
      config_count += 1

      target_file = File.new(File.join(config[:target_file_path], "#{(config[:new_model_name] || config[:model_name]).underscore}.json"), "w:UTF-8")
      log_file = File.new(File.join(config[:log_file_path], "#{config[:model_name].underscore}.log"), "w:UTF-8")

      model = config[:model_name].camelize.constantize
      record_count = model.count
      complete = 0
      errors = 0

      target_file.puts record_count

      previous_time = Time.now
      i = 0
      model.uncached do
        model.find_each do |object|
          begin
            passed_time = (Time.now - previous_time)
            time_remain = (passed_time * (record_count - i)).to_i
            previous_time = Time.now

            export_attributes = {}
            object.attributes.each do |attr, value|
              new_attr = config[:attributes][attr.to_sym]

              case new_attr
                when Symbol, String
                  export_attributes[new_attr] = value
                when FalseClass
                when Proc
                  new_attr, new_value = new_attr.call(attr, value, object)
                  export_attributes[new_attr] = new_value
                when NilClass
                  export_attributes[attr] = value
                else
                  raise "Unexpected attribute change rule"
              end
            end
            complete += 1
            puts "Export model: #{config[:model_name]} (#{config_count}/#{config_size}), record:#{i+1}/#{record_count} success. Estimated time remain: #{time_remain / 60}:#{time_remain % 60}"
          rescue Exception => e
            errors += 1
            puts "#{i+1}/#{record_count} fail with error: #{e.message}"
            log_file.puts "#{i+1}/#{record_count} fail with error: #{e.message}"
            log_file.puts "Record id = #{object.id}\n"
          end

          target_file.puts "#{export_attributes.to_json}"
          i += 1
        end
      end

      puts "Finish. Complete: #{complete}, Errors: #{errors}, Summary: #{record_count}"
      log_file.puts "Complete: #{complete}, Errors: #{errors}, Summary: #{record_count}"
      target_file.close
      log_file.close
    end

    puts "Export Finish"
  end

end