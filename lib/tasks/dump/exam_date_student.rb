# coding: utf-8
namespace :dump do

  task :export => [:prepare] do

    error_log = File.open('log/ExamDateStudent_dump_errors.log', 'w')
    count = ExamDateStudent.count
    errors = 0
    complete = 0
    offset = 0
    step = 100
    (count / step + 1).times do
      puts "Start package #{offset + 1} - #{offset + step}"

      Student.limit(step).offset(offset).each_with_index do |student, i|
        exam_results = []
        ExamDateStudent.where(:student_id => student.id, :accepted => true).each do |eds|
          exam_results << {:subject => eds.subject.name,
                           :date => eds.exam_date.date,
                           :classroom_number => eds.classroom_number,
                           :points_of_hundred => eds.points_of_hundred,
                           :points_of_five => eds.points_of_five,
                           :rating => eds.rating,
                           :min_point => eds.min_point,
                           :variant => eds.variant,
                           :mask_a => eds.mask_a,
                           :mask_b => eds.mask_b,
                           :mask_c => eds.mask_c
          }
        end

        student.exam_results = exam_results
        if student.save
          complete += 1
        else
          error_log.puts student.errors.inspect
          errors += 1
        end
      end

      offset += 100
    end

    puts "Errors: #{errors}. Complete #{complete}. Summary #{count}."
    error_log.puts "Errors: #{errors}. Complete #{complete}. Summary #{count}."

  end

end