# coding: utf-8
namespace :dump do

  task :prepare => [:environment] do
    require File.join(Rails.root, "config", "dump_config")
    ActiveRecord::Base.logger = nil
  end

end