# coding: utf-8
namespace :dump do

  task :import => [:prepare] do
    default_config = {
        :json_file_path => File.join(Rails.root, "tmp", "dumps"),
        :fail_file_path => File.join(Rails.root, "tmp", "dumps", "import_fails"),
    }

    begin
      Dir::mkdir(default_config[:fail_file_path])
    rescue
      unless File.exists?(default_config[:fail_file_path])
        raise Exception.new "Can`t create destination folder: #{default_config[:fail_file_path]}"
      end
    end

    model_size = import_dump_config.size
    model_count = 0

    import_dump_config.each do |model_name|
      config = ({:model_name => model_name}).merge(default_config)
      migration_links = {}

      model_count += 1

      json_file = File.open(File.join(config[:json_file_path], "#{config[:model_name].underscore}.json"))
      fail_file = File.new(File.join(config[:fail_file_path], "fail_#{File.basename(json_file)}"), "w")

      class_name = config[:model_name].camelize
      data_size = json_file.gets.to_i
      puts data_size

      count = 0
      complete = 0
      errors = 0

      #data_size = 100 if data_size > 100

      data_size.times do
        begin
          object = JSON.parse(json_file.gets)
          migration_link = MigrationLink.where(:old_id => object["id"], :target_type => class_name.underscore).first
          migration_link ||= MigrationLink.new :old_id => object.delete("id"), :target_type => class_name.underscore

          new_object = migration_link.new_id ? class_name.constantize.find(migration_link.new_id) : class_name.constantize.new

          count += 1
          object.each do |key, val|
            if key =~ /^.*_id$/
              belongs_to_field = key[/^.*(?=_id$)/]
              if object["#{belongs_to_field}_type"].present?
                foreign_class = object["#{belongs_to_field}_type"].camelize.constantize
              else
                foreign_class = belongs_to_field.camelize.constantize
              end
              migration_links[foreign_class] ||= MigrationLink.get_hash(foreign_class)
              foreign_object = foreign_class.where(:id => migration_links[foreign_class][val]).first
              new_object.send "#{belongs_to_field}=", foreign_object if foreign_object
            elsif key =~ /^.*_scan$/
              new_object.send "#{key}=", File.new(val) if File.exists?(val) and !File.directory?(val)
            else
              new_object.send "#{key}=", val
            end
          end

          unless new_object.save(:validate => false)
            raise "Object can't save #{new_object.errors.messages}"
          end

          migration_link.new_id = new_object.id
          migration_link.save

          complete += 1
          puts "Import model: #{class_name} (#{model_count}/#{model_size}), #{count}/#{data_size} success"
        rescue Exception => e
          errors += 1
          puts "Import model: #{class_name} (#{model_count}/#{model_size}), #{count}/#{data_size} fail"

          object[:id] = migration_link.old_id
          json_string = "/*#{e.message}*/\n#{object.to_json}\n\n" if count == data_size
          fail_file.puts json_string || "/*#{e.message}*/\n#{object.to_json},\n\n"
        end
      end

      json_file.close
      fail_file.close
      puts "Complete: #{complete}, Errors: #{errors}, Summary: #{data_size}"
    end

    puts "Import Finish."
  end

end
