# coding: utf-8

namespace :fbd_2019 do
  task :prepare => :environment do
    @config = {
        federal_exam_codes: APP_CONFIG['federal_exam_codes'],

        software_codes: {
            1 => 20,
            2 => 21,
            3 => 22,
            4 => 23,
            5 => 24,
            6 => 25,
            7 => 26,
            8 => 27,
            9 => 28
        },
        programming_language_codes: {
            1 => '01',
            2 => '01',
            3 => '01',
            4 => '01',
            5 => '02',
            6 => '02',
            7 => '03',
            8 => '03',
            9 => '04'
        },

        spreadsheet_editor_codes: {
            1 => 41,
            2 => 40,
            3 => 42,
            4 => 43,
            5 => 44,
            6 => 45,
            7 => 46,
            8 => 48
        }
    }

    def federal_exam_codes
      @config[:federal_exam_codes]
    end

    include FbdHelper
  end
end

module FbdHelper
  def transform_document_code code
    return 7 if code == 8 || code > 13
    code
  end

  def gender g
    g = true if g.nil?
    g ? "М" : "Ж"
  end

  def escape_for_export s
    return "" if s.nil?

    escaped = ""
    s.to_s.each_byte do |b|
      if b == ?#
        escaped += '№'
      elsif b == ?'
        escaped += '"'
      elsif b == ?%
        escaped += ' 0/0 '
      elsif b == ?_
        escaped += '-'
      elsif (0...31) === b
        escaped += "\\#{b}"
      else
        escaped += b.chr
      end
    end
    escaped
  end

  def line *args
    puts args.map { |el| escape_for_export(el) }.join("#")
  end

  def prepare_email email
    return "fake@fake.ru" if email.blank?
    email.to_s.split(/[\s,]/).join(";").gsub(/;+/, ";")
  end

  def prepare_phone phone
    phone.nil? ? "Нет" : phone.gsub(" ", "");
  end

  def prepare_mphone phone
    phone.nil? ? "" : phone.gsub(" ", "");
  end

  def federal_exam_code exam
    federal_exam_codes[exam.try :id]
  end

  def special_sit student
    student.special_seating ? 1 : 0
  end

  def transform_hidden hidden
    hidden.nil? ? 0 : 1
  end

  def transform_bool bool
    bool ? 1 : 0
  end

  def transform_time time
    time.strftime("%Y-%m-%d %H-%M-%S")
  end

  def blank_address?(ou)
    ou.ate.try(:okato).blank? ||
        ou.postcode.blank? ||
        ou.human_settlement_type_code.blank? ||
        ou.settlement_name.blank? ||
        ou.street_type_code.blank? ||
        ou.street_name.blank? ||
        ou.building_type_code.blank? ||
        ou.building_number.blank? ||
        ou.legal_postcode.blank? ||
        ou.legal_human_settlement_type_code.blank? ||
        ou.legal_settlement_name.blank? ||
        ou.legal_street_type_code.blank? ||
        ou.legal_street_name.blank? ||
        ou.legal_building_type_code.blank? ||
        ou.legal_building_number.blank?
  end

  def current_exam_type_ids
    Exam.where(id: federal_exam_codes.keys).pluck(:exam_type_id).uniq
  end

end