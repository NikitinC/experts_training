require 'barby'
require 'barby/barcode/ean_13'
require 'barby/outputter/png_outputter'
require 'chunky_png'

  # require File.join(Rails.root, 'config/kims.rb')

  namespace :generate_2014 do
    task :config => :environment do
      @config = Kims::CONFIG
      @answer_2_additional = Kims::ANSWER_2_ADDITIONAL
    end

    def generate_barcode barcode_number
      barcode = Barby::EAN13.new barcode_number
      File.open(File.join(Rails.root, 'log/barcodes', "#{barcode.data_with_checksum}.png"), 'w') do |f|
        f.write barcode.to_png(:height => 24, :margin => 0)
      end
      barcode.data_with_checksum
    end
  end

