#coding: utf-8
namespace :generate_2014 do
  task :kims => :config do
    additional_ready = {}

    @config.each do |config|
      #station_codes = config[:stations].keys.map { |code| code.to_s.rjust(4, '0') }
      #relation = Station.where :code => station_codes

      exam = config[:exam]
      exam_type_name = exam[:exam_type][:name]
      subject_code = exam[:subject][:code]
      subject_name = exam[:subject][:name]
      exam_date = exam[:date]
      variants_count = config[:variants_count]
      template_key = config[:template_key]

      exam_folder = File.join(
          I18n.transliterate(exam_type_name).downcase,
          "#{subject_code}_#{I18n.transliterate(subject_name).downcase}",
          exam_date
      )

      barcodes_folder_path = File.join Rails.root, 'public', 'documents', 'kims'
      %x{mkdir -p "#{barcodes_folder_path}"} unless File.exists? barcodes_folder_path
      barcodes_file_name = [
          'barcodes',
          I18n.transliterate(exam_type_name).downcase,
          I18n.transliterate(subject_name).downcase,
          "#{exam_date}.csv"
      ].join('-').gsub(/\s+/, '_')
      barcodes_file = File.open File.join(barcodes_folder_path, barcodes_file_name), 'w'

      # Дополнительные бланки ответов №2
      @answer_2_additional[:count].times do |i|
        barcodes = (1..200).to_a.map do |k|
          generate_barcode "234#{(200*i + k).to_s.rjust(9, '0')}"
        end

        Resque.enqueue(DocumentsGeneration, {
            :key => @answer_2_additional[:template_key],
            :filename => "answer_2_additional_#{i + 1}.pdf",
            :additional_folder => File.join('kims', exam_folder),
            :json_items => {}.to_json,
            :additional_data => {:barcodes => barcodes}
        })
      end


      config[:stations].each do |station|
        students = station[:students]
        counts = station[:counts]
        #exam_station = ExamStation.where(:exam_id => exam.id, :station_id => station.id).first
        #next if exam_station.nil?

        station_code = station[:code].to_s.rjust(4, '0')


        common_params = {
            :additional_folder => File.join('kims', exam_folder, station_code),
            :json_items => {:station => station, :exam => exam}.to_json
            #:model_name => 'ExamStation',
            #:filter => {:id => exam_station.id},
        }

        #generate title
        Resque.enqueue(DocumentsGeneration, {
            :key => :station_title,
            :filename => 'title.pdf',
            :additional_data => {
                :count_15 => counts[15],
                :count_5 => counts[5],
                :students => students
            },
        }.merge(common_params))


        code_offset = 0
        counts.each do |size, times|
          times.times do |i|

            variants = []
            while variants.length < size
              value = rand(variants_count) + 1
              variants << value if variants.last != value
            end

            first_kim_base_number = "#{station_code}#{(code_offset + i * size).to_s.rjust(5, '0')}"
            barcodes = (0...size).to_a.map do |k|
              [233, 231, 232].map { |prefix| generate_barcode "#{prefix}#{(first_kim_base_number.to_i + k).to_s.rjust(9, '0')}" }
            end

            barcodes.each_with_index do |three_barcodes, index|
              barcodes_file.puts [*three_barcodes, "10#{variants[index]}"].join(';')
            end

            #generate kims
            Resque.enqueue(DocumentsGeneration, {
                :key => template_key,
                :additional_data => {
                    :i => i,
                    :times => times,
                    :size => size,
                    :variants => variants,
                    :barcodes => barcodes
                },
                :filename => "#{size}_#{i + 1}.pdf"
            }.merge(common_params))

          end
          code_offset += times * size
        end
      end
      barcodes_file.close
    end
  end


end