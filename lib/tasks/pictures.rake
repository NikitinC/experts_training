namespace :pictures do
  task :recreate, [:student_start_id, :ou_start_id] => :environment do |t, opts|
    students = Student.reorder :id
    students = students.where("id > #{opts[:student_start_id]}") if opts[:student_start_id]
    students.find_each do |student|
      begin
        a = false
        a = student.document_scan.recreate_versions! if student.document_scan.present?
        a = student.education_scan.recreate_versions! if student.education_scan.present?
        a = student.disabilities_scan.recreate_versions! if  student.disabilities_scan.present?
        if a
          student.save validate: false
          puts "Recreated images for student: #{student.id}"
        end
      rescue
        puts "Recreated images for student: #{student.id} FAILED!!!!"
      end
    end

    ous = Ou.reorder :id
    ous = ous.where("id > #{opts[:ou_start_id]}") if opts[:ou_start_id]
    ous.find_each do |ou|
      begin
        a = false
        a = ou.license_scan.recreate_versions! if ou.license_scan.present?
        a = ou.accreditation_scan.recreate_versions! if ou.accreditation_scan.present?
        if a
          ou.save validate: false
          puts "Recreated images for ou: #{ou.id}"
        end
      rescue
        puts "Recreated images for ou: #{ou.id} FAILED!!!!"
      end
    end
  end
end
