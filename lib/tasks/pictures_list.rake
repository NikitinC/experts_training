namespace :db do
  task :pictures_list, [:student_start_id, :ou_start_id] => :environment do |t, opts|
    students = Student.reorder :id
    students = students.where("id > #{opts[:student_start_id]}") if opts[:student_start_id]
    students.find_each do |s|
      begin
        if s.is_vpl_spo then
          a = s.ege_statement_scan
          b = s.first_name
          c = s.second_name
          d = s.document_number
          puts c + ' ' + b + ' ('+ d + '); ' + a.to_s
        end
      end
    end
  end
end
