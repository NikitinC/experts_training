module FormTask
  def process
    puts ("form task")

    analytic_tasks = AnalyticTask.where(:is_active => true)

    analytic_tasks.each do |at|
      begin
        sql = at.sql_string

        ActiveRecord::Base.connection.execute(sql).each do |r|
          add_analytics_item at.short_name, r["#{at.object_column_name}"], "#{at.mdl_name}", r['oid'], r['mid'], r['comment']
        end
      rescue

        puts ("#{at.name} not working properly")

      end

    end

  end
end