module NoMath
  def process
    puts ("no math")

    sql = <<SQL
      select s.id as sid, m.id as mid, o.id as oid, 'Не назначен на математику' as comment
        FROM students s
        JOIN groups g ON g.id = s.group_id
        join ous o ON o.id = g.ou_id
        join mouos m ON m.id = o.mouo_id
        LEFT JOIN 
        (select es.student_id 
          from exam_students es
          join exams e ON e.id = es.exam_id
          join subjects sj ON sj.code = e.subject_code
         where sj.name like '%атемати%' and es.deleted_at is NULL
        ) math ON math.student_id = s.id
      where s.deleted_at is NULL and math.student_id is NULL and g.number in (9, 10, 11, 12)
      and not (ege_participant_category_code in (8, 4, 10, 3) or gia_active_results_math = true)

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_math, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end