module NoComposition
  def process
    puts ("no composition")

    sql = <<SQL
      SELECT s.id as sid, m.id as mid, o.id as oid, 
        'Не имеет зачёта по ИС(И)/ИС, не назначен и пояснения отсутствуют' as comment
          FROM students s
          JOIN groups g ON g.id = s.group_id
          JOIN ous o ON o.id = g.ou_id
          JOIN mouos m ON m.id = o.mouo_id
        JOIN ege_participant_categories pc ON cast(pc.code as int) = s.ege_participant_category_code
        LEFT JOIN (
        SELECT student_id
          FROM compositions_students cs
          JOIN compositions c ON c.id = cs.composition_id
          where c.date + INTERVAL '14 day' > now()
        ) nazn ON nazn.student_id = s.id
          WHERE s.deleted_at is NULL and pc.name like '%текущ%'
        and coalesce(s.has_composition, false) <> True
          and s.guid not in (select identifier from results where subject_code in (20,21) and Mark100 = 1)
        and nazn.student_id is NULL
        and length(coalesce(comment, '')) < 5
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_composition, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end

# переливаю списки тех, у кого есть зачёты

# use [erbd_ege_reg_22_66]
#
# SELECT ht.SubjectCode, ht.ExamDate, ht.ProcessCondition, ht.StationCode,
#        ht.VariantCode, m.PrimaryMark, m.Mark100, m.Mark5, m.TestResultA, m.TestResultB, m.TestResultC, m.TestResultD
# ,r.Barcode, '', r.Barcode
# ,'{' + lower(cast(ht.ParticipantFK as varchar(42))) + '}'
# FROM res_HumanTests ht
# JOIN res_Marks m ON m.HumanTestID = ht.HumanTestID
# JOIN sht_Sheets_R r ON r.ParticipantID = ht.ParticipantFK and r.SubjectCode = ht.SubjectCode and r.ExamDate = r.ExamDate
#
