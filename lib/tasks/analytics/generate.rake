namespace :analytics do

  desc 'Create analytics items'

  class AnalyticsWriter
    def initialize
      @acc = []
    end
    def add_analytics_item(alert_type, record_id, record_type, ou_id, mouo_id, comment=nil)
      item = AnalyticsItem.new(
                               :alert_type => alert_type,
                               :record_id => record_id,
                               :record_type => record_type,
                               :ou_id => ou_id,
                               :mouo_id => mouo_id,
                               :comment => comment
                           )
      item.calc_hash
      @acc << item
      flush if @acc.length > 1000
    end
    def flush
      AnalyticsItem.import(@acc)
      @acc = []
    end
  end


  task :generate, [:plugin_to_execute] => [:environment] do |t, args|
    # ActiveRecord::Base.logger = Logger.new(STDOUT)
    # Rails.logger.level = Logger::DEBUG

    AnalyticsItem.delete_all

    plugins_path = File.join(File.dirname(__FILE__), 'plugins')

    Dir.foreach plugins_path do |file|
      next unless file.end_with? '.rb'

      plugin = file.sub(/.rb$/, '')
      next if args.has_key?(:plugin_to_execute) && args[:plugin_to_execute] != plugin
      require File.join(plugins_path, file)

      aw = AnalyticsWriter.new
      aw.extend Object.const_get(plugin.camelize)

      aw.process
      aw.flush
    end
  end
end