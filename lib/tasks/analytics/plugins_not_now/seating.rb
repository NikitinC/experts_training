module Seating
  def process

    puts ("lack of staff on exams")

    sql = <<SQL
    SELECT o.id as oid, m.id as mid --,
     ,
	 replace('В ППЭ ' || st.code || ' на экзамен ' || af.name || ' ' || e.date || ' по ' || sj.short_name || ' не хватает работников ППЭ: ' ||
         COALESCE(geks_about, '') || COALESCE(techs_about, '') || COALESCE(ruks_about, '') || COALESCE(meds_about, '')
     || COALESCE(spec_about, '') || COALESCE(orgins_about, '') || COALESCE(orgouts_about, '') || '.', '; .', '.')
	 as comment

     FROM stations st
    	JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
		JOIN exams e ON e.id = est.exam_id
		JOIN subjects sj ON sj.code = e.subject_code
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
		JOIN attestation_forms af ON af.code = e.attestation_form_code

        JOIN(SELECT st.code,
                 CASE
                            WHEN
                                geks < geks_n THEN ' не хватает членов ГЭК (' || cast(geks as varchar(9)) || ' вместо ' || cast(geks_n as varchar(9)) || '); '
                        END geks_about,
                        CASE
                            WHEN
                        techs < geks_n AND on_home is NULL THEN ' технических специалистов (' || cast(techs as varchar(9)) || ' вместо ' || cast(geks_n as varchar(9)) || '); '
                        END techs_about,
                        CASE
                            WHEN
                                ruks <> 1 THEN ' руководителя ППЭ (' || cast(ruks as varchar(9)) || ' вместо 1 ); '
                        END ruks_about,
                        CASE
                            WHEN
                                meds < 1 AND on_home is NULL THEN ' медицинского работника; '
                        END meds_about,
                        CASE
                            WHEN
                                specs < auds AND gia11 = False and oge_phys = True THEN ' специалистов по инструктажу по Физике (' || cast(specs as varchar(9)) || ' вместо ' || cast(auds as varchar(9)) || '); '
                        END spec_about,
                        CASE
                            WHEN
                                orgins < auds*2 THEN ' организаторов в аудитории (' || cast(orgins as varchar(9)) || ' вместо ' || cast(auds*2 as varchar(9)) || '); '
                        END orgins_about,
                        CASE
                            WHEN
                                orgouts < cast(ceil(cast(auds as float)/2) as int) + 1 AND on_home is NULL THEN ' организаторов вне аудитории (' || cast(orgouts as varchar(9)) || ' вместо ' || cast(cast(ceil(cast(auds as float)/2) as int) + 1 as varchar(9)) || '); '
                        END orgouts_about
                        ,s.*

                FROM stations st
				JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL

            JOIN( SELECT
                    est.id as estid, st.code, gia11.flag as gia11, ppk.flag as ppk, on_home.stid as on_home,
                    CASE WHEN gia11.flag = True THEN COALESCE(geks11.cnt, 0) ELSE COALESCE(geks9.cnt, 0) END as geks,
                    CASE WHEN gia11.flag = True THEN COALESCE(ruk11.cnt, 0) ELSE COALESCE(ruk9.cnt, 0) END as ruks,
                    CASE WHEN gia11.flag = True THEN COALESCE(tech11.cnt, 0) ELSE COALESCE(tech9.cnt, 0) END as techs,
                    CASE WHEN gia11.flag = True THEN COALESCE(orgin11.cnt, 0) ELSE COALESCE(orgin9.cnt, 0) END as orgins,
                    CASE WHEN gia11.flag = True THEN COALESCE(orgout11.cnt, 0) ELSE COALESCE(orgout9.cnt, 0) END as orgouts,
                    CASE WHEN gia11.flag = True THEN COALESCE(med11.cnt, 0) ELSE COALESCE(med9.cnt, 0) END as meds,
                    CASE WHEN gia11.flag = True THEN 0 ELSE COALESCE(spec9.cnt, 0) END as specs,
                    COALESCE(auds.cnt, 0) as auds,
                    CASE WHEN ppk.flag = True THEN
                        CASE WHEN CAST(CEIL(CAST(COALESCE(auds.cnt, 0) as float) / 5) as int) < 2 THEN 2
                        ELSE CAST(CEIL(CAST(COALESCE(auds.cnt, 0) as float) / 5) as int)
                        END ELSE 1 END as geks_n,
                    CASE WHEN oge_phys.estid IS NULL THEN False ELSE True END as oge_phys

                  FROM stations st

                JOIN ous o ON o.id = st.ou_id
                JOIN mouos m ON m.id = o.mouo_id
				JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL

                -- Флаг, ППЭ на дому? --

                LEFT JOIN (SELECT st.id as stid
                        FROM stations st
                        JOIN station_additional_features saf ON saf.code = st.station_additional_feature_code
              			WHERE saf.name like '%на дому%'
              			group by st.id
                      ) on_home ON on_home.stid = st.id

                -- Флаг, Физика-9 ? --

                LEFT JOIN (SELECT est.id as estid
                        FROM stations st
                        JOIN exam_types_stations ets_oge ON ets_oge.station_id = st.id and ets_oge.exam_type_id in (26, 22, 23, 29)
                        JOIN exam_stations est ON est.station_id = st.id
                        JOIN exams e ON e.id = est.exam_id
                        JOIN subjects sj ON sj.code = e.subject_code
                        WHERE st.deleted_at is NULL and est.deleted_at is NULL and sj.name = 'Физика'
						group by est.id
                      ) oge_phys ON oge_phys.estid = est.id

                -- Флаг, ГИА-11 ? --

                JOIN (SELECT st.id as stid, CASE WHEN ets_ege.id is NOT NULL THEN True ELSE False END as flag
                        FROM stations st
                        LEFT JOIN exam_types_stations ets_ege ON ets_ege.station_id = st.id and ets_ege.exam_type_id in (24, 5, 19, 27, 29)
                        LEFT JOIN exam_types_stations ets_oge ON ets_oge.station_id = st.id and ets_oge.exam_type_id in (26, 22, 23, 29)
                        WHERE st.deleted_at is NULL
                        GROUP BY stid, flag
                      ) gia11 ON gia11.stid = st.id

                -- Флаг, Печать ЭМ ? --

                JOIN (SELECT st.id as stid, CASE WHEN saf.id is NULL THEN False ELSE True END as flag
                            FROM stations st
                            LEFT JOIN station_additional_features saf ON saf.code = st.station_additional_feature_code and saf.name like '%Печать ЭМ%'
                      ) ppk ON ppk.stid = st.id

                -- Количество Аудиторий на экзамен --

                LEFT JOIN (SELECT est.id as estid, count(distinct(c.id)) as cnt
                            FROM stations st
							JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
							JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
                            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
                            WHERE st.deleted_at is NULL
                            group by est.id
                      ) auds ON auds.estid = est.id

                -- Количество членов ГЭК --

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%лен ГЭК%') and (et.name like '%ЕГЭ%' or et.name like 'Апроб%')
                        group by est.id
                      ) geks11 ON geks11.estid = est.id

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%лен ГЭК%') and et.name like '%ОГЭ%'
                        group by est.id
                      ) geks9 ON geks9.estid = est.id

                -- Количество Руководителей ППЭ --

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Руководитель ППЭ%') and et.name like '%ОГЭ%'
                        group by est.id
                      ) ruk9 ON ruk9.estid = est.id

				       LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Руководитель ППЭ%') and (et.name like '%ЕГЭ%' or et.name like 'Апроб%')
                        group by est.id
                      ) ruk11 ON ruk11.estid = est.id


                -- Количество Организаторов в аудитории ППЭ --

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Организатор в аудитории%') and et.name like '%ОГЭ%'
                        group by est.id
                      ) orgin9 ON orgin9.estid = est.id

				       LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Организатор в аудитории%') and (et.name like '%ЕГЭ%' or et.name like 'Апроб%')
                        group by est.id
                      ) orgin11 ON orgin11.estid = est.id

                -- Количество Организаторов вне аудитории ППЭ --

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						            JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						            JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						            JOIN exams ex ON ex.id = est.exam_id
						            JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Организатор вне аудитории%') and et.name like '%ОГЭ%'
                        group by est.id
                      ) orgout9 ON orgout9.estid = est.id

				       LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						            JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						            JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						            JOIN exams ex ON ex.id = est.exam_id
						            JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Организатор вне аудитории%') and (et.name like '%ЕГЭ%' or et.name like 'Апроб%')
                        group by est.id
                      ) orgout11 ON orgout11.estid = est.id

                -- Количество Технических специалистов ППЭ --

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Технический специалист ППЭ%') and et.name like '%ОГЭ%'
                        group by est.id
                      ) tech9 ON tech9.estid = est.id

				       LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Технический специалист ППЭ%') and (et.name like '%ЕГЭ%' or et.name like 'Апроб%')
                        group by est.id
                      ) tech11 ON tech11.estid = est.id

                -- Количество Медицинских работников --

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Медицинский работник%') and et.name like '%ОГЭ%'
                        group by est.id
                      ) med9 ON med9.estid = est.id

				       LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Медицинский работник%') and (et.name like '%ЕГЭ%' or et.name like 'Апроб%')
                        group by est.id
                      ) med11 ON med11.estid = est.id

                -- Количество Специалистов по инструктажу и лабораторным работам --

                LEFT JOIN (SELECT est.id as estid, count(distinct(e.id)) as cnt
                        FROM stations st
						JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
                        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
						JOIN employees e ON e.id = ese.employee_id
                        JOIN station_posts sp ON sp.code = ese.station_post_code
						JOIN exams ex ON ex.id = est.exam_id
						JOIN exam_types et ON et.id = ex.exam_type_id
                        where e.deleted_at is NULL and sp.name like ('%Специалист по инструктажу и лабораторным работам%') and et.name like '%ОГЭ%'
                        group by est.id
                      ) spec9 ON spec9.estid = est.id


            WHERE st.deleted_at is NULL
            ) s ON s.estid = est.id

            WHERE st.deleted_at is NULL) com ON com.estid = est.id
            and COALESCE(geks_about, '') || COALESCE(techs_about, '') || COALESCE(ruks_about, '') || COALESCE(meds_about, '')
      || COALESCE(spec_about, '') || COALESCE(orgins_about, '') || COALESCE(orgouts_about, '') <> ''

    WHERE st.deleted_at is NULL and st.code <> '4100' and sj.name not like '%устная часть%'
    and (af.name in ('ЕГЭ', 'ГВЭ') or (sj.name like '%сский язык%' or sj.name like '%атематик%'))


SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    puts ("some posts per one day")

    sql = <<SQL
    SELECT o.id as oid, m.id as mid, e.id as eid, 'На ' || esp.date || ' сотрудник назначен в ' || cast (esp.cnt as varchar(3)) || ' должности(ях) в один день.' as comment
    from employees e
    JOIN (SELECT e.id as eid, ex.date, st.id as stid, count(distinct(sp.name)) as cnt
		FROM stations st
		JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
		JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
		JOIN employees e ON e.id = ese.employee_id
		JOIN station_posts sp ON sp.code = ese.station_post_code
		JOIN exams ex ON ex.id = est.exam_id
		JOIN exam_types et ON et.id = ex.exam_type_id
		where e.deleted_at is NULL
	  	group by e.id, ex.date, st.id
		) esp ON esp.eid = e.id
    JOIN stations st ON st.id = esp.stid
    JOIN ous o ON o.id = st.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    where e.deleted_at is NULL and cnt > 1
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end



    puts ("not equal station posts")

    sql = <<SQL
      SELECT e.id as eid, m.id as mid, o.id as oid, 'Предупреждение: сотрудник назначен в ППЭ не в своей должности' as comment
          FROM stations st
          JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
          JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
          JOIN employees e ON e.id = ese.employee_id
          JOIN station_posts sp ON sp.code = ese.station_post_code
          LEFT JOIN station_posts spe ON spe.code = e.station_post_code and substring(sp.name, 1, 11) = substring(spe.name, 1, 11)
          LEFT JOIN station_posts spo ON spo.code = e.station_post_code_oge and substring(sp.name, 1, 11) = substring(spo.name, 1, 11)
          JOIN exams ex ON ex.id = est.exam_id
          JOIN exam_types et ON et.id = ex.exam_type_id
          JOIN subjects sj ON sj.code = ex.subject_code
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
      where e.deleted_at is NULL and (coalesce(spo.id, 0) + coalesce (spe.id, 0) < 1)
      and sj.name not like '%устная часть%' and sp.name not like 'Специалист%'

      group by e.id, m.id, o.id


SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    puts ("not equal stations")

    sql = <<SQL
      SELECT e.id as eid, m.id as mid, o.id as oid, 'Предупреждение: сотрудник назначен не в ППЭ, к которому прикреплён' as comment
          FROM stations st
          JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
          JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
		  JOIN exams ex ON ex.id = est.exam_id
		  JOIN exam_types et ON et.id = ex.exam_type_id
          JOIN employees e ON e.id = ese.employee_id
          JOIN station_posts sp ON sp.code = ese.station_post_code
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
      where e.deleted_at is NULL and
	  ((cast(st.code as int) <> cast(e.on_station as int) and (et.name like '%ЕГЭ%' or et.name like 'Апроб%')) or
	   (cast(st.code as int) <> cast(e.on_station_oge as int) and et.name like '%ОГЭ%'))
      group by e.id, m.id, o.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    puts ("physic specialist without physic subject")

    sql = <<SQL
    SELECT e.id as eid, m.id as mid, o.id as oid, 'У сотрудника в должности Специалиста по физике отсутсвует предмет физика' as comment
        FROM stations st
        JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
        JOIN employees e ON e.id = ese.employee_id
        JOIN station_posts sp ON sp.code = ese.station_post_code
        JOIN exams ex ON ex.id = est.exam_id
        JOIN exam_types et ON et.id = ex.exam_type_id
        JOIN subjects sj ON sj.code = ex.subject_code
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
		    LEFT JOIN employee_ous eo ON eo.employee_id = e.id and eo.deleted_at is NULL
		    LEFT JOIN employee_ou_subjects esj ON esj.employee_ou_id = eo.id and esj.subject_code = 3
    where e.deleted_at is NULL and et.name like '%ОГЭ%' and esj.id is NULL
    and sj.name = 'Физика' and sp.name like 'Специалист%'
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    puts ("history on sociology")

    sql = <<SQL
    SELECT e.id as eid, m.id as mid, o.id as oid, sj.name, 'Специалист по истории не может присутствовать на экзамене по обществознанию' as comment
        FROM stations st
        JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
        JOIN employees e ON e.id = ese.employee_id
        JOIN station_posts sp ON sp.code = ese.station_post_code
        JOIN exams ex ON ex.id = est.exam_id
        JOIN exam_types et ON et.id = ex.exam_type_id
        JOIN subjects sj ON sj.code = ex.subject_code
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
		    LEFT JOIN employee_ous eo ON eo.employee_id = e.id and eo.deleted_at is NULL
		    LEFT JOIN employee_ou_subjects esj ON esj.employee_ou_id = eo.id
        LEFT JOIN subjects sj2 ON sj2.code = esj.subject_code and sj2.name like '%История%'
    where e.deleted_at is NULL and et.name like '%ОГЭ%' and sj2.id is not NULL
    and sj.name like '%Обществознание%' and sp.name like 'Организатор%'
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    puts ("equal speciality and exam subjects")

    sql = <<SQL
    SELECT e.id as eid, m.id as mid, o.id as oid, 'Совпадение специализации сотрудника и предмета на ' || cast(count(ex.id) as varchar(3)) || ' экзамене(ах)' as comment
        FROM stations st
        JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
        JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
        JOIN employees e ON e.id = ese.employee_id
        JOIN station_posts sp ON sp.code = ese.station_post_code
        JOIN exams ex ON ex.id = est.exam_id
        JOIN exam_types et ON et.id = ex.exam_type_id
        JOIN subjects sj ON sj.code = ex.subject_code
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        LEFT JOIN employee_ous eo ON eo.employee_id = e.id and eo.deleted_at is NULL
        LEFT JOIN employee_ou_subjects esj ON esj.employee_ou_id = eo.id
        LEFT JOIN subjects sj2 ON sj2.code = esj.subject_code and substring(coalesce(sj2.name, ''), 1, 7) = substring(coalesce(sj.name, ''), 1, 7)
    where e.deleted_at is NULL and et.name like '%ОГЭ%' and sj2.id is not NULL and sj.name not like '%в устной%'
    and (sp.name like 'Организатор%' or sp.name like '%обеседни%')  
    group by e.id, m.id, o.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    puts ("oge lans in ege")

    sql = <<SQL
    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid,
			'Сотруднику ГИА-11 указана несуществующая должность' as comment
      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
	  join station_posts sp ON sp.code = e.station_post_code
	  where e.deleted_at is NULL and (sp.name like '%нструктаж%' or sp.name like '%собеседник%')
	  group by e.id, o.id, m1.id, m2.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end



    puts ("some ppe heads in ppe")

    sql = <<SQL
    SELECT m.id as mid, o.id as oid, ruks.cnt, 'На дату ' || ex.date || ' в ППЭ ' || st.code || ' назначено ' || ruks.cnt || ' руководителей ППЭ.' as comment
        FROM stations st
        JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
		    JOIN exams ex ON ex.id = est.exam_id

        JOIN (SELECT st.id as stid, ex.date, count(distinct(e.id)) as cnt
			  FROM stations st
        	  JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
        	  JOIN exam_station_employees ese on ese.exam_station_id = est.id and ese.deleted_at is NULL
			  JOIN employees e ON e.id = ese.employee_id and e.deleted_at is NULL
       		  JOIN station_posts sp ON sp.code = ese.station_post_code
			  JOIN exams ex ON ex.id = est.exam_id
			  WHERE sp.name like '%Руководитель ППЭ%'
			  group by st.id, ex.date
			) as ruks on st.id = ruks.stid and ex.date = ruks.date

        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id

		WHERE ruks.cnt > 1

    group by m.id, o.id, ruks.cnt, ex.date, st.code, ruks.cnt
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("specialization equal exam")

    sql = <<SQL
    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid,
			'Специализация сотрудника совпадает с экзаменом' as comment
      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
	  left join employee_ou_subjects eos ON eos.employee_ou_id = eo.id and eo.deleted_at is NULL
	  join exam_station_employees ese ON ese.employee_id = e.id and ese.deleted_at is NULL
	  join exam_stations est ON est.id = ese.exam_station_id and est.deleted_at is NULL
	  join exams ex ON ex.id = est.exam_id and ex.deleted_at is NULL
	  join station_posts sp ON sp.code = ese.station_post_code
	  where e.deleted_at is NULL and ex.subject_code = eos.subject_code and eos.subject_code is NOT NULL
	  and sp.name like '%рганизатор%'
	  group by e.id, o.id, m1.id, m2.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end


  end
end