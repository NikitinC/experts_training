module PaksKege
  def process
    puts ("kegecomputers is not fill")

    sql = <<SQL
    select m.id as mid, o.id as oid, c.id as cid, 
        'В аудитории '|| c.number || ' ППЭ ' || s.code || ', не указано количество ПАК КЕГЭ' as comment
      from classrooms c
      join exam_station_classrooms esc ON esc.classroom_id = c.id and esc.deleted_at is NULL
      join exam_stations es ON es.id = esc.exam_station_id and es.deleted_at is NULL
      join exams e ON e.id = es.exam_id and e.subject_code = 5 and e.deleted_at is NULL
      join attestation_forms af ON af.code = e.attestation_form_code
      join stations s ON s.id = es.station_id and s.deleted_at is NULL
      join exam_types et ON et.id = e.exam_type_id and et.deleted_at is NULL and et.name like '%ЕГЭ%'
      join ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
    where c.deleted_at is NULL and coalesce(c.kegecomputers, -1) < 1
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :kegecomputers, r['cid'], 'Classroom', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
    select m.id as mid, o.id as oid, c.id as cid, 
        'В аудитории '|| c.number || ' ППЭ ' || s.code ||
			', количество мест, назначенных на экзамен КЕГЭ больше вместимости аудитории (' || cast(coalesce(esc.places, 0) as varchar(3)) || ' > ' || cast(coalesce(c.kegecomputers, 0) as varchar(3)) || ')' as comment
      from classrooms c
      join exam_station_classrooms esc ON esc.classroom_id = c.id and esc.deleted_at is NULL
      join exam_stations es ON es.id = esc.exam_station_id and es.deleted_at is NULL
      join exams e ON e.id = es.exam_id and e.subject_code = 5 and e.deleted_at is NULL
      join attestation_forms af ON af.code = e.attestation_form_code
      join stations s ON s.id = es.station_id and s.deleted_at is NULL
      join exam_types et ON et.id = e.exam_type_id and et.deleted_at is NULL and et.name like '%ЕГЭ%'
      join ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
    where c.deleted_at is NULL and coalesce(c.kegecomputers, -1) < esc.places and esc.places is NOT NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :kegecomputers, r['cid'], 'Classroom', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
    select m.id as mid, o.id as oid, c.id as cid, 
        'В аудитории '|| c.number || ' ППЭ ' || s.code ||
			', количество мест, назначенных на экзамен '|| e.date || ', больше вместимости аудитории (' || cast(coalesce(esc.places, 0) as varchar(3)) || ' > ' || cast(coalesce(c.available_seats, 0) as varchar(3)) || ')' as comment
      from classrooms c
      join exam_station_classrooms esc ON esc.classroom_id = c.id and esc.deleted_at is NULL
      join exam_stations es ON es.id = esc.exam_station_id and es.deleted_at is NULL
      join exams e ON e.id = es.exam_id and e.deleted_at is NULL
      join attestation_forms af ON af.code = e.attestation_form_code
      join stations s ON s.id = es.station_id and s.deleted_at is NULL
      join ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
    where c.deleted_at is NULL and coalesce(c.available_seats, -1) < esc.places and esc.places is NOT NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :classroomsplanned, r['cid'], 'Classroom', r['oid'], r['mid'], r['comment']
    end



  end
end