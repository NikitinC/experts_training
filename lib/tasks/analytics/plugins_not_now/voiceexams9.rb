module Voiceexams9
  def process
    puts ("voice exams for 9 doesn't in one day")

    sql = <<SQL
           SELECT s.id as sid, o.id as oid, m.id as mid, 'Учащийся назначен в разные дни экзаменов по иностранным или не назначен на одну из частей'
            FROM students s
            JOIN attestation_form_students afs ON afs.student_id = s.id
            JOIN attestation_forms af ON af.code = afs.attestation_form_code
            JOIN groups g ON g.id = s.group_id
            JOIN ous o ON o.id = g.ou_id
            JOIN mouos m ON m.id = o.mouo_id
            JOIN (SELECT s.id as sid
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (9, 10, 11, 13, 14, 29, 30, 31, 33, 34)
                group by s.id
                ) iya ON iya.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (9)
                ) eng ON eng.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (29)
                ) engu ON engu.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (10)
                ) deu ON deu.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (30)
                ) deuu ON deuu.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (11)
                ) fra ON fra.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (31)
                ) frau ON frau.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (13)
                ) isp ON isp.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (33)
                ) ispu ON ispu.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (14)
                ) kit ON kit.sid = s.id
            LEFT JOIN (SELECT s.id as sid, e.date
                FROM students s
                JOIN exam_students es ON es.student_id = s.id
                JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and es.deleted_at is NULL
                AND sj.fiscode in (34)
                ) kitu ON kitu.sid = s.id
              WHERE s.deleted_at is NULL and afs.deleted_at is NULL and o.deleted_at is NULL
              and af.name in ('ОГЭ', 'ГВЭ-9')
              and o.code not in ('999999', '111111') 
            and (
            coalesce(eng.date, '1956-01-01') <> coalesce(engu.date, '1956-01-01')
                  or
            coalesce(fra.date, '1956-01-01') <> coalesce(frau.date, '1956-01-01')
                    or
            coalesce(deu.date, '1956-01-01') <> coalesce(deuu.date, '1956-01-01')
                      or
            coalesce(kit.date, '1956-01-01') <> coalesce(kitu.date, '1956-01-01')
              )

SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :verbal_exams_equal_dates_9, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end
