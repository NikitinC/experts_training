module NoRt
  def process

#     sql = <<SQL
#       SELECT		s.id as sid, o.id as oid, m.id as mid,
#             exam.sid as rya,
#             'Не назначен на русский язык' as comment
#             -- 'Не назначен на математику' as comment
#         FROM students s
#         JOIN groups g ON g.id = s.group_id
#         JOIN ous o ON o.id = g.ou_id
#         JOIN mouos m ON m.id = o.mouo_id
#         LEFT JOIN (SELECT es.student_id as sid
#                FROM exam_students es
#                JOIN exams e ON e.id = es.exam_id
#                JOIN subjects sj ON sj.code = e.subject_code
#                WHERE sj.name like '%сский язык%'
#                -- WHERE sj.name like '%атематик%'
#                and es.deleted_at is NULL
#                GROUP BY es.student_id
#               ) exam ON exam.sid = s.id
#
#         WHERE s.deleted_at is NULL and g.number in (11, 12) and s.ege_participant_category_code in (1) and s.gia_active_results_rus = false
#         and exam.sid is NULL
# SQL
#
#     ActiveRecord::Base.connection.execute(sql).each do |r|
#       add_analytics_item :no_russian, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
#     end




    sql = <<SQL
      SELECT		s.id as sid, o.id as oid, m.id as mid,
            exam.sid as rya,
           -- 'Не назначен на русский язык' as comment
            'Не назначен на математику' as comment
        FROM students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        LEFT JOIN (SELECT es.student_id as sid 
               FROM exam_students es
               JOIN exams e ON e.id = es.exam_id
               JOIN subjects sj ON sj.code = e.subject_code
               -- WHERE sj.name like '%сский язык%'
               WHERE sj.name like '%атематик%'
               and es.deleted_at is NULL
               GROUP BY es.student_id
              ) exam ON exam.sid = s.id
      
        WHERE s.deleted_at is NULL and g.number in (9, 10, 11, 12) and s.ege_participant_category_code in (1) and s.gia_active_results_math = false
        and exam.sid is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_math, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end



  end
end