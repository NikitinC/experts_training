module Everystart
  def process

    sql = <<SQL
      SELECT ROW_NUMBER() OVER(ORDER BY l.name)
      ,
      'INSERT INTO public.employee_ous(id, ou_id, employee_id, employee_post_code, pluralist, deleted_at, created_at, updated_at, valid_cache) VALUES (' 
        || cast(ROW_NUMBER() OVER(ORDER BY l.name) - 1 + eoid.id as varchar) || ', ' || cast(o.id as varchar) || ', ' || cast(e.id as varchar) || ', ' || ep.code || ', '
          || 'False, NULL, ''' || cast(l.created_at as varchar) || ''', ''' || cast(l.updated_at as varchar) || ''', False );' as call,
          l.name
          
        FROM public.employees e
          LEFT JOIN employee_ous eo ON e.id = eo.employee_id and eo.deleted_at is NULL
          JOIN rlog_items l ON l.record_id = e.id and l.record_type = 'Employee'
          JOIN users u ON u.id = l.user_id and target_type = 'Ou'
          JOIN ous o ON o.id = u.target_id
          JOIN employee_posts ep ON ep.name like e.job_post
          JOIN (SELECT id+1 as id FROM employee_ous ORDER BY id DESC LIMIT 1) eoid ON 1 = 1
      WHERE e.deleted_at is NULL and eo.id is NULL and record_changes like '%job_post%'
      and record_changes not like '%deleted_at%'
      and l.updated_at > '2018-12-01'
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      ActiveRecord::Base.connection.exec_query(r['call'])
    end

    ActiveRecord::Base.connection.exec_query("update students set	document_number = document_series, document_series = '' where id in (select id from students where coalesce(document_number, '') = '' and document_series is NOT NULL)")

    sql = <<SQL
    SELECT e.id as eid, d.id as did, cast(ROW_NUMBER() OVER(ORDER BY e.id) as varchar),
      CASE
        WHEN eo.employee_post_code is NULL THEN
          'INSERT INTO public.employee_ous(id, ou_id, employee_id, employee_post_code, pluralist, deleted_at, created_at, updated_at, valid_cache) VALUES ('
          || cast(ROW_NUMBER() OVER(ORDER BY e.id) + 1 + eoid.id as varchar) || ', ' || cast(o.id as varchar) || ', ' || cast(e.id as varchar) || ', 163 , '
          || 'False, NULL, ''' || cast(d.created_at as varchar) || ''', ''' || cast(d.updated_at as varchar) || ''', False );'
        ELSE
          'INSERT INTO public.employee_ous(id, ou_id, employee_id, employee_post_code, pluralist, deleted_at, created_at, updated_at, valid_cache) VALUES ('
          || cast(ROW_NUMBER() OVER(ORDER BY e.id) + 1 + eoid.id as varchar) || ', ' || cast(o.id as varchar) || ', ' || cast(e.id as varchar) || ', '|| 
          cast(eo.employee_post_code as varchar) || ', ' || 'False, NULL, ''' || cast(d.created_at as varchar) || ''', ''' || cast(d.updated_at as varchar) || ''', False );'
      END as call		
      
      FROM drafts d
      JOIN employees e ON	d.data like '%' || e.first_name || '%'
                and d.data like '%' || e.middle_name || '%'
                and d.data like '%' || e.second_name || '%'
                and d.data like '%' || e.document_series || '%'
                and d.data like '%' || e.document_number || '%'
      JOIN (SELECT id+1 as id FROM employee_ous ORDER BY id DESC LIMIT 1) eoid ON 1 = 1
      JOIN users u ON u.id = d.user_id and u.target_type = 'Ou'
      JOIN ous o ON o.id = u.target_id
      LEFT JOIN (SELECT employee_id FROM employee_ous WHERE deleted_at is NULL GROUP BY employee_id) eonh ON eonh.employee_id = e.id
      LEFT JOIN (SELECT employee_id, min(employee_post_code) as employee_post_code FROM employee_ous eo GROUP BY (employee_id)) eo
        ON eo.employee_id = e.id
    where d.target_type = 'Employee' and e.deleted_at is NULL and eonh.employee_id is NULL
    order by e.id ASC
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      ActiveRecord::Base.connection.exec_query(r['call'])
    end


    sql = <<SQL
    update employees
	  set deleted_at = ':261'
    where id in (
      select e.id
      from employees e
      where e.deleted_at is NULL and e.id not in (select e.id
                          from employees e
                          join employee_ous eo ON e.id = eo.employee_id and eo.deleted_at is NULL
                           )
      )
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      ActiveRecord::Base.connection.exec_query(r['call'])
    end

  end
end


