module BrokenEmails
  def process
    puts ("all emails in one")


    sql = <<SQL
select m.id as mid, o.id as oid, e.id as eid, 
		'Не распознан домен Email сотрудника ' || email || '.' as comment
      from employees e
      join employee_ous eo on eo.employee_id=e.id
      join ous o on o.id=eo.ou_id
      join mouos m on m.id=o.mouo_id
      where 
        e.deleted_at is null and o.deleted_at is null and m.deleted_at is null  and eo.deleted_at is NULL
        and email not like '%@mail.ru'
        and right(email, length(email) - strpos(email, '@'))
        not in (
          'lenta.ru', 'school3-prv.ru', 'gimnazia99.ru', 'mouoslb.ru', 'школа100нт.рф', 'mail.ru', 'nm.ru', 'i-dist.ru',
          'school12al.ru', 'school12al.ru', 'makarenko.net', 'k66.ru', 'skbkontur.ru', 'newschool184.ru', 'moucot.ru',
          'mbou112.ru', 'ubs-ekb.ru', '62school.ru', 'mil.ru', 'myrambler.ru', 'planet-a.ru', 'kalinovo.ru',
          'aramilgo.ru', 'licey21.su', 'ou4.ru', 'ou22.ru', 'school4vp.ru', 'school13p.ru', 'bk.ru', 'yandex.ru',
          'gmail.com', 'pervouralsk.ru', 'goreftinsky.ru', 'kamensktel.ru', 'list.ru', 'uovp.ru', 'ekarpinsk.ru', 
          'severouralsk-edu.ru', 'ekadm.ru', 'uobgd.ru', 'edu-lesnoy.ru', 'adm-serov.ru', 'goruomoukru.ru',
          'zarobraz.ru', 'palladant.ru', 'rambler.ru', 'palladant.ru', 'inbox.ru', 'ya.ru', 'urfu.ru', 'yaxon.ru',
          'narod.ru', 'sch56-ngo.ru', 'koriphey.ru', 'lyceum130.ru', 'live.ru', 'icloud.com', 'gim47.ru', 'sc43.ru',
          'e1.ru', 'ekb27.ru', 'ekb55.ru', '124ural.ru', '22198.ru', '22vp.ru', '25sch.ru', '2exam.ru', '313dgb.ru', 
          '66.ru', 'akado-ural.ru', 'al-alex.ru', 'arefyev.su', 'Bk.ru', 'centerecho.ru', 'citydom.ru', 'crbkruf.ru',
          'crb-sysert.ru', 'dgbnt.ru', 'dla8.ru', 'dmail.com', 'ekb128.ru', 'hotmail.com', 'imail.ru', 'irro.ru',
          'isnet.ru', 'kadet-kazak.ru', 'kgo66.ru', 'km.ru', 'kzrb.ru', 'licej3.ru', 'licey21.ru', 'live.com',
          'mail.g177.ru', 'mart-school.ru', 'mkdou1.ru', 'oktschool18.ru', 'oovgo.ru', 'outlook.com', 'reft-17.ru',
          'ro.ru', 's1serov.ru', 'schl64.ru', 'school138nt.ru', 'school22-serov.ru', 'lc12.ru', 'school65.ru',
          'schule32.org', 'sky.ru', 'somkural.ru', 'sptserov.ru', 'sv66.ru', 'sysert.ru', 'ul66.ru', 'uor-ekb.ru', 'yahoo.com',
          'skytekh.com', 'mou60.com', 'yandex.by', 'yandex.com', 'avegen.com', 'yandex.kz', 'yandex.ua', 'gsosh.bizml.ru',
          'school62.ru', 'ecole39.ru', 'internet.ru', 'mis66.ru', 'your-tools.ru', 'shkola8.net', 'eduekb.ru', 'olympus.ru'
        )
        and strpos(email, ' ') = 0
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :emails, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end


    sql = <<SQL
select m.id as mid, o.id as oid,
		'Не распознан домен Email руководителя ОО ' || o.chief_email || '.' as comment
	from ous o
	join mouos m on m.id=o.mouo_id
	where 
		o.deleted_at is null and m.deleted_at is null 
		and right(o.chief_email, length(o.chief_email) - strpos(o.chief_email, '@'))
        not in (
          'lenta.ru', 'school3-prv.ru', 'gimnazia99.ru', 'mouoslb.ru', 'школа100нт.рф', 'mail.ru', 'nm.ru', 'i-dist.ru',
          'school12al.ru', 'school12al.ru', 'makarenko.net', 'k66.ru', 'skbkontur.ru', 'newschool184.ru', 'moucot.ru',
          'mbou112.ru', 'ubs-ekb.ru', '62school.ru', 'mil.ru', 'myrambler.ru', 'planet-a.ru', 'kalinovo.ru',
          'aramilgo.ru', 'licey21.su', 'ou4.ru', 'ou22.ru', 'school4vp.ru', 'school13p.ru', 'bk.ru', 'yandex.ru',
          'gmail.com', 'pervouralsk.ru', 'goreftinsky.ru', 'kamensktel.ru', 'list.ru', 'uovp.ru', 'ekarpinsk.ru', 
          'severouralsk-edu.ru', 'ekadm.ru', 'uobgd.ru', 'edu-lesnoy.ru', 'adm-serov.ru', 'goruomoukru.ru',
          'zarobraz.ru', 'palladant.ru', 'rambler.ru', 'palladant.ru', 'inbox.ru', 'ya.ru', 'urfu.ru', 'yaxon.ru',
          'narod.ru', 'sch56-ngo.ru', 'koriphey.ru', 'lyceum130.ru', 'live.ru', 'icloud.com', 'gim47.ru', 'sc43.ru',
          'e1.ru', 'ekb27.ru', 'ekb55.ru', '124ural.ru', '22198.ru', '22vp.ru', '25sch.ru', '2exam.ru', '313dgb.ru', 
          '66.ru', 'akado-ural.ru', 'al-alex.ru', 'arefyev.su', 'Bk.ru', 'centerecho.ru', 'citydom.ru', 'crbkruf.ru',
          'crb-sysert.ru', 'dgbnt.ru', 'dla8.ru', 'dmail.com', 'ekb128.ru', 'hotmail.com', 'imail.ru', 'irro.ru',
          'isnet.ru', 'kadet-kazak.ru', 'kgo66.ru', 'km.ru', 'kzrb.ru', 'licej3.ru', 'licey21.ru', 'live.com',
          'mail.g177.ru', 'mart-school.ru', 'mkdou1.ru', 'oktschool18.ru', 'oovgo.ru', 'outlook.com', 'reft-17.ru',
          'ro.ru', 's1serov.ru', 'schl64.ru', 'school138nt.ru', 'school22-serov.ru', 'lc12.ru', 'school65.ru',
          'schule32.org', 'sky.ru', 'somkural.ru', 'sptserov.ru', 'sv66.ru', 'sysert.ru', 'ul66.ru', 'uor-ekb.ru', 'yahoo.com',
          'skytekh.com', 'mou60.com', 'yandex.by', 'yandex.com', 'avegen.com', 'yandex.kz', 'yandex.ua', 'gsosh.bizml.ru',
          'school62.ru', 'ecole39.ru', 'internet.ru', 'mis66.ru', 'your-tools.ru', 'shkola8.net', 'eduekb.ru', 'olympus.ru'
          ,'liceum75.ru', 'uspu.su', 'ural.rt.ru', 'db11.ru'
        )
    and strpos(o.chief_email, ' ') = 0

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :ou_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    sql = <<SQL
select m.id as mid, o.id as oid,
		'Не распознан домен Email ответственного за ГИА в ОО ' || o.employee_preparation_ege_email || '.' as comment
	from ous o
	join mouos m on m.id=o.mouo_id
	where 
		o.deleted_at is null and m.deleted_at is null 
		and right(o.employee_preparation_ege_email, length(o.employee_preparation_ege_email) - strpos(o.employee_preparation_ege_email, '@'))
        not in (
          'lenta.ru', 'school3-prv.ru', 'gimnazia99.ru', 'mouoslb.ru', 'школа100нт.рф', 'mail.ru', 'nm.ru', 'i-dist.ru',
          'school12al.ru', 'school12al.ru', 'makarenko.net', 'k66.ru', 'skbkontur.ru', 'newschool184.ru', 'moucot.ru',
          'mbou112.ru', 'ubs-ekb.ru', '62school.ru', 'mil.ru', 'myrambler.ru', 'planet-a.ru', 'kalinovo.ru',
          'aramilgo.ru', 'licey21.su', 'ou4.ru', 'ou22.ru', 'school4vp.ru', 'school13p.ru', 'bk.ru', 'yandex.ru',
          'gmail.com', 'pervouralsk.ru', 'goreftinsky.ru', 'kamensktel.ru', 'list.ru', 'uovp.ru', 'ekarpinsk.ru', 
          'severouralsk-edu.ru', 'ekadm.ru', 'uobgd.ru', 'edu-lesnoy.ru', 'adm-serov.ru', 'goruomoukru.ru',
          'zarobraz.ru', 'palladant.ru', 'rambler.ru', 'palladant.ru', 'inbox.ru', 'ya.ru', 'urfu.ru', 'yaxon.ru',
          'narod.ru', 'sch56-ngo.ru', 'koriphey.ru', 'lyceum130.ru', 'live.ru', 'icloud.com', 'gim47.ru', 'sc43.ru',
          'e1.ru', 'ekb27.ru', 'ekb55.ru', '124ural.ru', '22198.ru', '22vp.ru', '25sch.ru', '2exam.ru', '313dgb.ru', 
          '66.ru', 'akado-ural.ru', 'al-alex.ru', 'arefyev.su', 'Bk.ru', 'centerecho.ru', 'citydom.ru', 'crbkruf.ru',
          'crb-sysert.ru', 'dgbnt.ru', 'dla8.ru', 'dmail.com', 'ekb128.ru', 'hotmail.com', 'imail.ru', 'irro.ru',
          'isnet.ru', 'kadet-kazak.ru', 'kgo66.ru', 'km.ru', 'kzrb.ru', 'licej3.ru', 'licey21.ru', 'live.com',
          'mail.g177.ru', 'mart-school.ru', 'mkdou1.ru', 'oktschool18.ru', 'oovgo.ru', 'outlook.com', 'reft-17.ru',
          'ro.ru', 's1serov.ru', 'schl64.ru', 'school138nt.ru', 'school22-serov.ru', 'lc12.ru', 'school65.ru',
          'schule32.org', 'sky.ru', 'somkural.ru', 'sptserov.ru', 'sv66.ru', 'sysert.ru', 'ul66.ru', 'uor-ekb.ru', 'yahoo.com',
          'skytekh.com', 'mou60.com', 'yandex.by', 'yandex.com', 'avegen.com', 'yandex.kz', 'yandex.ua', 'gsosh.bizml.ru',
          'school62.ru', 'ecole39.ru', 'internet.ru', 'mis66.ru', 'your-tools.ru', 'shkola8.net', 'eduekb.ru', 'olympus.ru'
          ,'liceum75.ru', 'uspu.su', 'ural.rt.ru', 'db11.ru'
        )
    and strpos(o.employee_preparation_ege_email, ' ') = 0
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :ou_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


sql = <<SQL
select m.id as mid, o.id as oid,
		'Не распознан домен Email ответственного за информационный обмен в ОО ' || o.information_exchange_email || '.' as comment
	from ous o
	join mouos m on m.id=o.mouo_id
	where 
		o.deleted_at is null and m.deleted_at is null 
    and right(o.information_exchange_email, length(o.information_exchange_email) - strpos(o.information_exchange_email, '@'))
        not in (
          'lenta.ru', 'school3-prv.ru', 'gimnazia99.ru', 'mouoslb.ru', 'школа100нт.рф', 'mail.ru', 'nm.ru', 'i-dist.ru',
          'school12al.ru', 'school12al.ru', 'makarenko.net', 'k66.ru', 'skbkontur.ru', 'newschool184.ru', 'moucot.ru',
          'mbou112.ru', 'ubs-ekb.ru', '62school.ru', 'mil.ru', 'myrambler.ru', 'planet-a.ru', 'kalinovo.ru',
          'aramilgo.ru', 'licey21.su', 'ou4.ru', 'ou22.ru', 'school4vp.ru', 'school13p.ru', 'bk.ru', 'yandex.ru',
          'gmail.com', 'pervouralsk.ru', 'goreftinsky.ru', 'kamensktel.ru', 'list.ru', 'uovp.ru', 'ekarpinsk.ru', 
          'severouralsk-edu.ru', 'ekadm.ru', 'uobgd.ru', 'edu-lesnoy.ru', 'adm-serov.ru', 'goruomoukru.ru',
          'zarobraz.ru', 'palladant.ru', 'rambler.ru', 'palladant.ru', 'inbox.ru', 'ya.ru', 'urfu.ru', 'yaxon.ru',
          'narod.ru', 'sch56-ngo.ru', 'koriphey.ru', 'lyceum130.ru', 'live.ru', 'icloud.com', 'gim47.ru', 'sc43.ru',
          'e1.ru', 'ekb27.ru', 'ekb55.ru', '124ural.ru', '22198.ru', '22vp.ru', '25sch.ru', '2exam.ru', '313dgb.ru', 
          '66.ru', 'akado-ural.ru', 'al-alex.ru', 'arefyev.su', 'Bk.ru', 'centerecho.ru', 'citydom.ru', 'crbkruf.ru',
          'crb-sysert.ru', 'dgbnt.ru', 'dla8.ru', 'dmail.com', 'ekb128.ru', 'hotmail.com', 'imail.ru', 'irro.ru',
          'isnet.ru', 'kadet-kazak.ru', 'kgo66.ru', 'km.ru', 'kzrb.ru', 'licej3.ru', 'licey21.ru', 'live.com',
          'mail.g177.ru', 'mart-school.ru', 'mkdou1.ru', 'oktschool18.ru', 'oovgo.ru', 'outlook.com', 'reft-17.ru',
          'ro.ru', 's1serov.ru', 'schl64.ru', 'school138nt.ru', 'school22-serov.ru', 'lc12.ru', 'school65.ru',
          'schule32.org', 'sky.ru', 'somkural.ru', 'sptserov.ru', 'sv66.ru', 'sysert.ru', 'ul66.ru', 'uor-ekb.ru', 'yahoo.com',
          'skytekh.com', 'mou60.com', 'yandex.by', 'yandex.com', 'avegen.com', 'yandex.kz', 'yandex.ua', 'gsosh.bizml.ru',
          'school62.ru', 'ecole39.ru', 'internet.ru', 'mis66.ru', 'your-tools.ru', 'shkola8.net', 'eduekb.ru', 'olympus.ru'
          ,'liceum75.ru', 'uspu.su', 'ural.rt.ru', 'db11.ru'
        )
    and strpos(o.information_exchange_email, ' ') = 0
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :ou_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


  end
end