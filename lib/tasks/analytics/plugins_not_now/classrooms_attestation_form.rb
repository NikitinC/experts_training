module ClassroomsAttestationForm
  def process
    Classroom.find_each do |c|
      if c.attestation_forms.empty?
        add_analytics_item (c.station.is_oge_or_gve9 ? :classroom_missing_attestation_form_9 : :classroom_missing_attestation_form_11),
                           c.id, 'Classroom', c.station.ou.id, c.station.ou.mouo.id
      else
        if c.is_attestation_form_incompatible
          add_analytics_item (c.station.is_oge_or_gve9 ? :classroom_wrong_attestation_form_9 : :classroom_wrong_attestation_form_11),
                             c.id, 'Classroom', c.station.ou.id,
                             c.station.ou.mouo.id
        end
      end
    end
  end
end