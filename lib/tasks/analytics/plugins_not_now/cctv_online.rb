module CctvOnline
  def process

    puts ("online features")

    sql = <<SQL
     SELECT s.code, s.online_cctv, saf.name, s.id as stid, o.id as oid, m.id as mid, 
      case 
        when s.online_cctv = true and s.online_cctv = true and s.code in ('4302', '4303', '6112', '7001', '7002', '7301') then 'По данным 2019 года ППЭ согласован оффлайн'
        when saf.name like '%УФСИН%' and s.online_cctv = true then 'Установлен признак онлайн-видеонаблюдения для ППЭ в УФСИН'
        when saf.name like '%на дому%' and s.online_cctv = true then 'Установлен признак онлайн-видеонаблюдения для ППЭ на дому'
        when saf.name like '%ГВЭ%' and saf.name not like '%ЕГЭ%' and s.online_cctv = true then 'Установлен признак онлайн-видеонаблюдения для ППЭ ГВЭ (ЕГЭ не проводится)'
        when saf.name like '%ЕГЭ%' and s.online_cctv = false and saf.name not like '%УФСИН%' and saf.name not like '%на дому%' then 'Неверный признак ППЭ-оффлайн'
      end as comment
	
	FROM stations s
	JOIN ous o ON o.id = s.ou_id
	join mouos m ON m.id = o.mouo_id
	JOIN station_additional_features saf ON saf.code = s.station_additional_feature_code
	where s.id in (SELECT station_id as id from exam_types_stations ets JOIN exam_types et ON et.id = ets.exam_type_id where et.name like '%ЕГЭ%' group by station_id)
	and s.deleted_at is NULL
	and s.code not in ('4100')
	and (
		(saf.name like '%УФСИН%' and s.online_cctv = true)
		or
		(saf.name like '%на дому%' and s.online_cctv = true)
		or
		(saf.name like '%ГВЭ%' and saf.name not like '%ЕГЭ%' and s.online_cctv = true)
		or
		(saf.name like '%ЕГЭ%' and s.online_cctv = false and saf.name not like '%УФСИН%' and saf.name not like '%на дому%' and s.code not in ('4302', '4303', '6112', '7001', '7002', '7301'))
		or
		(s.online_cctv = true and s.code in ('4302', '4303', '6112', '7001', '7002', '7301'))
	)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_is_not_updated, r['cid'], 'Classroom', r['oid'], r['mid'], r['comment']
    end


    puts ("online features")

    sql = <<SQL
     SELECT s.code, s.online_cctv, saf.name, s.id as sid, o.id as oid, m.id as mid, c.id as cid,
      case 
        when s.code in ('4302', '4303', '6112', '7001', '7002', '7301') and coalesce(v.name, '') like '%онлайн%' then 'В аудитории №' || c.number || ' должен быть оффлайн.'
        when saf.name like '%УФСИН%' and coalesce(v.name, '') like '%онлайн%' then 'Установлен признак онлайн-видеонаблюдения для аудитории '|| c.number|| ' в ППЭ в УФСИН'
        when saf.name like '%на дому%' and coalesce(v.name, '') like '%онлайн%' then 'Установлен признак онлайн-видеонаблюдения для аудитории '|| c.number|| ' в ППЭ на дому'
        when s.online_cctv = true and coalesce(v.name, '') not like '%онлайн%' then 'Установлен признак оффлайн-видеонаблюдения для аудитории '|| c.number|| ' в ППЭ-онлайн'
        when s.online_cctv = false and coalesce(v.name, '') like '%онлайн%' then 'Установлен признак онлайн-видеонаблюдения для аудитории '|| c.number|| ' в ППЭ-оффлайн'
      end as comment
      
      FROM stations s
      JOIN classrooms c ON c.station_id = s.id
      LEFT JOIN cctvs v ON v.code = c.cctv_code
      JOIN ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
      JOIN station_additional_features saf ON saf.code = s.station_additional_feature_code
      where s.id in (SELECT station_id as id from exam_types_stations ets JOIN exam_types et ON et.id = ets.exam_type_id where et.name like '%ЕГЭ%' group by station_id)
      and s.deleted_at is NULL
      and c.deleted_at is NULL
      and s.code not in ('4100')
      and (
        (s.code in ('4302', '4303', '6112', '7001', '7002', '7301') and coalesce(v.name, '') like '%онлайн%')
        or
        (saf.name like '%УФСИН%' and coalesce(v.name, '') like '%онлайн%')
        or
        (saf.name like '%на дому%' and coalesce(v.name, '') like '%онлайн%')
        or
        (s.online_cctv = true and coalesce(v.name, '') not like '%онлайн%')
        or
        (s.online_cctv = false and coalesce(v.name, '') like '%онлайн%')
      )
    order by s.code
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :auditorium_is_not_updated, r['cid'], 'Classroom', r['oid'], r['mid'], r['comment']
    end


  end
end