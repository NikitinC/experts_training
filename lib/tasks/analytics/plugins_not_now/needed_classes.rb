module NeededClasses
  def process
    puts ("sum class places and sum of participants")

    sql = <<SQL
      DROP TABLE IF EXISTS tmp;
      CREATE TEMPORARY TABLE tmp
            (stid int, oid int, mid int, st_code varchar(15), sj_name varchar(80), date varchar(12), parts int, places int);
      
      INSERT INTO tmp
      SELECT st.id, o.id, m.id, st.code, sj.name, e.date, count(s.id) as parts, coalesce(c.s, 0)
        FROM exam_students es
        JOIN exams e ON e.id = es.exam_id and e.exam_type_id in (5, 19)
        join subjects sj ON sj.code = e.subject_code
        JOIN students s ON s.id = es.student_id
        JOIN exam_stations est ON est.id = es.exam_station_id
        LEFT JOIN (select exam_station_id, sum(c.available_seats) as s
                from exam_station_classrooms esc
                join classrooms c ON c.id = esc.classroom_id
               where c.deleted_at is NULL and esc.deleted_at is NULL
               group by exam_station_id
              ) c ON c.exam_station_id = est.id
        JOIN stations st ON st.id = est.station_id
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      where es.deleted_at is NULL and s.deleted_at is NULL and st.code <> '4100'
      group by st.code, sj.name, e.date, c.s, st.id, m.id, o.id;
      
      SELECT stid, oid, mid, 'В ППЭ ' || st_code || ' на экзамен ' || date || ' ' || sj_name || ' назначено ' || cast(places as varchar(5)) || ' мест в аудиториях, а распределено ' || cast(parts as varchar(5)) || ' участников.' as comment
        FROM tmp WHERE parts > places and sj_name not like '%базовая%'
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_places, r['stid'], 'Station', r['oid'], r['mid'], r['comment']
    end

  end
end