module NoDocumentNumber
  def process
    puts ("no document number when series is NOT NULL")

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Не заполнен номер документа при заполненной серии' as comment
	  FROM students s
	  JOIN groups g ON g.id = s.group_id
	  JOIN ous o ON o.id = g.ou_id
	  JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and (s.document_number is NULL or length(replace(s.document_number, ' ', '')) < 1) and (length(s.document_series) > 0 and s.document_series is NOT NULL)
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_document_number, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end