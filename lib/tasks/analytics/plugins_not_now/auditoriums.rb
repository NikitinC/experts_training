module Auditoriums
  def process

    puts ("no auditoriums gia forms")

    sql = <<SQL
    SELECT o.id as oid, m.id as mid, 'Не обозначена форма аттестации для аудитории №'|| c.number || ' в ППЭ №' || st.code as comment
	  FROM classrooms c
    JOIN stations st ON st.id = c.station_id
    JOIN ous o ON o.id = st.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    LEFT JOIN attestation_form_classrooms afc ON afc.classroom_id = c.id and afc.deleted_at is NULL
    where c.deleted_at is NULL and afc.id is NULL and st.deleted_at is NULL
    and st.code <> '4100'
    and o.code <> '821308'
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :auditorium_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("bad places")

    sql = <<SQL
select m.id as mid, o.id as oid, c.id as cid, 'В ППЭ '|| s.code ||' неверно указано количество доступных в аудитории ' || c.number || ' (доступно ' || cast(rows_count * seats_per_row - (length(unavailable_places)+1)/4 as varchar(5)) || ', а указано ' || cast(available_seats as varchar(5)) || ').' as comment
	from classrooms c
	join stations s ON s.id = c.station_id and s.deleted_at is NULL
	join ous o ON o.id = s.ou_id
	join mouos m ON m.id = o.mouo_id
where c.deleted_at is NULL
and rows_count * seats_per_row - (length(unavailable_places)+1)/4 != available_seats

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :auditorium_is_not_updated, r['cid'], 'Classroom', r['oid'], r['mid'], r['comment']
    end



    puts ("auditorium and exams gia forms not equal")

    sql = <<SQL
    SELECT o.id as oid, m.id as mid, 'Не обозначена форма аттестации для аудитории №'|| c.number || ' в ППЭ №' || st.code as comment
	  FROM classrooms c
    JOIN stations st ON st.id = c.station_id
    JOIN ous o ON o.id = st.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    LEFT JOIN attestation_form_classrooms afc ON afc.classroom_id = c.id
    where c.deleted_at is NULL and afc.id is NULL and st.deleted_at is NULL
    and st.code <> '4100'
    and o.code <> '821308'
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :auditorium_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("auditoriums for some exams")

    sql = <<SQL
    SELECT o.id as oid, m.id as mid, st.id as stid, 'Station', 
     , 'В ППЭ ' || st.code || ' аудитория №' || au.number || ' назначена на ' || au.cnt || ' экзамена(ов) в дату ' || au.date || '.'
        as comment
        
     FROM stations st
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      
    JOIN (SELECT e.date, s.code, c.number, count(distinct(esc.id)) as cnt
      FROM exam_station_classrooms esc
      JOIN exam_stations es ON es.id = esc.exam_station_id
      JOIN exams e ON e.id = es.exam_id
      JOIN classrooms c ON c.id = esc.classroom_id
      JOIN stations s ON s.id = es.station_id
    WHERE esc.deleted_at is NULL and es.deleted_at is NULL and e.deleted_at is NULL and c.deleted_at is NULL and s.deleted_at is NULL
    GROUP BY e.date, s.code, c.number) au ON au.code = st.code and au.cnt > 1
    
    WHERE o.deleted_at is NULL and st.deleted_at is NULL and m.deleted_at is NULL
    and st.code <> '4100'
    and o.code <> '821308'
SQL
    #

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    puts ("auditoriums not used for exams")

    sql = <<SQL
      SELECT o.id as oid, m.id as mid, 
		    'Предупреждение: В ППЭ ' || st.code || ' аудитория №' || c.number || ' не задействована ни в одном экзамене.' as comment
      FROM classrooms c
      LEFT JOIN (SELECT c.id, count(ex.id) as cnt
              FROM stations st
          JOIN classrooms c ON c.station_id = st.id
              JOIN exam_stations est ON est.station_id = st.id and est.deleted_at is NULL
          JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL and esc.classroom_id = c.id
          JOIN exams ex ON ex.id = est.exam_id
          group by c.id
          ) e ON e.id = c.id
      JOIN stations st ON c.station_id = st.id
      JOIN ous o ON o.id = st.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      where e.id is NULL and c.deleted_at is NULL and st.deleted_at is NULL and o.deleted_at is NULL and m.deleted_at is NULL
        and st.code <> '4100'
        and o.code <> '821308'
      group by o.id, m.id, c.id, st.code, c.number
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("auditoriums for voice exams without any voice properties")

    sql = <<SQL
    SELECT c.id as cid, s.id as sid, o.id as oid, m.id as mid, 'Установлено количество ПАК инъяза, хотя аудитория ' || c.number || ' не отмечена как аудитория проведения' as comment
      FROM classrooms c
      JOIN stations s ON s.id = c.station_id
      join ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
      and c.deleted_at is NULL
      and s.deleted_at is NULL
      and s.code not in ('4100')
      and c.classroom_verbal_part_sing_code <> 2 and coalesce(c.verbal_part_paks, 0) > 0
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("auditoriums for voice exams without any voice properties")

    sql = <<SQL
      SELECT o.id as oid, m.id as mid, 
        'В ППЭ ' || s.code || ' на экзамен ' || e.date || ' - ' || sj.name || '(' || substring(et.name, 1, 3) || ') назначена аудитория №' || right('0000' || cast(c.number as varchar(4)), 4) || ' без признаков устной части или с 0 ПАК в аудиториях проведения.' as comment
        FROM classrooms c
        JOIN stations s ON s.id = c.station_id
        JOIN exam_station_classrooms esc ON esc.classroom_id = c.id
        JOIN exam_stations es ON es.id = esc.exam_station_id
        JOIN exams e ON e.id = es.exam_id
        JOIN exam_types et ON et.id = e.exam_type_id
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN ous o ON o.id = s.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      WHERE c.deleted_at is NULL and esc.deleted_at is NULL and es.deleted_at is NULL
      and sj.fiscode in (29, 30, 31, 33, 34)
      and coalesce(c.classroom_verbal_part_sing_code, 0) = 0
      and s.code <> '4100'
      and o.code <> '821308'
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    puts ("auditoriums for voice exams with 0 paks")

    sql = <<SQL
      SELECT o.id as oid, m.id as mid, 
        'В ППЭ ' || s.code || ' на экзамен ' || e.date || ' - ' || sj.name || '(' || substring(et.name, 1, 3) || ') назначена аудитория №' || right('0000' || cast(c.number as varchar(4)), 4) || ' с 0 ПАК на в аудиториях проведения.' as comment
        FROM classrooms c
        JOIN stations s ON s.id = c.station_id
        JOIN exam_station_classrooms esc ON esc.classroom_id = c.id
        JOIN exam_stations es ON es.id = esc.exam_station_id
        JOIN exams e ON e.id = es.exam_id
        JOIN exam_types et ON et.id = e.exam_type_id
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN ous o ON o.id = s.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      WHERE c.deleted_at is NULL and esc.deleted_at is NULL and es.deleted_at is NULL
      and sj.fiscode in (29, 30, 31, 33, 34)
      and coalesce(c.classroom_verbal_part_sing_code, 0) = 2
      and coalesce(c.verbal_part_paks, 0) = 0
      and s.code <> '4100'
      and o.code <> '821308'
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

#     puts ("auditoriums for voice exams and gve exams")
#
#     sql = <<SQL
#       SELECT o.id as oid, m.id as mid,
#         'В ППЭ ' || s.code || ' аудитория №' || right('0000' || cast(c.number as varchar(4)), 4) || ' назначена на иностанный устный и на экзамены ГВЭ.' as comment
#         FROM classrooms c
#         JOIN stations s ON s.id = c.station_id
#         JOIN exam_station_classrooms esc ON esc.classroom_id = c.id
#         JOIN exam_stations es ON es.id = esc.exam_station_id
#         JOIN exams e ON e.id = es.exam_id
#         JOIN exam_types et ON et.id = e.exam_type_id
#         JOIN subjects sj ON sj.code = e.subject_code
#         JOIN ous o ON o.id = s.ou_id
#         JOIN mouos m ON m.id = o.mouo_id
#         JOIN exam_station_classrooms esc2 ON esc2.classroom_id = c.id
#         JOIN exam_stations es2 ON es2.id = esc2.exam_station_id
#         JOIN exams e2 ON e2.id = es2.exam_id
#         JOIN attestation_forms af2 ON af2.code = e2.attestation_form_code
#       WHERE c.deleted_at is NULL and esc.deleted_at is NULL and es.deleted_at is NULL and es2.deleted_at is NULL and esc2.deleted_at is NULL
#       and sj.fiscode in (29, 30, 31, 33, 34) and af2.name like '%ГВЭ%'
#       and s.code <> '4100'
#       and o.code <> '821308'
#       GROUP BY o.id, m.id, s.code, c.number
# SQL
#
#
#     ActiveRecord::Base.connection.execute(sql).each do |r|
#       add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
#     end

    puts ("auditoriums for voice exams without any voice properties")

    sql = <<SQL
    SELECT c.id as cid, s.id as sid, o.id as oid, m.id as mid, 'В аудитории проведения (' || c.number || ') должен быть хотя бы 1 ПАК инъяза' as comment
      FROM classrooms c
      JOIN stations s ON s.id = c.station_id
      join ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
      and c.deleted_at is NULL
      and s.deleted_at is NULL
      and s.code not in ('4100')
      and c.classroom_verbal_part_sing_code = 2 and coalesce(c.verbal_part_paks, 0) < 1
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    puts ("auditoriums for voice or gve an usually exams in one day")

    sql = <<SQL
    select m.id as mid, c.id, o.id as oid, s.id as sid, 'Аудитория ' || c.number || ' в ППЭ ' || s.code || ' назначена на разные формы экзаменов ' || e.date || '.' as comment
      from exam_station_classrooms esc
      join exam_stations es ON es.id = esc.exam_station_id
      join exams e ON e.id = es.exam_id
      join stations s ON s.id = es.station_id
      join ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
      join classrooms c ON c.id = esc.classroom_id
      join subjects sj ON sj.code = e.subject_code
      join (
      select s.id as sid, c.id as cid, e.date
      from exam_station_classrooms esc
      join exam_stations es ON es.id = esc.exam_station_id
      join exams e ON e.id = es.exam_id
      join stations s ON s.id = es.station_id
      join classrooms c ON c.id = esc.classroom_id
      join subjects sj ON sj.code = e.subject_code
      where (sj.fiscode in (29, 30, 31, 33, 34) or sj.fiscode > 50)
        and esc.deleted_at is NULL
      group by s.id, c.id, e.date
      ) oth ON oth.sid = s.id and oth.cid = c.id and oth.date = e.date
    where (sj.fiscode not in (29, 30, 31, 33, 34) and sj.fiscode < 50)
    and esc.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL
    group by s.code, c.number, e.date, m.id, o.id, s.id, c.id
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['cid'], 'Classroom', r['oid'], r['mid'], r['comment']
    end

    puts ("auditoriums for kege without kege_paks")

    sql = <<SQL
      SELECT o.id as oid, m.id as mid, 
        'В ППЭ ' || s.code || ' на экзамен ' || e.date || ' - ' || sj.name || '(' || substring(et.name, 1, 3) || ') назначена аудитория №' || right('0000' || cast(c.number as varchar(4)), 4) || ' в которой недостаточно компьютеров КЕГЭ.' as comment
        FROM classrooms c
        JOIN stations s ON s.id = c.station_id
        JOIN exam_station_classrooms esc ON esc.classroom_id = c.id
        JOIN exam_stations es ON es.id = esc.exam_station_id
        JOIN exams e ON e.id = es.exam_id
        JOIN exam_types et ON et.id = e.exam_type_id
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN ous o ON o.id = s.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      WHERE c.deleted_at is NULL and esc.deleted_at is NULL and es.deleted_at is NULL
      and sj.fiscode in (5, 25) and et.name like '%ЕГЭ%'
      and coalesce(c.kegecomputers, -1) < coalesce(esc.places, coalesce(c.kegecomputers, 0))
      and s.code <> '4100'
      and o.code <> '821308'
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

  end
end