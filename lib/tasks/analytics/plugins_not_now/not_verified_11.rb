module NotVerified11
  def process
    Student.includes(:group => :ou).where(groups: {:number => [11, 12]}).find_each do |s|
      if s.verified_by.nil?
        add_analytics_item :not_verified_11,
                           s.id,
                           'Student',
                           s.group.ou.id,
                           s.group.ou.mouo_id
      end
    end
  end
end