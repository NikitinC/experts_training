module Noofficestation
  def process

    puts ("no office")

    sql = <<SQL
    SELECT office_code, office_name, office_online, o.id as oid, s.id as sid, m.id as mid, 'Не заполнены данные о штабе ППЭ в ППЭ ' || s.code as comment
      FROM stations s
      JOIN ous o ON o.id = s.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    
    WHERE office_code is NULL or office_name is NULL or office_online is NULL
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_is_not_updated, r['sid'], 'Station', r['oid'], r['mid'], r['comment']
    end



  end
end