module Students
  def process
    puts ("dictionary names")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid, 
    CASE
      WHEN s.gender <> n.gender THEN 'Пол участника не совпадает со словарём'
        WHEN n.id is NULL THEN 'Имя ' || s.first_name || ' отсутствует в словаре'
    END as comment
    FROM public.students s
    LEFT JOIN names n ON n.name = s.first_name
    JOIN groups g ON g.id = s.group_id
    JOIN ous o ON o.id = g.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    where (s.gender <> n.gender OR n.id is NULL) and s.deleted_at is NULL
    and o.code not in ('999999', '111111')
    group by s.id, o.id, m.id, s.gender, n.gender, n.id, s.first_name
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :names, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("birthday date")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid,
      'Дата рождения ' || s.birthday || ' не типична для учащихся ' || CAST(g.number as varchar(2)) || ' классов.' as comment
      FROM public.students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
        WHERE  ((g.number in (9) and s.Birthday < '1999-01-01')  OR (g.number in (9) and s.Birthday > '2003-12-31')
          OR (g.number in (11) and s.Birthday < '1998-01-01') OR (g.number in (11) and s.Birthday > '2001-12-31'))
            and s.ege_participant_category_code in (1, 8) and o.ou_kind_code not in ('1001', '1002', '1003', '1004', '1005')
            and s.deleted_at is NULL
            and o.code not in ('999999', '111111')
      group by s.id, o.id, m.id, s.birthday, g.number

    union

    SELECT s.id as sid, o.id as oid, m.id as mid,
      'Дата рождения вызывает недоумение' as comment
      FROM public.students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
        WHERE (s.Birthday < '1950-01-01' OR s.Birthday > '2005-01-01') and s.deleted_at is NULL
        and o.code not in ('999999', '111111')
      group by s.id, o.id, m.id, s.birthday, g.number

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :birthday, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end


    puts ("no attestation form 9")

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Не указана форма аттестации для учащегося ' || CAST (g.number as varchar(2)) || ' класса.'  as comment
      FROM students s
        LEFT JOIN attestation_form_students afs ON afs.student_id = s.id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and afs.id is NULL and g.number in (9, 10)
    group by s.id, m.id, o.id, g.number
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_attestation_form_9, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("no attestation form 11")

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Не указана форма аттестации для учащегося ' || CAST (g.number as varchar(2)) || ' класса.'  as comment
      FROM students s
        LEFT JOIN attestation_form_students afs ON afs.student_id = s.id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and afs.id is NULL and g.number in (11, 12)
    group by s.id, m.id, o.id, g.number
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_attestation_form_11, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end


    puts ("no compositions")

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Участник не назначен на итоговое сочинение (изложение)'  as comment
      FROM students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
        LEFT JOIN composition_students cs ON cs.student_id = s.id and cs.deleted_at is NULL
        
    WHERE s.deleted_at is NULL and cs.id is NULL and g.number in (11, 12) and coalesce(s.has_composition, false) <> true
        and s.ege_participant_category_code in (1)
    
    group by s.id, m.id, o.id, g.number
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_composition, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("no russian language")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid, 'Не назначен на Русский язык' as comment from students s
    JOIN groups g ON g.id = s.group_id
    join ous o ON o.id = g.ou_id
    join mouos m ON m.id = o.mouo_id
    where s.id not in(
        SELECT s.id
          FROM students s
          JOIN exam_students es ON es.student_id = s.id
          JOIN exams e ON e.id = es.exam_id
          JOIN subjects sj ON sj.code = e.subject_code and sj.fiscode in (1, 51, 3051, 4051, 5051, 6051, 7051)
          where s.deleted_at is NULL and es.deleted_at is NULL and e.deleted_at is NULL
        )
    and s.deleted_at is NULL and g.number in (9, 10, 11, 12)
    and s.gia_active_results_rus is NOT true
    and o.code not in ('111111', '999999')
    group by s.id, m.id, o.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_russian, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("no station for russian language")

    sql = <<SQL
SELECT s.id as sid, o.id as oid, m.id as mid, 'Учащийся на распределен в ППЭ на русский язык' as comment
        from students s
        JOIN groups g ON g.id = s.group_id
        join ous o ON o.id = g.ou_id
        join mouos m ON m.id = o.mouo_id
        JOIN exam_students es ON es.student_id = s.id
        JOIN exams e ON e.id = es.exam_id
        JOIN subjects sj ON sj.code = e.subject_code and sj.fiscode in (1, 51, 3051, 4051, 5051, 6051, 7051)
        where s.deleted_at is NULL and g.number in (9, 10, 11, 12) and es.deleted_at is NULL
		and es.exam_station_id is NULL
    and o.code not in ('111111', '999999')

    group by s.id, m.id, o.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_russian_ppe, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end



    puts ("students different stations for exam")

    sql = <<SQL
    SELECT o.id as oid, m.id as mid, c.comment as comment
        from ous o
        join mouos m ON m.id = o.mouo_id
      JOIN (
            SELECT o.id as oid, e.id, count (distinct(st.id)) as cnt, 'Учащиеся распределены в разные ППЭ на ГИА-11 (' || cast(count (distinct(st.id)) as char(9)) || ' ППЭ на экзамен ' || e.date || ' по ' || sj.short_name || ')' as comment
            from students s
            JOIN groups g ON g.id = s.group_id
            join ous o ON o.id = g.ou_id
            JOIN exam_students es ON es.student_id = s.id
            JOIN exams e ON e.id = es.exam_id
            JOIN subjects sj ON sj.code = e.subject_code
            JOIN exam_stations est ON est.id = es.exam_station_id
            JOIN stations st ON st.id = est.station_id
            where s.deleted_at is NULL and g.number in (11, 12) and es.deleted_at is NULL
            and st.deleted_at is NULL
            group by o.id, e.id, sj.short_name, e.date
    
        union
    
            SELECT o.id as oid, e.id, count (distinct(st.id)) as cnt, 'Учащиеся распределены в разные ППЭ на ГИА-9 (' || cast(count (distinct(st.id)) as char(9)) || ' ППЭ на экзамен ' || e.date || ' по ' || sj.short_name || ')' as comment
            from students s
            JOIN groups g ON g.id = s.group_id
            join ous o ON o.id = g.ou_id
            JOIN exam_students es ON es.student_id = s.id
            JOIN exams e ON e.id = es.exam_id
            JOIN subjects sj ON sj.code = e.subject_code
            JOIN exam_stations est ON est.id = es.exam_station_id
            JOIN stations st ON st.id = est.station_id
            where s.deleted_at is NULL and g.number in (9, 10) and es.deleted_at is NULL and e.deleted_at is NULL
            and st.deleted_at is NULL
            group by o.id, e.id, sj.short_name, e.date
      ) c ON c.oid = o.id
    
    where c.cnt > 1 and o.code not in ('111111', '999999')

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :russian_different_ppe, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    puts ("students has no composition and don't planned for it")

    sql = <<SQL
      SELECT s.id as sid, o.id as oid, m.id as mid, 'Участник не имеет зачёта по сочинению (изложению) и не назначен на него.' as comment
      
      FROM students s
      JOIN attestation_form_students afs ON afs.student_id = s.id
      JOIN attestation_forms af ON af.code = afs.attestation_form_code and af.name in ('ЕГЭ', 'ГВЭ-11')
      JOIN ege_participant_categories epc ON epc.code = s.ege_participant_category_code
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      WHERE CAST (UPPER(s.guid) as varchar[38]) NOT IN
      (SELECT guid from ht)
      and epc.name = 'Выпускник общеобразовательной организации текущего года'
      AND s.id NOT IN 
      (SELECT cs.student_id 
       FROM composition_students cs
      JOIN compositions c ON c.id = cs.composition_id and NOW()<c.date
      )
      and s.deleted_at is NULL
      and s.has_composition != True
      and o.code not in ('111111', '831308')
      group by s.id, o.id, m.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_composition, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end



    puts ("students spo and vpl valid emails")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid,
      'E-mail для участников категорий ВПЛ и СПО является обязательным полем' as comment
      FROM public.students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
        WHERE  s.ege_participant_category_code not in (1, 8) and o.code like '%0000'
            and s.deleted_at is NULL and length(coalesce(s.email,''))<3
      group by s.id, o.id, m.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :vpl_spo_verify, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end



    puts ("students vpl valid diplomas")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid,
      'Номер диплома для участников категории ВПЛ является обязательным полем' as comment
      FROM public.students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
        WHERE  s.ege_participant_category_code in (4) and o.code like '%0000'
            and s.deleted_at is NULL and length(coalesce(s.education_document_number,''))<3
      group by s.id, o.id, m.id
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :vpl_spo_verify, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end


    puts ("students 11 doesn't have a ege scan")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid, 'Учащемуся не загружен скан заявления на участие в ГИА' as comment
          		FROM students s
               	JOIN attestation_form_students afs ON afs.student_id = s.id
                JOIN attestation_forms af ON af.code = afs.attestation_form_code
                JOIN groups g ON g.id = s.group_id
                JOIN ous o ON o.id = g.ou_id
                JOIN mouos m ON m.id = o.mouo_id
               	JOIN exam_students es ON es.student_id = s.id
    			JOIN exams e ON e.id = es.exam_id
                JOIN subjects sj ON sj.code = e.subject_code
                WHERE s.deleted_at is NULL and afs.deleted_at is NULL and es.deleted_at is NULL and o.deleted_at is NULL
                and s.ege_statement_scan is NULL
                and o.code not in ('999999', '111111', '821308')
                -- and af.name in ('ЕГЭ', 'ГВЭ-11')
                group by o.id, s.id, m.id
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_ege_scan, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("students 9 doesn't have 4 exams")

    sql = <<SQL
      SELECT s.id as sid, o.id as oid, m.id as mid, 'Учащийся назначен не на 4 экзамена' as comment
        FROM students s
        JOIN attestation_form_students afs ON afs.student_id = s.id
        JOIN attestation_forms af ON af.code = afs.attestation_form_code
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        LEFT JOIN (SELECT s.id as sid, count(distinct(es.id)) as cnt
            FROM students s
            JOIN exam_students es ON es.student_id = s.id
            JOIN exams e ON e.id = es.exam_id
                  JOIN subjects sj ON sj.code = e.subject_code
            WHERE s.deleted_at is NULL and es.deleted_at is NULL
            AND sj.fiscode not in (29, 30, 31, 33)
            group by s.id
          ) exc ON exc.sid = s.id
        WHERE s.deleted_at is NULL and afs.deleted_at is NULL and o.deleted_at is NULL
          and s.ege_statement_scan is NULL
          and af.name in ('ОГЭ', 'ГВЭ-9')
          and s.ege_participant_category_code = 1
          and s.limited_facilities_group_code = 1
          and coalesce(exc.cnt, 0) <> 4
          and o.code not in ('999999', '111111') 
        group by o.id, s.id, m.id

SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_four_exams_9_noOVZ, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end




  end
end