module Marks
  def process

    puts ("неверно выставлены оценки")

    sql = <<SQL
      SELECT s.id as sid, o.id as oid, m.id as mid, 'Не внесены или некорректно внесены отметки по предмету ' || sj.name as comment
        FROM exam_students es
        JOIN students s ON s.id = es.student_id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN exams e ON e.id = es.exam_id
        JOIN subjects sj ON sj.code = e.subject_code
        WHERE es.deleted_at is NULL and s.deleted_at is NULL and o.code not in ('999999', '111111', '821308') and g.number <> 2
        and ((mark5 + mark5p + mark5_attestat is NULL)
           or
           (mark5p < 0 or mark5p > 5 or mark5 < 0 or mark5 > 5 or mark5_attestat < 0 or mark5_attestat>5)
           )
        group by s.id, o.id, m.id, sj.name
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :marks, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("не указаны преподаватели")

    sql = <<SQL
      SELECT m.id as mid, s.id as sid, o.id as oid, 'Не указан преподаватель по предмету ' || sj.name as comment
      
        FROM exam_students es
        JOIN students s ON s.id = es.student_id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN ates a ON a.id = o.ate_id
        JOIN exams e ON e.id = es.exam_id
        JOIN subjects sj ON sj.code = e.subject_code
        
      WHERE es.deleted_at is NULL and s.deleted_at is NULL
        and o.code not like '%0000' and o.code not in ('111111', '999999', '821308')
        and (
            (sj.fiscode in (1, 51) and g."Teacher01" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (2, 22, 52) and g."Teacher02" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (3, 53) and g."Teacher03" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (4, 54) and g."Teacher04" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (5, 55) and g."Teacher05" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (6, 56) and g."Teacher06" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (7, 57) and g."Teacher07" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (8, 58) and g."Teacher08" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (9, 29, 59) and g."Teacher09" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (10, 60, 30) and g."Teacher10" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (11, 61, 31) and g."Teacher11" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (12, 62) and g."Teacher12" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (13, 63, 33) and g."Teacher13" is NULL and (es.professor is NULL or length(es.professor)<2))
        or (sj.fiscode in (18, 68) and g."Teacher18" is NULL and (es.professor is NULL or length(es.professor)<2))
          )
      
      order by o.code, g.number, sj.name
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :marks, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end


  end
end