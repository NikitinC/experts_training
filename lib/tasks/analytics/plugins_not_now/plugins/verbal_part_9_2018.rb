module  VerbalPart92018
  def process

    puts ("students 9 different days for foreign languages")


    sql = <<SQL
    SELECT
      CASE WHEN pism.sid is NOT NULL THEN pism.sid
      ELSE ustn.sid END as sid,
      CASE WHEN pism.oid is NOT NULL THEN pism.oid
      ELSE ustn.oid END as oid,
      CASE WHEN pism.mid is NOT NULL THEN pism.mid
      ELSE ustn.mid END as mid,
      'Дни или ППЭ устной и письменной части экзаменов по ин.языкам не совпадают' as comment
      FROM exams e
      JOIN subjects sj ON sj.code = e.subject_code
      JOIN exam_types et ON et.id = e.exam_type_id
      LEFT JOIN (SELECT e.id as eid, s.id as sid, sj.fiscode, st.id as stid, e.date, o.id as oid, m.id as mid
        FROM exam_students es
        JOIN students s ON s.id = es.student_id AND s.deleted_at is NULL
        JOIN exam_stations est ON est.id = es.exam_station_id and est.deleted_at is NULL
        JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
        JOIN exams e ON e.id = es.exam_id and e.deleted_at is NULL
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        WHERE sj.fiscode in (9, 10, 11, 13)
        ) pism ON pism.eid = e.id
      FULL OUTER JOIN (
        SELECT e.id as eid, s.id as sid, sj.fiscode, st.id as stid, e.date, o.id as oid, m.id as mid
        FROM exam_students es
        JOIN students s ON s.id = es.student_id AND s.deleted_at is NULL
        JOIN exam_stations est ON est.id = es.exam_station_id and est.deleted_at is NULL
        JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
        JOIN exams e ON e.id = es.exam_id and e.deleted_at is NULL
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        WHERE sj.fiscode in (29, 30, 31, 33)
        ) ustn ON cast(ustn.fiscode as int) = cast(pism.fiscode as int) + 20 and ustn.stid = pism.stid and ustn.sid = pism.sid and ustn.date = pism.date
    WHERE et.name like '%ОГЭ%' and (pism.sid is NULL or ustn.sid is NULL) and (pism.sid is NOT NULL or ustn.sid is NOT NULL)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :verbal_exams_equal_dates_9, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("no needed paks in take exam auditoriums 11")

    sql = <<SQL
      SELECT
        o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает станций записи ин. языка в аудиториях проведения' as comment,
        st.code, e.date,
        coalesce(students.cnt, 0) as students,
        coalesce(auds_p.cnt, 0) as auds_p_cnt,
        coalesce(auds_p.places, 0) as auds_p_places,
        coalesce(auds_p.paks, 0) as auds_p_paks,

        coalesce(auds_t.cnt, 0) as auds_t_cnt,
        coalesce(auds_t.places, 0) as auds_t_places,
        coalesce(auds_t.paks, 0) as auds_t_paks,
        paks.needed_paks

        FROM exams e
        JOIN exam_types et ON et.id = e.exam_type_id
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
        JOIN stations st ON st.id = est.station_id
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN (
            SELECT stud.date, stud.stid, sum (stud.cnt) as cnt
            FROM stations st
            JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(s.id)) as cnt
            FROM exam_stations est
            JOIN exam_students es ON es.exam_station_id = est.id and es.deleted_at is NULL
            JOIN students s ON s.id = es.student_id and s.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
            JOIN exam_types et ON et.id = e.exam_type_id
            JOIN subjects sj ON sj.code = e.subject_code
            where est.deleted_at is NULL and sj.name like '%устная часть%'
            group by e.date, st.id, e.id
            ) as stud ON st.id = stud.stid
            group by stud.date, stud.stid
        ) students ON students.date = e.date and students.stid = st.id
        JOIN (
            SELECT paks_e.date, paks_e.stid, sum (paks_e.needed_paks) as needed_paks
            FROM stations st
            JOIN (SELECT e.date, st.id as stid, e.id, ceil(cast(count(distinct(s.id)) as float)/4) as needed_paks
            FROM exam_stations est
            JOIN exam_students es ON es.exam_station_id = est.id and es.deleted_at is NULL
            JOIN students s ON s.id = es.student_id and s.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
            JOIN exam_types et ON et.id = e.exam_type_id
            JOIN subjects sj ON sj.code = e.subject_code
            where est.deleted_at is NULL and sj.name like '%устная часть%'
            group by e.date, st.id, e.id
            ) as paks_e ON st.id = paks_e.stid
            group by paks_e.date, paks_e.stid
        ) paks ON paks.date = e.date and paks.stid = st.id
        LEFT JOIN (
        SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
          FROM exam_stations est
          JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
          JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
          JOIN exams e ON e.id = est.exam_id
          JOIN stations st ON st.id = est.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 4
          and sj.fiscode in (29, 30, 31, 33)
          group by e.date, st.id
        ) auds_p ON auds_p.date = e.date and auds_p.stid = st.id
        LEFT JOIN (
        SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
          FROM exam_stations est
          JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
          JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
          JOIN exams e ON e.id = est.exam_id
          JOIN stations st ON st.id = est.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 2
          and sj.fiscode in (29, 30, 31, 33)
          group by e.date, st.id
        ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id


      WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
      and st.code <> '4100' and et.name like '%ЕГЭ%'
      and coalesce(paks.needed_paks, 0) > coalesce(auds_t.paks, 0)

      group by m.id, o.id, st.code, e.date, students.cnt, auds_p.cnt, auds_p.places, auds_p.paks, auds_t.cnt, auds_t.places, auds_t.paks, paks.needed_paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating_11, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    puts ("no needed paks in take exam auditoriums 9")

    sql = <<SQL
      SELECT
        o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает станций записи ин. языка в аудиториях проведения' as comment,
        st.code, e.date,
        coalesce(students.cnt, 0) as students,
        coalesce(auds_p.cnt, 0) as auds_p_cnt,
        coalesce(auds_p.places, 0) as auds_p_places,
        coalesce(auds_p.paks, 0) as auds_p_paks,

        coalesce(auds_t.cnt, 0) as auds_t_cnt,
        coalesce(auds_t.places, 0) as auds_t_places,
        coalesce(auds_t.paks, 0) as auds_t_paks,
        paks.needed_paks

        FROM exams e
        JOIN exam_types et ON et.id = e.exam_type_id
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
        JOIN stations st ON st.id = est.station_id
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN (
            SELECT stud.date, stud.stid, sum (stud.cnt) as cnt
            FROM stations st
            JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(s.id)) as cnt
            FROM exam_stations est
            JOIN exam_students es ON es.exam_station_id = est.id and es.deleted_at is NULL
            JOIN students s ON s.id = es.student_id and s.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
            JOIN exam_types et ON et.id = e.exam_type_id
            JOIN subjects sj ON sj.code = e.subject_code
            where est.deleted_at is NULL and sj.name like '%устная часть%'
            group by e.date, st.id, e.id
            ) as stud ON st.id = stud.stid
            group by stud.date, stud.stid
        ) students ON students.date = e.date and students.stid = st.id
        JOIN (
            SELECT paks_e.date, paks_e.stid, sum (paks_e.needed_paks) as needed_paks
            FROM stations st
            JOIN (SELECT e.date, st.id as stid, e.id, ceil(cast(count(distinct(s.id)) as float)/3) as needed_paks
            FROM exam_stations est
            JOIN exam_students es ON es.exam_station_id = est.id and es.deleted_at is NULL
            JOIN students s ON s.id = es.student_id and s.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
            JOIN exam_types et ON et.id = e.exam_type_id
            JOIN subjects sj ON sj.code = e.subject_code
            where est.deleted_at is NULL and sj.name like '%устная часть%'
            group by e.date, st.id, e.id
            ) as paks_e ON st.id = paks_e.stid
            group by paks_e.date, paks_e.stid
        ) paks ON paks.date = e.date and paks.stid = st.id
        LEFT JOIN (
        SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
          FROM exam_stations est
          JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
          JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
          JOIN exams e ON e.id = est.exam_id
          JOIN stations st ON st.id = est.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 4
          and sj.fiscode in (29, 30, 31, 33)
          group by e.date, st.id
        ) auds_p ON auds_p.date = e.date and auds_p.stid = st.id
        LEFT JOIN (
        SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
          FROM exam_stations est
          JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
          JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
          JOIN exams e ON e.id = est.exam_id
          JOIN stations st ON st.id = est.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 2
          and sj.fiscode in (29, 30, 31, 33)
          group by e.date, st.id
        ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id


      WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
      and st.code <> '4100' and et.name like '%ОГЭ%'
      and coalesce(paks.needed_paks, 0) > coalesce(auds_t.paks, 0)

      group by m.id, o.id, st.code, e.date, students.cnt, auds_p.cnt, auds_p.places, auds_p.paks, auds_t.cnt, auds_t.places, auds_t.paks, paks.needed_paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating_verbal_9, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("no needed places in prepare exam auditoriums")

    sql = <<SQL
      SELECT
        o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает мест в аудиториях подготовки' as comment,
        st.code, e.date,
        coalesce(students.cnt, 0) as students,
        coalesce(auds_p.cnt, 0) as auds_p_cnt,
        coalesce(auds_p.places, 0) as auds_p_places,
        coalesce(auds_p.paks, 0) as auds_p_paks,

        coalesce(auds_t.cnt, 0) as auds_t_cnt,
        coalesce(auds_t.places, 0) as auds_t_places,
        coalesce(auds_t.paks, 0) as auds_t_paks,
        paks.needed_paks

        FROM exams e
        JOIN exam_types et ON et.id = e.exam_type_id
        JOIN subjects sj ON sj.code = e.subject_code
        JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
        JOIN stations st ON st.id = est.station_id
        JOIN ous o ON o.id = st.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN (
            SELECT stud.date, stud.stid, sum (stud.cnt) as cnt
            FROM stations st
            JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(s.id)) as cnt
            FROM exam_stations est
            JOIN exam_students es ON es.exam_station_id = est.id and es.deleted_at is NULL
            JOIN students s ON s.id = es.student_id and s.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
            JOIN exam_types et ON et.id = e.exam_type_id
            JOIN subjects sj ON sj.code = e.subject_code
            where est.deleted_at is NULL and sj.name like '%устная часть%'
            group by e.date, st.id, e.id
            ) as stud ON st.id = stud.stid
            group by stud.date, stud.stid
        ) students ON students.date = e.date and students.stid = st.id
        JOIN (
            SELECT paks_e.date, paks_e.stid, sum (paks_e.needed_paks) as needed_paks
            FROM stations st
            JOIN (SELECT e.date, st.id as stid, e.id, ceil(cast(count(distinct(s.id)) as float)/3) as needed_paks
            FROM exam_stations est
            JOIN exam_students es ON es.exam_station_id = est.id and es.deleted_at is NULL
            JOIN students s ON s.id = es.student_id and s.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
            JOIN exam_types et ON et.id = e.exam_type_id
            JOIN subjects sj ON sj.code = e.subject_code
            where est.deleted_at is NULL and sj.name like '%устная часть%'
            group by e.date, st.id, e.id
            ) as paks_e ON st.id = paks_e.stid
            group by paks_e.date, paks_e.stid
        ) paks ON paks.date = e.date and paks.stid = st.id
        LEFT JOIN (
        SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
          FROM exam_stations est
          JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
          JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
          JOIN exams e ON e.id = est.exam_id
          JOIN stations st ON st.id = est.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 4
          and sj.fiscode in (29, 30, 31, 33)
          group by e.date, st.id
        ) auds_p ON auds_p.date = e.date and auds_p.stid = st.id
        LEFT JOIN (
        SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
          FROM exam_stations est
          JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
          JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
          JOIN exams e ON e.id = est.exam_id
          JOIN stations st ON st.id = est.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 2
          and sj.fiscode in (29, 30, 31, 33)
          group by e.date, st.id
        ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id


      WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
      and st.code <> '4100'
      and coalesce(students.cnt, 0) > coalesce(auds_p.places, 0)

      group by m.id, o.id, st.code, e.date, students.cnt, auds_p.cnt, auds_p.places, auds_p.paks, auds_t.cnt, auds_t.places, auds_t.paks, paks.needed_paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating_verbal_9, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("not verbal auditorium on verbal exams")

    sql = <<SQL
    SELECT m.id as mid, o.id as oid, 'В ППЭ ' || st.code || ' аудитория ' || c.number || ' с признаком "устная часть невозможна" назначена на ' || e.date || ' на устную часть.' as comment
		FROM exam_stations est
		JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
		JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
		JOIN exams e ON e.id = est.exam_id
		JOIN stations st ON st.id = est.station_id
		JOIN exam_types et ON et.id = e.exam_type_id
		JOIN subjects sj ON sj.code = e.subject_code
		JOIN ous o ON o.id = st.ou_id
    JOIN mouos m ON m.id = o.mouo_id
		where c.classroom_verbal_part_sing_code = 0
		and sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
		and st.code <> '4100'

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("orgs out auditorium on verbal exams")

    sql = <<SQL
        SELECT
          o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает организаторов вне аудитории на устную часть ин. языков' as comment,
          st.code, e.date,
          coalesce(empl.cnt, 0) as e_out,
          coalesce(auds_p.cnt, 0) as auds_p_cnt,
          coalesce(auds_t.cnt, 0) as auds_t_cnt

          FROM exams e
          JOIN exam_types et ON et.id = e.exam_type_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
          JOIN stations st ON st.id = est.station_id
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
          LEFT JOIN (
              SELECT empl.date, empl.stid, sum (empl.cnt) as cnt
              FROM stations st
              JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(emp.id)) as cnt
              FROM exam_stations est
              JOIN exam_station_employees ese ON ese.exam_station_id = est.id and ese.deleted_at is NULL
              JOIN employees emp ON emp.id = ese.employee_id and emp.deleted_at is NULL
              JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
              JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
              JOIN exam_types et ON et.id = e.exam_type_id
              JOIN subjects sj ON sj.code = e.subject_code
              JOIN station_posts sp ON sp.code = ese.station_post_code
              where est.deleted_at is NULL and sj.name like '%устная часть%'
              and sp.name like '%вне аудитории%'
              group by e.date, st.id, e.id
              ) as empl ON st.id = empl.stid
              group by empl.date, empl.stid
          ) empl ON empl.date = e.date and empl.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id
            JOIN stations st ON st.id = est.station_id
            where c.classroom_verbal_part_sing_code = 4
            group by e.date, st.id
          ) auds_p ON auds_p.date = e.date and auds_p.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id
            JOIN stations st ON st.id = est.station_id
            where c.classroom_verbal_part_sing_code = 2
            group by e.date, st.id
          ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id


        WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
        and st.code <> '4100'
        and coalesce(empl.cnt, 0) < coalesce(auds_t.cnt, 0) + 1

        group by m.id, o.id, st.code, e.date, empl.cnt, auds_p.cnt, auds_p.places, auds_p.paks, auds_t.cnt, auds_t.places, auds_t.paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("orgs in prepare auditoriums on verbal exams")

    sql = <<SQL
        SELECT
          o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает организаторов в аудитории подготовки на устную часть ин. языков' as comment,
          st.code, e.date,
          coalesce(empl.cnt, 0) as e_out,
          coalesce(auds_p.cnt, 0) as auds_p_cnt,
          coalesce(auds_t.cnt, 0) as auds_t_cnt

          FROM exams e
          JOIN exam_types et ON et.id = e.exam_type_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
          JOIN stations st ON st.id = est.station_id
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
          LEFT JOIN (
              SELECT empl.date, empl.stid, sum (empl.cnt) as cnt
              FROM stations st
              JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(emp.id)) as cnt
              FROM exam_stations est
              JOIN exam_station_employees ese ON ese.exam_station_id = est.id and ese.deleted_at is NULL
              JOIN employees emp ON emp.id = ese.employee_id and emp.deleted_at is NULL
              JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
              JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
              JOIN exam_types et ON et.id = e.exam_type_id
              JOIN subjects sj ON sj.code = e.subject_code
              JOIN station_posts sp ON sp.code = ese.station_post_code
              where est.deleted_at is NULL and sj.name like '%устная часть%'
              and sp.name like '%в аудитории подготовки%'
              group by e.date, st.id, e.id
              ) as empl ON st.id = empl.stid
              group by empl.date, empl.stid
          ) empl ON empl.date = e.date and empl.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
			JOIN subjects sj ON sj.code = e.subject_code
            where c.classroom_verbal_part_sing_code = 4
				and sj.fiscode in (29, 30, 31, 33)
            group by e.date, st.id
          ) auds_p ON auds_p.date = e.date and auds_p.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
            JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
			      JOIN subjects sj ON sj.code = e.subject_code
            where c.classroom_verbal_part_sing_code = 2
				and sj.fiscode in (29, 30, 31, 33)
            group by e.date, st.id
          ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id


        WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
        and st.code <> '4100'
        and coalesce(empl.cnt, 0) < coalesce(auds_p.cnt, 0)*2

        group by m.id, o.id, st.code, e.date, empl.cnt, auds_p.cnt, auds_p.places, auds_p.paks, auds_t.cnt, auds_t.places, auds_t.paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("orgs in take exams auditoriums on verbal exams")

    sql = <<SQL
        SELECT
          o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает организаторов в аудитории проведения на устную часть ин. языков' as comment,
          st.code, e.date,
          coalesce(empl.cnt, 0) as e_out,
          coalesce(auds_p.cnt, 0) as auds_p_cnt,
          coalesce(auds_t.cnt, 0) as auds_t_cnt

          FROM exams e
          JOIN exam_types et ON et.id = e.exam_type_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
          JOIN stations st ON st.id = est.station_id
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
          LEFT JOIN (
              SELECT empl.date, empl.stid, sum (empl.cnt) as cnt
              FROM stations st
              JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(emp.id)) as cnt
              FROM exam_stations est
              JOIN exam_station_employees ese ON ese.exam_station_id = est.id and ese.deleted_at is NULL
              JOIN employees emp ON emp.id = ese.employee_id and emp.deleted_at is NULL
              JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
              JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
              JOIN exam_types et ON et.id = e.exam_type_id
              JOIN subjects sj ON sj.code = e.subject_code
              JOIN station_posts sp ON sp.code = ese.station_post_code
              where est.deleted_at is NULL and sj.name like '%устная часть%'
              and sp.name like '%в аудитории проведения%'
              group by e.date, st.id, e.id
              ) as empl ON st.id = empl.stid
              group by empl.date, empl.stid
          ) empl ON empl.date = e.date and empl.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id
            JOIN stations st ON st.id = est.station_id
            JOIN subjects sj ON sj.code = e.subject_code
            where c.classroom_verbal_part_sing_code = 4
				      and sj.fiscode in (29, 30, 31, 33)
            group by e.date, st.id
          ) auds_p ON auds_p.date = e.date and auds_p.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id
            JOIN stations st ON st.id = est.station_id
            JOIN subjects sj ON sj.code = e.subject_code
            where c.classroom_verbal_part_sing_code = 2
              and sj.fiscode in (29, 30, 31, 33)
            group by e.date, st.id
          ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id


        WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
        and st.code <> '4100'
        and coalesce(empl.cnt, 0) < coalesce(auds_p.cnt, 0)

        group by m.id, o.id, st.code, e.date, empl.cnt, auds_p.cnt, auds_p.places, auds_p.paks, auds_t.cnt, auds_t.places, auds_t.paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("orgs OperatorPK in take exams auditoriums on verbal exams")

    sql = <<SQL
        SELECT
          o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает организаторов в аудитории с ролью Операторов ПК в аудитории проведения устной части ин. языков' as comment,
          st.code, e.date,
          coalesce(empl.cnt, 0) as e_out,
          coalesce(auds_p.cnt, 0) as auds_p_cnt,
          coalesce(auds_t.cnt, 0) as auds_t_cnt

          FROM exams e
          JOIN exam_types et ON et.id = e.exam_type_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
          JOIN stations st ON st.id = est.station_id
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
          LEFT JOIN (
              SELECT empl.date, empl.stid, sum (empl.cnt) as cnt
              FROM stations st
              JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(emp.id)) as cnt
              FROM exam_stations est
              JOIN exam_station_employees ese ON ese.exam_station_id = est.id and ese.deleted_at is NULL
              JOIN employees emp ON emp.id = ese.employee_id and emp.deleted_at is NULL
              JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
              JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
              JOIN exam_types et ON et.id = e.exam_type_id
              JOIN subjects sj ON sj.code = e.subject_code
              JOIN station_posts sp ON sp.code = ese.station_post_code
              where est.deleted_at is NULL and sj.name like '%устная часть%'
              and sp.name like 'Оператор ПК%'
              group by e.date, st.id, e.id
              ) as empl ON st.id = empl.stid
              group by empl.date, empl.stid
          ) empl ON empl.date = e.date and empl.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id
            JOIN stations st ON st.id = est.station_id
            JOIN subjects sj ON sj.code = e.subject_code
            where c.classroom_verbal_part_sing_code = 4
            and sj.fiscode in (29, 30, 31, 33)
            group by e.date, st.id
          ) auds_p ON auds_p.date = e.date and auds_p.stid = st.id
          LEFT JOIN (
          SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
            FROM exam_stations est
            JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
            JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
            JOIN exams e ON e.id = est.exam_id
            JOIN stations st ON st.id = est.station_id
            JOIN subjects sj ON sj.code = e.subject_code
            where c.classroom_verbal_part_sing_code = 2
            and sj.fiscode in (29, 30, 31, 33)
            group by e.date, st.id
          ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id


        WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
        and st.code <> '4100'
        and coalesce(empl.cnt, 0) < coalesce(auds_p.cnt, 0)

        group by m.id, o.id, st.code, e.date, empl.cnt, auds_p.cnt, auds_p.places, auds_p.paks, auds_t.cnt, auds_t.places, auds_t.paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end



    puts ("geks in take exams auditoriums on verbal exams")

    sql = <<SQL
        SELECT
          o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает Членов ГЭК на устную часть ин. языков' as comment,
          st.code, e.date,
          coalesce(empl.cnt, 0) as employees,
		  coalesce(auds_t.cnt, 0) as auds_take_cnt,
		  ceil(coalesce(auds_t.cnt) / 4) as employees_needed

          FROM exams e
          JOIN exam_types et ON et.id = e.exam_type_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
          JOIN stations st ON st.id = est.station_id
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
          LEFT JOIN (
              SELECT empl.date, empl.stid, sum (empl.cnt) as cnt
              FROM stations st
              JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(emp.id)) as cnt
              FROM exam_stations est
              JOIN exam_station_employees ese ON ese.exam_station_id = est.id and ese.deleted_at is NULL
              JOIN employees emp ON emp.id = ese.employee_id and emp.deleted_at is NULL
              JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
              JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
              JOIN exam_types et ON et.id = e.exam_type_id
              JOIN subjects sj ON sj.code = e.subject_code
              JOIN station_posts sp ON sp.code = ese.station_post_code
              where est.deleted_at is NULL and sj.name like '%устная часть%'
              and sp.name like 'Член ГЭК%'
              group by e.date, st.id, e.id
              ) as empl ON st.id = empl.stid
              group by empl.date, empl.stid
          ) empl ON empl.date = e.date and empl.stid = st.id
      LEFT JOIN (
     	 	SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
	        FROM exam_stations est
	        JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
	        JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
	        JOIN exams e ON e.id = est.exam_id
	        JOIN stations st ON st.id = est.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 2
          and sj.fiscode in (29, 30, 31, 33)
	        group by e.date, st.id
      ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id

        WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
        and st.code <> '4100'
        and coalesce(empl.cnt, 0) < ceil(coalesce(auds_t.cnt) / 4)

        group by m.id, o.id, st.code, e.date, empl.cnt, auds_t.cnt, auds_t.places, auds_t.paks
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    puts ("ruks on verbal exams")

    sql = <<SQL
        SELECT
          o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает Руководителя ППЭ на устную часть ин. языков' as comment,
          st.code, e.date,
          coalesce(empl.cnt, 0) as e_out

          FROM exams e
          JOIN exam_types et ON et.id = e.exam_type_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
          JOIN stations st ON st.id = est.station_id
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
          LEFT JOIN (
              SELECT empl.date, empl.stid, sum (empl.cnt) as cnt
              FROM stations st
              JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(emp.id)) as cnt
              FROM exam_stations est
              JOIN exam_station_employees ese ON ese.exam_station_id = est.id and ese.deleted_at is NULL
              JOIN employees emp ON emp.id = ese.employee_id and emp.deleted_at is NULL
              JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
              JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
              JOIN exam_types et ON et.id = e.exam_type_id
              JOIN subjects sj ON sj.code = e.subject_code
              JOIN station_posts sp ON sp.code = ese.station_post_code
              where est.deleted_at is NULL and sj.name like '%устная часть%'
              and sp.name like 'Руководитель ППЭ%'
              group by e.date, st.id, e.id
              ) as empl ON st.id = empl.stid
              group by empl.date, empl.stid
          ) empl ON empl.date = e.date and empl.stid = st.id

        WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
        and st.code <> '4100'
        and coalesce(empl.cnt, 0) < 1

        group by m.id, o.id, st.code, e.date, empl.cnt

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    puts ("techs in take exams auditoriums on verbal exams")

    sql = <<SQL
        SELECT
          o.id as oid, m.id as mid, 'В ППЭ ' || st.code || ' на дату ' || e.date || ' не хватает Технических специалистов на устную часть ин. языков' as comment,
          st.code, e.date,
          coalesce(empl.cnt, 0) as employees,
		  coalesce(auds_t.cnt, 0) as auds_take_cnt,
		  ceil(coalesce(auds_t.cnt) / 4) as employees_needed

          FROM exams e
          JOIN exam_types et ON et.id = e.exam_type_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN exam_stations est ON est.exam_id = e.id and est.deleted_at is NULL
          JOIN stations st ON st.id = est.station_id
          JOIN ous o ON o.id = st.ou_id
          JOIN mouos m ON m.id = o.mouo_id
          LEFT JOIN (
              SELECT empl.date, empl.stid, sum (empl.cnt) as cnt
              FROM stations st
              JOIN (SELECT e.date, st.id as stid, e.id, count(distinct(emp.id)) as cnt
              FROM exam_stations est
              JOIN exam_station_employees ese ON ese.exam_station_id = est.id and ese.deleted_at is NULL
              JOIN employees emp ON emp.id = ese.employee_id and emp.deleted_at is NULL
              JOIN exams e ON e.id = est.exam_id and e.deleted_at is NULL
              JOIN stations st ON st.id = est.station_id and st.deleted_at is NULL
              JOIN exam_types et ON et.id = e.exam_type_id
              JOIN subjects sj ON sj.code = e.subject_code
              JOIN station_posts sp ON sp.code = ese.station_post_code
              where est.deleted_at is NULL and sj.name like '%устная часть%'
              and sp.name like 'Технический%'
              group by e.date, st.id, e.id
              ) as empl ON st.id = empl.stid
              group by empl.date, empl.stid
          ) empl ON empl.date = e.date and empl.stid = st.id
      LEFT JOIN (
     	 	SELECT e.date, st.id as stid, count(distinct(c.id)) as cnt, sum(c.available_seats) as places, sum(c.verbal_part_paks) as paks
	        FROM exam_stations est
	        JOIN exam_station_classrooms esc ON esc.exam_station_id = est.id and esc.deleted_at is NULL
	        JOIN classrooms c ON c.id = esc.classroom_id and c.deleted_at is NULL
	        JOIN exams e ON e.id = est.exam_id
	        JOIN stations st ON st.id = est.station_id
	        JOIN subjects sj ON sj.code = e.subject_code
          where c.classroom_verbal_part_sing_code = 2
          and sj.fiscode in (29, 30, 31, 33)
	        group by e.date, st.id
      ) auds_t ON auds_t.date = e.date and auds_t.stid = st.id

        WHERE sj.name like '%устная часть%' and e.deleted_at is NULL and et.deleted_at is NULL
        and st.code <> '4100'
        and coalesce(empl.cnt, 0) < ceil(coalesce(auds_t.cnt) / 4)

        group by m.id, o.id, st.code, e.date, empl.cnt, auds_t.cnt, auds_t.places, auds_t.paks

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :seating, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


  end
end