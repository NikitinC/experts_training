module MandatoryExamx11Vtg
  def process
    sql = <<SQL
select
	s.id as sid, o.id as oid, o.mouo_id as mid, epc.name as epc

from students s
join groups g on g.id = s.group_id and g.deleted_at is null
join ous o on o.id = g.ou_id
left join ege_participant_categories epc on epc.code = s.ege_participant_category_code

where
  s.deleted_at is null and g.deleted_at is null and o.deleted_at is null
  and (
	g.number > 10 or exists (
		select 1 from attestation_form_students afs where afs.student_id = s.id and afs.deleted_at is null and afs.attestation_form_code in (1,8)
	)
)

and (
	(
	not exists (
		select 1 from exam_students es
		join exams e on e.id = es.exam_id and es.deleted_at is null
		where es.deleted_at is null and es.student_id = s.id and e.deleted_at is null
		and e.exam_type_id in (5, 19, 24) and e.subject_code in (1, 51, 111, 112, 113, 114)
		) and (s.gia_active_results_rus = false or s.ege_participant_category_code = 1)
	)
	or (
	not exists (
		select 1 from exam_students es
		join exams e on e.id = es.exam_id and es.deleted_at is null
		where es.deleted_at is null and es.student_id = s.id and e.deleted_at is null and
      e.exam_type_id in (5, 19, 24) and e.subject_code in (2, 34, 52, 251)
	) and (s.gia_active_results_math = false or s.ege_participant_category_code = 1)
	)
)

and o.code not in ('111111', '999999')  and s.ege_participant_category_code in (1, 8)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_mandatory_exams_11_vtg, r['sid'], 'Student', r['oid'], r['mid'], r['epc']
    end

  end
end