module Retake
  def process

    puts ("участник назначен на несколько экзаменов в один день")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid, 'Участник назначен на ' || cast(ec.cnt as varchar(4)) || ' экзамена в одну дату ' || cast (ec.date as varchar(12)) || '.' as comment, ec.date, ec.cnt
    FROM students s
    JOIN groups g ON g.id = s.group_id
    JOIN ous o ON o.id = g.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    JOIN (SELECT s.id, e.date, count(e.id) as cnt
      FROM exam_students es
      JOIN students s ON s.id = es.student_id
      JOIN exams e ON e.id = es.exam_id
      JOIN subjects sj ON sj.code = e.subject_code
      WHERE es.deleted_at is NULL and e.deleted_at is NULL and s.deleted_at is NULL
        and sj.name not like '%устн%'
      group by s.id, e.date) ec ON ec.id = s.id
    WHERE ec.cnt > 1 and o.code not in ('999999', '111111')

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :retake, r['eid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("участнику удалили экзамен")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid, 'Участнику удалён экзамен '|| sj.name || ' ' || cast (e.date as varchar(12)) || '.' as comment
      FROM exam_students_backup b
      JOIN students s ON s.id = b.student_id
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      JOIN exams e ON e.id = b.exam_id
      JOIN subjects sj ON sj.code = e.subject_code
      LEFT JOIN exam_students es ON es.student_id = b.student_id and es.exam_id = b.exam_id
      WHERE es.id is NULL and b.deleted_at is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :retake, r['eid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("участнику удалили экзамен")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid, 'Участник предположительно может пересдавать предмет '|| sj.name || ' в резервные дни 28-29 июня 2018 года.' as comment
      FROM exam_students_retake r
      JOIN students s ON s.guid = r.student_guid
      JOIN subjects sj ON sj.fiscode = r.subject_fiscode
      JOIN exam_students_backup esb ON esb.student_id = s.id
      JOIN exams e ON e.id = esb.exam_id and e.subject_code = sj.code
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      LEFT JOIN 
      (select s.id as sid, ex.date, ex.subject_code from exam_students es
       JOIN students s ON es.student_id = s.id
       JOIN exam_students_retake r ON s.guid = r.student_guid
       JOIN exams ex ON ex.id = es.exam_id
      where ex.date in ('2018-06-28', '2018-06-29')
      ) ex ON ex.subject_code = e.subject_code and s.id = ex.sid
      where ex.date is NULL
    group by s.id, o.id, m.id, sj.name
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :retake, r['eid'], 'Student', r['oid'], r['mid'], r['comment']
    end


  end
end