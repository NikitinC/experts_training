namespace :fbd_2017 do
  task :participants_exams, [:exam_type_ids, :exam_codes] => :prepare do

    # composition_id_to_fct_code = Hash[Composition.all.map{|x| [x.id, x.fct_code]}]
    #
    # Student \
    #       .includes('attestation_form_students')
    #       .includes('composition_students')
    #     .where({attestation_form_students: {attestation_form_code: [AttestationForm::EGE, AttestationForm::GVE11], deleted_at: nil}})
    #     .all.each do |s|
    #
    #     s.composition_students.each do |c|
    #         next if c.deleted_at
    #         fail "Invalid composition #{s.id}" if composition_id_to_fct_code[c.composition_id].nil?
    #         line(
    #             s.guid, # 1. GUID участника
    #             composition_id_to_fct_code[c.composition_id], # 2. Код дня экзамена
    #             transform_time(c.created_at), # 3. Дата-время создания
    #             nil# 4. Форма ГВЭ
    #         )
    #     end
    # end

#    Экзамены



    ExamStudent.find_each do |es| # 5 и 19 - типы экзаменов ЕГЭ
      # puts es.student.id

      exam_code = 0
      exam_code = es.exam.code if es.exam.exam_type_id.in? [5, 19]
      gve_code = nil
      next if es.exam.exam_type_id.nil?
      next if es.exam.code.nil?
      next if exam_code == 0
      # по данным из столбца ExamFormatCode таблицы rbd_ParticipantsExams выявил, что
      # 1 - устная форма; 2 - диктант; 3 - изложение; 4 - сочинение
      # в РБД коды ФЦТ данных экзаменов увеличил на 6000 - устная форма
      # 5000 - диктант; 4000 - изложение; 3000 - сочинение

      if exam_code > 6000 then #устная часть
        exam_code = exam_code - 6000
        gve_code = 1
      end
      if exam_code > 5000 then #диктант
        exam_code = exam_code - 5000
        gve_code = 2
      end
      if exam_code > 4000 then #изложение
        exam_code = exam_code - 4000
        gve_code = 3
      end
      if exam_code > 3000 then #сочинение
        exam_code = exam_code - 3000
        gve_code = 4
      end



      if exam_code && es.student
        line(
            es.student.guid, # 1. GUID участника
            exam_code, # 2. Код дня экзамена
            transform_time(es.created_at), # 3. Дата-время создания
            gve_code # 3. Код типа ГВЭ
        )
      end
    end

    # # Сочинения
    # #
    # CompositionStudent.where(composition_id: federal_composition_codes.keys).find_each do |item|
    #   composition_code = federal_composition_code item.composition
    #
    #   if composition_code && item.student
    #     line(
    #         item.student.guid, # 1. GUID участника
    #         composition_code, # 2. Код дня экзамена
    #         transform_time(item.created_at) # 3. Дата-время создания
    #     )
    #   end
    # end

  end
end