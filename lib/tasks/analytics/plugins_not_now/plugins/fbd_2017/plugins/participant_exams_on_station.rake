namespace :fbd_2017 do
  task :participant_exams_on_station, [:exam_type_ids, :exam_codes] => :prepare do
    ExamStudent.find_each do |es|
      exam_code = es.exam.code
      if exam_code && es.student && es.exam_station && es.exam_station.station
        line es.student.guid, # 1. guid участника
             exam_code, # 2. Код дня экзамена
             es.exam_station.station.guid, # 3. GUID ППЭ
             transform_time(es.created_at) # 4 Дата-время создания
      end
    end
  end
end