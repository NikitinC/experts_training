# coding: utf-8
namespace :fbd_2017 do
  task :station_worker_on_station, [:exam_type_ids, :exam_codes] => :prepare do
    sql = <<SQL
SELECT distinct(e.guid) as worker_guid,
	CASE
    	WHEN s.guid is NULL THEN s2.guid
        ELSE s.guid
    END as station_guid,
    0,
	CASE
    	WHEN sp.fiscode is NULL THEN sp2.fiscode
        ELSE sp.fiscode
    END as fiscode
    , e.created_at, e.updated_at
          FROM employees e
    	  JOIN (
                SELECT distinct(e.id) as eid
                  FROM employees e
                  JOIN exam_station_employees ese ON ese.employee_id = e.id
                  where ese.exam_type_id in (5) and ese.deleted_at is NULL
                UNION
                SELECT distinct(e.id) as eid
                        FROM stations s
                        JOIN exam_types_stations ets ON ets.station_id = s.id and ets.exam_type_id in (5)
                        JOIN employees e ON e.on_station = cast(s.code as int)
                WHERE e.deleted_at is NULL and s.deleted_at is NULL

     ) etap ON etap.eid = e.id
	   	  LEFT JOIN exam_station_employees ese ON ese.employee_id = e.id and ese.exam_type_id in (5) and ese.deleted_at is NULL
          LEFT JOIN station_posts sp ON sp.code = ese.station_post_code
          LEFT JOIN station_posts sp2 ON sp2.code = e.station_post_code
          LEFT JOIN stations s ON s.id = ese.station_id and s.deleted_at is NULL
          LEFT JOIN (SELECT s.code, s.guid
                from stations s
                join exam_types_stations ets ON ets.station_id = s.id and ets.exam_type_id in (5)
                where s.deleted_at is NULL
                ) s2 ON cast(s2.code as int) = e.on_station
          where e.deleted_at is NULL
          group by e.id, sp.fiscode, s.guid, s2.guid, sp2.fiscode
SQL

    rwas = ''
    ActiveRecord::Base.connection.execute(sql).each do |r|
      if !r['fiscode'].nil? then
           line(
              r['worker_guid'],
              r['station_guid'],
              0,
              r['fiscode'],
              r['created_at'],
              r['updated_at'],
           )
      end
    end
  end
end