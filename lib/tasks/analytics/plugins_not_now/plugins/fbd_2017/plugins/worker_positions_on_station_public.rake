module WorkerPositionsOnStationPublic
  def filename
    '66_WorkerPositionsOnStation_public.cs_'
  end

  def process **args

    known_observers = PublicObserver.where(valid_cache: true, deleted_at: nil).joins(exam_station_public_observers: :exam)
        .where(exam_station_public_observers: {deleted_at: nil}, exams: {exam_type_id: args[:exam_types]}).uniq.map{
      |x| x.id.to_i
    }

    ExamStationPublicObserver.where(public_observer_id: known_observers).find_each do |espo|
      exam_code = espo.exam_station.try(:exam).try(:code)
      station_post_code = 6
      if exam_code
        line(
            espo.public_observer.guid, # 1. GUID работника ППЭ
            exam_code, # 2. Код дня экзамена
            espo.exam_station.station.guid, # 3. GUID ППЭ
            station_post_code, # 4. Код должности в ППЭ
            transform_time(espo.created_at), # 5. Дата-время создания
            nil # 6. Код роли организатора
        )
      end
    end
  end
end