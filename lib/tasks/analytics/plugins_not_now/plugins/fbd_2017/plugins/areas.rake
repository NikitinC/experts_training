# encoding: UTF-8

namespace :fbd_2017 do
  task :areas => :prepare do
    Ate.find_each do |ate|
      Mouo.find_each(:conditions => "mouos.code = '%s'" % ate.code) do |m|
        line_array = [
            ate.guid, # 1. GUID
            transform_deleted_at(ate.deleted_at), # 2. Признак удаленной строки
            ate.code, # 3. Код административно-территориальной единицы
            ate.name, # 4. Наименование административно-территориальной единицы
            m.specialist_final_certification_full_name, # 5. ФИО сотрудника, ответственного за проведение ЕГЭ
            prepare_phone(m.specialist_final_certification_phone), # 6. Телефон(ы) сотрудника, ответственного за проведение ЕГЭ
            prepare_email(m.specialist_final_certification_email) # 7. Адрес(а) электронной почты сотрудника, ответственного за проведение ЕГЭ
        ]
        puts line_array.map { |el| escape_for_export(el) }.join("#")
        end
    end
  end
end