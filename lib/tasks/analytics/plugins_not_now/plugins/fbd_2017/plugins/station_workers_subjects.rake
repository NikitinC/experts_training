namespace :fbd_2017 do
  task :station_workers_subjects, [:exam_type_ids, :exam_codes] => :prepare do
    sql = <<SQL
    SELECT e.guid as guid, sj.fiscode, e.station_post_code
      FROM employees e
        JOIN (
    SELECT distinct(e.id) as eid
              FROM employees e
            JOIN exam_station_employees ese ON ese.employee_id = e.id
              where ese.exam_type_id in (5, 19, 24) and ese.deleted_at is NULL
    UNION
    SELECT distinct(e.id) as eid
        FROM stations s
            JOIN exam_types_stations ets ON ets.station_id = s.id and ets.exam_type_id in (5,19,24)
            JOIN employees e ON e.on_station = cast(s.code as int)
            WHERE e.deleted_at is NULL
        ) etap ON etap.eid = e.id
        JOIN employee_ous eo ON eo.employee_id = e.id and eo.deleted_at is NULL
        JOIN employee_ou_subjects eos ON eos.employee_ou_id = eo.id and eos.deleted_at is NULL
        JOIN subjects sj ON sj.code = eos.subject_code
      where e.deleted_at is NULL and sj.fiscode > 0
SQL

      rwas = ''
      ActiveRecord::Base.connection.execute(sql).each do |r|
        if !r['station_post_code'].nil? then
          line(
              r['guid'],
              r['fiscode']
          )
        end
    end
  end

end