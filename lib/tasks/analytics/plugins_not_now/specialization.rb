module Specialization
  def process
    puts ("specializations")

    sql = <<SQL
      SELECT e.id as id,
           o.id as ou_id,
             CASE 
              WHEN e.job_post = '' THEN ep.name 
              ELSE e.job_post
            END as post,
              CASE
                  WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN m1.id
                  ELSE m2.id
              END as mouo,
            kk.s as ksum,
            kk.sp as sp   
        FROM employees e
          JOIN (SELECT e.id as eid, sum(sj.fiscode) as s, length(coalesce(e.why_no_specialization_text, '')) * length(coalesce(e.why_no_specialization_fio, '')) * length(coalesce(e.why_no_specialization_post, '')) as sp
            FROM employees e
              JOIN employee_ous eo ON eo.employee_id = e.id and eo.deleted_at is NULL
              JOIN employee_ou_subjects eos ON eos.employee_ou_id = eo.id and eos.deleted_at is NULL
              JOIN subjects sj ON sj.code = eos.subject_code
            where e.deleted_at is NULL
            group by e.id, e.why_no_specialization_text, e.why_no_specialization_fio, e.why_no_specialization_post) kk ON kk.eid = e.id
        LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
          LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
          LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
          LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
          JOIN employee_posts ep ON ep.code = eo.employee_post_code
        where e.deleted_at is NULL
SQL



    ActiveRecord::Base.connection.execute(sql).each do |r|
      post = r['post']
      ou = r['ou_id']
      sp = r['sp'].to_i
      sum = r['ksum'].to_i
      mouo_guid = r['mouo']
      eid = r['id']
      sql2 = "SELECT COALESCE(sj.fiscode, 0) as fiscode FROM employees e JOIN employee_ous eo ON eo.employee_id = e.id and eo.deleted_at is NULL JOIN employee_ou_subjects eos ON eos.employee_ou_id = eo.id and eos.deleted_at is NULL JOIN subjects sj ON sj.code = eos.subject_code where e.id = " + eid
      res = ActiveRecord::Base.connection.execute(sql2)
      k = 0
      arr=[0]
      res.each do |a|
        arr[k] = a['fiscode']
        k = k + 1
      end


      if post == 'Учитель' then
       # puts post
        add_analytics_item :specialization_post,
                           eid,
                           'Employee',
                           ou,
                           mouo_guid,
                           'Должность Учитель не отражает преподаваемый предмет, исправьте'
      end

      if post == 'Прочие' then
        # puts post
        add_analytics_item :specialization_post,
                           eid,
                           'Employee',
                           ou,
                           mouo_guid,
                           'Должность Прочие не отражает специфики деятельности, исправьте'
      end

      if post == 'Педагог' then
        # puts post
        add_analytics_item :specialization_post,
                           eid,
                           'Employee',
                           ou,
                           mouo_guid,
                           'Должность Педагог не отражает преподаваемый предмет, исправьте'
      end

      if post == 'Преподаватель' then
        # puts post
        add_analytics_item :specialization_post,
                           eid,
                           'Employee',
                           ou,
                           mouo_guid,
                           'Должность Преподаватель не отражает преподаваемый предмет, исправьте'
      end

      if post == 'Старший преподаватель' then
        # puts post
        add_analytics_item :specialization_post,
                           eid,
                           'Employee',
                           ou,
                           mouo_guid,
                           'Должность Старший преподаватель не отражает преподаваемый предмет, исправьте'
      end


      if sum<1 then
        # puts 'no arr'
        if sp == 0 then
          add_analytics_item :specialization_no,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Заполните раздел "Причины отсутствия в РБД специализации сотрудника" в карточке сотрудника'
        end
      end

      if sum>0 then
        # puts 'see a specitality'
        if post == 'Учитель русского языка' && !(arr.include? '1') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Русский язык, Русский язык (ГВЭ)'
        end

        if post == 'Учитель математики' && !(arr.include? '2') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Математика, Математика профильная, Математика базовая, Математика (ГВЭ)'

        end

        if post == 'Учитель физики' && !(arr.include? '3') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Физика'
        end

        if post == 'Учитель химии' && !(arr.include? '4') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Химия'
        end

        if post == 'Учитель информатики' && !(arr.include? '5') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Информатика'
        end

        if post == 'Учитель биологии' && !(arr.include? '6') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Биология'
        end

        if post == 'Учитель истории России' && !(arr.include? '7') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет История'
        end

        if post == 'Учитель географии' && !(arr.include? '8') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет География'
        end

        if post == 'Учитель английского языка' && !(arr.include? '9') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Англ. язык'
        end

        if post == 'Учитель немецкого языка' && !(arr.include? '10') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Нем. язык'
        end

        if post == 'Учитель французского языка' && !(arr.include? '11') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Франц. язык'
        end

        if post == 'Учитель испанского языка' && !(arr.include? '13') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Исп. язык'
        end

        if post == 'Учитель литературы' && !(arr.include? '18') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Литература'
        end

        if post == 'Учитель русского языка и литературы' && !(arr.include? '1') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Русский язык'
        end

        if post == 'Учитель русского языка и литературы' && !(arr.include? '18') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Литература'
        end

        if post == 'Учитель физики и информатики' && !(arr.include? '3') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Физика'
        end

        if post == 'Учитель физики и информатики' && !(arr.include? '5') then
          add_analytics_item :specialization_sj,
                             eid,
                             'Employee',
                             ou,
                             mouo_guid,
                             'Добавьте предмет Информатика'
        end
      end

    end
  end



end