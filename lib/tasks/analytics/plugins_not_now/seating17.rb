module Seating17
  class NoAunts < StandardError
  end

  class NoClassrooms < StandardError
  end

  def process
    puts ("Seating")

    def make_seating(sca)
      students, classrooms, aunts = sca
      students = students.select{|x| true}
      classrooms = classrooms.select{|x| true}
      aunts = aunts.select{|x| true}

      students_pop = []
      classrooms_pop = []
      aunts_pop = []

      classrooms_sorted = classrooms.sort_by{|e| e.available_seats}

      while students.length > 0
        raise NoAunts.new if aunts.length == 0
        aunts_pop.append a1=aunts.pop
        raise NoAunts.new if aunts.length == 0
        aunts_pop.append a2=aunts.pop

        raise NoClassrooms.new if classrooms_sorted.length == 0
        classrooms_pop.append classroom = classrooms_sorted.pop

        # puts a1, a2, classroom, classroom.available_seats
        classroom.available_seats.times do
          break if students.length == 0
          students_pop.append s=students.pop
          # puts '   ' + s.name
        end
      end

      [students_pop, classrooms_pop, aunts_pop]
    end

    def exclude(a, b)
      students_to_reject, classrooms_to_reject, aunts_to_reject = b
      students_to_reject = students_to_reject.map(&:id)
      classrooms_to_reject = classrooms_to_reject.map(&:id)
      aunts_to_reject = aunts_to_reject.map(&:id)
      [
          a[0].reject{|x| students_to_reject.include? x.id},
          a[1].reject{|x| classrooms_to_reject.include? x.id},
          a[2].reject{|x| aunts_to_reject.include? x.id}
      ]
    end

    ExamStation.find_each do |es|
      next if es.exam.date <= Date.today
      next if es.exam.is_verbal
      # puts "#{es.id} #{es}"
      comments = []

      uniq_posts = es.exam_station_employees.map(&:station_post_code).uniq
      comments.append 'Нет руководителя ППЭ' unless uniq_posts.include? 1
      comments.append 'Нет члена ГЭК' unless uniq_posts.include? 5
      comments.append 'Нет технического специалиста' unless uniq_posts.include? 7
      comments.append 'Нет организаторов вне аудитории' unless uniq_posts.include? 3

      students = es.exam_students.map(&:student)
      # comments.append 'Нет участников' if students.length == 0

      classroom_attestation_forms = (
        es.is_gve ? [AttestationForm::GVE9, AttestationForm::GVE11] : [AttestationForm::EGE, AttestationForm::OGE]
      )

      classrooms = es.classrooms.select{|x|
        x.attestation_forms.length > 0 ?
            x.attestation_forms.map{ |y|
              classroom_attestation_forms.include?(y.code) ? 1 : 0
            }.inject(:+) > 0 :
            false
      }
      comments.append 'Нет аудиторий' if classrooms.length == 0

      aunts = es.exam_station_employees.select{|x| x.station_post_code == 2}.map(&:employee).to_a
      comments.append 'Нет сотрудников' if aunts.length == 0

      begin
        students, classrooms, aunts =
            exclude(
              [students, classrooms, aunts],
              make_seating([students.select{|x| x.special_seating},
                            classrooms.select{|x| x.special_sit},
                            aunts]
              )
        )

        begin
          students, classrooms, aunts =
              exclude(
                  [students, classrooms, aunts],
                  make_seating([students,
                                classrooms.select{|x| !x.special_sit},
                                aunts]
                  )
              )
          # puts students if students.length > 0
          comments.append 'Ошибка алгоритма рассадки' if students.length > 0
        rescue NoClassrooms
          students, classrooms, aunts =
              exclude(
                  [students, classrooms, aunts],
                  make_seating([students,
                                classrooms,
                                aunts]
                  )
              )
          comments.append 'Ошибка алгоритма рассадки' if students.length > 0
          comments.append 'Рассадка прошла только с использованием аудиторий со спецрассадкой в качестве основных'
        end
      rescue NoAunts
        comments.append 'Недостаточно организаторов в аудиториях'
      rescue NoClassrooms
        comments.append 'Недостаточно аудиторий'
      end

      # puts comments
      add_analytics_item es.exam.attestation_form.is_ege_or_gve11 ? :seating_11 : :seating_9,
                         es.id, 'ExamStation', es.station.ou.id, es.station.ou.mouo.id,
                         comments.join("\n") if comments.length > 0

    end
  end
end