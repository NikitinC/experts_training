require 'csv'

module Composition
  def process
    csv = CSV.read(Rails.root + "lib/tasks/analytics/composition.csv", :headers => true, :col_sep => ";")
    Student.find_each do |s|
      k = csv.detect {|h| h[0] == s.guid.at(1..-2).upcase}

      if s.is_ege_or_gve11 and !s.has_composition and !s.is_vpl_spo and k.nil? then
        no = true
        s.composition_students.each do |cs|
          if cs.composition_id > 14 then
            no = false
          end
        end
        if no then
          add_analytics_item :no_composition,
                             s.id,
                             'Student',
                             s.group.ou.id,
                             s.group.ou.mouo_id,
                             'Участник не имеет зачёта по сочинению и не назначен на дополнительные сроки'
        end


      end

    end
  end
end


  ### Для создания файла composition.csv используется запрос
  # use [erbd_ege_reg_17_66]
  #
  # SELECT ParticipantFK, Mark5
  # FROM res_HumanTests ht
  # JOIN res_Marks m ON m.HumanTestID = ht.HumanTestID
  # where ht.SubjectCode in (20, 21) and m.Mark5 = 5