module NoAttestationForm11
  def process

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Не указана ни одна форма аттестации' as comment
      FROM students s
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      LEFT JOIN attestation_form_students afs ON afs.student_id = s.id and afs.deleted_at is NULL
    WHERE s.deleted_at is NULL and afs.id is NULL
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_attestation_form_11, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end