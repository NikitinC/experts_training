module NoOgeScan
  def process
    puts ("no oge scan analytics")

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Не загружено заявление на ГИА-9' as comment
    FROM students s
    JOIN groups g ON g.id = s.group_id
    JOIN ous o ON o.id = g.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and g.number in (9, 10)
    and coalesce(length(s.ege_statement_scan), 0) < 3
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_ege_scan, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end