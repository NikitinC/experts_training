module Seating2017
  def process
    puts ("seating 2017")

    sql = <<SQL
SELECT  e.date as "date", st.code as "ppe_code",
        CASE
         WHEN st.station_additional_feature_code in (5, 9, 13, 15) THEN 'v'
         ELSE 'x'
        END AS "onHome",
      	sj.fiscode as "subject_code", sj.name as "subject_name",
        coalesce(classrooms.classrooms_count, 0) as "classrooms_normal_count",
        coalesce(classrooms.classrooms_seats_sum, 0) as "classrooms_normal_seats_sum", 
        coalesce(classrooms.classrooms_min_seats, 0) as "classrooms_normal_seats_min", 
        coalesce(classrooms.classrooms_max_seats, 0) as "classrooms_normal_seats_max",
        coalesce(classrooms_limit.classrooms_count, 0) as "classrooms_limit_count",
        coalesce(classrooms_limit.classrooms_seats_sum, 0) as "classrooms_limit_seats_sum", 
        coalesce(classrooms_limit.classrooms_min_seats, 0) as "classrooms_limit_seats_min", 
        coalesce(classrooms_limit.classrooms_max_seats, 0) as "classrooms_limit_seats_max",
        coalesce(student_normal.student_normal_cnt, 0) as "student_normal_count",
        coalesce(student_limit.student_limit_cnt, 0) as "student_limit_count", 
        coalesce(sw_ruk.employee_cnt, 0) as "sw_ruk",
        coalesce(sw_org_in.employee_cnt, 0) as "sw_org_in",
        coalesce(sw_org_out.employee_cnt, 0) as "sw_org_out",
        coalesce(sw_tech.employee_cnt, 0) as "sw_tech",
        coalesce(sw_gek.employee_cnt, 0) as "sw_gek",
        coalesce(sw_med.employee_cnt, 0)  as "sw_med",
        coalesce(sw_instruktor.employee_cnt, 0) as "sw_instruktor",
        coalesce(sw_org_proved_operatorPK.employee_cnt, 0) as "sw_org_proved_operatorPK",
        coalesce(sw_org_proved.employee_cnt, 0) as "sw_org_proved",
        coalesce(sw_org_podgotovka.employee_cnt, 0) as "sw_org_podgotovka",
        coalesce(classrooms_podgotovka_seats.cnt, 0) as "classrooms_podgotovka_seats_count",
        coalesce(classrooms_provedenie_paks.cnt, 0) as "classrooms_provedenie_paks_count",
        coalesce(classrooms_podgotovka_count.cnt, 0) as "classrooms_podgotovka_count",
        coalesce(classrooms_provedenie_count.cnt, 0) as "classrooms_provedenie_count",
        coalesce(needed_verbal_paks.needed_stations, 0) as "classrooms_provedenie_paks_needed",
        coalesce(needed_verbal_paks3.needed_stations, 0) as "classrooms_provedenie_paks_needed3",
        CASE
         WHEN st.station_additional_feature_code in (36, 292, 356, 44, 300, 364, 868, 876) THEN 'v'
         ELSE 'x'
        END AS "printKIM",
        CASE
         WHEN st.station_additional_feature_code in (260, 292, 356, 268, 300, 364, 772, 876, 780) THEN 'v'
         ELSE 'x'
        END AS "Scan",
        CASE
        	WHEN e.exam_type_id in (5, 19, 24) then 'gia11'
            ELSE 'gia9'
        END as "gia",
        o.id as oid,
        m.id as mid,
        est.id as esid
        
        
 FROM exam_stations est
 JOIN exams e ON e.id = est.exam_id
    JOIN stations st ON st.id = est.station_id
    JOIN ous o ON o.id = st.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    JOIN subjects sj ON sj.code = e.subject_code
    LEFT JOIN (SELECT psc.exam_station_id as exam_station_id, count(distinct(c.id)) as classrooms_count, sum(c.available_seats) as classrooms_seats_sum, min(c.available_seats) as classrooms_min_seats, max(c.available_seats) as classrooms_max_seats
          FROM exam_station_classrooms psc
          JOIN classrooms c ON c.id = psc.classroom_id
          WHERE psc.deleted_at is NULL and c.deleted_at is NULL and c.special_sit is false
          group by psc.exam_station_id) as classrooms ON classrooms.exam_station_id = est.id
    LEFT JOIN (SELECT psc.exam_station_id as exam_station_id, count(distinct(c.id)) as classrooms_count, sum(c.available_seats) as classrooms_seats_sum, min(c.available_seats) as classrooms_min_seats, max(c.available_seats) as classrooms_max_seats
          FROM exam_station_classrooms psc
          JOIN classrooms c ON c.id = psc.classroom_id
          WHERE psc.deleted_at is NULL and c.deleted_at is NULL and c.special_sit is true
          group by psc.exam_station_id) as classrooms_limit ON classrooms_limit.exam_station_id = est.id
    LEFT JOIN (SELECT es.exam_station_id, count(distinct(s.id)) as student_normal_cnt
                FROM exam_students es
                JOIN students s ON s.id = es.student_id
                WHERE es.deleted_at is NULL and s.deleted_at is NULL and s.special_seating = false
             group by es.exam_station_id) as student_normal ON student_normal.exam_station_id = est.id
    LEFT JOIN (SELECT es.exam_station_id, count(distinct(s.id)) as student_limit_cnt
                FROM exam_students es
                JOIN students s ON s.id = es.student_id
                WHERE es.deleted_at is NULL and s.deleted_at is NULL and s.special_seating = true
             group by es.exam_station_id) as student_limit ON student_limit.exam_station_id = est.id
    LEFT JOIN (SELECT ex.date, ex.exam_type_id, st.code, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code = 1
             group by ex.date, st.code, ex.exam_type_id) as sw_ruk ON sw_ruk.date = e.date and sw_ruk.code = st.code and sw_ruk.exam_type_id = e.exam_type_id

    LEFT JOIN (SELECT ese.exam_station_id, ex.exam_type_id, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL
                 and ese.station_post_code = 2
             group by ese.exam_station_id, ex.exam_type_id) as sw_org_in ON sw_org_in.exam_station_id = est.id and sw_org_in.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ex.date, ex.exam_type_id, st.code, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code in (3, 38, 39, 40, 41)
             group by ex.date, st.code, ex.exam_type_id) as sw_org_out ON sw_org_out.date = e.date and sw_org_out.code = st.code and sw_org_out.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ex.date, ex.exam_type_id, st.code, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code = 7
             group by ex.date, st.code, ex.exam_type_id) as sw_tech ON sw_tech.date = e.date and sw_tech.code = st.code and sw_tech.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ex.date, ex.exam_type_id, st.code, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code = 5
             group by ex.date, st.code, ex.exam_type_id) as sw_gek ON sw_gek.date = e.date and sw_gek.code = st.code and sw_gek.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ex.date, st.code, ex.exam_type_id, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code = 8
             group by ex.date, st.code, ex.exam_type_id) as sw_med ON sw_med.date = e.date and sw_med.code = st.code and sw_med.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ese.exam_station_id, ex.exam_type_id, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL
                 and ese.station_post_code = 9
             group by ese.exam_station_id, ex.exam_type_id) as sw_instruktor ON sw_instruktor.exam_station_id = est.id and sw_instruktor.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ex.date, ex.exam_type_id, st.code, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code = 26
             group by ex.date, st.code, ex.exam_type_id) as sw_org_proved ON sw_org_proved.date = e.date and sw_org_proved.code = st.code and sw_org_proved.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ex.date, ex.exam_type_id, st.code, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code = 27
             group by ex.date, st.code, ex.exam_type_id) as sw_org_proved_operatorPK ON sw_org_proved_operatorPK.date = e.date and sw_org_proved_operatorPK.code = st.code and sw_org_proved_operatorPK.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT ex.date, st.code, ex.exam_type_id, count(distinct(e.id)) as employee_cnt
                FROM exam_station_employees ese
                JOIN employees e ON e.id = ese.employee_id
                JOIN exam_stations es ON es.id = ese.exam_station_id
                JOIN stations st ON st.id = es.station_id
                JOIN exams ex ON ex.id = es.exam_id
                WHERE ese.deleted_at is NULL and e.deleted_at is NULL and es.deleted_at is NULL and ex.deleted_at is NULL and st.deleted_at is NULL
                 and ese.station_post_code = 25
             group by ex.date, st.code, ex.exam_type_id) as sw_org_podgotovka ON sw_org_podgotovka.date = e.date and sw_org_podgotovka.code = st.code and sw_org_podgotovka.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT e.date, s.code, e.exam_type_id, sum(c.available_seats) as cnt
          FROM exam_station_classrooms esc
          JOIN classrooms c ON c.id = esc.classroom_id
          JOIN exam_stations es ON es.id = esc.exam_station_id
          JOIN exams e ON e.id = es.exam_id
          JOIN stations s ON s.id = es.station_id
          WHERE esc.deleted_at is NULL and c.deleted_at is NULL and es.deleted_at is NULL and e.deleted_at is NULL
           and c.classroom_verbal_part_sing_code = 4 -- аудитория подготовки
          group by e.date, s.code, e.exam_type_id) as classrooms_podgotovka_seats ON classrooms_podgotovka_seats.date = e.date and classrooms_podgotovka_seats.code = st.code and classrooms_podgotovka_seats.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT e.date, s.code, e.exam_type_id, sum(c.verbal_part_paks) as cnt
          FROM exam_station_classrooms esc
          JOIN classrooms c ON c.id = esc.classroom_id
          JOIN exam_stations es ON es.id = esc.exam_station_id
          JOIN exams e ON e.id = es.exam_id
          JOIN stations s ON s.id = es.station_id
          WHERE esc.deleted_at is NULL and c.deleted_at is NULL and es.deleted_at is NULL and e.deleted_at is NULL
           and c.classroom_verbal_part_sing_code = 2 -- аудитория проведения
          group by e.date, s.code, e.exam_type_id) as classrooms_provedenie_paks ON classrooms_provedenie_paks.date = e.date and classrooms_provedenie_paks.code = st.code and classrooms_provedenie_paks.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT e.date, s.code, e.exam_type_id, count(distinct(c.id)) as cnt
          FROM exam_station_classrooms esc
          JOIN classrooms c ON c.id = esc.classroom_id
          JOIN exam_stations es ON es.id = esc.exam_station_id
          JOIN exams e ON e.id = es.exam_id
          JOIN subjects sj ON sj.code = e.subject_code
          JOIN stations s ON s.id = es.station_id
          WHERE esc.deleted_at is NULL and c.deleted_at is NULL and es.deleted_at is NULL and e.deleted_at is NULL
           and sj.fiscode in (29, 30, 31, 33)
           and c.classroom_verbal_part_sing_code = 4 -- аудитория подготовки
          group by e.date, s.code, e.exam_type_id) as classrooms_podgotovka_count ON classrooms_podgotovka_count.date = e.date and classrooms_podgotovka_count.code = st.code and classrooms_podgotovka_count.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT e.date, s.code, e.exam_type_id, count(distinct(c.id)) as cnt
          FROM exam_station_classrooms esc
          JOIN classrooms c ON c.id = esc.classroom_id
          JOIN exam_stations es ON es.id = esc.exam_station_id
          JOIN exams e ON e.id = es.exam_id
          JOIN stations s ON s.id = es.station_id
          JOIN subjects sj ON sj.code = e.subject_code
          WHERE esc.deleted_at is NULL and c.deleted_at is NULL and es.deleted_at is NULL and e.deleted_at is NULL
           and sj.fiscode in (29, 30, 31, 33)
           and c.classroom_verbal_part_sing_code = 2 -- аудитория проведения
          group by e.date, s.code, e.exam_type_id) as classrooms_provedenie_count ON classrooms_provedenie_count.date = e.date and classrooms_provedenie_count.code = st.code and classrooms_provedenie_count.exam_type_id = e.exam_type_id

    LEFT JOIN (SELECT e.date, st.code, e.exam_type_id, sum(coalesce(everysubject.cnt, 0)) + coalesce(everysubject_limit.cnt, 0) as "needed_stations"
                FROM exam_stations est
    JOIN exams e ON e.id = est.exam_id
    JOIN stations st ON st.id = est.station_id
                JOIN subjects sj ON sj.code = e.subject_code
                LEFT JOIN (SELECT es.exam_station_id, e.exam_type_id, ceiling(cast(count(distinct(s.id)) as float)/4) as cnt
                                FROM exam_students es
                                JOIN students s ON s.id = es.student_id
                                JOIN exams e ON e.id = es.exam_id
                                JOIN exam_stations est ON est.id = es.exam_station_id
                                JOIN subjects sj ON sj.code = e.subject_code
                                WHERE es.deleted_at is NULL and s.deleted_at is NULL and e.deleted_at is NULL and est.deleted_at is NULL
                                and s.special_seating = false
                                and sj.fiscode in (29, 30, 31, 33)
                                group by es.exam_station_id, e.exam_type_id)
                                as everysubject ON everysubject.exam_station_id = est.id and everysubject.exam_type_id = e.exam_type_id
                LEFT JOIN (SELECT es.exam_station_id, e.exam_type_id, count(distinct(s.id)) as cnt
                                FROM exam_students es
                                JOIN students s ON s.id = es.student_id
                                JOIN exams e ON e.id = es.exam_id
                                JOIN exam_stations est ON est.id = es.exam_station_id
                                JOIN subjects sj ON sj.code = e.subject_code
                                WHERE es.deleted_at is NULL and s.deleted_at is NULL and e.deleted_at is NULL and est.deleted_at is NULL
                                and s.special_seating = true
                                and sj.fiscode in (29, 30, 31, 33)
                                group by es.exam_station_id, e.exam_type_id)
                                as everysubject_limit ON everysubject_limit.exam_station_id = est.id and everysubject_limit.exam_type_id = e.exam_type_id
                where sj.fiscode in (29, 30, 31, 33)
                group by e.date, st.code, e.exam_type_id, everysubject_limit.cnt) as needed_verbal_paks ON needed_verbal_paks.date = e.date and needed_verbal_paks.code = st.code and needed_verbal_paks.exam_type_id = e.exam_type_id
    LEFT JOIN (SELECT e.date, st.code, e.exam_type_id, sum(coalesce(everysubject.cnt, 0)) + coalesce(everysubject_limit.cnt, 0) as "needed_stations"
                FROM exam_stations est
    JOIN exams e ON e.id = est.exam_id
    JOIN stations st ON st.id = est.station_id
                JOIN subjects sj ON sj.code = e.subject_code
                LEFT JOIN (SELECT es.exam_station_id, e.exam_type_id, ceiling(cast(count(distinct(s.id)) as float)/3) as cnt
                                FROM exam_students es
                                JOIN students s ON s.id = es.student_id
                                JOIN exams e ON e.id = es.exam_id
                                JOIN exam_stations est ON est.id = es.exam_station_id
                                JOIN subjects sj ON sj.code = e.subject_code
                                WHERE es.deleted_at is NULL and s.deleted_at is NULL and e.deleted_at is NULL and est.deleted_at is NULL
                                and s.special_seating = false
                                and sj.fiscode in (29, 30, 31, 33)
                                group by es.exam_station_id, e.exam_type_id)
                                as everysubject ON everysubject.exam_station_id = est.id and everysubject.exam_type_id = e.exam_type_id
                LEFT JOIN (SELECT es.exam_station_id, e.exam_type_id, count(distinct(s.id)) as cnt
                                FROM exam_students es
                                JOIN students s ON s.id = es.student_id
                                JOIN exams e ON e.id = es.exam_id
                                JOIN exam_stations est ON est.id = es.exam_station_id
                                JOIN subjects sj ON sj.code = e.subject_code
                                WHERE es.deleted_at is NULL and s.deleted_at is NULL and e.deleted_at is NULL and est.deleted_at is NULL
                                and s.special_seating = true
                                and sj.fiscode in (29, 30, 31, 33)
                                group by es.exam_station_id, e.exam_type_id)
                                as everysubject_limit ON everysubject_limit.exam_station_id = est.id and everysubject_limit.exam_type_id = e.exam_type_id
                where sj.fiscode in (29, 30, 31, 33)
                group by e.date, st.code, e.exam_type_id, everysubject_limit.cnt) as needed_verbal_paks3 ON needed_verbal_paks3.date = e.date and needed_verbal_paks3.code = st.code and needed_verbal_paks3.exam_type_id = e.exam_type_id


where est.deleted_at is NULL and e.deleted_at is NULL and st.deleted_at is NULL and e.exam_type_id in (5, 22)


SQL



    ActiveRecord::Base.connection.execute(sql).each do |r|
      date = r['date']
      on_home = r['onHome']
      ppe_code = r['ppe_code']
      subject_code = r['subject_code'].to_i
      subject_name = r['subject_name']
      classrooms_normal_seats_sum = r['classrooms_normal_seats_sum'].to_i
      classrooms_normal_seats_min = r['classrooms_normal_seats_min'].to_i
      classrooms_normal_seats_max = r['classrooms_normal_seats_max'].to_i
      classrooms_normal_count = r['classrooms_normal_count'].to_i
      classrooms_limit_seats_sum = r['classrooms_limit_seats_sum'].to_i
      classrooms_limit_seats_min = r['classrooms_limit_seats_min'].to_i
      classrooms_limit_seats_max = r['classrooms_limit_seats_max'].to_i
      classrooms_limit_count = r['classrooms_limit_count'].to_i
      student_normal_count = r['student_normal_count'].to_i
      student_limit_count = r['student_limit_count'].to_i

      classrooms_podgotovka_seats_count = r['classrooms_podgotovka_seats_count'].to_i
      classrooms_provedenie_paks_count = r['classrooms_provedenie_paks_count'].to_i
      classrooms_provedenie_paks_needed = r['classrooms_provedenie_paks_needed'].to_i
      classrooms_provedenie_paks_needed3 = r['classrooms_provedenie_paks_needed3'].to_i
      classrooms_podgotovka_count = r['classrooms_podgotovka_count'].to_i
      classrooms_provedenie_count = r['classrooms_provedenie_count'].to_i

      sw_ruk = r['sw_ruk'].to_i
      sw_org_in = r['sw_org_in'].to_i
      sw_org_out = r['sw_org_out'].to_i
      sw_tech = r['sw_tech'].to_i
      sw_gek = r['sw_gek'].to_i
      sw_med = r['sw_med'].to_i
      sw_instruktor = r['sw_instruktor'].to_i
      sw_org_proved_operatorPK = r['sw_org_proved_operatorPK'].to_i
      sw_org_proved  = r['sw_org_proved'].to_i
      sw_org_podgotovka  = r['sw_org_podgotovka'].to_i
      printKIM  = r['printKIM']
      gia_type  = r['gia']
      scan  = r['Scan']
      oid  = r['oid'].to_i
      mid  = r['mid'].to_i
      sid  = r['esid'].to_i


      if student_normal_count + student_limit_count == 0 and !(['2017-06-19', '2017-06-20','2017-06-21', '2017-06-22', '2017-06-23', '2017-06-28', '2017-06-29', '2017-07-01'].include?(date)) then
        add_analytics_item :seating_ppe,
                           sid,
                           'ExamStation',
                           oid,
                           mid,
                           'ППЭ ' + ppe_code + ' следует снять с экзамена ' + date + ' ' + subject_name + '.'
      end

      if student_normal_count + student_limit_count > 0 then

        if subject_code > 28 and subject_code < 34 then #устная часть

          if classrooms_podgotovka_seats_count <  student_limit_count + student_normal_count then
            add_analytics_item :seating_verbal_podg,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' недостаточно мест в аудиториях подготовки на ' + date + '.'
          end

          if classrooms_provedenie_paks_count <  classrooms_provedenie_paks_needed then
            add_analytics_item :seating_verbal_proved,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' недостаточно станций в аудиториях проведения ' + date + ' устной части экзамена.'
          end

          if classrooms_provedenie_paks_count <  classrooms_provedenie_paks_needed3 and gia_type == 'gia9' then
            add_analytics_item :seating_verbal_proved,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' недостаточно станций в аудиториях проведения ' + date + ' устной части экзамена для обеспечения очереди не более "3".'
          end


          if classrooms_podgotovka_count*2 > sw_org_podgotovka then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' недостаточно организаторов в аудиториях подготовки на ' + date + '.'
          end

          if classrooms_provedenie_count > sw_org_proved then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' недостаточно организаторов в аудиториях проведения на ' + date + '.'
          end

          if classrooms_provedenie_count > sw_org_proved_operatorPK then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' недостаточно операторов ПК в аудиториях проведения на ' + date + '.'
          end

          # Достаточность организаторов

          if sw_ruk != 1 then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' должен быть назначен 1 руководитель ППЭ на все экзамены ' + date + ', а не ' + sw_ruk.to_s + '.'
          end

          if (sw_gek -1)*4 < (classrooms_provedenie_count) and gia_type == 'gia11' then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' не хватает членов ГЭК для реализации технологии Говорения по иностранным языкам на ' + date + '.'
          end

          if sw_gek < 1 and gia_type == 'gia9' then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' не хватает членов ГЭК для реализации технологии Говорения по иностранным языкам на ' + date + '.'
          end


          if sw_tech*4 < (classrooms_provedenie_count) and gia_type == 'gia11' then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' не хватает технических специалистов для реализации технологии говорения на ' + date + '.'
          end

          if sw_tech < 1 and gia_type == 'gia9' then
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' не хватает технических специалистов для реализации технологии говорения на ' + date + '.'
          end

          if sw_org_out < classrooms_provedenie_count
            add_analytics_item :seating_verbal_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' на ' + date + ' должно быть назначено не менее ' + classrooms_provedenie_count.to_s + ' организаторов вне аудитории.'
          end

        end














        if subject_code < 29 or subject_code > 33 then # не устная часть
          # Достаточность аудиторий

          if (classrooms_normal_seats_sum - student_normal_count) > classrooms_normal_seats_min then
            add_analytics_item :seating_auds,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' следует сократить количество аудиторий для участников без спецрассадки на экзамен ' + date + ' ' + subject_name + '.'
          end

          if classrooms_normal_seats_sum < student_normal_count then
            add_analytics_item :seating_auds,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' следует добавить количество аудиторий для участников без спецрассадки на экзамен ' + date + ' ' + subject_name + '.'
          end

          if (classrooms_limit_seats_sum - student_limit_count) > classrooms_limit_seats_min then
            add_analytics_item :seating_auds,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' следует сократить количество аудиторий для участников со спецрассадой на экзамен ' + date + ' ' + subject_name + '.'
          end

          if classrooms_limit_seats_sum < student_limit_count then
            add_analytics_item :seating_auds,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' следует добавить количество аудиторий для участников со спецрассадкой на экзамен ' + date + ' ' + subject_name + '.'
          end

          # Достаточность организаторов

          if sw_ruk != 1 then
            add_analytics_item :seating_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' должен быть назначен 1 руководитель ППЭ на все экзамены ' + date + ', а не ' + sw_ruk.to_s + '.'
          end

          if sw_med < 1 and on_home == 'x' then
            add_analytics_item :seating_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' должен быть назначен хотя бы 1 медицинский работник на все экзамены ' + date + '.'
          end

          if subject_code == 3 and gia_type == 'gia9' and sw_instruktor < 1 then
            add_analytics_item :seating_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' должен быть назначен ответственный за инструктаж по технике безопасности на экзамен ' + date + ' по физике.'
          end


          if scan == 'v' then # если реализуется технология сканирования в ППЭ
            if sw_gek < 2 then
              add_analytics_item :seating_org,
                                 sid,
                                 'ExamStation',
                                 oid,
                                 mid,
                                 'В ППЭ ' + ppe_code + ' должно быть назначено не менее 2 членов ГЭК для реализации технологии сканирования ' + date + '.'
            end

          end

          if printKIM == 'v' then # если реализуется технология печати КИМ в аудиториях
            if (sw_gek - 1)*5 < (classrooms_normal_count + classrooms_limit_count) then
              add_analytics_item :seating_org,
                                 sid,
                                 'ExamStation',
                                 oid,
                                 mid,
                                 'В ППЭ ' + ppe_code + ' не хватает членов ГЭК для реализации технологии печати КИМ в ППЭ на ' + date + '.'
            end

            if sw_tech*5 < (classrooms_normal_count + classrooms_limit_count) then
              add_analytics_item :seating_org,
                                 sid,
                                 'ExamStation',
                                 oid,
                                 mid,
                                 'В ППЭ ' + ppe_code + ' не хватает технических специалистов для реализации технологии печати КИМ в ППЭ на ' + date + '.'
            end

          end


          if sw_org_in < (classrooms_normal_count + classrooms_limit_count) * 2 then
            add_analytics_item :seating_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' на ' + date + ' на экзамен ' + subject_name + ' назначено ' + (classrooms_normal_count + classrooms_limit_count).to_s + ' аудиторий, не хватает ' + ((classrooms_normal_count + classrooms_limit_count) * 2 - sw_org_in).to_s + ' организаторов в аудитории.'
          end

          if sw_org_out < 2 and on_home == 'x' then
            add_analytics_item :seating_org,
                               sid,
                               'ExamStation',
                               oid,
                               mid,
                               'В ППЭ ' + ppe_code + ' на ' + date + ' должно быть назначено не менее 2 организаторов вне аудитории.'
          end


        end




      end


    end
  end

end