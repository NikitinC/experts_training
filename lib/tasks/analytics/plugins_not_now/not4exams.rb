module Not4exams
  def process
    puts ("students 9 doesn't have 4 exams")

    sql = <<SQL
      SELECT s.id as sid, o.id as oid, m.id as mid, 'Учащийся назначен не на 4 экзамена' as comment
        FROM students s
        JOIN attestation_form_students afs ON afs.student_id = s.id
        JOIN attestation_forms af ON af.code = afs.attestation_form_code
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        LEFT JOIN (SELECT s.id as sid, count(distinct(es.id)) as cnt
            FROM students s
            JOIN exam_students es ON es.student_id = s.id
            JOIN exams e ON e.id = es.exam_id
                  JOIN subjects sj ON sj.code = e.subject_code
            WHERE s.deleted_at is NULL and es.deleted_at is NULL
            AND sj.fiscode not in (29, 30, 31, 33)
            group by s.id
          ) exc ON exc.sid = s.id
        WHERE s.deleted_at is NULL and afs.deleted_at is NULL and o.deleted_at is NULL
          and s.ege_statement_scan is NULL
          and af.name in ('ОГЭ', 'ГВЭ-9')
          and s.ege_participant_category_code = 1
          and s.limited_facilities_group_code = 1
          and coalesce(exc.cnt, 0) <> 4
          and o.code not in ('999999', '111111') 
        group by o.id, s.id, m.id

SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_four_exams_9_noOVZ, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end
