module ClassIsNotUpdated
  def process

    puts ("class profile")

    sql = <<SQL
    SELECT g.id as gid, o.id as oid, o.mouo_id as mid, case when g.study_speciality_code is NULL then 'Не выбран профиль класса' else 'Выбранный профиль класса исключён из справочников; выберите иной профиль' end as comment
          FROM public.groups g
        JOIN ous o ON o.id = g.ou_id
            where o.deleted_at is NULL and g.deleted_at is NULL
            and RIGHT(o.code, 4) <> '0000'
            and o.code not in ('999999', '111111', '821308')
        and (g.study_speciality_code > 1000 or g.study_speciality_code is NULL)
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :class_is_not_updated, r['gid'], 'Group', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
    SELECT g.id as gid, o.id as oid, o.mouo_id as mid, case when g.study_form_code is NULL then 'Не заполнен профиль класса' end as comment
          FROM public.groups g
        JOIN ous o ON o.id = g.ou_id
            where o.deleted_at is NULL and g.deleted_at is NULL
            and RIGHT(o.code, 4) <> '0000'
            and o.code not in ('999999', '111111', '821308')
            and g.study_form_code is NULL
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :class_is_not_updated, r['gid'], 'Group', r['oid'], r['mid'], r['comment']
    end


  end
end