module Employee
  def process

#     sql = <<SQL
#     select m.id as mid, o.id as oid, e.id as eid, 'Предупреждение ГИА-9: сотрудник закреплен на экзамен '||' '||ex.date||' '||sj.name||' '||'в ППЭ'||' '||s.code||' '||'не в своей должности' as comment from employees e
#     join exam_station_employees ese on ese.employee_id=e.id
#     join exam_stations es on es.id=ese.exam_station_id
#     join stations s on s.id=es.station_id
#     join exams ex on ex.id=es.exam_id
#     join subjects sj on sj.code=ex.subject_code
#     join ous o on o.id=s.ou_id
#     join mouos m on m.id=o.mouo_id
#     join exam_types et ON et.id = ese.exam_type_id
#     where e.deleted_at is null and ese.deleted_at is null
#     and cast (e.station_post_code_oge as varchar(1))<> substring( cast (ese.station_post_code as varchar(1)) from 1 for 1)
#     and et.name like '%ОГЭ%'
#
#     union
#
#     select m.id as mid, o.id as oid, e.id as eid, 'Предупреждение ГИА-11: сотрудник закреплен на экзамен '||' '||ex.date||' '||sj.name||' '||'в ППЭ'||' '||s.code||' '||'не в своей должности' as comment from employees e
#     join exam_station_employees ese on ese.employee_id=e.id
#     join exam_stations es on es.id=ese.exam_station_id
#     join stations s on s.id=es.station_id
#     join exams ex on ex.id=es.exam_id
#     join subjects sj on sj.code=ex.subject_code
#     join ous o on o.id=s.ou_id
#     join mouos m on m.id=o.mouo_id
#     join exam_types et ON et.id = ese.exam_type_id
#     where e.deleted_at is null and ese.deleted_at is null
#     and cast (e.station_post_code as varchar(1))<> substring( cast (ese.station_post_code as varchar(1)) from 1 for 1)
#     and et.name like '%ЕГЭ%'
#
# SQL
#
#     puts ("employee ppe post not equal exam post")
#
#     ActiveRecord::Base.connection.execute(sql).each do |r|
#       add_analytics_item :employee_ppe_post_not_equals, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
#     end



    sql = <<SQL
        select m.id as mid, o.id as oid, e.id as eid, 'Предупреждение ГИА-11: сотрудник'||' '||e.first_name||' '||e.middle_name||' '||e.second_name||' '||'прикреплен к ППЭ ГИА11, '||' '||e.on_station||' '||' но не назначен ни на один экзамен' as comment from employees e
        join employee_ous eo on eo.employee_id=e.id
        left join ous o on o.id=eo.ou_id
        join mouos m on m.id=o.mouo_id
        where e.id not in (select ese.employee_id from exam_station_employees ese where ese.deleted_at is null and ese.exam_type_id in ('5','19'))
        and e.deleted_at is null and o.deleted_at is null and m.deleted_at is null and eo.deleted_at is null
        and e.on_station is not null 

    union

        select m.id as mid, o.id as oid, e.id as eid, 'Предупреждение ГИА-9: сотрудник'||' '||e.first_name||' '||e.middle_name||' '||e.second_name||' '||'прикреплен к ППЭ ГИА11, '||' '||e.on_station||' '||' но не назначен ни на один экзамен' as comment from employees e
        join employee_ous eo on eo.employee_id=e.id
        left join ous o on o.id=eo.ou_id
        join mouos m on m.id=o.mouo_id
        where e.id not in (select ese.employee_id from exam_station_employees ese where ese.deleted_at is null and ese.exam_type_id in ('22','23'))
        and e.deleted_at is null and o.deleted_at is null and m.deleted_at is null and eo.deleted_at is null
        and e.on_station_oge is not null 

SQL

    puts ("employee not working")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :employee_ppe_not_working, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
        select distinct m.id as mid, o.id as oid, e.id as eid, 'Нет специализации ФИЗИКА у сотрудника, назначенного на экзамен в качестве специалиста по инструктажу и лабораторным работам'||' '|| e.first_name||' '||e.middle_name||' '||e.second_name as comment 
		    from employees e 
        join employee_ous eo on eo.employee_id=e.id
        left join employee_ou_subjects es on es.employee_ou_id=eo.id and es.subject_code = 3
        join ous o on o.id=eo.ou_id
        join mouos m on m.id=o.mouo_id
        where e.deleted_at is null and eo.deleted_at is null and o.deleted_at is null and m.deleted_at is null
        and e.station_post_code_oge=9
        and es.subject_code is NULL
SQL

    puts ("no physics labs")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_physics_labs, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end


    sql = <<SQL
        select m.id as mid, o.id as oid,  'В ППЭ'||' '||org.scode||' '||'на экзамен'||' '||org.date||' '||org.sjn||','||'назначено'||' '||org.colorg||' '||'организатора в аудиторию вместо'||' '||2*org.colaud as comment from stations s
        join ous o on o.id=s.ou_id
        join mouos m on m.id=o.mouo_id
        left join(select s.id as sid, s.code as scode, e.date as date, e.subject_code, sj.name as sjn, count(distinct esc.id) as colaud, count(distinct ese.id)as colorg, 2*count(distinct esc.id)-count(distinct ese.id) as col from exam_stations es
        join stations s on s.id=es.station_id
        join exams e on e.id=es.exam_id
        join subjects sj on sj.code=e.subject_code
        join exam_station_classrooms esc on esc.exam_station_id=es.id
        join exam_station_employees ese on ese.exam_station_id=es.id
        where es.deleted_at is null
        and s.deleted_at is null 
        and e.deleted_at is null
        and s.code<>'4100'
        and e.exam_type_id in ('22','5','23') 
        and not e.subject_code in ('35','36','37','38')
        and ese.station_post_code=2 
        and esc.deleted_at is null and ese.deleted_at is null
        group by sid, s.code, e.date, e.subject_code, sj.name) org on org.sid=s.id
        where org.col>1
SQL

    puts ("no_orgs")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_orgs, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
        select m.id as mid, o.id as oid, 'На экзамен'||' '||e.date||' '||sj.name||' '||'Назначено недостаточно'||' '||'('||classrooms.classrooms_seats_sum||' '||')'||'мест для участников с ОВЗ' as comment  from exam_students es 
        join exam_stations est on est.id=es.exam_station_id
        join stations s on s.id=est.station_id
        join ous o on o.id=s.ou_id
        join mouos m on m.id=o.mouo_id
        join exams e on e.id=es.exam_id
        join subjects sj on sj.code=e.subject_code
        join students st on st.id=es.student_id
        LEFT JOIN (SELECT psc.exam_station_id as exam_station_id, count(distinct(c.id)) as classrooms_count, sum(c.available_seats) as classrooms_seats_sum, min(c.available_seats) as classrooms_min_seats, max(c.available_seats) as classrooms_max_seats
                  FROM exam_station_classrooms psc
                  JOIN classrooms c ON c.id = psc.classroom_id
                  WHERE psc.deleted_at is NULL and c.deleted_at is NULL and c.special_sit is true
                  group by psc.exam_station_id) as classrooms ON classrooms.exam_station_id = est.id
        where es.deleted_at is null and est.deleted_at is null and s.deleted_at is null 
        
        and not st.limited_facilities_group_code in ('1')
        
        group by m.id, o.id, e.date, sj.name,classrooms.classrooms_seats_sum
        having count(es.id)>classrooms.classrooms_seats_sum
SQL

    puts ("no_auds_ovz")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_auds_ovz, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    sql = <<SQL
    select m.id as mid, o.id as oid,  'В ППЭ'||' '||org.scode||' '||'на экзамен'||' '||org.date||' '||org.sjn||','||'назначено'||' '||org.colorg||' '||'руководителя ППЭ' as comment from stations s
    join ous o on o.id=s.ou_id
    join mouos m on m.id=o.mouo_id
    left join(select s.id as sid, s.code as scode, e.date as date, e.subject_code, sj.name as sjn,  count(distinct ese.employee_id)as colorg from exam_stations es
    join stations s on s.id=es.station_id
    join exams e on e.id=es.exam_id
    join subjects sj on sj.code=e.subject_code
    join exam_station_classrooms esc on esc.exam_station_id=es.id
    join exam_station_employees ese on ese.exam_station_id=es.id
    where es.deleted_at is null
    and s.deleted_at is null 
    and e.deleted_at is null
    and s.code<>'4100'
    and e.exam_type_id in ('22','5','23') 
    /*and not e.subject_code in ('35','36','37','38')*/
    and ese.station_post_code=1 
    and esc.deleted_at is null and ese.deleted_at is null
    group by sid, s.code, e.date, e.subject_code, sj.name) org on org.sid=s.id
    where org.colorg>1 

SQL

    puts ("some_ppe_heads")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :some_ppe_heads, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
      select e.id as eid, m.id as mid, o.id as oid,
        case
          when ese.employee_id is NOT NULL and e.station_post_code is NULL and s.code is NULL then 'Не заполнены должность в ППЭ и код ППЭ для сотрудника ГИА-11'
          when ese.employee_id is NOT NULL and e.station_post_code is NULL then 'У сотрудника ГИА-11 не указана должность в ППЭ'
          when ese.employee_id is NOT NULL and s.code is NULL then 'Сотрудник не закреплён за ППЭ ГИА-11, участвующем в 2020 году, хотя назначен на экзамены'
        end as comment
        from employees e
        join employee_ous eo ON eo.employee_id = e.id and eo.deleted_at is NULL
        join ous o ON o.id = eo.ou_id
        join mouos m ON m.id = o.mouo_id
        left join (select s.code
               from exam_types_stations ets
               join stations s ON s.id = ets.station_id and s.deleted_at is NULL
            join exam_types et ON et.id = ets.exam_type_id and et.name like '%ЕГЭ%' and et.deleted_at is NULL
               group by s.code
            ) s ON cast(s.code as int) = cast(e.on_station as int)
        left join 
        (select ese.employee_id
          from exam_station_employees ese
          join exam_stations es ON es.id = ese.exam_station_id and es.deleted_at is NULL
          join stations s ON s.id = es.station_id and s.deleted_at is NULL
          join exams ex ON ex.id = es.exam_id and ex.deleted_at is NULL
          join exam_types et2 ON et2.id = ex.exam_type_id and et2.name like '%ЕГЭ%' and et2.deleted_at is NULL
         where ese.deleted_at is NULL
         group by ese.employee_id
         )ese ON ese.employee_id = e.id
      where e.deleted_at is NULL
      and (
        (ese.employee_id is NOT NULL and s.code is NULL)
        or
        (ese.employee_id is NOT NULL and e.station_post_code is NULL)
        )
      group by e.id, m.id, o.id, ese.employee_id, s.code,  e.station_post_code

SQL

    puts ("not_ppe_workers")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :not_ppe_worker, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
    SELECT
      m4.id as mid,
      o3.id as oid,
      emp.id as eid,
      'Email совпадает с Email эксперта, отличного от данного работника ППЭ (сравниваются email, номер документа и фамилия)' as comment
      
      FROM experts e
      LEFT JOIN ous o1 ON o1.id = e.ou_id
      LEFT JOIN mouos m1 ON m1.id = o1.mouo_id
      JOIN users u ON u.id = e.user
      LEFT JOIN ous o2 ON o2.id = u.target_id and u.target_type = 'Ou'
      LEFT JOIN mouos m2 ON m2.id = o2.mouo_id
      LEFT JOIN mouos m3 ON m3.id = u.target_id and u.target_type = 'Mouo'
      LEFT JOIN experts e2 ON e2.id <> e.id and e2.email = e.email
      LEFT JOIN employees emp ON emp.email = e.email and emp.document_number <> e.document_number and e.surname <> emp.second_name and emp.deleted_at is NULL
      left join employee_ous eo ON eo.employee_id = emp.id and eo.deleted_at is NULL
      left join ous o3 ON o3.id = eo.ou_id
      left join mouos m4 ON m4.id = o3.mouo_id
    where length(e.email) > 3
    and (e2.id is NOT NULL or emp.id is NOT NULL)
    and emp.id is NOT NULL

SQL

    puts ("email like expert")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :worker_not_updated, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end




  end
end