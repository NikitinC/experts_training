module IncorrectClass
  def process
    Group.find_each do |g|
      next if [9, 10, 11, 12].include? g.number
      next if g.ou.code.end_with? '0000'
      add_analytics_item :incorrect_class,
                         g.id,
                         'Group',
                         g.ou.id,
                         g.ou.mouo_id
    end
  end
end