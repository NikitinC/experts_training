module Kontrol
  def process

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, count(distinct(e.subject_code)) as cnt, 'Участник назначен на несколько контрольных работ' as comment
      FROM students s
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
      JOIN exams e ON e.id = es.exam_id
      JOIN attestation_form_students afs ON afs.student_id = s.id and afs.deleted_at is NULL
      JOIN attestation_forms af ON af.code = afs.attestation_form_code and af.name in ('ОГЭ', 'ГВЭ-9')
      JOIN exam_types et ON et.id = e.exam_type_id
    WHERE s.deleted_at is NULL and et.name like '%Контрольные%'
    GROUP BY s.id, m.id, o.id
    having count(distinct(e.subject_code)) > 1
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :kr9, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end


    sql = <<SQL
      SELECT s.id as sid, o.id as oid, m.id as mid, 'Не назначен на контрольную работу' as comment
        FROM students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN attestation_form_students afs ON afs.student_id = s.id and afs.deleted_at is NULL
        JOIN attestation_forms af ON af.code = afs.attestation_form_code and af.name in ('ОГЭ', 'ГВЭ-9')
        LEFT JOIN (
            SELECT student_id
              FROM exam_students es
              JOIN exams e ON e.id = es.exam_id
              JOIN exam_types et ON et.id = e.exam_type_id and et.name like '%Контрольные%'
            WHERE es.deleted_at is NULL 
            GROUP BY student_id
          ) es ON es.student_id = s.id 
      
      WHERE s.deleted_at is NULL and es.student_id is NULL and limited_facilities_group_code = 1
      GROUP BY s.id, m.id, o.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :kr9, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end