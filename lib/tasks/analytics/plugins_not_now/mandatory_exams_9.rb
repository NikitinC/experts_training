module MandatoryExams9
  def process
    sql = <<SQL
select
	s.id as sid, o.id as oid, o.mouo_id as mid

from students s
join groups g on g.id = s.group_id and g.deleted_at is null
join ous o on o.id = g.ou_id

where
  s.deleted_at is null and g.deleted_at is null and o.deleted_at is null
  and (
	g.number in (9, 10) or exists (
		select 1 from attestation_form_students afs where afs.student_id = s.id and afs.deleted_at is null and afs.attestation_form_code in (3, 9)
	)
)

and (
	(
		not exists (
			select 1 from exam_students es
			join exams e on e.id = es.exam_id and es.deleted_at is null
			where es.deleted_at is null and es.student_id = s.id and e.deleted_at is null
			and e.exam_type_id in (22, 23, 25, 26) and e.subject_code in (1, 111, 112, 113, 114)
			) and (s.gia_active_results_rus = false or s.ege_participant_category_code = 1)
	)
	or (
		not exists (
			select 1 from exam_students es
			join exams e on e.id = es.exam_id and es.deleted_at is null
			where es.deleted_at is null and es.student_id = s.id and e.deleted_at is null and
			e.exam_type_id in (22, 23, 25, 26) and e.subject_code in (33, 52, 251)
		) and (s.gia_active_results_math = false or s.ege_participant_category_code = 1)
	)
)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_mandatory_exams_9, r['sid'], 'Student', r['oid'], r['mid']
    end


    # sql = <<SQL
# select
# 	s.id as sid, o.id as oid, o.mouo_id as mid
#
# from students s
# join groups g on g.id = s.group_id and g.deleted_at is null
# join ous o on o.id = g.ou_id
#
# join exam_students es on es.deleted_at is null and es.student_id = s.id
# join exams e on e.id = es.exam_id and es.deleted_at is null and e.deleted_at is null and e.exam_type_id in (22, 23, 25, 26)
#
# where
#   s.deleted_at is null and g.deleted_at is null and o.deleted_at is null
#   and (
# 	g.number in (9, 10) or exists (
# 		select 1 from attestation_form_students afs where afs.student_id = s.id and afs.deleted_at is null and afs.attestation_form_code in (3, 9)
# 	)
# )
#
# and not exists (
# 		select 1 from attestation_form_students afs where afs.student_id = s.id and afs.deleted_at is null and afs.attestation_form_code in (9)
# 	)
#
# and e.subject_code not in (35, 36, 37, 38)
# and s.limited_facilities_group_code = 1 -- Только не ОВЗ
# and s.ege_participant_category_code = 1 -- Только ВТГ
# and o.code not in ('111111', '999999')
#
# group by s.id, o.id, o.mouo_id
#
# having count(distinct e.subject_code) != 4
# SQL
#
#     ActiveRecord::Base.connection.execute(sql).each do |r|
#       add_analytics_item :no_four_exams_9_noOVZ, r['sid'], 'Student', r['oid'], r['mid']
#     end


  end
end