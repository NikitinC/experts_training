module VerbalExams9
  def process
    sql = <<SQL
SELECT s.id as sid, o.id as oid, m.id as mid, eng.engdate, engu.engudate, deu.deudate, deuu.deuudate, fra.fradate, frau.fraudate, isp.ispdate, ispu.ispudate
	FROM students s
    JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
    JOIN (SELECT DISTINCT (s.id) as sid
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams e ON e.id = es.exam_id and e.subject_code in (9, 10, 11, 13, 35, 36, 37, 38)
          where s.deleted_at is NULL
         ) ss ON ss.sid = s.id
    LEFT JOIN (SELECT DISTINCT (s.id) as sid, eng.date as engdate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams eng ON eng.id = es.exam_id and eng.subject_code = 9
          where s.deleted_at is NULL
         ) eng ON eng.sid = ss.sid
    LEFT JOIN (SELECT DISTINCT (s.id) as sid, engu.date as engudate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams engu ON engu.id = es.exam_id and engu.subject_code = 35
          where s.deleted_at is NULL
         ) engu ON engu.sid = ss.sid
    LEFT JOIN (SELECT DISTINCT (s.id) as sid, deu.date as deudate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams deu ON deu.id = es.exam_id and deu.subject_code = 10
          where s.deleted_at is NULL
         ) deu ON deu.sid = ss.sid         
    LEFT JOIN (SELECT DISTINCT (s.id) as sid, deuu.date as deuudate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams deuu ON deuu.id = es.exam_id and deuu.subject_code = 36
          where s.deleted_at is NULL
         ) deuu ON deuu.sid = ss.sid         
     LEFT JOIN (SELECT DISTINCT (s.id) as sid, fra.date as fradate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams fra ON fra.id = es.exam_id and fra.subject_code = 11
          where s.deleted_at is NULL
         ) fra ON fra.sid = ss.sid
     LEFT JOIN (SELECT DISTINCT (s.id) as sid, frau.date as fraudate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams frau ON frau.id = es.exam_id and frau.subject_code = 38
          where s.deleted_at is NULL
         ) frau ON frau.sid = ss.sid
     LEFT JOIN (SELECT DISTINCT (s.id) as sid, isp.date as ispdate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams isp ON isp.id = es.exam_id and isp.subject_code = 13
          where s.deleted_at is NULL
         ) isp ON isp.sid = ss.sid         
     LEFT JOIN (SELECT DISTINCT (s.id) as sid, ispu.date as ispudate
          FROM students s
          JOIN groups g ON g.id = s.group_id and g.number in (9, 10)
          JOIN exam_students es ON es.student_id = s.id and es.deleted_at is NULL
          JOIN exams ispu ON ispu.id = es.exam_id and ispu.subject_code = 37
          where s.deleted_at is NULL
         ) ispu ON ispu.sid = ss.sid         
    JOIN ous o ON o.id = g.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    
	WHERE	((coalesce(eng.engdate, '2017.01.01') != coalesce(engu.engudate, '2017.01.01')) OR
    		(coalesce(deu.deudate, '2017.01.01') != coalesce(deuu.deuudate, '2017.01.01')) OR
            (coalesce(fra.fradate, '2017.01.01') != coalesce(frau.fraudate, '2017.01.01')) OR
            (coalesce(isp.ispdate, '2017.01.01') != coalesce(ispu.ispudate, '2017.01.01')))
	  	and s.deleted_at is NULL

SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :verbal_exams_equal_dates_9, r['sid'], 'Student', r['oid'], r['mid']
    end

  end
end