module Marks
  def process
    puts ("marks its and previous year for student exams")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid
      FROM public.exam_students es
        JOIN students s ON s.id = es.student_id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        where ((mark5p is NULL) or (mark5 is NULL) = true)
        and es.deleted_at is NULL and s.deleted_at is NULL
        and s.gia_admission = true
        and s.ege_participant_category_code not in (3, 4)
        and s.deleted_at is NULL
        and substring (o.code, 3, 4) <> '0000'
        and o.code not in ('999999', '111111')
        group by s.id, o.id, m.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :marks5_year, r['sid'], 'Student', r['oid'], r['mid'], 'Не заполнены поля оценок по выбранным экзаменам за выпускной и(или) предвыпускной класс'
    end

    puts ("marks for attestat")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid
      FROM public.exam_students es
        JOIN students s ON s.id = es.student_id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        where (mark5_attestat is NULL = true)
        and es.deleted_at is NULL and s.deleted_at is NULL
        and s.gia_admission = true
        and s.ege_participant_category_code not in (3, 4)
        and s.deleted_at is NULL
        and substring (o.code, 3, 4) <> '0000'
        and o.code not in ('999999', '111111')
        group by s.id, o.id, m.id
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :marks5_attestat, r['sid'], 'Student', r['oid'], r['mid'], 'Не заполнены поля оценок по выбранным экзаменам в аттестате'
    end

#     puts ("professor for exam subjects")
#
#     sql = <<SQL
#     SELECT s.id as sid, o.id as oid, m.id as mid
#       FROM public.exam_students es
#         JOIN students s ON s.id = es.student_id
#         JOIN groups g ON g.id = s.group_id
#         JOIN ous o ON o.id = g.ou_id
#         JOIN mouos m ON m.id = o.mouo_id
#         where professor is NULL
#         and es.deleted_at is NULL and s.deleted_at is NULL
#         and s.gia_admission = true
#         and s.ege_participant_category_code not in (3, 4)
#         and s.deleted_at is NULL
#         and substring (o.code, 3, 4) <> '0000'
#         and o.code not in ('999999', '111111')
#         or length(professor) < 3
#         group by s.id, o.id, m.id
# SQL
#
#
#     ActiveRecord::Base.connection.execute(sql).each do |r|
#       add_analytics_item :marks5_professor, r['sid'], 'Student', r['oid'], r['mid'], 'Не заполнены педагоги, преподававшие предметы'
#     end

  end
end