module BadHasComposition
  def process

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Признак наличия действующего зачёта по ИС(И)/ИС указана для участника категории, отличной от выпускника текущего года.' as comment
    FROM students s
    JOIN groups g ON g.id = s.group_id
    JOIN ous o ON o.id = g.ou_id
    JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and coalesce(has_composition, false) = true and s.ege_participant_category_code <> 1
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :bad_has_compositions, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Отсутствуют основания для ИС(И) в форме Изложения' as comment
	  FROM students s
	  JOIN groups g ON g.id = s.group_id
	  JOIN ous o ON o.id = g.ou_id
	  JOIN mouos m ON m.id = o.mouo_id
	  JOIN composition_students cs ON cs.student_id = s.id
	  JOIN compositions c ON c.id = cs.composition_id
    WHERE s.deleted_at is NULL and c.fct_code in (179, 181, 183) and coalesce(s.closed_institution, False) = False and coalesce(s.limited_facilities_group_code, 1) < 2 and cs.deleted_at is NULL
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :bad_izlozhenie, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end