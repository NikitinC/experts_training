module Names
  def process
    puts ("dictionary names")

    sql = <<SQL
      SELECT s.id as sid, o.id as oid, m.id as mid, 
      CASE
        WHEN s.gender <> n.gender THEN
            CASE
                WHEN s.gender = true THEN 'Мужской пол участника не совпадает со словарём (женский)'
                WHEN s.gender = false THEN 'Женский пол участника не совпадает со словарём (мужской)'
            END
        WHEN n.id is NULL THEN 'Имя ' || s.first_name || ' отсутствует в словаре'
      END as comment
      FROM students s
      LEFT JOIN names n ON n.name = s.first_name
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      where s.deleted_at is NULL AND (s.gender <> n.gender OR n.id is NULL) -- and o.id = 1149
      AND s.id NOT IN (SELECT s.id FROM students s JOIN names n ON n.name = s.first_name and s.gender = n.gender)
      group by s.id, o.id, m.id, s.gender, n.gender, n.id, s.first_name
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :names, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("second_name and gender")

    sql = <<SQL
      SELECT s.id as sid, o.id as oid, m.id as mid, 
        case s.gender 
          when false then 'Женский пол участника не соответствует отчеству ' || s.middle_name || '.'
          when true then 'Мужской пол участника не соответствует отчеству ' || s.middle_name || '.'
        end as comment
        FROM students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      where s.deleted_at is NULL	
          and ((middle_name like '%ич' and gender <> true)
          or (middle_name like '%на' and gender <> false)
          or (middle_name like '%на' and gender <> false)
          or (middle_name like '%кызы' and gender <> false)
          or (middle_name like '%Кызы' and gender <> false)
          or (middle_name like '%гызы' and gender <> false)
          or (middle_name like '%Гызы' and gender <> false)
          or (middle_name like '%кизи' and gender <> false)
          or (middle_name like '%Кизи' and gender <> false)
          or (middle_name like '%оглы' and gender <> true)
          or (middle_name like '%Оглы' and gender <> True)
          or (middle_name like '%угли' and gender <> true)
          or (middle_name like '%Угли' and gender <> True)
          or (middle_name like '%Углы' and gender <> True)
          or (middle_name like '%уулу' and gender <> True)
          )
        and (middle_name not like '%бек')
        and (middle_name <> 'Джамшед')
        and (middle_name <> 'Дык Хинь')
        and (middle_name <> 'Леонид')
          
          
      union
      
      SELECT s.id as sid, o.id as oid, m.id as mid, 'Проверьте отчество участника.' as comment
        FROM students s
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      where	s.deleted_at is NULL
          and middle_name not like '%ич' 
          and middle_name <> '' 
          and middle_name <> 'Бао' 
          and middle_name <> 'Фэрэл' 
          and middle_name <> 'Сайумар' 
          and middle_name <> 'Пламенов' 
          and middle_name <> 'Данных' 
          and middle_name <> 'Витауто' 
          and middle_name <> 'Муссо' 
          and middle_name <> 'Орестис' 
          and middle_name <> 'Насиф-кзы' 
          and middle_name <> 'Алишерзод' 
          and middle_name <> 'Махмадрасул' 
          and middle_name <> 'Джамшед' 
          and middle_name <> 'Марлен' 
          and middle_name <> 'Вардгеси' 
          and middle_name <> 'Шахмарлы' 
          and middle_name <> 'Адель' 
          and middle_name <> 'Абдушаффофзода' 
          and middle_name <> 'Занг' 
          and middle_name <> 'Махмуд' 
          and middle_name <> 'Нуржигит' 
          and middle_name <> 'Мирбаба' 
          and middle_name <> 'Суробшо' 
          and middle_name <> 'Иг' 
          and middle_name <> 'Махмадсодир' 
          and middle_name <> 'Мохамад' 
          and middle_name <> 'Тохоржон' 
          and middle_name <> 'Камандаркзы' 
          and middle_name <> 'Иброим' 
          and middle_name <> 'Субхиддин' 
          and middle_name <> 'Малик' 
          and middle_name <> 'Анварзода' 
          and middle_name <> 'Назим'
          and middle_name <> 'Ле Хюи'
          and middle_name <> 'Дык Хинь'
          and middle_name <> 'Леонид'
          and middle_name <> 'Саидбег'
          and middle_name <> 'Халил Ахмад'
          and middle_name <> 'Костас'
          and middle_name <> 'Славова'
          and middle_name <> 'Лонг'
          and middle_name <> 'Носир'
          and middle_name <> 'Джеймс'
          and middle_name <> 'Атанасов'
          and middle_name <> 'Нуржанулы'
          and middle_name <> 'Бархо'
          and middle_name <> 'Тхань'
          and middle_name <> 'Абосидин'
          and middle_name <> 'Яхшимурод углы'
          and middle_name not like '%бек' 
          and middle_name not like '%на'
          and middle_name not like '%кызы'
          and middle_name not like '%Кызы'
          and middle_name not like '%гызы'
          and middle_name not like '%Гызы'
          and middle_name not like '%кизи'
          and middle_name not like '%Кизи'
          and middle_name not like '%оглы'
          and middle_name not like '%Оглы'
          and middle_name not like '%угли'
          and middle_name not like '%Угли'
          and middle_name not like '%Углы'
          and middle_name not like '%Уулу'
          and middle_name not like '%уулу'
        
          and length(middle_name) > 0
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :names, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("dictionary names")

    sql = <<SQL
    select
      s.id as sid,
      o.id as oid,
      m.id as mid,
      'Проверьте пол участника: ' || 	s.second_name || ' ' || s.first_name || coalesce(' ' || s.middle_name, '') as comment,
      s.second_name || ' ' || s.first_name || coalesce(' ' || s.middle_name, '') as fio,
      case when sn.gender = true then 1 when sn.gender = false then -1 else 0 end as surname,
      case when n.gender = true then 1 when n.gender = false then -1 else 0 end as name,
          (case
              when s.middle_name like '%вна' then -1
              when s.middle_name like '%вич' then 1
              when s.middle_name like '%кызы' then -1
              when s.middle_name like '%Кызы' then -1
              when s.middle_name like '%кизи' then -1
              when s.middle_name like '%Кизи' then -1
              when s.middle_name like '%кзы' then -1
              when s.middle_name like '%Кзы' then -1
              when s.middle_name like '%бек' then 1
            when s.middle_name like '%глы' then 1
            when s.middle_name like '%гли' then 1
            else 0
            end) as middle_name,
      case
        case when n.gender = true then 1 when n.gender = false then -1 else 0 end +
        case when sn.gender = true then 1 when sn.gender = false then -1 else 0 end +
        (case
              when s.middle_name like '%вна' then -1
              when s.middle_name like '%вич' then 1
              when s.middle_name like '%кызы' then -1
              when s.middle_name like '%Кызы' then -1
              when s.middle_name like '%кизи' then -1
              when s.middle_name like '%Кизи' then -1
              when s.middle_name like '%кзы' then -1
              when s.middle_name like '%Кзы' then -1
              when s.middle_name like '%бек' then 1
            when s.middle_name like '%глы' then 1
            when s.middle_name like '%гли' then 1
            else 0
            end)
          when 2 then 'М'
          when 3 then 'М'
          when 1 then 'М'
          when -2 then 'Ж'
          when -3 then 'Ж'
          when -1 then 'Ж'
        else 'непонятно'
      end as "Пол",
      case s.gender when true then 'М' else 'Ж' end as "Установлен пол"
    from students s
    join groups g ON g.id = s.group_id
    join ous o ON o.id = g.ou_id
    join mouos m ON m.id = o.mouo_id
    left join names n ON n.name = s.first_name and n.gender = s.gender
    left join surnames sn ON sn.surname = s.second_name
    where (case
        case when n.gender = true then 1 when n.gender = false then -1 else 0 end +
        case when sn.gender = true then 1 when sn.gender = false then -1 else 0 end +
        (case
              when s.middle_name like '%вна' then -1
              when s.middle_name like '%вич' then 1
              when s.middle_name like '%кызы' then -1
              when s.middle_name like '%Кызы' then -1
              when s.middle_name like '%кизи' then -1
              when s.middle_name like '%Кизи' then -1
              when s.middle_name like '%кзы' then -1
              when s.middle_name like '%Кзы' then -1
              when s.middle_name like '%бек' then 1
            when s.middle_name like '%глы' then 1
            when s.middle_name like '%гли' then 1
            else 0
            end)
          when 2 then 'М'
          when 3 then 'М'
          when 1 then 'М'
          when -2 then 'Ж'
          when -3 then 'Ж'
          when -1 then 'Ж'
        else 'непонятно'
      end) <> (case s.gender when true then 'М' else 'Ж' end) and s.deleted_at is NULL
      and s.id not in (953838)
      and s.id not in (1012924)
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :names, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end


  end
end