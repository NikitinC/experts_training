module BadStations
  def process

    puts ("bad stations")

    sql = <<SQL
      SELECT s.id as stid, o.id as oid, m.id as mid, 'В ППЭ нет аудиторий' as comment
      FROM stations s
      JOIN ous o ON o.id = s.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    WHERE s.id not in (SELECT station_id from classrooms)
    and s.deleted_at is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_is_not_updated, r['stid'], 'Station', r['oid'], r['mid'], r['comment']
    end

    puts ("bad stations")

    sql = <<SQL
      SELECT s.id as stid, o.id as oid, m.id as mid, 'В ППЭ нет экзаменов' as comment
        FROM stations s
        JOIN ous o ON o.id = s.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      WHERE s.id not in (SELECT station_id from exam_stations)
      and s.deleted_at is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_is_not_updated, r['stid'], 'Station', r['oid'], r['mid'], r['comment']
    end

    puts ("bad stations")

    sql = <<SQL
      SELECT s.id as stid, o.id as oid, m.id as mid, 'В ППЭ нет участников' as comment
        FROM stations s
        JOIN ous o ON o.id = s.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      WHERE s.id not in (SELECT est.station_id 
                  from exam_stations est
                 join exam_students es ON es.exam_station_id = est.id
                )
      and s.deleted_at is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_is_not_updated, r['stid'], 'Station', r['oid'], r['mid'], r['comment']
    end

    puts ("bad stations")

    sql = <<SQL
      SELECT s.id as stid, o.id as oid, m.id as mid, 'Для ППЭ утверждены печать ЭМ и сканирование в аудиториях.' as comment
        FROM stations s
        JOIN ous o ON o.id = s.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN exam_types_stations ets ON ets.station_id = s.id
        JOIN exam_types et ON et.id = ets.exam_type_id
        LEFT JOIN station_additional_features saf ON saf.code = s.station_additional_feature_code and saf.name like '%канирован%'
        LEFT JOIN station_additional_features prn ON prn.code = s.station_additional_feature_code and prn.name like '%ечать%'
      WHERE s.code in ('0103',  '0203',  '0205',  '0303',  '0401',  '0501',  '0601',  '0701',  '0702',  '0801',  '0901',  '1001',  '1101',  '1201',  '1301',  '1401',  '1501',  '1601',  '1602',  '1603',  '1703',  '1801',  '1802',  '1903',  '1904',  '2001',  '2101',  '2202',  '2301',  '2302',  '2303',  '2401',  '2502',  '2601',  '2701',  '2703',  '2802',  '2901',  '3003',  '3101',  '3201',  '3301',  '3401',  '3402',  '3501',  '3602',  '3603',  '3609',  '3705',  '3801',  '3902',  '4001',  '4205',  '4301',  '4302',  '4303',  '4405',  '4501',  '4502',  '4503',  '4504',  '4601',  '4701',  '4801',  '4901',  '5002',  '5102',  '5201',  '5301',  '5303',  '5412',  '5419',  '5501',  '5601',  '5602',  '5603',  '5609',  '5610',  '5611',  '5614',  '5617',  '5629',  '5702',  '5703',  '5705',  '5706',  '5815',  '5816',  '5830',  '5831',  '5902',  '5903',  '6002',  '6003',  '6107',  '6109',  '6110',  '6112',  '6201',  '6303',  '6401',  '6402',  '6501',  '6601',  '6701',  '6802',  '6901',  '7001',  '7101',  '7201',  '7301',  '8101',  '8104',  '8105',  '8106',  '8107',  '8201',  '8204',  '8205',  '8220',  '8221',  '8222',  '8223',  '8301',  '8302',  '8306',  '8315',  '8401',  '8409',  '8412',  '8416',  '8501',  '8502',  '8503',  '8505',  '8507',  '8509',  '8516',  '8521',  '8528',  '8605',  '8607',  '8608',  '8609',  '8610',  '8611',  '8612',  '8620',  '8622',  '8629',  '8701',  '8703',  '8706',  '8708',  '8712')
      and et.name like '%ЕГЭ%'
      and s.deleted_at is NULL
       and (saf.id is NULL or prn.id is NULL)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_is_not_updated, r['stid'], 'Station', r['oid'], r['mid'], r['comment']
    end


  end
end