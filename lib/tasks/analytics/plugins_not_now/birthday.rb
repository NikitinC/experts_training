require 'date'

module Birthday
  def process
    puts ("birthday analytics")

    sql = <<SQL
    SELECT s.id as sid, o.id as oid, m.id as mid, 'Год рождения участника не типичен для указанного класса.' as comment
      FROM students s
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
      JOIN ou_kinds k ON k.code = o.ou_kind_code
      JOIN study_forms sf ON sf.code = s.study_form_code
    where
        s.deleted_at is NULL
        -- and s.id <> 945183 -- это Высотин Эмин Рафшанович
        -- and s.id <> 975399 -- это Вахонин Юрий Юрьевич
        -- and s.id <> 982933 -- это Георгий Блинов
        -- and s.id not in (976872, 976875, 976868, 979958, 976883, 976877) -- это 6 человек из 510102
        and EXTRACT(YEAR FROM s.birthday) > 1970
        and EXTRACT(YEAR FROM s.birthday) < 2008
        and s.limited_facilities_group_code = 1
        and s.ege_participant_category_code = 1
        and s.closed_institution = false
        and g.study_form_code <> 4   -- не вечерний класс
        and k.ou_type_code <> '10'  -- не вечерняя школа
        and sf.name in ('Очная')
        and
        (
        (EXTRACT(YEAR FROM s.birthday) not in (2001, 2002, 2003, 2004, 2005, 2006, 2007) and g.number in (9)
            or 
          (EXTRACT(YEAR FROM s.birthday) not in (2000, 2001, 2002, 2003, 2004, 2005) and g.number in (11))
        )
      )
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :birthday, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end