module EmployeeSubjectEqualsExam
  def process

    sql = <<SQL
select distinct em.id as eid, o.id as oid, o.mouo_id as mid, concat('В ППЭ ', s.code, ' ', e1.date, ' проводится экзамен "', sj1.name, '"') as comment
from exam_station_employees ese
join exam_stations es  on es.id = ese.exam_station_id
join employees em on em.id = ese.employee_id and em.deleted_at is null
join exams e on e.id = es.exam_id
join stations s on s.id = es.station_id
join ous o on o.id = s.ou_id and o.deleted_at is null
join employee_ous eo on eo.deleted_at is null and eo.employee_id = em.id
join employee_ou_subjects eosj on eosj.employee_ou_id = eo.id and eosj.deleted_at is null

join exam_stations es1 on es1.deleted_at is null and es1.station_id = es.station_id
join exams e1 on e1.id = es1.exam_id and e1.deleted_at is null and e1.date = e.date and e1.subject_code = eosj.subject_code
join subjects sj1 on sj1.code = e1.subject_code
join exam_types et ON et.id = ese.exam_type_id
join station_posts sp ON sp.code = ese.station_post_code
where ese.deleted_at is null and es.deleted_at is null

and sp.name like '%ганизатор%' and et.name like '%ЕГЭ%'
SQL

    puts ("employee subjects equals exam")

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :employee_subject_equals_exam, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end



  end
end