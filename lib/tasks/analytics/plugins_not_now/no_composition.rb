module NoComposition
  def process
    puts ("no composition")

    sql = <<SQL
      SELECT s.id as sid, m.id as mid, o.id as oid, 
        'Не имеет зачёта по ИС(И)/ИС, пояснения отсутствуют' as comment
          FROM students s
          JOIN groups g ON g.id = s.group_id
          JOIN ous o ON o.id = g.ou_id
          JOIN mouos m ON m.id = o.mouo_id
        JOIN ege_participant_categories pc ON pc.code = s.ege_participant_category_code
        LEFT JOIN (
        SELECT student_id
          FROM composition_students cs
          JOIN compositions c ON c.id = cs.composition_id
          where c.date + INTERVAL '14 day' > now() and cs.deleted_at is NULL
        ) nazn ON nazn.student_id = s.id
          WHERE s.deleted_at is NULL and pc.name like '%текущего%'
        and coalesce(s.has_composition, false) <> True
          and upper(replace(replace(cast(s.guid as varchar(50)), '}', ''), '{', '')) not in (select cast(guid as varchar(50)) from ht)
        -- and g.number in (11, 12)
        and nazn.student_id is NULL
        and length(comment) < 5
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_composition, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end

# переливаю списки тех, у кого есть зачёты

# create table #tmp
# (guid uniqueidentifier)
#
# use [erbd_ege_reg_21_66]
#
# INSERT INTO #tmp
# SELECT ht.ParticipantFK
# FROM [res_HumanTests] ht
# JOIN res_Marks m ON m.HumanTestID = ht.HumanTestID
# WHERE ht.SubjectCode in (20, 21) and m.Mark5 > 2
# GROUP BY ht.ParticipantFK
#
# use [erbd_gia_reg_21_66]
#
# INSERT INTO #tmp
# SELECT ht.ParticipantFK
# FROM [res_HumanTests] ht
# JOIN res_Marks m ON m.HumanTestID = ht.HumanTestID
# WHERE ht.SubjectCode in (20, 21) and m.Mark5 > 2
# GROUP BY ht.ParticipantFK
#
# SELECT 'insert into ht values (' + cast(row_number() over (order by guid) as varchar(10)) + ', ''' + cast(guid as varchar(42)) + ''');'
# from #tmp
#
# drop table #tmp


# Создаю назначения тем, кто получил незачёты


# use [erbd_gia_reg_21_66]
#
# -- min = 214958
#
# SELECT 'insert into composition_students select mm.i-1, cs.student_id, 31, cs.ou_id, NULL, NULL, NULL, ''2021-02-23 22:56'', ''2021-02-23 22:56'', NULL, NULL, NULL
# 			from composition_students cs
# 			join students s ON s.id = cs.student_id
# 			join compositions c ON c.id = cs.composition_id
# 			join (select min(id) as i from composition_students) mm ON 1 = 1
# 			where cs.deleted_at is NULL and s.deleted_at is NULL and c.deleted_at is NULL
# 			and upper(replace(replace(cast(s.guid as varchar(50)), ''}'', ''''), ''{'', '''')) = ''' + cast(ht.ParticipantFK as varchar(42)) + '''
# 			and c.fct_code = 1;'
#   FROM [res_HumanTests] ht
#   JOIN res_Marks m ON m.HumanTestID = ht.HumanTestID
# WHERE ht.SubjectCode in (20, 21) and m.Mark5 < 3
# GROUP BY ht.ParticipantFK
#
# -- в запросе 1 - код ФЦТ "как в первый раз"
# -- 31 - id нового назначения
#

# убираю двойные назначения

# update composition_students
# set deleted_at = 'Уже были назначены'
# where id in (
# select cs.id
# from composition_students cs
# join (select student_id, count(id) as cnt
# from composition_students
# where composition_id = 31 and deleted_at is NULL
# group by student_id) c ON c.student_id = cs.student_id and c.cnt > 1
# where cs.composition_id = 31 and cs.id < 214958
# )