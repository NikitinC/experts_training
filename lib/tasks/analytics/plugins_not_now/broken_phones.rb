module BrokenPhones
  def process
    puts ("all phones in one")


    sql = <<SQL
    select	m.id as mid, o.id as oid, e.id as eid, 
        'Не распознан телефон сотрудника ' || e.phone || '.' as comment
    from employees e
          join employee_ous eo on eo.employee_id=e.id
          join ous o on o.id=eo.ou_id
          join mouos m on m.id=o.mouo_id
    where e.deleted_at is NULL
      and 
        ((cast(substring(e.phone, 2, 3) as int) < 900 and cast(substring(e.phone, 2, 3) as int) <> 343)
        or
        (cast(substring(e.phone, 2, 3) as int) > 899 and 
          cast(substring(e.phone, 2, 3) as int) in (907, 935, 940, 942, 943, 944, 945, 946, 947, 948, 949, 957, 959, 972, 973, 974, 975, 976, 979, 990, 998)
         ))
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :phones, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end


    sql = <<SQL
    select m.id as mid, o.id as oid,
		'Не распознан телефон руководителя ОО ' || cast(o.chief_phone as varchar(22)) || '.' as comment
	from ous o
	join mouos m on m.id=o.mouo_id
  where o.deleted_at is NULL
    and (
      (cast(substring(o.chief_phone, 2, 3) as int) < 900 and cast(substring(o.chief_phone, 2, 3) as int) <> 343)
      or
      (cast(substring(o.chief_phone, 2, 3) as int) > 899 and 
        cast(substring(o.chief_phone, 2, 3) as int) in (907, 935, 940, 942, 943, 944, 945, 946, 947, 948, 949, 957, 959, 972, 973, 974, 975, 976, 979, 990, 998)
       )
      )
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :ou_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
    select m.id as mid, o.id as oid,
		'Не распознан телефон ответственного за ответственного за ГИА ' || cast(o.employee_preparation_ege_phone as varchar(22)) || '.' as comment
	from ous o
	join mouos m on m.id=o.mouo_id
  where o.deleted_at is NULL
    and (
      (cast(substring(o.employee_preparation_ege_phone, 2, 3) as int) < 900 and cast(substring(o.employee_preparation_ege_phone, 2, 3) as int) <> 343)
      or
      (cast(substring(o.employee_preparation_ege_phone, 2, 3) as int) > 899 and 
        cast(substring(o.employee_preparation_ege_phone, 2, 3) as int) in (907, 935, 940, 942, 943, 944, 945, 946, 947, 948, 949, 957, 959, 972, 973, 974, 975, 976, 979, 990, 998)
       )
      )
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :ou_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


    sql = <<SQL
    select m.id as mid, o.id as oid,
		'Не распознан телефон ответственного за информационный обмен ' || cast(o.information_exchange_phone as varchar(22)) || '.' as comment
	from ous o
	join mouos m on m.id=o.mouo_id
  where o.deleted_at is NULL
    and (
      (cast(substring(o.information_exchange_phone, 2, 3) as int) < 900 and cast(substring(o.information_exchange_phone, 2, 3) as int) <> 343)
      or
      (cast(substring(o.information_exchange_phone, 2, 3) as int) > 899 and 
        cast(substring(o.information_exchange_phone, 2, 3) as int) in (907, 935, 940, 942, 943, 944, 945, 946, 947, 948, 949, 957, 959, 972, 973, 974, 975, 976, 979, 990, 998)
       )
      )
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :ou_is_not_updated, r['oid'], 'Ou', r['oid'], r['mid'], r['comment']
    end


  end
end