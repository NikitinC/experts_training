module Inn
  def process

    puts ("inn now")

    sql = <<SQL
      DROP TABLE IF EXISTS tmpinn;
            
      CREATE TEMPORARY TABLE tmpinn
            (eid int, inn varchar(20), innchecksum varchar(20));
      
      INSERT INTO tmpinn
      SELECT id, substring(inn, 1, 12), substring(inn, 1, 10) ||
          right(cast(
          (cast(substring(inn, 1, 1) as int)*7
          +
          cast(substring(inn, 2, 1) as int)*2 -- 2 digit
          +
          cast(substring(inn, 3, 1) as int)*4 -- 3 digit
          +
          cast(substring(inn, 4, 1) as int)*10 -- 4 digit
          +
          cast(substring(inn, 5, 1) as int)*3 -- 5 digit
          +
          cast(substring(inn, 6, 1) as int)*5 -- 6 digit
          +
          cast(substring(inn, 7, 1) as int)*9 -- 7 digit
          +
          cast(substring(inn, 8, 1) as int)*4 -- 8 digit
          +
          cast(substring(inn, 9, 1) as int)*6 -- 9 digit
          +
          cast(substring(inn, 10, 1) as int)*8 -- 10 digit
          )%11
          as varchar(5)), 1)
          ||
          
          right(cast(
          (cast(substring(inn, 1, 1) as int)*3
          +
          cast(substring(inn, 2, 1) as int)*7 -- 2 digit
          +
          cast(substring(inn, 3, 1) as int)*2 -- 3 digit
          +
          cast(substring(inn, 4, 1) as int)*4 -- 4 digit
          +
          cast(substring(inn, 5, 1) as int)*10 -- 5 digit
          +
          cast(substring(inn, 6, 1) as int)*3 -- 6 digit
          +
          cast(substring(inn, 7, 1) as int)*5 -- 7 digit
          +
          cast(substring(inn, 8, 1) as int)*9 -- 8 digit
          +
          cast(substring(inn, 9, 1) as int)*4 -- 9 digit
          +
          cast(substring(inn, 10, 1) as int)*6 -- 10 digit
           +
           cast(substring(inn, 11, 1) as int)*8 -- 10 digit
          )%11
          as varchar(5)), 1)
          
          as inncontrol
        FROM employees
          
      where inn is NOT NULL and length(inn) = 12 
      and substring(inn, 1, 1) != '_'
      and substring(inn, 11, 1) != '_'
      and deleted_at is NULL;
      
      select e.id as eid,  o.id as oid, 
        case when m.id is NOT NULL then m.id else m2.id end as mid,
        'Ошибка в номере ИНН' as comment
      from tmpinn inn
      join employees e ON e.id = inn.eid
      left join employee_ous eo ON eo.employee_id = e.id
      left join ous o ON o.id = eo.ou_id
      left join mouos m ON m.id = o.mouo_id
      left join mouos m2 ON m2.id = e.owner_mouo_id
      where inn.inn <> inn.innchecksum

SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :broken_inn, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
  DROP TABLE IF EXISTS tmpinn;
      
CREATE TEMPORARY TABLE tmpinn
      (eid int, inn varchar(20), innchecksum varchar(20));

INSERT INTO tmpinn
SELECT id, substring(inn, 1, 12), substring(inn, 1, 10) ||
		right(cast(
		(cast(substring(inn, 1, 1) as int)*7
		+
		cast(substring(inn, 2, 1) as int)*2 -- 2 digit
		+
		cast(substring(inn, 3, 1) as int)*4 -- 3 digit
		+
		cast(substring(inn, 4, 1) as int)*10 -- 4 digit
		+
		cast(substring(inn, 5, 1) as int)*3 -- 5 digit
		+
		cast(substring(inn, 6, 1) as int)*5 -- 6 digit
		+
		cast(substring(inn, 7, 1) as int)*9 -- 7 digit
		+
		cast(substring(inn, 8, 1) as int)*4 -- 8 digit
		+
		cast(substring(inn, 9, 1) as int)*6 -- 9 digit
		+
		cast(substring(inn, 10, 1) as int)*8 -- 10 digit
		)%11
		as varchar(5)), 1)
		||
		
		right(cast(
		(cast(substring(inn, 1, 1) as int)*3
		+
		cast(substring(inn, 2, 1) as int)*7 -- 2 digit
		+
		cast(substring(inn, 3, 1) as int)*2 -- 3 digit
		+
		cast(substring(inn, 4, 1) as int)*4 -- 4 digit
		+
		cast(substring(inn, 5, 1) as int)*10 -- 5 digit
		+
		cast(substring(inn, 6, 1) as int)*3 -- 6 digit
		+
		cast(substring(inn, 7, 1) as int)*5 -- 7 digit
		+
		cast(substring(inn, 8, 1) as int)*9 -- 8 digit
		+
		cast(substring(inn, 9, 1) as int)*4 -- 9 digit
		+
		cast(substring(inn, 10, 1) as int)*6 -- 10 digit
		 +
		 cast(substring(inn, 11, 1) as int)*8 -- 10 digit
		)%11
		as varchar(5)), 1)
		
		as inncontrol
	FROM experts
		
where inn is NOT NULL and length(inn) = 12 
and substring(inn, 1, 1) != '_'
and substring(inn, 11, 1) != '_';

select 
	case
		when coalesce(m3.id, 0) > 0 then m3.id
		when coalesce(m1.id, 0) > 0 then m1.id
		when coalesce(m2.id, 0) > 0 then m2.id
	end as mid,
	case
		when coalesce(o1.id, 0) > 0 then o1.id
		when coalesce(o2.id, 0) > 0 then o2.id
	end as oid,
	e.id as eid,

	'Ошибка в номере ИНН ' || coalesce(inn.inn, 'не заполнен') || '.' as comment
from experts e
left join tmpinn inn ON e.id = inn.eid
LEFT JOIN ous o1 ON o1.id = e.ou_id
LEFT JOIN mouos m1 ON m1.id = o1.mouo_id
JOIN users u ON u.id = e.user
LEFT JOIN ous o2 ON o2.id = u.target_id and u.target_type = 'Ou'
LEFT JOIN mouos m2 ON m2.id = o2.mouo_id
LEFT JOIN mouos m3 ON m3.id = u.target_id and u.target_type = 'Mouo'


where inn.inn <> inn.innchecksum -- or inn.inn is NULL


SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :broken_inn, r['eid'], 'Expert', r['oid'], r['mid'], r['comment']
    end

  end
end