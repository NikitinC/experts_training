module HasComposition
  def process
    puts ("composition analytics")

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Установлен признак зачёта по сочинению(изложению) при наличии результата' as comment
	  FROM students s
	  JOIN groups g ON g.id = s.group_id
	  JOIN ous o ON o.id = g.ou_id
	  JOIN mouos m ON m.id = o.mouo_id
	  JOIN ht ON cast(ht.guid as varchar(38)) = cast(s.guid as varchar(38))
    WHERE s.deleted_at is NULL and s.has_composition = True
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :has_composition, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end