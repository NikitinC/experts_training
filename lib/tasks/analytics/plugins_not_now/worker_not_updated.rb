module WorkerNotUpdated
  def process

#     Employee.find_each do |e|
#       if e.deleted_at.nil?
#         if e.email.nil? then
#             mouo_id = e.owner_mouo_id
#             mouo_id = e.main_ou.try(:mouo).try(:id) if mouo_id.nil?
#             mouo_id = e.mouo.try(:id) if mouo_id.nil?
#
#             ou_id = e.ou_id
#             ou_id = e.main_ou.try(:id) if ou_id.nil?
#             add_analytics_item :worker_not_updated,
#                                e.id,
#                                'Employee',
#                                ou_id,
#                                mouo_id
#         end
#       end
#     end
#   end
# end

    puts ("dictionary names")

    sql = <<SQL
    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid
            ,CASE
      WHEN e.gender <> n.gender THEN 'Пол сотрудника не совпадает со словарём'
        WHEN n.id is NULL THEN 'Имя ' || e.first_name || ' отсутствует в словаре'
    END as comment

      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
        LEFT JOIN names n ON n.name = e.first_name
      where e.deleted_at is NULL and (e.gender <> n.gender OR n.id is NULL)
      AND e.id NOT IN (SELECT e.id FROM employees e JOIN names n ON n.name = e.first_name and e.gender = n.gender)
      group by e.id, o.id, m1.id, m2.id, e.gender, n.gender, n.id, e.first_name
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :worker_not_updated, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end


    puts ("not full ppe and post_code entered")

    sql = <<SQL
    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid,
			CASE
				WHEN COALESCE(on_station, 0) = 0 THEN 'Не указан ППЭ у сотрудника ГИА-11'
				WHEN station_post_code is NULL THEN 'Не указана возможная должность в ППЭ у сотрудника ГИА-11'
			END as comment
      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
        JOIN employee_posts ep ON ep.code = eo.employee_post_code
      where e.deleted_at is NULL and
	  ((COALESCE(on_station, 0) = 0 and station_post_code is NOT NULL)  or (COALESCE(on_station, 0) <> 0 and station_post_code is NULL))

union

    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid,
			CASE
				WHEN COALESCE(on_station_oge, 0) = 0 THEN 'Не указан ППЭ у сотрудника ГИА-9'
				WHEN station_post_code_oge is NULL THEN 'Не указана возможная должность в ППЭ у сотрудника ГИА-9'
			END as comment
      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
        JOIN employee_posts ep ON ep.code = eo.employee_post_code
      where e.deleted_at is NULL and
	  ((COALESCE(on_station_oge, 0) = 0 and station_post_code_oge is NOT NULL)  or (COALESCE(on_station_oge, 0) <> 0 and station_post_code_oge is NULL))
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :worker_not_updated, r['eid'], 'Employee', r['oid'], r['mid'], r['comment']
    end

    puts ("worker nor updated")

    sql = <<SQL
    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
           CASE
            WHEN e.job_post = '' THEN ep.name
            ELSE e.job_post
          END as post,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid
      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
        JOIN employee_posts ep ON ep.code = eo.employee_post_code
      where e.deleted_at is NULL and (e.email is NULL OR e.phone is NULL)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :worker_not_updated, r['eid'], 'Employee', r['oid'], r['mid'], 'Необходимо отредактировать карточку сотрудника'
    end

    sql = <<SQL
    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid,
            sp.name
      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
        JOIN station_posts sp ON sp.code = e.station_post_code
        JOIN (SELECT lower(email) as email, count(distinct(id)) as cnt from employees where deleted_at is NULL group by email) as w ON w.email = lower(e.email)
        JOIN (SELECT phone, count(distinct(id)) as cnt from employees where deleted_at is NULL group by phone) as ww ON ww.phone = e.phone
      where e.deleted_at is NULL and (w.cnt > 1 or ww.cnt>1) and sp.code in (1, 5, 7)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :worker_not_updated, r['eid'], 'Employee', r['oid'], r['mid'], 'Сотрудникам данной категории необходимо указывать уникальные телефоны и email'
    end

    sql = <<SQL
    SELECT e.id as eid,
         COALESCE (o.id, 0) as oid,
            CASE
                WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN COALESCE(m1.id, 0)
                ELSE COALESCE(m2.id, 0)
            END as mid,
            sp.name
      FROM employees e
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.deleted_at is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.deleted_at is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.deleted_at is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.deleted_at is NULL
        JOIN station_posts sp ON sp.code = e.station_post_code
        LEFT JOIN (SELECT lower(email) as email, count(distinct(id)) as cnt from employees where deleted_at is NULL group by email) as w ON w.email = lower(e.email)
		LEFT JOIN (SELECT email, count(distinct(id)) as cnt from experts group by email) as experts ON experts.email = e.email
      where e.deleted_at is NULL and (coalesce(w.cnt, 0) > 1 or coalesce(experts.cnt,0) > 1)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :worker_not_updated, r['eid'], 'Employee', r['oid'], r['mid'], 'Сотрудникам необходимо указывать уникальные email'
    end


  end
end