module Experts
  def process
    puts ("experts analytics")

    sql = <<SQL
    SELECT
	case
		when coalesce(m3.id, 0) > 0 then m3.id
		when coalesce(m1.id, 0) > 0 then m1.id
		when coalesce(m2.id, 0) > 0 then m2.id
	end as mid,
	case
		when coalesce(o1.id, 0) > 0 then o1.id
		when coalesce(o2.id, 0) > 0 then o2.id
	end as oid,
	e.id as eid,
	'не заполнен email' as comment
	
	FROM experts e
	LEFT JOIN ous o1 ON o1.id = e.ou_id
	LEFT JOIN mouos m1 ON m1.id = o1.mouo_id
	JOIN users u ON u.id = e.user
	LEFT JOIN ous o2 ON o2.id = u.target_id and u.target_type = 'Ou'
	LEFT JOIN mouos m2 ON m2.id = o2.mouo_id
	LEFT JOIN mouos m3 ON m3.id = u.target_id and u.target_type = 'Mouo'
where length(e.email) < 3 and e.deleted_at is NULL

union


SELECT
	case
		when coalesce(m3.id, 0) > 0 then m3.id
		when coalesce(m1.id, 0) > 0 then m1.id
		when coalesce(m2.id, 0) > 0 then m2.id
	end as mid,
	case
		when coalesce(o1.id, 0) > 0 then o1.id
		when coalesce(o2.id, 0) > 0 then o2.id
	end as oid,
	e.id as eid,
	case
		when e2.id is NOT NULL then 'email не уникален id: ' || cast(e.id as varchar(5)) || ', ' ||  cast(e2.id as varchar(5)) || '.'
		else 'Email совпадает с Email сотрудником ППЭ, отличным от данного эксперта (сравниваются email, номер документа и фамилия)'
	end as commment
	
	FROM experts e
	LEFT JOIN ous o1 ON o1.id = e.ou_id
	LEFT JOIN mouos m1 ON m1.id = o1.mouo_id
	JOIN users u ON u.id = e.user
	LEFT JOIN ous o2 ON o2.id = u.target_id and u.target_type = 'Ou'
	LEFT JOIN mouos m2 ON m2.id = o2.mouo_id
	LEFT JOIN mouos m3 ON m3.id = u.target_id and u.target_type = 'Mouo'
	LEFT JOIN experts e2 ON e2.id <> e.id and e2.email = e.email
	LEFT JOIN employees emp ON emp.email = e.email and emp.document_number <> e.document_number and e.surname <> emp.second_name and emp.deleted_at is NULL
where length(e.email) > 3 and e.deleted_at is NULL
and (e2.id is NOT NULL or emp.id is NOT NULL)
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :experts, r['eid'], 'Expert', r['oid'], r['mid'], r['comment']
    end

  end
end