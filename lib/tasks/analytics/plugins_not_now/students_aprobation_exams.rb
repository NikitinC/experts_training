module StudentsAprobationExams
  def process
    puts ("StudentsAprobationExams")

    sql = <<SQL
      SELECT s.id as sid, m.id as mid, o.id as oid, 'Участник назначен на апробационные экзамены' as comment
        FROM exam_students es
        JOIN students s ON s.id = es.student_id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
        JOIN exams e ON e.id = es.exam_id
      WHERE e.exam_type_id = 29 and es.deleted_at is NULL and s.deleted_at is NULL
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :bad_exams, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end