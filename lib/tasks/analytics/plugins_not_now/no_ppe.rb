module NoPpe
  def process

    sql = <<SQL
select
	distinct 	s.id as sid, o.id as oid, o.mouo_id as mid

from students s
join groups g on g.id = s.group_id and g.deleted_at is null
join ous o on o.id = g.ou_id and o.deleted_at is null and o.code not in ('111111', '999999')
join exam_students es on es.deleted_at is null and es.student_id = s.id
join exams e on e.id = es.exam_id and e.exam_type_id in (5, 19, 24)
join subjects sj ON sj.code = e.subject_code

where s.deleted_at is null and
(
	exists (
			select 1 from attestation_form_students afs where afs.student_id = s.id and afs.deleted_at is null and afs.attestation_form_code in (1, 8)
	)

	or g.number > 10 or g.number = 2
)

and not exists (
	select 1 from stations st
	join exam_stations se on se.station_id = st.id and se.deleted_at is null
	where st.deleted_at is null and es.exam_station_id = se.id
)

 and sj.name like '%сский язык%'

SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_ppe_11, r['sid'], 'Student', r['oid'], r['mid']
    end




sql = <<SQL
select
	distinct 	s.id as sid, o.id as oid, o.mouo_id as mid

from students s
join groups g on g.id = s.group_id and g.deleted_at is null
join ous o on o.id = g.ou_id and o.deleted_at is null and o.code not in ('111111', '999999')
join exam_students es on es.deleted_at is null and es.student_id = s.id
join exams e on e.id = es.exam_id and e.exam_type_id in (22, 23, 25, 26)
join subjects sj ON sj.code = e.subject_code

where s.deleted_at is null and
(
	exists (
			select 1 from attestation_form_students afs where afs.student_id = s.id and afs.deleted_at is null and afs.attestation_form_code in (3, 9)
	)

	or g.number in (9, 10)
)

and not exists (
	select 1 from stations st
	join exam_stations se on se.station_id = st.id and se.deleted_at is null
	where st.deleted_at is null and es.exam_station_id = se.id
)

 and sj.name like '%атематик%'
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_ppe_9, r['sid'], 'Student', r['oid'], r['mid']
    end



  end
end