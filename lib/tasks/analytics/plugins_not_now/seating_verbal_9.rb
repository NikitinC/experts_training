module SeatingVerbal9
  class NoAunts < StandardError
  end

  class NoClassrooms < StandardError
  end

  class NoUserAunts < StandardError
  end

  def process

    def make_seating(scau)
      students, classrooms, aunts, user_aunts = scau
      students = students.select{|x| true}
      classrooms = classrooms.select{|x| true}
      aunts = aunts.select{|x| true}
      user_aunts = (user_aunts.nil? ? nil : user_aunts.select{|x| true})

      students_pop = []
      classrooms_pop = []
      aunts_pop = []
      user_aunts_pop = []

      classrooms_sorted = classrooms.sort_by{|e| e.available_seats}

      while students.length > 0
        raise NoAunts.new if aunts.length == 0
        aunts_pop.append a1=aunts.pop
        if user_aunts.nil?
          raise NoAunts.new if aunts.length == 0
          aunts_pop.append a2=aunts.pop
        else
          raise NoUserAunts.new if user_aunts.length == 0
          user_aunts_pop.append a2=user_aunts.pop
        end

        raise NoClassrooms.new if classrooms_sorted.length == 0
        classrooms_pop.append classroom = classrooms_sorted.pop

        # puts a1, a2, classroom, students.length
        (user_aunts.nil? ? classroom.available_seats : 16).times do
          break if students.length == 0
          students_pop.append s=students.pop
          # puts '   ' + s.name
        end
      end

      [students_pop, classrooms_pop, aunts_pop, user_aunts_pop]
    end

    def exclude(a, b)
      students_to_reject, classrooms_to_reject, aunts_to_reject, user_aunts_to_reject = b
      students_to_reject = students_to_reject.map(&:id)
      classrooms_to_reject = classrooms_to_reject.map(&:id)
      aunts_to_reject = aunts_to_reject.map(&:id)
      user_aunts_to_reject = (user_aunts_to_reject.nil? ? nil : user_aunts_to_reject.map(&:id))

      [
          a[0].reject{|x| students_to_reject.include? x.id},
          a[1].reject{|x| classrooms_to_reject.include? x.id},
          a[2].reject{|x| aunts_to_reject.include? x.id},
          a[3].nil? ? nil : a[3].reject{|x| user_aunts_to_reject.include? x.id}
      ]
    end

    ExamStation.find_each do |es|
      next if es.exam.date <= Date.today
      next if (not es.exam.is_verbal) or (not es.exam.attestation_form.is_oge_or_gve9)
      # puts "#{es.id} #{es}"
      comments = []

      students = es.exam_students.map(&:student)
      next if students.length == 0

      uniq_posts = es.exam_station_employees.map(&:station_post_code).uniq
      comments.append 'Нет руководителя ППЭ' unless uniq_posts.include? 1
      comments.append 'Нет члена ГЭК' unless uniq_posts.include? 5
      comments.append 'Нет технического специалиста' unless uniq_posts.include? 7
      comments.append 'Нет организаторов вне аудитории' unless uniq_posts.include? 3
      comments.append 'Нет медицинского работника' unless uniq_posts.include? 8


      # проведения
      classrooms = es.classrooms
                       .select{|x| x.attestation_forms.map(&:code).include? AttestationForm::OGE }
                       .select{|x| x.classroom_verbal_part_sing_code == 2}
      comments.append 'Нет аудиторий проведения' if classrooms.length == 0

      aunts = es.exam_station_employees.select{|x| x.station_post_code == 2}.map(&:employee).to_a
      comments.append 'Нет сотрудников' if aunts.length == 0

      user_aunts = es.exam_station_employees.select{|x| x.station_post_code == 52}.map(&:employee).to_a
      comments.append 'Нет операторов ПК' if user_aunts.length == 0

      begin
        students, classrooms, aunts, user_aunts =
            exclude(
              [students, classrooms, aunts, user_aunts],
              make_seating([students, classrooms, aunts, user_aunts])
        )

        comments.append 'Ошибка алгоритма рассадки' if students.length > 0
      rescue NoAunts
        comments.append 'Недостаточно организаторов в аудиториях'
      rescue NoClassrooms
        comments.append 'Недостаточно аудиторий проведения'
      rescue NoUserAunts
        comments.append 'Недостаточно операторов ПК'
      end

      # подготвки
      classrooms = es.classrooms
                       .select{|x| x.attestation_forms.map(&:code).include? AttestationForm::OGE }
                       .select{|x| x.classroom_verbal_part_sing_code == 4}
      comments.append 'Нет аудиторий подготовки' if classrooms.length == 0

      students = es.exam_students.map(&:student)

      begin
        students, classrooms, aunts, _ =
            exclude(
                [students, classrooms, aunts, nil],
                make_seating([students, classrooms, aunts, nil])
            )

        comments.append 'Ошибка алгоритма рассадки' if students.length > 0
      rescue NoAunts
        comments.append 'Недостаточно организаторов в аудиториях'
      rescue NoClassrooms
        comments.append 'Недостаточно аудиторий подготовки'
      rescue NoUserAunts
        comments.append 'Недостаточно операторов ПК'
      end


      # puts comments
      add_analytics_item :seating_verbal_9,
                         es.id, 'ExamStation', es.station.ou.id, es.station.ou.mouo.id,
                         comments.join("\n") if comments.length > 0

    end
  end
end