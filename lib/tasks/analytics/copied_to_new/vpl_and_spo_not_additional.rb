module VplAndSpoNotAdditional
  def process

#       sql = <<SQL
SELECT s.id as sid, ous.id as oid, m.id as mid
	from students s
    join exam_students es ON es.student_id = s.id and s.ege_participant_category_code in (4)
	join exams e ON e.id = es.exam_id
    join exam_types et ON et.id = e.exam_type_id
    join groups g ON g.id = s.group_id
    join ous ON ous.id = g.ou_id
    join mouos m ON m.id = ous.mouo_id
    where et.id = 5 and e.reserved = false and es.deleted_at is NULL and e.deleted_at is NULL
# SQL
#
#       ActiveRecord::Base.connection.execute(sql).each do |r|
#         add_analytics_item :not_additional_dates, r['sid'], 'Student', r['oid'], r['mid'], 'Участник не может сдавать экзамены в основные даты!!!'
#       end


    sql = <<SQL
SELECT s.id as sid, ous.id as oid, m.id as mid, 'Участник назначен на ' || sj.name || ' (' || r.date || '), хотя основной день ' || enr.date || ' у него свободен.' as comment
 	from students s
    JOIN (select es.student_id, e.exam_type_id, e.date, sj.code, sj.fiscode
		    from exam_students es
			join exams e ON e.id = es.exam_id
		    join subjects sj ON sj.code = e.subject_code
		    join students s ON s.id = es.student_id and s.ege_participant_category_code in (1, 3, 8)
		 	where e.reserved = true and es.deleted_at is NULL and s.deleted_at is NULL
		 	) r ON r.student_id = s.id
	JOIN exams enr ON enr.exam_type_id = r.exam_type_id and enr.subject_code = r.code and enr.reserved = false
    LEFT JOIN (select es.student_id, e.date, sj.code, sj.fiscode
		    from exam_students es
			join exams e ON e.id = es.exam_id
		    join subjects sj ON sj.code = e.subject_code
		    join students s ON s.id = es.student_id and s.ege_participant_category_code in (1, 3, 8)
		 	where e.reserved = false and es.deleted_at is NULL and s.deleted_at is NULL
		 	) esnr ON esnr.student_id = r.student_id and esnr.date = enr.date
	JOIN subjects sj ON sj.code = r.code
    join groups g ON g.id = s.group_id
    join ous ON ous.id = g.ou_id
    join mouos m ON m.id = ous.mouo_id
    where s.deleted_at is NULL and enr.deleted_at is NULL and g.deleted_at is NULL and ous.deleted_at is NULL and
		esnr.student_id is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :not_additional_dates, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    sql = <<SQL
SELECT s.id as sid, ous.id as oid, m.id as mid, 'Нарушение пункта №13 Порядка: выбор математики базовой категориями ВПЛ, СПО или учащийся иностранных ОО.' as comment
 	from students s
    join exam_students es ON es.student_id = s.id
	join exams e ON e.id = es.exam_id
	join subjects sj ON sj.code = e.subject_code
    join groups g ON g.id = s.group_id
    join ous ON ous.id = g.ou_id
    join mouos m ON m.id = ous.mouo_id
	join ege_participant_categories c ON c.code = s.ege_participant_category_code
    where s.deleted_at is NULL and es.deleted_at is NULL and g.deleted_at is NULL and ous.deleted_at is NULL and
		sj.fiscode in (22) and (c.name like '%ВПЛ%' or c.name like '%среднего профес%' or c.name like '%ностранн%')
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_base_mathematics, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    end
end


