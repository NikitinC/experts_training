module BadExams
  def process
    puts ("bad exams")

    sql = <<SQL
      DROP TABLE IF EXISTS tmp;

      CREATE TEMPORARY TABLE tmp
      (student_id int,
       date date,
       cnt int);
      
      INSERT INTO tmp
      SELECT es.student_id, e.date, count(distinct(es.id))
        FROM exam_students es
        JOIN exams e ON e.id = es.exam_id
        JOIN subjects sj ON sj.code = e.subject_code
      where es.deleted_at is NULL and e.deleted_at is NULL and sj.name not like '%устная часть%'
      GROUP BY es.student_id, e.date;
      
      SELECT s.id as sid, m.id as mid, o.id as oid, 'Участник назначен на несколько экзаменов ' || t.date || '.' as comment
        FROM tmp t
        JOIN students s ON s.id = t.student_id
          JOIN groups g ON g.id = s.group_id
          JOIN ous o ON o.id = g.ou_id
          JOIN mouos m ON m.id = o.mouo_id
      where t.cnt > 1 and s.deleted_at is NULL and g.deleted_at is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :bad_exams, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

    puts ("bad exams 2")

    sql = <<SQL
    DROP TABLE IF EXISTS students_exams_is;

    CREATE TEMP TABLE students_exams_is (
        mid int,
      oid int,
        sid int,
      subject_name varchar(80),
      cnt int
    );

    insert into students_exams_is
    select m.id as mid, o.id as oid, s.id as sid, sj.name, count(e.id)
      from exam_students es
      join students s ON s.id = es.student_id
      join exams e ON e.id = es.exam_id and e.exam_type_id not in (34)
      join subjects sj ON sj.code = e.subject_code
      join groups g ON g.id = s.group_id
      join ous o ON o.id = g.ou_id
      join mouos m ON m.id = o.mouo_id
    where es.deleted_at is NULL and s.deleted_at is NULL and e.deleted_at is NULL
    group by m.id, o.id, s.id, sj.name;

    select mid, sid, oid, 'Участник назначен на один и тот же экзамен 2 раза' as comment
      from students_exams_is
      where cnt > 1

    union

    select inyaz.mid, inyaz.sid, inyaz.oid, 'Участник назначен только на одну часть экзамена по иностранному языку' as comment
      from students_exams_is inyaz
      full outer join students_exams_is inyazustn ON inyaz.sid = inyazustn.sid and (inyazustn.subject_name = inyaz.subject_name || ' (устная часть)' or inyaz.subject_name = inyazustn.subject_name || ' (устная часть)')
      join attestation_form_students afs ON afs.student_id = inyaz.sid
      join attestation_forms af ON af.code = afs.attestation_form_code and af.name in ('ОГЭ')
    where inyaz.subject_name like '%язык%' and inyaz.subject_name not like '%сский язы%'
    and inyazustn.subject_name is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :bad_exams, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end



  end
end