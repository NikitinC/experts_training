module NoEqualForms
  def process
    puts ("no equal form")

    sql = <<SQL
      SELECT s.id as sid, m.id as mid, o.id as oid, 'Участник назначен на экзамен в форме ' || af.name || ', но данная форма у него не выбрана.' as comment
        FROM exam_students es
        JOIN exams e ON e.id = es.exam_id
        JOIN attestation_forms af ON af.code = e.attestation_form_code
        LEFT JOIN attestation_form_students afs ON afs.attestation_form_code = af.code and afs.student_id = es.student_id
        JOIN students s ON s.id = es.student_id
        JOIN groups g ON g.id = s.group_id
        JOIN ous o ON o.id = g.ou_id
        JOIN mouos m ON m.id = o.mouo_id
      WHERE s.deleted_at is NULL and es.deleted_at is NULL and afs.id is NULL
      GROUP BY s.id, m.id, o.id, af.name
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :bad_exams, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end