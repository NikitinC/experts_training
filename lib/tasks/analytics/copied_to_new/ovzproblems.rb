require 'date'

module Ovzproblems
  def process
    puts ("OVZ analytics problems")

    sql = <<SQL
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Указана спецрассадка для учащегося без ОВЗ' as comment
      FROM students s
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and s.limited_facilities_group_code = 1 and s.special_seating = true
    
    union

    SELECT s.id as sid, m.id as mid, o.id as oid, 'Указаны КИМ на Шрифте Байля для учащегося без ОВЗ' as comment
      FROM students s
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and s.limited_facilities_group_code = 1 and s.braille_kim = true

    union
    
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Учащемуся со спецрассадкой не выставлен принцип рассадки' as comment
      FROM students s
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and s.special_seating = true
      and (coalesce(limit_seating, 0) = 0 and length(ege_statement_scan) > 3)

    union
    
    SELECT s.id as sid, m.id as mid, o.id as oid, 'Учащемуся с ОВЗ не выставлен признак спецрассадки' as comment
      FROM students s
      JOIN groups g ON g.id = s.group_id
      JOIN ous o ON o.id = g.ou_id
      JOIN mouos m ON m.id = o.mouo_id
    WHERE s.deleted_at is NULL and s.limited_facilities_group_code > 1 and s.special_seating = false

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :ovz_problems, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end