module Snils
  def process

    puts ("snils now")

    sql = <<SQL
      DROP TABLE IF EXISTS tmp;
      DROP TABLE IF EXISTS tmp2;
      
      CREATE TEMPORARY TABLE tmp
      (snils varchar(15), sid int, checksum int, checksumin int);
      CREATE TEMPORARY TABLE tmp2
      (snils varchar(15), sid int, checksum int, checksumin int);
      
      INSERT INTO tmp
      SELECT snils, id as sid, 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 11, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 1 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 10, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 2 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 9, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 3 +
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 7, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 4 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 6, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 5 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 5, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 6 +
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 3, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 7 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 2, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 8 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 1, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 9,
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 13, 2),'0'), '[^0-9]+', '', 'g') AS INTEGER)
        FROM public.students
      where snils is NOT NULL and length(snils) > 1 and snils~E'\\d\\d\\d-\\d\\d\\d-\\d\\d\\d \\d\\d';
      
      INSERT INTO tmp2
      SELECT snils, sid,
          case
		  	when checksum % 101 > 99 then
				case
					when checksum%101 < 100 then checksum
					when checksum%101 = 100 then 0
					when checksum%101 = 101 then 0
					else checksum % 101	% 101
				end
            when checksum < 100 then checksum
            when checksum = 100 then 0
            when checksum = 101 then 0
            else checksum % 101
          end as checksum
          , checksumin
        FROM tmp;
      
      SELECT t.sid as sid,  o.id as oid, m.id as mid, 'Неверный номер СНИЛС (' || t.snils || ') у участника' as comment
      
      FROM tmp2 t
      join students s ON s.id = t.sid
      join groups g ON g.id = s.group_id
      join ous o ON o.id = g.ou_id
      join mouos m ON m.id = o.mouo_id
      WHERE checksum != checksumin and s.deleted_at is NULL
SQL

    i = 0;
    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :broken_snils, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
      i = i + 1
    end


    sql = <<SQL
      DROP TABLE IF EXISTS tmp;
      DROP TABLE IF EXISTS tmp2;
      
      CREATE TEMPORARY TABLE tmp
      (snils varchar(15), sid int, checksum int, checksumin int);
      CREATE TEMPORARY TABLE tmp2
      (snils varchar(15), sid int, checksum int, checksumin int);
      
      INSERT INTO tmp
      SELECT snils, id as sid, 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 11, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 1 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 10, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 2 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 9, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 3 +
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 7, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 4 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 6, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 5 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 5, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 6 +
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 3, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 7 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 2, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 8 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 1, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 9,
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 13, 2),'0'), '[^0-9]+', '', 'g') AS INTEGER)
        FROM employees
      where snils is NOT NULL and length(snils) > 1 and snils~E'\\d\\d\\d-\\d\\d\\d-\\d\\d\\d \\d\\d';
      
      INSERT INTO tmp2
      SELECT snils, sid,
          case
            when checksum % 101 > 99 then
            case
              when checksum%101 < 100 then checksum
              when checksum%101 = 100 then 0
              when checksum%101 = 101 then 0
              else checksum % 101	% 101
            end            
            when checksum < 100 then checksum
            when checksum = 100 then 0
            when checksum = 101 then 0
            else checksum % 101
          end as checksum
          , checksumin
        FROM tmp;
      
      SELECT s.id as sid,  o.id as oid, case when m.id is NOT NULL then m.id else m2.id end as mid, 'Неверный номер СНИЛС (' || coalesce(t.snils, 'не заполнен') || ') у сотрудника' as comment
      
      FROM employees s
      left join tmp2 t ON s.id = t.sid
      left join employee_ous eo ON eo.employee_id = s.id
      left join ous o ON o.id = eo.ou_id
      left join mouos m ON m.id = o.mouo_id
      left join mouos m2 ON m2.id = s.owner_mouo_id
      WHERE (checksum != checksumin) and s.deleted_at is NULL
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :broken_snils, r['sid'], 'Employee', r['oid'], r['mid'], r['comment']
      i = i + 1
    end


    sql = <<SQL
           DROP TABLE IF EXISTS tmp;
      DROP TABLE IF EXISTS tmp2;
      
      CREATE TEMPORARY TABLE tmp
      (snils varchar(15), sid int, checksum int, checksumin int);
      CREATE TEMPORARY TABLE tmp2
      (snils varchar(15), sid int, checksum int, checksumin int);
      
      INSERT INTO tmp
      SELECT snils, id as sid, 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 11, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 1 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 10, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 2 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 9, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 3 +
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 7, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 4 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 6, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 5 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 5, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 6 +
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 3, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 7 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 2, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 8 + 
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 1, 1),'0'), '[^0-9]+', '', 'g') AS INTEGER) * 9,
          CAST(REGEXP_REPLACE(COALESCE(substring(snils, 13, 2),'0'), '[^0-9]+', '', 'g') AS INTEGER)
        FROM experts
      where snils is NOT NULL and length(snils) > 1 and snils~E'\\d\\d\\d-\\d\\d\\d-\\d\\d\\d \\d\\d';
      
      INSERT INTO tmp2
      SELECT snils, sid,
          case
            when checksum % 101 > 99 then
            case
              when checksum%101 < 100 then checksum
              when checksum%101 = 100 then 0
              when checksum%101 = 101 then 0
              else checksum % 101	% 101
            end            
            when checksum < 100 then checksum
            when checksum = 100 then 0
            when checksum = 101 then 0
            else checksum % 101
          end as checksum
          , checksumin
        FROM tmp;
      
SELECT 
	  	case
		when coalesce(m3.id, 0) > 0 then m3.id
		when coalesce(m1.id, 0) > 0 then m1.id
		when coalesce(m2.id, 0) > 0 then m2.id
	end as mid,
	case
		when coalesce(o1.id, 0) > 0 then o1.id
		when coalesce(o2.id, 0) > 0 then o2.id
	end as oid,
	e.id as eid,
	  
	 'Неверный номер СНИЛС (' || coalesce(t.snils, 'не заполнен') || ') у эксперта.' as comment
      
      FROM experts e 
      join tmp2 t ON e.id = t.sid
      LEFT JOIN ous o1 ON o1.id = e.ou_id
	LEFT JOIN mouos m1 ON m1.id = o1.mouo_id
	JOIN users u ON u.id = e.user
	LEFT JOIN ous o2 ON o2.id = u.target_id and u.target_type = 'Ou'
	LEFT JOIN mouos m2 ON m2.id = o2.mouo_id
	LEFT JOIN mouos m3 ON m3.id = u.target_id and u.target_type = 'Mouo'
WHERE (checksum != checksumin or checksumin is NULL)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :broken_snils, r['eid'], 'Expert', r['oid'], r['mid'], r['comment']
      i = i + 1
    end


    puts (i)
  end
end