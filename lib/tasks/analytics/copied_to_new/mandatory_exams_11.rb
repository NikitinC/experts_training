module MandatoryExams11
  def process
    sql = <<SQL
select s.id as sid, o.id as oid, m.id as mid,
	'Не назначен на ' ||
		case
			when (math.sid is NULL and s.gia_active_results_math <> True) and (rus.sid is NULL and s.gia_active_results_rus <> True) then 'математику и русский язык.'
			when (rus.sid is NULL and s.gia_active_results_rus <> True) then 'русский язык.'
			when (math.sid is NULL and s.gia_active_results_math <> True) then 'математику.'
		end as comment
	from students s
	join groups g on g.id = s.group_id and g.deleted_at is null
	join ous o on o.id = g.ou_id
	join mouos m ON m.id = o.mouo_id
	join (select student_id
		  from attestation_form_students afs 
			join attestation_forms af ON af.code = afs.attestation_form_code
		  where afs.deleted_at is NULL and af.name in ('ЕГЭ', 'ГВЭ-11')
		  ) afs ON afs.student_id = s.id
	left join (select student_id as sid
		  from exam_students es
		  join exams e ON e.id = es.exam_id
		  join subjects sj ON sj.code = e.subject_code
		  where (es.deleted_at is NULL or es.deleted_at = 'Особенности ЕГЭ-2020') and sj.name like '%усский%'
		 ) rus ON rus.sid = s.id
	left join (select student_id as sid
		  from exam_students es
		  join exams e ON e.id = es.exam_id
		  join subjects sj ON sj.code = e.subject_code
		  where (es.deleted_at is NULL or es.deleted_at = 'Особенности ЕГЭ-2020') and sj.name like '%атематик%'
		 ) math ON math.sid = s.id
where s.deleted_at is NULL and s.ege_participant_category_code in (1, 8)
				and
				((math.sid is NULL and s.gia_active_results_math <> True)
				or (rus.sid is NULL and s.gia_active_results_rus <> True))  
and o.code not in ('111111', '999999')  -- and s.ege_participant_category_code in (1, 8)
SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :no_mandatory_exams_11, r['sid'], 'Student', r['oid'], r['mid'], r['comment']
    end

  end
end