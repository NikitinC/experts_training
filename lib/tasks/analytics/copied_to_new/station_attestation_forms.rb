module StationAttestationForms
  def process

    puts ("station attestation forms and participants")

    sql = <<SQL
      DROP TABLE IF EXISTS tmp;
      
      CREATE TEMPORARY TABLE tmp
      (
        ppe_id int,
        ppe_code varchar(120),
        af_name varchar(50),
        saf_name varchar(250),
        isppe varchar(250)
      );
      
      INSERT INTO tmp
      SELECT st.id, st.code, replace(replace(af.name, '-11', ''), '-9', ''), saf.name,
        case 
          when length(saf.name) = length (replace(saf.name, replace( replace(af.name, '-11', ''), '-9', ''), '')) then 'В ППЭ назначены '|| cast (count(es) as varchar(5)) || ' человеко-экзамен(а,ов) на экзамен в форме ' || af.name || ', но данный тип экзамена у ППЭ не указан.'
          else 'содержит'
        end as isppe
        
        FROM exam_students es
        JOIN students s ON s.id = es.student_id
        JOIN exams e ON e.id = es.exam_id
        JOIN attestation_forms af ON af.code = e.attestation_form_code
        JOIN exam_stations est ON est.id = es.exam_station_id
        JOIN stations st ON st.id = est.station_id
        JOIN station_additional_features saf ON saf.code = st.station_additional_feature_code
      where es.deleted_at is NULL and s.deleted_at is NULL and est.deleted_at is NULL and st.deleted_at is NULL
      group by st.code, af.name, saf.name, st.id;
      
      INSERT INTO tmp
      SELECT t.ppe_id, t.ppe_code, '-', t.saf_name,
        case
          when t.saf_name like '%ГВЭ-9%' and t.saf_name like '%ОГЭ%' then
            case
              when gve.ppe_id is NOT NULL and oge.ppe_id is NOT NULL then 'ok'
              when gve.ppe_id is NULL and oge.ppe_id is NULL then 'У ППЭ указано ГВЭ и ОГЭ, а распределённых участников ГВЭ и ОГЭ нет'
              when gve.ppe_id is NULL and oge.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, а распределённых участников ГВЭ нет'
              when gve.ppe_id is NOT NULL and oge.ppe_id is NULL then 'У ППЭ указано ОГЭ, а распределённых участников ОГЭ нет'
            end
          when t.saf_name like '%ГВЭ-11%' and t.saf_name like '%ЕГЭ%' then
            case
              when gve.ppe_id is NOT NULL and ege.ppe_id is NOT NULL then 'ok'
              when gve.ppe_id is NULL and ege.ppe_id is NULL then 'У ППЭ указано ГВЭ и ЕГЭ, а распределённых участников ГВЭ и ЕГЭ нет'
              when gve.ppe_id is NULL and ege.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, а распределённых участников ГВЭ нет'
            end
          when t.saf_name like '%ЕГЭ%' and t.saf_name not like '%ГВЭ%' then
            case
              when gve.ppe_id is NULL and ege.ppe_id is NOT NULL then 'ok'
              when gve.ppe_id is NULL and ege.ppe_id is NULL then 'У ППЭ указано ЕГЭ, а распределённых участников ЕГЭ нет'
              when gve.ppe_id is NOT NULL and ege.ppe_id is NULL then 'У ППЭ указано только ЕГЭ, а распределённых участников ЕГЭ нет, зато есть распределённые участники ГВЭ'
              when gve.ppe_id is NOT NULL and ege.ppe_id is NOT NULL then 'У ППЭ указано ЕГЭ, а есть ещё участники ГВЭ'
            end
          when t.saf_name like '%ОГЭ%' and t.saf_name not like '%ГВЭ%' then
            case
              when gve.ppe_id is NULL and oge.ppe_id is NOT NULL then 'ok'
              when gve.ppe_id is NULL and ege.ppe_id is NULL then 'У ППЭ указано ОГЭ, а распределённых участников ОГЭ нет'
              when gve.ppe_id is NOT NULL and ege.ppe_id is NULL then 'У ППЭ указано только ОГЭ, а распределённых участников ОГЭ нет, зато есть распределённые участники ГВЭ'
            end
          when t.saf_name like '%ГВЭ-11%' and t.saf_name not like '%ЕГЭ%' then
            case
              when gve.ppe_id is NOT NULL and ege.ppe_id is NULL then 'ok'
              when gve.ppe_id is NULL and ege.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, а распределённых участников ГВЭ нет, зато есть распределённые участники ЕГЭ'
              when gve.ppe_id is NULL and ege.ppe_id is NULL then 'В ППЭ нет участников'
            end
          when t.saf_name like '%ГВЭ-9%' and t.saf_name not like '%ОГЭ%' then
            case
              when gve.ppe_id is NOT NULL and oge.ppe_id is NULL then 'ok'
              when gve.ppe_id is NULL and oge.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, а распределённых участников ГВЭ нет, зато есть распределённые участники ОГЭ'
              when gve.ppe_id is NULL and oge.ppe_id is NULL then 'В ППЭ нет участников'
              when gve.ppe_id is NOT NULL and oge.ppe_id is NOT NULL then 'В ППЭ указано только ГВЭ, а есть участники и ГВЭ и ОГЭ'
            end
        end as comment
        -- ,gve.af_name gve, ege.af_name ege, oge.af_name oge
        from (select  t.ppe_id, t.ppe_code, t.saf_name from tmp t group by t.ppe_id, t.ppe_code, t.saf_name) t
        left join tmp gve ON gve.ppe_id = t.ppe_id and gve.af_name = 'ГВЭ'
        left join tmp ege ON ege.ppe_id = t.ppe_id and ege.af_name = 'ЕГЭ'
        left join tmp oge ON oge.ppe_id = t.ppe_id and oge.af_name = 'ОГЭ';
      
      SELECT s.id as stid, m.id as mid, o.id as oid, isppe as comment
        from tmp t
        join stations s ON t.ppe_id = s.id
        join ous o ON o.id = s.ou_id
        join mouos m ON m.id = o.mouo_id
        where isppe not in ('содержит', 'ok');
SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_attestation_forms_students, r['stid'], 'Station', r['oid'], r['mid'], r['comment']
    end

    puts ("station attestation forms and classrooms")

    sql = <<SQL
    DROP TABLE IF EXISTS tmp;

    CREATE TEMPORARY TABLE tmp
    (
      ppe_id int,
      ppe_code varchar(120),
      af_name varchar(50),
      saf_name varchar(250),
      isppe varchar(250)
    );
    
    INSERT INTO tmp
    SELECT st.id, st.code, replace(replace(af.name, '-11', ''), '-9', ''), saf.name,
      case 
        when length(saf.name) = length (replace(saf.name, replace( replace(af.name, '-11', ''), '-9', ''), '')) then 'В ППЭ есть аудитории с формой ' || af.name || ', но данный тип экзамена у ППЭ не указан.'
        else 'содержит'
      end as isppe
      FROM attestation_form_classrooms afc
      JOIN classrooms c ON c.id = afc.classroom_id
      JOIN attestation_forms af ON af.code = afc.attestation_form_code
      JOIN stations st ON st.id = c.station_id
      JOIN station_additional_features saf ON saf.code = st.station_additional_feature_code
    where st.deleted_at is NULL and c.deleted_at is NULL
    group by st.code, af.name, saf.name, st.id;
    
    INSERT INTO tmp
    SELECT t.ppe_id, t.ppe_code, '-', t.saf_name,
      case
        when t.saf_name like '%ГВЭ-9%' and t.saf_name like '%ОГЭ%' then
          case
            when gve.ppe_id is NOT NULL and oge.ppe_id is NOT NULL then 'ok'
            when gve.ppe_id is NULL and oge.ppe_id is NULL then 'У ППЭ указано ГВЭ и ОГЭ, но аудиторий ГВЭ и ОГЭ нет'
            when gve.ppe_id is NULL and oge.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, но аудиторий ГВЭ нет'
            when gve.ppe_id is NOT NULL and oge.ppe_id is NULL then 'У ППЭ указано ОГЭ, но аудиторий ОГЭ нет'
          end
        when t.saf_name like '%ГВЭ-11%' and t.saf_name like '%ЕГЭ%' then
          case
            when gve.ppe_id is NOT NULL and ege.ppe_id is NOT NULL then 'ok'
            when gve.ppe_id is NULL and ege.ppe_id is NULL then 'У ППЭ указано ГВЭ и ЕГЭ, но аудиторий ГВЭ и ЕГЭ нет'
            when gve.ppe_id is NULL and ege.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, но аудиторий ГВЭ нет'
          end
        when t.saf_name like '%ЕГЭ%' and t.saf_name not like '%ГВЭ%' then
          case
            when gve.ppe_id is NULL and ege.ppe_id is NOT NULL then 'ok'
            when gve.ppe_id is NULL and ege.ppe_id is NULL then 'У ППЭ указано ЕГЭ, но аудиторий ЕГЭ нет'
            when gve.ppe_id is NOT NULL and ege.ppe_id is NULL then 'У ППЭ указано только ЕГЭ, но аудиторий ЕГЭ нет, зато есть аудитории ГВЭ'
            when gve.ppe_id is NOT NULL and ege.ppe_id is NOT NULL then 'У ППЭ указано ЕГЭ, но ещё есть аудитории ГВЭ'
          end
        when t.saf_name like '%ОГЭ%' and t.saf_name not like '%ГВЭ%' then
          case
            when gve.ppe_id is NULL and oge.ppe_id is NOT NULL then 'ok'
            when gve.ppe_id is NULL and ege.ppe_id is NULL then 'У ППЭ указано ОГЭ, но аудиторий ОГЭ нет'
            when gve.ppe_id is NOT NULL and ege.ppe_id is NULL then 'У ППЭ указано только ОГЭ, но аудиторий ОГЭ нет, зато есть аудитории ГВЭ'
          end
        when t.saf_name like '%ГВЭ-11%' and t.saf_name not like '%ЕГЭ%' then
          case
            when gve.ppe_id is NOT NULL and ege.ppe_id is NULL then 'ok'
            when gve.ppe_id is NULL and ege.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, но аудиторий ГВЭ нет, зато есть аудитории ЕГЭ'
            when gve.ppe_id is NULL and ege.ppe_id is NULL then 'В ППЭ нет участников'
          end
        when t.saf_name like '%ГВЭ-9%' and t.saf_name not like '%ОГЭ%' then
          case
            when gve.ppe_id is NOT NULL and oge.ppe_id is NULL then 'ok'
            when gve.ppe_id is NULL and oge.ppe_id is NOT NULL then 'У ППЭ указано ГВЭ, но аудиторий ГВЭ нет, зато аудитории ОГЭ'
            when gve.ppe_id is NULL and oge.ppe_id is NULL then 'В ППЭ нет участников'
            when gve.ppe_id is NOT NULL and oge.ppe_id is NOT NULL then 'В ППЭ указано только ГВЭ, но есть аудитории ГВЭ и ОГЭ'
          end
      end as comment
      -- ,gve.af_name gve, ege.af_name ege, oge.af_name oge
      from (select  t.ppe_id, t.ppe_code, t.saf_name from tmp t group by t.ppe_id, t.ppe_code, t.saf_name) t
      left join tmp gve ON gve.ppe_id = t.ppe_id and gve.af_name = 'ГВЭ'
      left join tmp ege ON ege.ppe_id = t.ppe_id and ege.af_name = 'ЕГЭ'
      left join tmp oge ON oge.ppe_id = t.ppe_id and oge.af_name = 'ОГЭ';
    
    SELECT s.id as stid, m.id as mid, o.id as oid, isppe as comment
      from tmp t
      join stations s ON t.ppe_id = s.id
      join ous o ON o.id = s.ou_id
      join mouos m ON m.id = o.mouo_id
      where isppe not in ('содержит', 'ok');

SQL


    ActiveRecord::Base.connection.execute(sql).each do |r|
      add_analytics_item :station_attestation_forms_classrooms, r['stid'], 'Station', r['oid'], r['mid'], r['comment']
    end

  end
end