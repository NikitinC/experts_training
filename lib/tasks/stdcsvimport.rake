require 'csv'

namespace :db do
  desc 'Import Directories'

  task :stdcsvimport => :environment do
    CSV.foreach(Rails.root + "db/students.csv", :encoding => 'cp1251:utf-8', :headers => true, :col_sep => ";") do |row|
      begin
        Student.create!(row.to_hash)
      rescue
        puts "Не удалось загрузить:" + row['document_number']
      end
    end

  end

end
