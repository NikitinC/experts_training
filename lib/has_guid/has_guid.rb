module HasGuid
  def self.included base
    base.class_exec do
      extend BaseClassMethods
    end
  end
  module BaseClassMethods
    def has_guid
      include HasGuid::InstanceMethods
      attr_accessible :guid
      before_create :fill_guid
    end
  end
  module InstanceMethods
    private
    def fill_guid
      self.guid = "{#{UUID.generate}}"
    end
  end
end