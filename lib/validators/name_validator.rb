# coding: utf-8

require 'validators/_cache'

class NameValidator < ActiveModel::EachValidator

  include ValidatorsCache

  def validate_each(record, attribute, value)
    return if get_exceptions.include? value

    unless value =~ /\A[a-zA-Zа-яА-ЯёЁ\-\s\.']+\z/
      record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name"))
    end
  end
end
