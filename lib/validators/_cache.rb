module ValidatorsCache
  def get_exceptions
    Rails.cache.fetch('russian_validators_exceptions_cache',  expires_in: 30.minutes) do
      (PageFragment.get(:russian_validator_exceptions).content || '').lines.map(&:chomp)
    end
  end
end
