# coding: utf-8
class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    #unless value =~ /^([_а-яА-Яa-zA-Z0-9-]+(\.[_a-zA-Zа-яА-Я0-9-]+)*@[a-zа-я0-9-]+(\.[a-zа-я0-9-]+)*(\.[a-zа-я]{2,6}))((,|\s*)[_a-zA-Zа-яА-Я0-9-]+(\.[_a-zA-Zа-яА-Я0-9-]+)*@[a-zа-я0-9-]+(\.[a-zа-я0-9-]+)*(\.[a-zа-я]{2,6}))*$/i
    #unless value =~ /^([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,6}))$/i
    unless value =~ /.+@.+\..+/i
      record.errors[attribute] << (options[:message] || I18n.t("messages.validation.is_not_email"))
    end
  end
end