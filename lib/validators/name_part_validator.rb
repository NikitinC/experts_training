# coding: utf-8

require 'validators/_cache'

class NamePartValidator < ActiveModel::EachValidator

  include ValidatorsCache

  def validate_each(record, attribute, value)
    return if get_exceptions.include? value
    value = value.sub(/\s(кизи|угли|гызы|кызы|оглы|огли|кизы|куинь|уулу|аглы|Уулу|Кизи|Кызы|Оглы|Угли)\z/, '')
    unless value =~ /\A[А-ЯЁ][а-яё]+(-[А-ЯЁ]?[а-яё]+)?\z/
      record.errors[attribute] << (options[:message] || I18n.t("messages.validation.must_be_a_name"))
    end
  end
end
