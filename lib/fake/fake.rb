Dir[File.join(Rails.root, "spec/factories/**/*")].each { |file| require file }

def fake_ous n = 3, ate = nil, mouo = nil
  mouo ||= FactoryGirl.create(:mouo)
  ate ||= FactoryGirl.create(:ate)
  ous = []
  n.times do
    ous << FactoryGirl.create(:ou, {
        mouo: mouo, ate: ate
    })
  end
  ous
end

def fake_public_observers n = 10, mouo = nil
  public_observers = []
  n.times do
    #public_observer = FactoryGirl.create(:public_observer)
    public_observers << FactoryGirl.create(:public_observer, {mouo: mouo})
  end
  public_observers
end

def fake_groups n = 3, ou = nil
  ou ||= FactoryGirl.create(:ou)
  groups = []
  n.times do
    groups << FactoryGirl.create(:group, {ou: ou})
  end
  groups
end

def fake_students n = 3, group = nil
  group ||= FactoryGirl.create(:group)
  students = []
  n.times do
    students << FactoryGirl.create(:student, {group: group})
  end
  students
end

def fake_employees n = 3, ou = nil
  ou ||= FactoryGirl.create(:ou)
  employees = []
  n.times do
    employees << (employee = FactoryGirl.create(:employee))
    FactoryGirl.create(:employee_ou, {:employee => employee, :ou => ou})
  end
  employees
end

def fake_admin_user
  u = User.new(:password => "admin")
  u.login = "admin"
  u.target_type = "Rcoi"
  u.save
end

def fake_exam_types n = 1
  ets = []
  n.times do
    et = FactoryGirl.create(:exam_type)
    rand(8..12).times do
      FactoryGirl.create(:exam, {:exam_type => et})
    end
    ets << et
  end
  ets
end

def fake_appeal n = 10
  appeals = Array.new
  n.times do
    appeals << FactoryGirl.create(:appeal)
  end
  appeals
end

def fake_all n_ous = 3, n_groups = 3, n_students = 10, n_employees = 10
  ous = fake_ous(n_ous)
  ous.each do |ou|
    groups = fake_groups(n_groups, ou)
    groups.each do |group|
      students = fake_students(n_students, group)
    end
    employees = fake_employees(n_employees, ou)
  end
  fake_admin_user
  fake_exam_type
  fake_public_observers
end