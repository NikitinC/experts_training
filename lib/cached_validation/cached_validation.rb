module CachedValidation
  def self.included base
    base.send :extend, ClassMethods
  end

  module ClassMethods
    def has_cached_validation
      include CachedValidation::InstanceMethods
      before_save :update_valid_cache

      scope :invalid, where(:valid_cache => false)
      scope :valid, where(:valid_cache => true)
    end

  end

  module InstanceMethods
    def update_valid_cache
      return unless self.perform_validations
      self.send :write_attribute, :valid_cache, self.valid?
      self.errors.clear
    end
  end
end