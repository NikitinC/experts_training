module DirectoryAssociationMethods
  def self.included(base)
    base.send :extend, ClassMethods
  end

  module ClassMethods
    def belongs_to_directory(name)
      belongs_to name, foreign_key: "#{name}_code", primary_key: :code
    end

    def directory_has_many(name, options = {})
      has_many name, {foreign_key: "#{self.name.underscore}_code", primary_key: :code}.merge(options)
    end
  end
end