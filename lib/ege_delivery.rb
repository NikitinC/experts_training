class EgeDelivery < Mail::SMTP

  def initialize(options)
    self.settings = ActionMailer::Base.smtp_settings
  end

  def deliver!(mail)
    user_id = User.where(:email => mail.to).first.try(:id)
    mail.subject = mail.body = "Invalid user email" unless user_id
    Notification.create(:user_id => user_id, :title => mail.subject, :content => mail.body.to_s)
    super(mail)
  end

end