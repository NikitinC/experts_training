module Places
  def filename
    '66_Places.cs_'
  end

  def process **args
    args[:classrooms].each do |c|

      # if c.classroom_verbal_part_sing_code
      #   (c.rows_count * c.seats_per_row).times do |i|
      #     row = i / c.seats_per_row + 1
      #     number = i % c.seats_per_row + 1
      #
      #     line(
      #         c.station.guid, # 1. GUID ППЭ
      #         c.number, # 2. Номер аудитории
      #         row, # 3. Номер ряда
      #         number, # 4. Порядковый номер посадочного места в ряду
      #         (i + 1) > c.available_seats ? 1 : 0, # 5. Признак исключения из рассадки
      #         1 # 6. Признак устной части
      #     )
      #   end
      # else
        (c.rows_count * c.seats_per_row - c.available_seats).times do |i|
          line(
              c.station.guid, # 1. GUID ППЭ
              c.number, # 2. Номер аудитории
              c.rows_count - i, # 3. Номер ряда
              c.seats_per_row, # 4. Порядковый номер посадочного места в ряду
              1, # 5. Признак исключения из рассадки
              0 # 6. Признак устной части
          )
        end
      # end

    end
  end
end
