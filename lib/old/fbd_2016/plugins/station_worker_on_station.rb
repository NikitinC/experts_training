module StationWorkerOnStation
  def filename
    '66_StationWorkerOnStation.cs_'
  end

  def process **args

    ExamStationEmployee \
        .where(station_id: args[:station_ids])
        .find_each do |ese|
      station_post_code = transform_station_post_code(ese.station_post_code)
      station_id = ese.station_id

      next unless args[:employee_ids].include? ese.employee_id

      if station_id && station_post_code
        line(
            ese.employee.guid, # 1. GUID работника ППЭ
            ese.station.guid, # 2. GUID ППЭ
            0, # 3. Признак прикрепления      !! deal with it !!!
            station_post_code, # 4. Код должности в ППЭ
            transform_time(ese.created_at), # 5. Дата-время создания
            transform_time(ese.created_at), # 6. Дата-время создания
        )
      end
    end
  end
end