module Auditoriums
  def filename
    '66_Auditoriums.cs_'
  end

  def process **args

    args[:classrooms].each do |c|

      line(
          c.guid, # 1. GUID
          c.hidden ? 1 : 0, # 2. Признак удаленной строки
          classroom_name(c), # 3. Наименование аудитории
          c.number, # 4. Номер аудитории
          c.rows_count, # 5. Количество рядов в аудитории
          c.seats_per_row, # 6. Количество посадочных мест в ряду
          c.station.guid, # 7. GUID ППЭ
          transform_bool(c.organizer_order), # 8. Расположение рядов в аудитории
          transform_bool(c.special_sit), # 9. Признак специализированной рассадки
          transform_time(c.created_at), # 10. Дата-время создания
          transform_time(c.updated_at), # 11. Дата-время обновления
          c.cctv.present? ? 1 : 0, # 12. Наличие видеонаблюдения
          c.classroom_verbal_part_sing_code || 0 # 13. Признак устной части
      )
    end
  end

  def classroom_name c
    c.name.blank? ? c.number.to_s : c.name
  end

end
