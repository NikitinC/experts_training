module Stations
  def filename
    '66_Stations.cs_'
  end

  def process **args
      args[:stations].each do |s|
        fail "No ou for ppe #{s.id}" if s.ou.nil?

        line(
            s.guid, # 1. GUID
            (s.hidden.present? ? 1 : 0), # 2. Признак удаленной строки
            s.code, # 3. Код ППЭ
            s.fbd_name, # 4. Наименование ППЭ
            s.in_home? ? '' : s.ou.guid, # 5. GUID ОУ, на территории которого находится ППЭ
            nil, #TODO 6. GUID ППОИ, на территории которого обрабатываются бланки ППЭ
            prepare_phone(s.phones), # 7. Телефон(ы) ППЭ
            s.email, # 8. Адрес(а) электронной почты ППЭ
            transform_bool(s.tom), # 9. Признак расположения ППЭ в ТОМ
            s.ou.mouo.guid, # 10. GUID МОУО, к которому относится ППЭ
            s.ou.ate.guid, # 11. GUID АТЕ, на территории которой расположен ППЭ
            transform_time(s.created_at), # 12. Дата-время создания
            transform_time(s.updated_at), # 13. Дата-время обновления
            station_cctv(s), # 14. Наличие видеонаблюдения
            s.station_exam_form_code, # todo WTF # 15. Форма экзамена
            station_additional_features(s),  #16 Доп. Признаки ППЭ
            station_address(s) # 17. Адрес ППЭ
        )
      end
  end

  # 1– ППЭ на дому
  # 2- Независимый центр подготовки
  # 4– ЕГЭ
  # 8- ГВЭ
  # 16- Родной язык/литература
  def station_additional_features(station)
    transform_additional_features_to_additional_features(station.station_additional_feature) +
        transform_station_type_code_to_additional_feature_code(station.station_type_code)
  end

  # из-за косяка проектирования пришлось написать этот бред
  def transform_additional_features_to_additional_features(code)
    {
        4 => 4,
        8 => 8,
        14 => 4,
        18 => 8
    }[code] || 0
  end

  def transform_station_type_code_to_additional_feature_code(code)
    {
        1 => 0,
        2 => 1,
        3 => 2
    }[code] || 0
  end

  def station_address(station)
    station.in_home? ? station.address_guid : station.ou.try(:address_guid)
  end

  def station_cctv station
    station.hq_cctv.present? || station.corridor_cctv.present? || station.entrance_cctv.present? ? 1 : 0
  end
end
