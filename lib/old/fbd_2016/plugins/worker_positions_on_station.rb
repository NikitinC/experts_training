module WorkerPositionsOnStation
  def filename
    '66_WorkerPositionsOnStation.cs_'
  end

  def process **args
    ExamStationEmployee
        .includes('exam_station')
        .where({
                   exam_stations: {
                       station_id: args[:station_ids], hidden: nil,
                   },
                   exam_type_id: args[:exam_types]
              })
        .find_each do |ese|

      next unless args[:employee_ids].include? ese.employee_id

      exam_code = ese.exam_station.try(:exam).try(:code)
      station_post_code = transform_station_post_code(ese.station_post_code)
      if exam_code && station_post_code
        line(
            ese.employee.guid, # 1. GUID работника ППЭ
            exam_code, # 2. Код дня экзамена
            ese.exam_station.station.guid, # 3. GUID ППЭ
            station_post_code, # 4. Код должности в ППЭ
            transform_time(ese.created_at), # 5. Дата-время создания
            nil # 6. Код роли организатора
        )
      end
    end

    # ExamStationPublicObserver.find_each do |espo|
    #   exam_code = federal_exam_code(espo.exam_station.try :exam)
    #   station_post_code = 6
    #   if exam_code
    #     line(
    #         espo.public_observer.guid, # 1. GUID работника ППЭ
    #         exam_code, # 2. Код дня экзамена
    #         espo.exam_station.station.guid, # 3. GUID ППЭ
    #         station_post_code, # 4. Код должности в ППЭ
    #         transform_time(espo.created_at), # 5. Дата-время создания
    #         nil # 6. Код роли организатора
    #     )
    #   end
    # end
  end
end