module ParticipantExamsOnStation
  def filename
    '66_ParticipantExamsOnStation.cs_'
  end

  def process **args
    # ExamStudent \
    #       .includes('exam')
    #     .where({exams: {exam_type_id: args[:exam_types], hidden: nil}})
    #     .where(student_id: args[:students])
    #     .all.each do |es|

    sql = <<SQL
select es.id, s.guid as sguid, e.code, st.guid as stguid, es.created_at from exam_students es
join exams e on e.id = es.exam_id
join students s on s.id = es.student_id
join exam_stations est on est.id = es.exam_station_id
join stations st on st.id = est.station_id
join groups g on g.id = s.group_id and g.hidden is null
join ous o on o.id = g.ou_id and o.hidden is null and o.code <> '111111'

where es.hidden is null and e.hidden is null and s.hidden is null and est.hidden is null and st.hidden is null

and e.exam_type_id in (#{args[:exam_types].join(',')})
and es.student_id in (#{args[:student_ids].join(',')})
and e.id not in (#{args[:VERBAL_EXAMS].join(',')})
SQL
    ActiveRecord::Base.connection.execute(sql).each do |es|
      fail "no code #{es["id"]}" if es['code'].nil?
      fail "no created_at #{es["id"]}" if es['created_at'].empty?

      line es['sguid'], # 1. guid участника
           es['code'], # 2. Код дня экзамена
           es['stguid'], # 3. GUID ППЭ
           transform_time(DateTime.parse(es['created_at'])) # 4 Дата-время создания
    end
  end
end
