module ParticipantsExams
  def filename
    '66_ParticipantsExams.cs_'
  end


  def process **args
    composition_id_to_fct_code = Hash[Composition.all.map{|x| [x.id, x.fct_code]}]

    sql = <<SQL
select s.guid as guid, c.fct_code as fct_code, cs.created_at as created_at from composition_students cs
join compositions c on cs.composition_id = c.id
join students s on s.id = cs.student_id

where c.hidden is null and s.hidden is null and cs.hidden is null

and cs.student_id in (#{args[:student_ids].join(',')})
SQL

    ActiveRecord::Base.connection.execute(sql).each do |es|
      line  es['guid'], # 1. GUID участника
            es['fct_code'], # 2. Код дня экзамена
            transform_time(DateTime.parse(es['created_at'])) # 3. Дата-время создания
    end


    sql = <<SQL
select s.guid as sguid, e.code, es.created_at from exam_students es
join exams e on e.id = es.exam_id
join students s on s.id = es.student_id

where es.hidden is null and e.hidden is null and s.hidden is null

and e.exam_type_id in (#{args[:exam_types].join(',')})
and es.student_id in (#{args[:student_ids].join(',')})
SQL
    ActiveRecord::Base.connection.execute(sql).each do |es|
      fail 'no code #{es["id"]}' if es['code'].empty?
      fail 'no created_at #{es["id"]}' if es['created_at'].empty?

      line es['sguid'], # 1. guid участника
           es['code'], # 2. Код дня экзамена
           transform_time(DateTime.parse(es['created_at'])) # 4 Дата-время создания
    end


  end
end
