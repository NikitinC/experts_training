module StationWorkers
  def filename
    '66_StationWorkers.cs_'
  end

  def process **args

      Employee.find_each do |e|
        mouo_guid = e.main_ou.try(:mouo).try(:guid)
        mouo_guid = e.mouo.try(:guid) if mouo_guid.nil?

        next unless args[:employee_ids].include? e.id

        line(
            e.guid, # 1. GUID
            transform_hidden(e.hidden), # 2. Признак удаленной строки
            nil, # 3. Код работника ППЭ
            e.second_name, # 4. Фамилия
            e.first_name, # 5. Имя
            e.middle_name, # 6. Отчество
            e.main_ou.try(:guid) || e.ous.first.try(:guid), # 7. GUID ОУ основного места работы
            e.employee_post.try(:name) || e.job_post, # 8. Должность по основному месту работы
            (e.main_ou.try(:guid) || e.ous.first.try(:guid)) ? nil : e.job_place, # 9. Основное место работы (если не ОУ)
            mouo_guid, # 10. GUID МОУО, к которому прикреплён работник
            e.document_series, # 11. Серия документа
            e.document_number, # 12. Номер документа
            e.document_type.nil? ? 7 : e.document_type.fct_ege_code, # 13. Код типа документа, удостоверяющего личность
            gender(e.gender), # 14. Пол
            e.birthday.nil? ? 1981 : e.birthday.year, # 15. Год рождения
            transform_time(e.created_at), # 16. Дата-время создания
            transform_time(e.updated_at), # 17. Дата-время изменения
            transform_bool(e.took_part_in_ege), # 18. Признак участия в ЕГЭ прошлых лет
            e.teaching_experience, # 19. Общий преподавательский стаж работы
            e.diploma_speciality, #20 Квалификация работника ППЭ
            e.station_post_code, #21 Код Возможной должности в ППЭ
            nil #22 Код уровня проф. образования
        )
      end

      # PublicObserver.where(valid_cache: true).joins(exam_station_public_observers: :exam)
      #     .where(exam_station_public_observers: {hidden: nil}, exams: {exam_type_id: current_exam_type_ids}).uniq
      #     .find_each do |public_observer|
      #
      #   possible_post_code = 6
      #
      #   line(
      #       public_observer.guid, # 1. GUID
      #       transform_hidden(public_observer.hidden), # 2. Признак удаленной строки
      #       nil, # 3. Код работника ППЭ
      #       public_observer.second_name, # 4. Фамилия
      #       public_observer.first_name, # 5. Имя
      #       public_observer.middle_name, # 6. Отчество
      #       nil, # 7. GUID ОУ основного места работы
      #       'Общественный наблюдатель', # 8. Должность по основному месту работы
      #       'Общественный наблюдатель', # 9. Основное место работы (если не ОУ)
      #       nil, # 10. GUID МОУО, к которому прикреплён работник
      #       public_observer.document_series, # 11. Серия документа
      #       public_observer.document_number, # 12. Номер документа
      #       transform_document_code(public_observer.document_type_code), # 13. Код типа документа, удостоверяющего личность
      #       nil, # 14. Пол
      #       public_observer.birthday.year, # 15. Год рождения
      #       transform_time(public_observer.created_at), # 16. Дата-время создания
      #       transform_time(public_observer.updated_at), # 17. Дата-время изменения
      #       nil, # 18. Признак участия в ЕГЭ прошлых лет
      #       nil, # 19. Общий преподавательский стаж работы
      #       nil, #20 Квалификация работника ППЭ
      #       possible_post_code, #21 Код Возможной должности в ППЭ
      #       nil #22 Код уровня проф. образования
      #   )
      # end
    end
end