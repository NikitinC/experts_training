module Address
  def filename
    '66_Address.cs_'
  end

  def process **args
    args[:ous].each do |item|
      fail "Address guid is nil - #{item.id}" if item.address_guid.nil?
      fail "Address is blank id  - #{item.id}" if blank_address? item

      line(
          item.address_guid, # 1. GUID адреса
          item.postcode, # 2. Индекс
          item.human_settlement_type_code, # 3. Код типа населенного пункта
          item.settlement_name, # 4. Наименование населенного пункта
          item.street_type_code, # 5. Код типа улицы
          item.street_name, # 6. Наименование улицы
          item.building_type_code, # 7. Код типа строения
          item.building_number, # 8. Номер здания
          item.ate.try(:okato), # 9. Код ОКАТО района
          item.created_at, # 10. Дата-время создания
          item.updated_at, # 11. Дата-время изменения
      )

      line(
          item.legal_address_guid, # 1. GUID адреса
          item.legal_postcode, # 2. Индекс
          item.legal_human_settlement_type_code, # 3. Код типа населенного пункта
          item.legal_settlement_name, # 4. Наименование населенного пункта
          item.legal_street_type_code, # 5. Код типа улицы
          item.legal_street_name, # 6. Наименование улицы
          item.legal_building_type_code, # 7. Код типа строения
          item.legal_building_number, # 8. Номер здания
          item.ate.try(:okato), # 9. Код ОКАТО района
          item.created_at, # 10. Дата-время создания
          item.updated_at, # 11. Дата-время изменения
      )
    end

    args[:stations].select{|x| x.is_in_home }.each do |item|
      next unless args[:station_ids].include? item.id

      line(
          item.address_guid, # 1. GUID адреса
          item.postcode, # 2. Индекс
          item.human_settlement_type_code, # 3. Код типа населенного пункта
          item.settlement_name, # 4. Наименование населенного пункта
          item.street_type_code, # 5. Код типа улицы
          item.street_name, # 6. Наименование улицы
          item.building_type_code, # 7. Код типа строения
          item.building_number, # 8. Номер здания
          item.ou.try(:ate).try(:okato), # 9. Код ОКАТО района
          item.created_at, # 10. Дата-время создания
          item.updated_at, # 11. Дата-время изменения
      )
    end

  end


  def blank_address?(ou)
    ou.ate.try(:okato).blank? ||
        ou.postcode.blank? ||
        ou.human_settlement_type_code.blank? ||
        ou.settlement_name.blank? ||
        ou.street_type_code.blank? ||
        ou.street_name.blank? ||
        ou.building_type_code.blank? ||
        ou.building_number.blank? ||
        ou.legal_postcode.blank? ||
        ou.legal_human_settlement_type_code.blank? ||
        ou.legal_settlement_name.blank? ||
        ou.legal_street_type_code.blank? ||
        ou.legal_street_name.blank? ||
        ou.legal_building_type_code.blank? ||
        ou.legal_building_number.blank?
  end
end
