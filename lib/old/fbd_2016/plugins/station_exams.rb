module StationExams

  def filename
    '66_StationExams.cs_'
  end

  def process **args
    ExamStation \
      .where(station_id: args[:station_ids])
      .find_each do |es|
        fail "Station is nil - #{es.id}" if es.station.nil?
	next unless args[:exam_types].include? es.exam.exam_type_id

        line(
            es.station.guid,
            es.exam.code,
            # es.classrooms.select{|x| args[:classroom_ids].include? x.id}.map(&:capacity).sum,
            ExamStationClassroom \
                .where(exam_station_id: es).all
                .map(&:classroom)
                .select{|x| args[:classroom_ids].include? x.id}
                .map(&:capacity).sum,
            transform_time(es.created_at)
        )
    end
  end
end
