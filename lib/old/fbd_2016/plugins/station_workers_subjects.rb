module StationWorkersSubjects
  def filename
    '66_StationWorkersSubjects.cs_'
  end

  def process **args
    sql = <<SQL
select distinct e.guid as guid, eos.subject_code as code from employees e
join employee_ous eo on eo.employee_id = e.id and eo.hidden is null
join employee_ou_subjects eos on eos.employee_ou_id = eo.id and eos.hidden is null

where e.hidden is null
and eos.subject_code in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 18)

and e.id in (#{args[:employee_ids].join(',')})
SQL
    ActiveRecord::Base.connection.execute(sql).each do |es|
      line es['guid'], # 1. guid
           es['code'] # 2. Код образовательного предмета
    end
  end
end
