module AuditoriumExams
  def filename
    '66_AuditoriumExams.cs_'
  end

  def process **args

    def verbal_code(esc, verbal_exams)
      if verbal_exams.include?(esc.exam_station.exam.code.to_i)
        return esc.classroom.classroom_verbal_part_sing_code == 2 ? "0" : "1"
      end
      ""
    end

    stations_to_export = *args[:stations].map(&:id)

    ExamStationClassroom \
        .where(classroom_id: args[:classrooms].map(&:id))
        .find_each do |esc|

      next if esc.exam_station.nil? || esc.classroom.nil?
      next unless args[:exam_types].include? esc.exam_station.exam.exam_type_id
      next if esc.exam_station.exam.subject_code > 50

      line(
          esc.classroom.station.guid, # 1. GUID ППЭ
          esc.classroom.number, # 2. номер аудитории
          esc.exam_station.exam.code, # 3. Код дня экзамена
          esc.classroom.capacity, # 4. Количество мест на экзамен
          transform_time(esc.created_at), # 5. Дата-время создания
          transform_time(esc.updated_at), # 6. Дата-время изменения
          verbal_code(esc, args[:VERBAL_EXAMS])
      )
    end
  end
end
