# encoding: UTF-8

require "fileutils"

namespace :fbd_2016 do
  task :run => :environment do |t, args|
    type = 'ege'
    exam_types = [19]
    VERBAL_EXAMS = [765, 773, 775, 781, 784, 720, 725, 726, 745, 800, 801, 805, 729, 735, 747, 752, 755, 813, 743, 761, 772, 776, 803, 804] # ids of exams
    include_plugins = args.extras.count > 0 && args.extras

    attestation_forms = (type == 'ege' ?
        [AttestationForm::EGE, AttestationForm::GVE11] :
        [AttestationForm::OGE, AttestationForm::GVE9])

    classroom_attestation_form = (type == 'ege' ? [AttestationForm::EGE] : [AttestationForm::OGE])
    classroom_attestation_form = [AttestationForm::GVE9, AttestationForm::OGE]

    path = 'log/fbd_2016/'
    FileUtils.rmtree path
    FileUtils.mkdir path

    ActiveRecord::Base.uncached do
      students =
          Student \
            .includes('attestation_form_students')
            .where({attestation_form_students: {attestation_form_code: attestation_forms, hidden: nil}})
            .all

      student_ids = students.map(&:id)

      ous = Ou.all
      ous = ous.reject{|x| x.code.end_with? '0000'} if type == 'oge'
      stations = Station\
                     .includes('exam_types')  # .where({exam_types: {id: exam_types}})
                     .select{|x| type == 'ege' ? x.is_ege_or_gve11 : x.is_oge_or_gve9}
                     .reject{|x| x.code == '8223'}

      station_ids = stations.map(&:id)
      classrooms = Classroom.all
                       .reject{|c| c.station.nil?}
                       .select{|c| station_ids.include? c.station.id}
                       .select{|c| (c.attestation_form_classrooms.map(&:attestation_form_code) & classroom_attestation_form).length > 0}
                       .reject{|x| x.name.include? 'РТ'}
      classroom_ids = classrooms.map(&:id)

      sql = <<SQL
select distinct e.id from employees e
    left join exam_station_employees ese on ese.employee_id = e.id and ese.hidden is null
    left join exam_stations es on es.id = ese.exam_station_id and es.hidden is null
    left join stations s on s.id = es.station_id and s.hidden is null
    where e.hidden is null
    -- and (e.station_post_code = 5 or ese.station_post_code = 5)
    and s.id in (#{station_ids.join(',')})
SQL
      employee_ids = ActiveRecord::Base.connection.execute(sql).map{|x| x['id'].to_i}

      plugins_path = File.join(File.dirname(__FILE__), 'plugins')

      Dir.foreach plugins_path do |file|
        next unless file.end_with? '.rb'

        plugin = file.sub(/.rb$/, '')
        next if include_plugins and not include_plugins.include?(plugin)
        next if args.has_key?(:plugin_to_execute) && args[:plugin_to_execute] != plugin
        require File.join(plugins_path, file)

        base = Base.new
        base.extend Object.const_get(plugin.camelize)
        base.open_file
        base.process students: students,
                     student_ids: student_ids,
                     ous: ous,
                     stations: stations,
                     exam_types: exam_types,
                     attestation_forms: attestation_forms,
                     type: type,
                     station_ids: station_ids,
                     classrooms: classrooms,
                     classroom_ids: classroom_ids,
                     employee_ids: employee_ids,
                     VERBAL_EXAMS: VERBAL_EXAMS
      end

    end
  end


  class Base

    def initialize
      @counter = 0
    end

    def open_file
      path = 'log/fbd_2016/'
      @file = File.open(path + self.filename, "w:cp1251")
    end

    def line *args
      @file.write(args.map { |el| escape_for_export(el) }.join("#"))
      @file.write("\n")
      puts "#{self.filename} - #{@counter}" if @counter % 1000 == 0
      @counter += 1
    end







    def escape_for_export s
      return "" if s.nil?
      s = s.to_s.gsub('#', '№')
      s = s.gsub('\'', '"')
      s = s.gsub('%', '0/0')
      s = s.gsub('_', '-')
      s = s.gsub(/[\x00-\x1f]/, '-')
    end



    def gender g
      g = true if g.nil?
      g ? "М" : "Ж"
    end


    def transform_time time
      time.strftime("%Y-%m-%d %H-%M-%S")
    end

    def transform_hidden hidden
      hidden.nil? ? 0 : 1
    end


    def prepare_email email
      fail if email.blank?
      email.to_s.split(/[\s,]/).join(";").gsub(/;+/, ";")
    end

    def prepare_phone phone
      phone.nil? ? "Нет" : phone.gsub(" ", "");
    end

    def transform_bool bool
      bool ? 1 : 0
    end

    def transform_station_post_code(code)
      {
          1 => 1,
          2 => 2,
          3 => 3,
          4 => 4,
          5 => 5,
          7 => 7,
          8 => nil,
          51 => nil
      }[code.to_i]
    end


  end

end
