namespace :fbd_2014 do
  task :stations => :prepare do
    ActiveRecord::Base.uncached do
      Station.unscoped.valid.find_each do |s|

        spo_exceptions_ids = [513]
        spo_mouo_codes = [95, 96, 97]

        next if s.ou.nil?
        spo = spo_mouo_codes.include?(s.ou.mouo.try(:code)) && !spo_exceptions_ids.include?(s.id)
        have_exams = s.exam_stations.where(:exam_id => federal_exam_codes.keys).any?

        #todo think
        next if !have_exams || s.hidden.present?

        line(
            s.guid, # 1. GUID
            (s.hidden.present? || spo || !have_exams ? 1 : 0), # 2. Признак удаленной строки
            s.code, # 3. Код ППЭ
            s.fbd_name, # 4. Наименование ППЭ
            s.ou.real_address, # 5. Адрес ППЭ
            s.ou.guid, # 6. GUID ОУ, на территории которого находится ППЭ
            nil, #TODO 7. GUID ППОИ, на территории которого обрабатываются бланки ППЭ
            prepare_phone(s.phones), # 8. Телефон(ы) ППЭ
            s.email, # 9. Адрес(а) электронной почты ППЭ
            transform_bool(s.tom), # 10. Признак расположения ППЭ в ТОМ
            s.ou.mouo.guid, # 11. GUID МОУО, к которому относится ППЭ
            s.ou.ate.guid, # 12. GUID АТЕ, на территории которой расположен ППЭ
            transform_time(s.created_at), # 13. Дата-время создания
            transform_time(s.updated_at), # 14. Дата-время обновления
            station_cctv(s), # 15. Наличие видеонаблюдения
            0 # todo WTF
        )
      end
    end
  end

  def station_cctv station
    station.hq_cctv.present? || station.corridor_cctv.present? || station.entrance_cctv.present? ? 1 : 0
  end
end
