# coding: utf-8
require 'json'

namespace :fbd_2014 do
  task :auditorium_exams => :prepare do

    ActiveRecord::Base.uncached do

      ExamStationClassroom.find_each do |esc|
        next if esc.exam_station.nil? || esc.classroom.nil? || esc.classroom.station.nil?

        code = federal_exam_code(esc.exam_station.exam)

        next if code.nil?

        line(
            esc.classroom.station.guid, # 1. GUID ППЭ
            esc.classroom.number, # 2. номер аудитории
            code, # 3. Код дня экзамена
            esc.classroom.capacity, # 4. Количество мест на экзамен
            transform_time(esc.created_at), # 5. Дата-время создания
            transform_time(esc.updated_at) # 6. Дата-время изменения
        )

      end
    end
  end
end