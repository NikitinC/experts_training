namespace :fbd_2014 do
  task :station_exams => :prepare do

    ExamStation.find_each do |es|
      exam_code = federal_exam_code es.exam

      next if es.station.nil? || exam_code.nil?

      line(
          es.station.guid,
          exam_code,
          es.capacity,
          transform_time(es.created_at)
      )
    end
  end
end