namespace :fbd_2014 do
  task :places => :prepare do
    ActiveRecord::Base.uncached do

      # При повторной передаче данные будут перезаписаны
      Classroom.valid.find_each do |c|
        next if c.station.nil?
        next if c.rows_count.nil? || c.seats_per_row.nil? || c.available_seats.nil?

        #todo think
        next if c.station.exam_stations.where(:exam_id => federal_exam_codes.keys).count.zero?

        (c.rows_count * c.seats_per_row - c.available_seats).times do |i|
          line(
              c.station.guid, # 1. GUID ППЭ
              c.number, # 2. Номер аудитории
              c.rows_count - i, # 3. Номер ряда
              c.seats_per_row, # 4. Порядковый номер посадочного места в ряду
              1 # 5. Признак исключения из рассадки
          )

        end
      end
    end
  end
end
