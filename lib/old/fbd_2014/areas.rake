namespace :fbd_2014 do
  task :areas => :prepare do
    Ate.where(valid_cache: true).find_each do |ate|
      line_array = [
          ate.guid, # 1. GUID
          transform_hidden(ate.hidden), # 2. Признак удаленной строки
          ate.code, # 3. Код административно-территориальной единицы
          ate.name, # 4. Наименование административно-территориальной единицы
          "", # 5. ФИО сотрудника, ответственного за проведение ЕГЭ
          "", # 6. Телефон(ы) сотрудника, ответственного за проведение ЕГЭ
          "" # 7. Адрес(а) электронной почты сотрудника, ответственного за проведение ЕГЭ
      ]
      puts line_array.map { |el| escape_for_export(el) }.join("#")
    end
  end
end