namespace :fbd_2014 do
  task :station_workers => :prepare do
    Employee.unscoped.where(valid_cache: true).joins(:exam_station_employees => :exam_station)
    .where(:exam_station_employees => {:hidden => nil}, :exam_stations => {:exam_id => federal_exam_codes.keys}).uniq.find_each do |e|
      line e.guid, # 1. GUID
           transform_hidden(e.hidden), # 2. Признак удаленной строки
           nil, # 3. Код работника ППЭ
           e.second_name, # 4. Фамилия
           e.first_name, # 5. Имя
           e.middle_name, # 6. Отчество
           e.main_ou.try(:guid), # 7. GUID ОУ основного места работы
           e.employee_post.try(:name), # 8. Должность по основному месту работы
           nil, # 9. Основное место работы (если не ОУ)
           e.main_ou.try(:mouo).try(:guid), # 10. GUID МОУО, к которому прикреплён работник
           e.document_series, # 11. Серия документа
           e.document_number, # 12. Номер документа
           transform_document_code(e.document_type_code), # 13. Код типа документа, удостоверяющего личность
           gender(e.gender), # 14. Пол
           e.birthday.year, # 15. Год рождения
           transform_time(e.created_at), # 16. Дата-время создания
           transform_time(e.updated_at), # 17. Дата-время изменения
           nil, # 18. Признак участия в ЕГЭ прошлых лет
           e.teaching_experience, # 19. Общий преподавательский стаж работы
           nil, #20 Квалификация работника ППЭ
           nil, #21 Код Возможной должности в ППЭ
           nil  #22 Код уровня проф. образования
    end
  end
end