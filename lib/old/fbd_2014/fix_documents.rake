#coding: utf-8
namespace :fbd_2014 do
  task :fix_documents => :prepare do
    Student.uncached do
      index = 0
      Student.find_each do |student|

        student.document_series.gsub!(/\s/, "") # Удаляем пробельные символы
        code = student.document_type_code
        size = Student.count

        english_to_russian = {
            "A" => "А",
            "B" => "В",
            "C" => "С",
            "E" => "Е",
            "Y" => "У",
            "K" => "К",
            "H" => "Н",
            "X" => "Х",
            "P" => "Р",
            "O" => "О",
            "M" => "М",
            "T" => "Т",
        }

        if (code == 11) # документы только с кириллицей  (военный билет)
                        #перевод в заглавные
          student.document_series = student.document_series.mb_chars.upcase
          #замена русских букв
          english_to_russian.each do |k, v|
            student.document_series.gsub!(k, v)
          end
        end

        if (code == 5 || (7..8) === code || (58..63) === code) #для документов с произвольной серией
          unless student.document_series =~ /(^[А-Яа-я0-9-]*$)|(^[A-Za-z0-9-]*$)|(^[А-Яа-яIiVvXxLlCcDdMm-]*$)/
            #если не удовлетворяет маске то заменим все анлийские буквы на русские
            english_to_russian.each do |k, v|
              student.document_series.gsub!(k, v)
            end
          end
        end

        student.document_number.gsub!(/\s/, "") # Удаляем пробельные символы

        index += 1
        if student.save
        else
          student.save(:validate => false)
          puts "#{index}/#{size} error id=#{student.id}"
        end

      end
    end
  end

end
