# coding: utf-8

namespace :fbd_2014 do
  task participants: :prepare do
    ActiveRecord::Base.uncached do
      Student.unscoped.joins(:exam_students)
      .where(exam_students: {exam_id: federal_exam_codes.keys, hidden: nil})
      .uniq.find_each do |s|
        #.where(groups: {number: [11, 12]})  #Только 11 и 12 классы (предварительная загрузка)

        hidden = s.hidden.present?

        #todo thing about it
        next if hidden

        #todo thing about it
        #закомментировано для oge
        participant_category_code = transform_participant_category_code(s.ege_participant_category_code)
        # next unless participant_category_code

        line(
            s.guid, # 1. GUID
            (hidden ? 1 : 0), # 2. Признак удаленной строки
            '', # 3. Код участника TODO: Реализовать
            s.second_name, # 4. Фамилия
            s.first_name, # 5. Имя
            s.middle_name, # 6. Отчество
            s.document_series, # 7. Серия документа
            s.document_number, # 8. Номер документа
            transform_document_code(s.document_type_code), # 9. Тип документа
            gender(s.gender), # 10. Пол
            "#{s.group.number} #{s.group.letter}", # 11. Класс
            s.birthday && I18n.localize(s.birthday), # 12. Дата рождения
            special_sit(s), # 13. Признак специализированной рассадки
            participant_category_code, # 14. Код категории участника
            transform_region_code(s), # 15. Код субъекта Российской Федерации, в котором участник закончил ОУ
            s.group.try(:ou).try(:guid), # 16. GUID ОУ, основное ОУ участника (основное место регистрации), Уникальный идентификатор ОУ участника по ТЗ
            s.group.try(:ou).try(:guid), # 17. GUID ОУ, выпускное ОУ, Уникальный идентификатор ОУ участника по ТЗ
            transform_time(s.created_at), # 18. Дата-время создания
            transform_time(s.updated_at), # 19. Дата-время обновления
            transform_attestation_form_code(s.attestation_form_code), # 20.Форма ГИА
            transform_gia_active_results(s), # 21. Действующие результаты ГИА
            transform_study_form_code(s.study_form_code), # 22. Код формы обучения
            transform_citizenship_code(s.citizenship_code) # 23. Гражданство
        )
      end
    end
  end

  def transform_region_code(s)
    return s.region_code if s.graduate_of_other_ou
    '66'
  end

  def transform_gia_active_results(s)
    return 0 if !s.gia_active_results_math && !s.gia_active_results_rus
    return 1 if !s.gia_active_results_math && s.gia_active_results_rus
    return 2 if s.gia_active_results_math && !s.gia_active_results_rus
    return 3 if s.gia_active_results_math && s.gia_active_results_rus
  end

  def transform_attestation_form_code(code)
    {
        1 => 3,
        2 => 2,
        3 => 0,
        4 => 0,
        5 => 0,
    }[code.to_i]

    # 1 – означает, что участник проходит ГИА в текущем году в форме ЕГЭ.
    # 2 – означает, участник проходит участник проходит что ГИА в форме ГВЭ;
    # 3 – означает, что участник проходит ГИА в форме ЕГЭ и ГВЭ;
    # 5 – означает, что участник проходит ГИА в форме ЕГЭ и Другой форме;
  end

  def transform_gia_results_code(gia_results_code)
    gia_results_code ? gia_results_code - 1 : 0 # TODO:  Согласовать, что будет если  gia_results_code == nil
  end

  def transform_study_form_code(study_form_code)
    return 1 if study_form_code.nil? # TODO:  Согласовать, что будет если  study_form_code == nil
    study_form_code == 7 ? 2 : study_form_code
  end

  def transform_citizenship_code(citizenship_code)
    # TODO:  Согласовать, что будет если  citizenship_code == nil
    return 1 if citizenship_code == 1
    return 2 if (2..9) === citizenship_code
  end

  def transform_participant_category_code(participant_category_code)
    {
        1 => 1,
        3 => 3,
        4 => 4,
        8 => 8,
        11 => 3,
        13 => 3,
        14 => 3,
        15 => 1
    }[participant_category_code.to_i]
  end
end
