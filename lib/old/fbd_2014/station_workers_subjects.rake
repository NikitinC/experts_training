namespace :fbd_2014 do
  task :station_workers_subjects => :prepare do
    Employee.unscoped.where(valid_cache: true).joins(:exam_station_employees => :exam_station)
    .where(:exam_station_employees => {:hidden => nil}, :exam_stations => {:exam_id => federal_exam_codes.keys}).uniq.find_each do |e|
      arr = [
          e.guid, # 1. GUID
          transform_tutor_code(e.main_employee_ou.try(:subjects).try(:first).try(:code) || e.employee_ous.first.try(:subjects).try(:first).try(:code)) # 2. Код образовательного предмета
      ]
      puts arr.map{ |el| escape_for_export(el) }.join('#') if arr[1].present?
    end
  end
end

def transform_tutor_code code
  ((1..13) === code || code == 18) ? code : nil
end
