namespace :fbd_2014 do
  task :participants_exams => :prepare do

    ExamStudent.where(:exam_id => federal_exam_codes.keys).find_each do |es|
      exam_code = federal_exam_code es.exam

      if exam_code && es.student
        line es.student.guid, # 1. GUID участника
             exam_code, # 2. Код дня экзамена
             transform_time(es.created_at) # 3. Дата-время создания

      end
    end
  end
end