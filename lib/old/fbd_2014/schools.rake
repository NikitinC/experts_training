# coding: utf-8

namespace :fbd_2014 do
  task :schools => :prepare do
    ActiveRecord::Base.uncached do
      Ou.unscoped.where("valid_cache = true or code like '%0000'").find_each do |ou|
        next if ou.code.blank?

        #todo think
        next if ou.students.includes(:exam_students).where(exam_students: {exam_id: federal_exam_codes.keys, hidden: nil}).count.zero?


        line ou.guid, # 1. GUID
             transform_hidden(ou.hidden), # 2. Признак удаленной строки
             ou.code, # 3. Код ОУ
             transform_full_name_ou(ou.full_name), # 4. Полное наименование ОУ по уставу
             transform_view_ou(ou.ou_kind_code), # 5. Код вида ОУ
             transform_business_entity_type_code(ou.business_entity_type_code), # 6. Код вида организационно-правовой формы образовательного учреждения
             transform_mouo(ou.mouo).try(:guid), # 7. GUID органа управления образованием, которому непосредственно подчинено ОУ
             ou.ate.try(:guid), # 8. GUID территории, к которой непосредственно подчинено ОУ
             transform_type_human_settlement(ou.human_settlement_type_code), # 9. Код типа населенного пункта, где расположено ОУ
             transform_ur_address(ou.legal_address), # 10. Юридический адрес ОУ
             transform_ur_address(ou.real_address), # 11. Фактический адрес ОУ
             transform_chief_post(ou.chief_post), # 12. Должность руководителя ОУ
             transform_chief_full_name(ou.chief_full_name), # 13. ФИО руководителя ОУ
             prepare_phone(ou.chief_phone), # 14. Телефоны ОУ
             prepare_phone(ou.chief_phone), # 15. Факсы ОУ
             prepare_email(ou.chief_email), # 16. E-mail ОУ
             ou.students.where(ege_participant_category_code: [1, 3]).count, # 17. Кол-во обучающихся в выпускных классах
             #1 Выпускник общеобразовательного учреждения текущего года
             #3	Обучающийся образовательного учреждения среднего профессионального образования
             ou.students.where(ege_participant_category_code: [11, 13, 15]).count, #TODO уточнить категории  18. Кол-во обучающихся в предвыпускных  классах
             #11	Обучающийся предвыпускного курса учреждения начального профессионального образования
             #13	Учащийся учреждения начального профессионального образования (кроме выпускных и предвыпускных курсов)
             #15	Обучающийся предвыпускного класса общеобразовательного учреждения
             transform_employee_preparation_ege_full_name(ou.employee_preparation_ege_full_name), # 19. ФИО работника, отвечающего за подготовку и проведение ЕГЭ
             ou.website, # 20. Адрес WWW – сайта ОУ
             ou.license_registration_number, # 21. Регистрационный номер лицензии
             date_to_str(ou.license_begin), # 22. Дата выдачи лицензии на ведение образовательной деятельности
             date_to_str(ou.license_end), # 23. Дата окончания лицензии на ведение образовательной деятельности
             date_to_str(ou.accreditation_begin), # 24. Дата выдачи свидетельства о государственной аккредитации
             date_to_str(ou.accreditation_end), # 25. Дата окончания действия свидетельства о государственной аккредитации
             0, # 26. Признак: ОУ является специальным пунктом регистрации выпускников прошлых лет, значение «0» по ТЗ
             ou.short_name, # 27. Краткое наименование ОУ
             get_okato_for_ou(ou), # 28. Код района или города, на территории которого находится ОУ, Из справочника районов и городов указывается код ОКАТО. Если для какой-то территории нет ОКАТО, то нужно ставить 65000
             ou.accreditation_registration_number, # 29. Регистрационный номер свидетельства о государственной аккредитации
             0, # 30. Признак расположения ОУ в ТОМ, значение «0» по ТЗ
             ou.created_at.strftime("%Y-%m-%d %H-%M-%S"), # 31. Дата-время создания
             ou.updated_at.strftime("%Y-%m-%d %H-%M-%S") # 32. Дата-время обновления
      end
    end
    nil
  end

  def transform_business_entity_type_code code
    # Наш вариант
    # 1  Федеральное государственное образовательное учреждение
    # 2  Государственное образовательное учреждение
    # 3  Муниципальное образовательное учреждение
    # 4  Негосударственное образовательное учреждение
    # 5  Иное

    # Вариант федральной базы
    # 01  Государственные ОУ
    # 02  Иное
    # 03  Муниципальные ОУ
    # 04  Негос ОУ
    # 05  Федер ОУ

    transform_hash = {
        1 => "05",
        2 => "01",
        3 => "03",
        4 => "04",
        5 => "02"
    }

    transform_hash[code]
  end

  def transform_type_human_settlement code
    if code
      code == 1 ? "01" : "02"
    end
  end

  def get_okato_for_ou ou
    ou.ate.try(:okato) || 65401
  end

  def date_to_str date
    date.nil? ? "" : date.strftime("%d.%m.%Y")
  end

  def transform_view_ou view_ou
    view_ou.blank? ? "0101" : view_ou
  end

  def transform_chief_full_name chief_full_name
    chief_full_name.blank? ? "НЕТ" : chief_full_name
  end

  def transform_chief_post chief_post
    chief_post.blank? ? "Директор" : chief_post
  end

  def transform_full_name_ou full_name_ou
    full_name_ou.blank? ? "Образовательное учреждение" : full_name_ou
  end

  def transform_employee_preparation_ege_full_name employee_preparation_ege_full_name
    employee_preparation_ege_full_name.blank? ? "НЕТ" : employee_preparation_ege_full_name
  end

  def transform_ur_address ur_address
    ur_address.blank? ? "НЕТ" : ur_address
  end

  def transform_ate_guid ou
    if ou.ate.code.size == 2
      ou.ate.guid
    else
      ate_code = ou.code[0, 2]
      ate = Ate.find_by_code ate_code
      ate.guid
    end
  end

  def transform_mouo mouo
    return unless mouo
    return mouo.mouo if mouo.mouo
    mouo
  end
end
