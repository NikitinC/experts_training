# coding: utf-8

namespace :fbd_2014 do
  task :worker_positions_on_station => :prepare do
    ExamStationEmployee.find_each do |ese|
      exam_code = federal_exam_code(ese.exam_station.try :exam)
      station_post_code = transform_station_post_code(ese.station_post_code)
      if exam_code && station_post_code
        line ese.employee.guid, # 1. GUID работника ППЭ
             exam_code, # 2. Код дня экзамена
             ese.exam_station.station.guid, # 3. GUID ППЭ
             station_post_code, # 4. Код должности в ППЭ
             transform_time(ese.created_at), # 5. Дата-время создания
             nil # 6. Код роли организатора
      end
    end
  end

  def transform_station_post_code code
    return code if (1..5) === code
  end
end