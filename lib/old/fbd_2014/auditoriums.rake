namespace :fbd_2014 do
  task :auditoriums => :prepare do
    ActiveRecord::Base.uncached do
      Classroom.unscoped.find_each do |c|
        next if c.station.nil?
        hidden = c.hidden.present?

        #todo think
        #next if c.exam_stations.where(:exam_id => federal_exam_codes.keys).count.zero?
        next if c.station.exam_stations.where(:exam_id => federal_exam_codes.keys).count.zero?
        next if hidden

        clones_count = Classroom.where(station_id: c.station_id).where('number similar to ?', "0*#{c.number.to_i}").count
        next if (!c.valid_cache && !hidden) || (clones_count > 0 && hidden)

        line(
            c.guid, # 1. GUID
            hidden ? 1 : 0, # 2. Признак удаленной строки
            classroom_name(c), # 3. Наименование аудитории
            c.number, # 4. Номер аудитории
            c.rows_count, # 5. Количество рядов в аудитории
            c.seats_per_row, # 6. Количество посадочных мест в ряду
            c.station.guid, # 7. GUID ППЭ
            transform_bool(c.organizer_order), # 8. Расположение рядов в аудитории
            transform_bool(c.special_sit), # 9. Признак специализированной рассадки
            transform_time(c.created_at), # 10. Дата-время создания
            transform_time(c.updated_at), # 11. Дата-время обновления
            c.cctv.present? # 12. Наличие видеонаблюдения
        )
      end
    end
  end

  def classroom_name c
    c.name.blank? ? c.number.to_s : c.name
  end

end
