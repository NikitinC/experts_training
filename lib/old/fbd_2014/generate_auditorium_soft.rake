namespace :fbd_2014 do
  task :generate_auditorium_soft => :prepare do
    Classroom.unscoped.valid.find_each do |c|
      next if c.station.nil?
      [*(20..28).to_a, 40, 41, 44].each do |i|
        line(
            UUID.generate, # 1. GUID
            c.guid, # 2. GUID Аудитории
            i # Код ПО
        )
      end
    end
  end
end
