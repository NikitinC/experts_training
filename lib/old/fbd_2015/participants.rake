# coding: utf-8

namespace :fbd_2015 do
  task :participants, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do

      Student \
          .includes('attestation_form_students')
          .where({attestation_form_students: {attestation_form_code: [AttestationForm::EGE, AttestationForm::GVE11], hidden: nil}})
          .find_each do |s|


        hidden = s.hidden.present?

        line(
            s.guid, # 1. GUID
            (hidden ? 1 : 0), # 2. Признак удаленной строки
            s.second_name, # 3. Фамилия
            s.first_name, # 4. Имя
            s.middle_name, # 5. Отчество
            s.document_series, # 6. Серия документа
            s.document_number, # 7. Номер документа
            s.document_type.fct_ege_code , # 8. Тип документа
            gender(s.gender), # 9. Пол
            "#{s.group.number}#{s.group.letter}", # 10. Класс
            s.birthday && I18n.localize(s.birthday), # 11. Дата рождения
            special_sit(s), # 12. Признак специализированной рассадки
            s.ege_participant_category_code, # 13. Код категории участника
            transform_region_code(s), # 14. Код субъекта Российской Федерации, в котором участник закончил ОУ
            s.group.ou.guid, # 15. GUID ОУ, основное ОУ участника (основное место регистрации), Уникальный идентификатор ОУ участника по ТЗ
            s.group.ou.guid, # 16. GUID ОУ, выпускное ОУ, Уникальный идентификатор ОУ участника по ТЗ
            transform_time(s.created_at), # 17. Дата-время создания
            transform_time(s.updated_at), # 18. Дата-время обновления
            transform_attestation_form_code(s), # 19.Форма ГИА
            transform_gia_active_results(s), # 20. Действующие результаты ГИА
            transform_study_form_code(s.study_form_code), # 21. Код формы обучения
            transform_citizenship_code(s.citizenship_code), # 22. Гражданство
            transform_snils(s.snils) # 23. Снилс
        )
      end
    end
  end

  def transform_snils(value)
    value && value.tr('- ', '')
  end

  def transform_region_code(s)
    return s.region_code if s.graduate_of_other_ou
    '66'
  end

  def transform_gia_active_results(s)
    return 0 if !s.gia_active_results_math && !s.gia_active_results_rus
    return 1 if !s.gia_active_results_math && s.gia_active_results_rus
    return 2 if s.gia_active_results_math && !s.gia_active_results_rus
    return 3 if s.gia_active_results_math && s.gia_active_results_rus
  end

  def transform_attestation_form_code(s)
    # 1 – означает, что участник проходит ГИА в текущем году в форме ЕГЭ.
    # 2 – означает, участник проходит участник проходит что ГИА в форме ГВЭ;
    # 3 – означает, что участник проходит ГИА в форме ЕГЭ и ГВЭ;
    # 5 – означает, что участник проходит ГИА в форме ЕГЭ и Другой форме;
    fail "No attestation form #{s.id}, #{s.attestation_form_students.to_s}" if s.attestation_form_students.empty?

    res = s.attestation_form_students.map(&:attestation_form_code).uniq.map{ |x|
            x == AttestationForm::EGE ? 1 : (
            x == AttestationForm::GVE11 ? 2 : 1000)
         }.inject(:+)
    fail "Invalid attestation form #{s.id}, #{s.attestation_form_students.to_s}"  if res < 1 or res > 3
    res
  end


  def transform_study_form_code(study_form_code)
    fail if study_form_code.nil?
    study_form_code
  end

  def transform_citizenship_code(citizenship_code)
    fail if citizenship_code.nil?
    citizenship_code
  end

end
