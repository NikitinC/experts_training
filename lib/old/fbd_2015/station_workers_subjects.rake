namespace :fbd_2015 do
  task :station_workers_subjects, [:exam_type_ids, :exam_codes] => :prepare do
    Employee.find_each do |e|
      # Employee.joins(:exam_station_employees).where(valid_cache: true).find_each do |e|

      next unless e.station_post_code == 5

      arr = [
          e.guid, # 1. GUID
          transform_tutor_code(e.main_employee_ou.try(:subjects).try(:first).try(:code) || e.employee_ous.first.try(:subjects).try(:first).try(:code)) # 2. Код образовательного предмета
      ]
      puts arr.map{ |el| escape_for_export(el) }.join('#') if arr[1].present?
    end
  end
end

def transform_tutor_code code
  ((1..13) === code || code == 18) ? code : nil
end
