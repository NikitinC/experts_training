namespace :fbd_2015 do
  task :places, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do

      stations_to_export = []
      Station.find_each do |s|
        next if [4314].include? s.code.to_i

        fail "No ou for ppe #{s.id}" if s.ou.nil?

        next if (s.exam_types.map(&:id) & [5, 19, 24]).empty?

        stations_to_export.append s.id
      end


      Classroom.unscoped.uniq.find_each do |c|
        next if c.station.nil?
        next unless stations_to_export.include? c.station.id
        next if (c.attestation_form_classrooms.map(&:attestation_form_code).uniq & [AttestationForm::EGE, AttestationForm::GVE11]).empty?
        hidden = c.hidden.present?

        next if hidden

        clones_count = Classroom.where(station_id: c.station_id).where('number similar to ?', "0*#{c.number.to_i}").count
        next if (!c.valid_cache && !hidden) || (clones_count > 0 && hidden)


        if c.classroom_verbal_part_sing_code
          (c.rows_count * c.seats_per_row).times do |i|
            row = i / c.seats_per_row + 1
            number = i % c.seats_per_row + 1

            line(
                c.station.guid, # 1. GUID ППЭ
                c.number, # 2. Номер аудитории
                row, # 3. Номер ряда
                number, # 4. Порядковый номер посадочного места в ряду
                (i + 1) > c.available_seats ? 1 : 0, # 5. Признак исключения из рассадки
                1 # 6. Признак устной части
            )
          end
        else
          (c.rows_count * c.seats_per_row - c.available_seats).times do |i|
            line(
                c.station.guid, # 1. GUID ППЭ
                c.number, # 2. Номер аудитории
                c.rows_count - i, # 3. Номер ряда
                c.seats_per_row, # 4. Порядковый номер посадочного места в ряду
                1, # 5. Признак исключения из рассадки
                0 # 6. Признак устной части
            )
          end
        end

      end
    end
  end
end
