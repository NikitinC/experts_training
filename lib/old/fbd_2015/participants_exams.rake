namespace :fbd_2015 do
  task :participants_exams, [:exam_type_ids, :exam_codes] => :prepare do

    composition_id_to_fct_code = Hash[Composition.all.map{|x| [x.id, x.fct_code]}]

    Student \
          .includes('attestation_form_students')
          .includes('composition_students')
        .where({attestation_form_students: {attestation_form_code: [AttestationForm::EGE, AttestationForm::GVE11], hidden: nil}})
        .all.each do |s|

        s.composition_students.each do |c|
            next if c.hidden
            fail "Invalid composition #{s.id}" if composition_id_to_fct_code[c.composition_id].nil?
            line(
                s.guid, # 1. GUID участника
                composition_id_to_fct_code[c.composition_id], # 2. Код дня экзамена
                transform_time(c.created_at) # 3. Дата-время создания
            )
        end
    end

    # Экзамены
    #
    # exam_group_ids = federal_exam_codes_from_kind.keys
    # ExamStudent.where(exam_id: exam_group_ids).find_each do |es|
    #   exam_code = federal_exam_code es.exam
    #
    #   if exam_code && es.student
    #     line(
    #         es.student.guid, # 1. GUID участника
    #         exam_code, # 2. Код дня экзамена
    #         transform_time(es.created_at) # 3. Дата-время создания
    #     )
    #   end
    # end
    #
    # # Сочинения
    # #
    # CompositionStudent.where(composition_id: federal_composition_codes.keys).find_each do |item|
    #   composition_code = federal_composition_code item.composition
    #
    #   if composition_code && item.student
    #     line(
    #         item.student.guid, # 1. GUID участника
    #         composition_code, # 2. Код дня экзамена
    #         transform_time(item.created_at) # 3. Дата-время создания
    #     )
    #   end
    # end

  end
end