# coding: utf-8

namespace :fbd_2015 do
  task :station_worker_on_station, [:exam_type_ids, :exam_codes] => :prepare do

    ExamStationEmployee.where(exam_type_id: current_exam_type_ids).find_each do |ese|
      station_post_code = transform_station_post_code(ese.station_post_code)
      station_id = ese.station_id

      if station_id && station_post_code
        line(
            ese.employee.guid, # 1. GUID работника ППЭ
            ese.station.guid, # 2. GUID ППЭ
            0, # 3. Признак прикрепления      !! deal with it !!!
            station_post_code, # 4. Код должности в ППЭ
            transform_time(ese.created_at), # 5. Дата-время создания
            transform_time(ese.created_at), # 6. Дата-время создания
        )
      end
    end
  end
end