# coding: utf-8
require 'json'

namespace :fbd_2015 do
  task :auditorium_exams, [:exam_type_ids, :exam_codes] => :prepare do

    VERBAL_EXAMS = [187, 188, 189, 190, 195, 194, 193, 192, 201, 202, 203, 204, 200, 199, 198, 197, 206, 207, 208, 209]

    def verbal_code(esc)
      if VERBAL_EXAMS.include?(esc.exam_station.exam.code.to_i)
        return esc.classroom.classroom_verbal_part_sing_code == 2 ? "0" : "1"
      end
      return ""
    end

    ActiveRecord::Base.uncached do

      ExamStationClassroom.find_each do |esc|
        next if esc.exam_station.nil? || esc.classroom.nil? || esc.classroom.station.nil?

        code = federal_exam_code(esc.exam_station.exam)

        next if code.nil?

        line(
            esc.classroom.station.guid, # 1. GUID ППЭ
            esc.classroom.number, # 2. номер аудитории
            code, # 3. Код дня экзамена
            esc.classroom.capacity, # 4. Количество мест на экзамен
            transform_time(esc.created_at), # 5. Дата-время создания
            transform_time(esc.updated_at), # 6. Дата-время изменения
            verbal_code(esc)
        )
      end
    end
  end
end