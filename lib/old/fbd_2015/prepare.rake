# coding: utf-8

namespace :fbd_2015 do
  task prepare: :environment do |t, args|
    # exam_type_ids = args[:exam_type_ids].try(:gsub, "'", '').try(:split, ' ') || []
    # exam_codes = args[:exam_codes].try(:gsub, "'", '').try(:split, ' ') || []
    #
    # raise 'unacceptable arguments' if exam_type_ids.empty? && exam_codes.empty?
    #
    # relation = Exam
    # relation = relation.where(code: exam_codes) if exam_codes.any?
    #
    # #todo create exam_kinds
    # def get_exam_type_kind_ids(exam_type_id)
    #   [[5, 17, 19], [22, 23]].select { |x| x.include? exam_type_id }.first || [exam_type_id]
    # end
    #
    # if exam_type_ids.any?
    #   exam_type_from_kind_ids = exam_type_ids.map { |x| get_exam_type_kind_ids(x) }.flatten.uniq
    #   exams_from_kind = relation.where(exam_type_id: exam_type_from_kind_ids)
    #   relation = relation.where(exam_type_id: exam_type_ids)
    # end
    #
    # federal_exam_codes = {}
    # relation.find_each { |x| federal_exam_codes[x.id] = x.code }
    #
    # exams_from_kind ||= relation
    # federal_exam_codes_from_kind = {}
    # exams_from_kind.find_each { |x| federal_exam_codes_from_kind[x.id] = x.code }
    #
    # @config = {
    #     federal_exam_codes: federal_exam_codes,
    #     federal_composition_codes: APP_CONFIG['federal_composition_codes'],
    #     federal_exam_codes_from_kind: federal_exam_codes_from_kind,
    #
    #     software_codes: {
    #         1 => 20,
    #         2 => 21,
    #         3 => 22,
    #         4 => 23,
    #         5 => 24,
    #         6 => 25,
    #         7 => 26,
    #         8 => 27,
    #         9 => 28
    #     },
    #
    #     programming_language_codes: {
    #         1 => '01',
    #         2 => '01',
    #         3 => '01',
    #         4 => '01',
    #         5 => '02',
    #         6 => '02',
    #         7 => '03',
    #         8 => '03',
    #         9 => '04'
    #     },
    #
    #     spreadsheet_editor_codes: {
    #         1 => 41,
    #         2 => 40,
    #         3 => 42,
    #         4 => 43,
    #         5 => 44,
    #         6 => 45,
    #         7 => 46,
    #         8 => 48
    #     }
    # }

    def federal_exam_codes
      @config[:federal_exam_codes] || {}
    end

    def federal_exam_codes_from_kind
      @config[:federal_exam_codes_from_kind] || {}
    end

    def federal_composition_codes
      @config[:federal_composition_codes] || {}
    end

    def federal_exam_code(exam)
      federal_exam_codes[exam.try :id]
    end

    include FbdHelper
  end
end

module FbdHelper
  def station_relation
    Station.unscoped.valid
  end

  def current_exam_type_ids
    Exam.where(id: federal_exam_codes.keys).pluck(:exam_type_id).uniq
  end

  def transform_document_code code
    return 7 if code == 8 || code > 13
    code
  end

  def gender g
    g = true if g.nil?
    g ? "М" : "Ж"
  end

  def escape_for_export s
    return "" if s.nil?

    escaped = ""
    s.to_s.each_byte do |b|
      if b == ?#
        escaped += '№'
      elsif b == ?'
        escaped += '"'
      elsif b == ?%
        escaped += ' 0/0 '
      elsif b == ?_
        escaped += '-'
      elsif (0...31) === b
        escaped += "\\#{b}"
      else
        escaped += b.chr
      end
    end
    escaped
  end

  def line *args
    puts args.map { |el| escape_for_export(el) }.join("#")
  end

  def prepare_email email
    fail if email.blank?
    email.to_s.split(/[\s,]/).join(";").gsub(/;+/, ";")
  end

  def prepare_phone phone
    phone.nil? ? "Нет" : phone.gsub(" ", "");
  end

  def special_sit student
    student.special_seating ? 1 : 0
  end

  def transform_hidden hidden
    hidden.nil? ? 0 : 1
  end

  def transform_bool bool
    bool ? 1 : 0
  end

  def transform_time time
    time.strftime("%Y-%m-%d %H-%M-%S")
  end

  def transform_station_post_code(code)
    code if (1..7) === code
  end

  def valid_ou?(ou)
    exam_codes = federal_exam_codes_from_kind

    return false if ou.address_guid.nil? || ou.code.blank? || blank_address?(ou)

    have_not_students_with_exam = !ou.students.includes(:exam_students)
                                       .where(exam_students: {exam_id: exam_codes.keys, hidden: nil}).any?
    have_not_students_with_composition_in_this_ou =
        CompositionStudent.where(ou_id: ou, composition_id: federal_composition_codes.keys).empty?
    return false if have_not_students_with_composition_in_this_ou && have_not_students_with_exam

    true
  end

  def blank_address?(ou)
    ou.ate.try(:okato).blank? ||
        ou.postcode.blank? ||
        ou.human_settlement_type_code.blank? ||
        ou.settlement_name.blank? ||
        ou.street_type_code.blank? ||
        ou.street_name.blank? ||
        ou.building_type_code.blank? ||
        ou.building_number.blank? ||
        ou.legal_postcode.blank? ||
        ou.legal_human_settlement_type_code.blank? ||
        ou.legal_settlement_name.blank? ||
        ou.legal_street_type_code.blank? ||
        ou.legal_street_name.blank? ||
        ou.legal_building_type_code.blank? ||
        ou.legal_building_number.blank?
  end
end