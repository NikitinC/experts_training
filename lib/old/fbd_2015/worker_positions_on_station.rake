# coding: utf-8

namespace :fbd_2015 do
  task :worker_positions_on_station, [:exam_type_ids, :exam_codes] => :prepare do
    ExamStationEmployee.find_each do |ese|
      exam_code = federal_exam_code(ese.exam_station.try :exam)
      station_post_code = transform_station_post_code(ese.station_post_code)
      if exam_code && station_post_code
        line(
            ese.employee.guid, # 1. GUID работника ППЭ
            exam_code, # 2. Код дня экзамена
            ese.exam_station.station.guid, # 3. GUID ППЭ
            station_post_code, # 4. Код должности в ППЭ
            transform_time(ese.created_at), # 5. Дата-время создания
            nil # 6. Код роли организатора
        )
      end
    end

    ExamStationPublicObserver.find_each do |espo|
      exam_code = federal_exam_code(espo.exam_station.try :exam)
      station_post_code = 6
      if exam_code
        line(
            espo.public_observer.guid, # 1. GUID работника ППЭ
            exam_code, # 2. Код дня экзамена
            espo.exam_station.station.guid, # 3. GUID ППЭ
            station_post_code, # 4. Код должности в ППЭ
            transform_time(espo.created_at), # 5. Дата-время создания
            nil # 6. Код роли организатора
        )
      end
    end
  end

  def transform_station_post_code(code)
    return {
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        10 => 7,
        11 => 7,
        14 => 2,
        15 => 2,
        17 => 2,
        16 => 2,
        9 => 3,
        7 => 3,
        8 => 3
    }[code.to_i]
  end
end