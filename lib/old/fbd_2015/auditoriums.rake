namespace :fbd_2015 do
  task :auditoriums, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do

      stations_to_export = []
      Station.find_each do |s|
        next if [4314].include? s.code.to_i

        fail "No ou for ppe #{s.id}" if s.ou.nil?

        next if (s.exam_types.map(&:id) & [5, 19, 24]).empty?

        stations_to_export.append s.id
      end


      Classroom.unscoped.uniq.find_each do |c|
        next if c.station.nil?
        next unless stations_to_export.include? c.station.id
        next if (c.attestation_form_classrooms.map(&:attestation_form_code).uniq & [AttestationForm::EGE, AttestationForm::GVE11]).empty?
        hidden = c.hidden.present?

        next if hidden

        clones_count = Classroom.where(station_id: c.station_id).where('number similar to ?', "0*#{c.number.to_i}").count
        next if (!c.valid_cache && !hidden) || (clones_count > 0 && hidden)

        line(
            c.guid, # 1. GUID
            hidden ? 1 : 0, # 2. Признак удаленной строки
            classroom_name(c), # 3. Наименование аудитории
            c.number, # 4. Номер аудитории
            c.rows_count, # 5. Количество рядов в аудитории
            c.seats_per_row, # 6. Количество посадочных мест в ряду
            c.station.guid, # 7. GUID ППЭ
            transform_bool(c.organizer_order), # 8. Расположение рядов в аудитории
            transform_bool(c.special_sit), # 9. Признак специализированной рассадки
            transform_time(c.created_at), # 10. Дата-время создания
            transform_time(c.updated_at), # 11. Дата-время обновления
            c.cctv.present? ? 1 : 0, # 12. Наличие видеонаблюдения
            c.classroom_verbal_part_sing_code || 0 # 13. Признак устной части
        )
      end
    end
  end

  def classroom_name c
    c.name.blank? ? c.number.to_s : c.name
  end

end
