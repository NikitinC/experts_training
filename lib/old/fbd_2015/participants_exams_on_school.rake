namespace :fbd_2015 do
  task :participants_exams_on_school, [:exam_type_ids, :exam_codes] => :prepare do

    composition_id_to_fct_code = Hash[Composition.all.map{|x| [x.id, x.fct_code]}]

    Student \
          .includes('attestation_form_students')
        .includes('composition_students')
        .where({attestation_form_students: {attestation_form_code: [AttestationForm::EGE, AttestationForm::GVE11], hidden: nil}})
        .find_each do |s|

      s.composition_students.each do |c|
        next if c.hidden
        fail "Invalid composition #{s.id}" if composition_id_to_fct_code[c.composition_id].nil?

        line(
            s.guid, # 1. GUID участника
            composition_id_to_fct_code[c.composition_id], # 2. Код дня экзамена
            c.ou.try(:guid), # 3. GUID ОО(место проведения)
            transform_time(c.created_at), # 4. Дата-время создания
            transform_time(c.updated_at) # 5. Дата-время изменения
        )
      end

    end
  end
end