# coding: utf-8
namespace :fbd_2017 do
  task :governments => :prepare do

    Mouo.unscoped.find_each do |m|
      line m.guid, # 1. GUID
           transform_hidden(m.hidden), # 2. Признак удаленной строки
           m.code, # 3. Код ОУО
           m.name, # 4. Полное наименование ОУО
           m.legal_address, # 5. Юридический адрес
           m.real_address, # 6. Физический адрес
           "начальник", # 7. Должность руководителя ОУО
           m.chief_full_name, # 8. ФИО руководителя ОУО
           prepare_phone(m.chief_phone), # 9. Телефон(ы) ОУО
           prepare_phone(m.chief_fax), # 10. Факс(ы) ОУО
           prepare_email(m.chief_email), # 11. Email ОУО
           m.specialist_final_certification_full_name, # 12. ФИО специалиста, ответственного за проведение ЕГЭ
           prepare_email(m.specialist_final_certification_email), # 13. Email специалиста, ответственного за проведение ЕГЭ
           prepare_phone(m.specialist_final_certification_phone), # 14. Телефон(ы) специалиста, ответственного за проведение ЕГЭ
           m.website, # 15. http address
           code2level(m.code) # 16. Уровень ОУО
    end
    nil
  end

  def code2level code
    code.to_i < 94 ? 2 : 1
  end
end