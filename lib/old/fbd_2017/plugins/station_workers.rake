#coding: utf-8
namespace :fbd_2017 do
  task :station_workers, [:exam_type_ids, :exam_codes] => :prepare do
    sql = <<SQL
    SELECT e.guid as guid,
      CASE
          WHEN e.hidden is NULL THEN 0
            ELSE 1
        END as deleted,
        '' as workercode,
        e.second_name as surname,
        e.first_name as name,
        e.middle_name as middle_name,
        CASE
          WHEN e.job_place = '' THEN o.guid
            ELSE ''
        END as worked_at,
      CASE
            WHEN e.job_post = '' THEN ep.name
            ELSE e.job_post
        END as worked_as,
        CASE
          WHEN e.job_place = '' THEN ''
            ELSE e.job_place
        END as worked_at_if_not_school,
        CASE
          WHEN COALESCE(m1.id, 0) > COALESCE (m2.id, 0) THEN m1.guid
            ELSE m2.guid
        END as mouo,
        e.document_series as series,
        e.document_number as number,
        dt.fct_ege_code as document_type,
        CASE
          WHEN gender = TRUE THEN 'М'
            ELSE 'Ж'
        END as gender,
        EXTRACT(YEAR FROM e.birthday),
        e.created_at,
        e.updated_at,
        CASE
          WHEN took_part_in_ege = TRUE THEN 1
            ELSE 0
        END as took_part_in_ege,
        e.teaching_experience,
        e.diploma_speciality,
        e.station_post_code,
        e.education_code,
        overlay (e.phone placing '' from 6 for 1),
        e.email,
        CASE
          WHEN took_part_in_gve = TRUE THEN 1
            ELSE 0
        END as took_part_in_gve
        FROM employees e
        JOIN (
                    SELECT distinct(e.id) as eid
                      FROM employees e
                      JOIN exam_station_employees ese ON ese.employee_id = e.id
                      where ese.exam_type_id in (5) and ese.hidden is NULL
                    UNION
                    SELECT distinct(e.id) as eid
                            FROM stations s
                            JOIN exam_types_stations ets ON ets.station_id = s.id and ets.exam_type_id in (5)
                            JOIN employees e ON e.on_station = cast(s.code as int) and s.hidden is NULL
                    WHERE e.hidden is NULL

         ) etap ON etap.eid = e.id
      LEFT JOIN employee_ous eo ON eo.employee_id = e.id AND eo.hidden is NULL
        LEFT JOIN ous o ON o.id = eo.ou_id and o.hidden is NULL
        LEFT JOIN mouos m1 ON m1.id = o.mouo_id and m1.hidden is NULL
        LEFT JOIN mouos m2 ON m2.id = e.owner_mouo_id and m2.hidden is NULL
        JOIN document_types dt ON dt.code = e.document_type_code
        LEFT JOIN employee_posts ep ON ep.code = eo.employee_post_code
      where e.hidden is NULL
        order by guid
SQL

    rwas = ''
    ActiveRecord::Base.connection.execute(sql).each do |r|
      if rwas != r['guid'] then # && r['station_post_code'] == '1' then
        line(
            r['guid'],
            r['deleted'],
            r['workercode'],
            r['surname'],
            r['name'],
            r['middle_name'],
            r['worked_at'],
            r['worked_as'],
            r['worked_at_if_not_school'],
            r['mouo'],
            r['series'],
            r['number'],
            r['document_type'],
            r['gender'],
            r['date_part'],
            r['created_at'],
            r['updated_at'],
            r['took_part_in_ege'],
            r['teaching_experience'],
            r['diploma_speciality'],
            r['station_post_code'],
            r['education_code'],
            r['overlay'],
            r['email'],
            r['took_part_in_gve']
        )
        rwas = r['guid']
      end
    end
  end

end