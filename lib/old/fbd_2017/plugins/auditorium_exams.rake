# coding: utf-8
require 'json'

namespace :fbd_2017 do
  task :auditorium_exams, [:exam_type_ids, :exam_codes] => :prepare do

    VERBAL_EXAMS = [187, 188, 189, 190, 195, 194, 193, 192, 201, 202, 203, 204, 200, 199, 198, 197, 206, 207, 208, 209, 250, 251, 252, 253]

    def verbal_code(esc)
      if VERBAL_EXAMS.include?(esc.exam_station.exam.code.to_i)
        return esc.classroom.classroom_verbal_part_sing_code == 2 ? "0" : "1"
      end
      return ""
    end

    ActiveRecord::Base.uncached do

      ExamStationClassroom.find_each do |esc|
        next if esc.exam_station.nil? || esc.classroom.nil? || esc.classroom.station.nil?

        exam_code = esc.exam_station.exam.code if esc.exam_station.exam.exam_type_id.in? [5]
        gve_code = nil

        next if VERBAL_EXAMS.include?(esc.exam_station.exam.code.to_i)

        next if esc.exam_station.exam.exam_type_id.nil? || esc.exam_station.exam.nil?
        # по данным из столбца ExamFormatCode таблицы rbd_ParticipantsExams выявил, что
        # 1 - устная форма; 2 - диктант; 3 - изложение; 4 - сочинение
        # в РБД коды ФЦТ данных экзаменов увеличил на 6000 - устная форма
        # 5000 - диктант; 4000 - изложение; 3000 - сочинение

        next if exam_code.nil?

        if exam_code > 6000 then #устная часть
          exam_code = exam_code - 6000
          gve_code = 1
        end
        if exam_code > 5000 then #диктант
          exam_code = exam_code - 5000
          gve_code = 2
        end
        if exam_code > 4000 then #изложение
          exam_code = exam_code - 4000
          gve_code = 3
        end
        if exam_code > 3000 then #сочинение
          exam_code = exam_code - 3000
          gve_code = 4
        end


        line(
            esc.classroom.station.guid, # 1. GUID ППЭ
            esc.classroom.number, # 2. номер аудитории
            exam_code, # 3. Код дня экзамена
            esc.classroom.capacity, # 4. Количество мест на экзамен
            transform_time(esc.created_at), # 5. Дата-время создания
            transform_time(esc.updated_at), # 6. Дата-время изменения
            verbal_code(esc),
            gve_code #код проведения ГВЭ
        )
      end
    end
  end
end