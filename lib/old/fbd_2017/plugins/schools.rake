# coding: utf-8

namespace :fbd_2017 do
  task :schools, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do
      Ou.find_each do |ou|

        line(
            ou.guid, # 1. GUID
            transform_hidden(ou.hidden), # 2. Признак удаленной строки
            ou.code, # 3. Код ОУ
            transform_full_name_ou(ou.full_name), # 4. Полное наименование ОУ по уставу
            transform_view_ou(ou.ou_kind_code), # 5. Код вида ОУ
            transform_business_entity_type_code(ou.business_entity_type_code), # 6. Код вида организационно-правовой формы образовательного учреждения
            ou.mouo.guid, # 7. GUID органа управления образованием, которому непосредственно подчинено ОУ
            ou.ate.guid, # 8. GUID территории, к которой непосредственно подчинено ОУ
            transform_type_human_settlement(ou.human_settlement_type_code), # 9. Код типа населенного пункта, где расположено ОУ
            transform_chief_post(ou.chief_post), # 10. Должность руководителя ОУ
            transform_chief_full_name(ou.chief_full_name), # 11. ФИО руководителя ОУ
            prepare_phone(ou.chief_phone), # 12. Телефоны ОУ
            prepare_phone(ou.chief_phone), # 13. Факсы ОУ
            prepare_email(ou.chief_email), # 14. E-mail ОУ
            ou.students.where(ege_participant_category_code: [1, 3]).count, # 15. Кол-во обучающихся в выпускных классах
            #1 Выпускник общеобразовательного учреждения текущего года
            #3	Обучающийся образовательного учреждения среднего профессионального образования
            ou.next_year_participant_count,
            #11	Обучающийся предвыпускного курса учреждения начального профессионального образования
            #13	Учащийся учреждения начального профессионального образования (кроме выпускных и предвыпускных курсов)
            #15	Обучающийся предвыпускного класса общеобразовательного учреждения
            transform_employee_preparation_ege_full_name(ou.employee_preparation_ege_full_name), # 17. ФИО работника, отвечающего за подготовку и проведение ЕГЭ
            ou.website, # 18. Адрес WWW – сайта ОУ
            ou.license_registration_number, # 19. Регистрационный номер лицензии
            date_to_str(ou.license_begin), # 20. Дата выдачи лицензии на ведение образовательной деятельности
            date_to_str(ou.license_end), # 21. Дата окончания лицензии на ведение образовательной деятельности
            date_to_str(ou.accreditation_begin), # 22. Дата выдачи свидетельства о государственной аккредитации
            date_to_str(ou.accreditation_end), # 23. Дата окончания действия свидетельства о государственной аккредитации
            0, # 24. Признак: ОУ является специальным пунктом регистрации выпускников прошлых лет, значение «0» по ТЗ
            ou.short_name, # 25. Краткое наименование ОУ
            ou.accreditation_registration_number, # 26. Регистрационный номер свидетельства о государственной аккредитации
            transform_bool(ou.is_TOM), # 27. Признак расположения ОУ в ТОМ, значение «0» по ТЗ
            0, # ou.is_registration_special_station ? 1 : (
            #   ou.is_composition_special_station ? 2 : (
            #   ou.is_registration_special_station && ou.is_composition_special_station ? 3 : 0
            #   )
            # ), #28. Дополнительные признаки ОО
            ou.created_at.strftime("%Y-%m-%d %H-%M-%S"), # 29. Дата-время создания
            ou.updated_at.strftime("%Y-%m-%d %H-%M-%S") # 30. Дата-время обновления
        )
      end
    end
    nil
  end

  def transform_business_entity_type_code code
    # fct and our codes are equal
    code
  end

  def transform_type_human_settlement code
    if code
      code == 1 ? "02" : "01"
    end
  end

  def get_okato_for_ou ou
    ou.ate.try(:okato) || 65401
  end

  def date_to_str date
    date.nil? ? "" : date.strftime("%d.%m.%Y")
  end

  def transform_view_ou view_ou
    fail if view_ou.blank?
    view_ou
  end

  def transform_chief_full_name chief_full_name
    fail if chief_full_name.blank?
    chief_full_name
  end

  def transform_chief_post chief_post
    fail if chief_post.blank?
     chief_post
  end

  def transform_full_name_ou full_name_ou
    fail if full_name_ou.blank?
     full_name_ou
  end

  def transform_employee_preparation_ege_full_name employee_preparation_ege_full_name
    fail if employee_preparation_ege_full_name.blank?
    employee_preparation_ege_full_name
  end

  def transform_ur_address ur_address
    fail if ur_address.blank?
    ur_address
  end
end
