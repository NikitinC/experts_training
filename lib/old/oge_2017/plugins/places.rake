namespace :oge_2017 do
  task :places, [:exam_type_ids, :exam_codes] => :prepare do
    sql = <<SQL
    SELECT st.guid as ppe, c.rows_count as row_cnt, c.seats_per_row as col_cnt, c.number as aud_num,
      coalesce(c.unavailable_places, '') as unavailable_places, coalesce(c.verbal_part_paks,0) as inyaz_paks

      FROM classrooms c
      JOIN stations st ON st.id = c.station_id
      JOIN exam_types_stations ets ON ets.station_id = st.id and ets.exam_type_id in (22, 23, 25, 26)
      where c.unavailable_places != '' or c.verbal_part_paks > 0

SQL

    ActiveRecord::Base.connection.execute(sql).each do |r|
      inyaz_cnt = 0
      rs = r['row_cnt'].to_i
      cs = r['col_cnt'].to_i
      rs.times do |rows_i|
        cs.times do |cols_i|
          place = (rows_i + 1).to_s + ',' + (cols_i+1).to_s
          if r['unavailable_places'].include? (place)
            line(
                r['ppe'],
                r['aud_num'],
                (rows_i+1).to_s,
                (cols_i+1).to_s,
                1,
                0
            )
          else
            if inyaz_cnt < (r['inyaz_paks'].to_i)
              line(
                  r['ppe'],
                  r['aud_num'],
                  (rows_i+1).to_s,
                  (cols_i+1).to_s,
                  0,
                  1
              )
              inyaz_cnt = inyaz_cnt + 1
            end
          end
        end
      end
    end
  end
end
