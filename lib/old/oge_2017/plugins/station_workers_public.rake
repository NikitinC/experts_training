module StationWorkersPublic
  def filename
    '66_StationWorkers_public.cs_'
  end

  def process **args

      PublicObserver.where(valid_cache: true, hidden: nil).joins(exam_station_public_observers: :exam)
          .where(exam_station_public_observers: {hidden: nil}, exams: {exam_type_id: args[:exam_types]}).uniq
          .find_each do |public_observer|

        possible_post_code = 6

        line(
            public_observer.guid, # 1. GUID
            transform_hidden(public_observer.hidden), # 2. Признак удаленной строки
            nil, # 3. Код работника ППЭ
            public_observer.second_name, # 4. Фамилия
            public_observer.first_name, # 5. Имя
            public_observer.middle_name, # 6. Отчество
            nil, # 7. GUID ОУ основного места работы
            'Общественный наблюдатель', # 8. Должность по основному месту работы
            'Общественный наблюдатель', # 9. Основное место работы (если не ОУ)
            nil, # 10. GUID МОУО, к которому прикреплён работник
            public_observer.document_series, # 11. Серия документа
            public_observer.document_number, # 12. Номер документа
            public_observer.document_type.nil? ? 7 : public_observer.document_type.fct_ege_code, # 13. Код типа документа, удостоверяющего личность
            nil, # 14. Пол
            public_observer.birthday.year, # 15. Год рождения
            transform_time(public_observer.created_at), # 16. Дата-время создания
            transform_time(public_observer.updated_at), # 17. Дата-время изменения
            nil, # 18. Признак участия в ЕГЭ прошлых лет
            nil, # 19. Общий преподавательский стаж работы
            nil, #20 Квалификация работника ППЭ
            possible_post_code, #21 Код Возможной должности в ППЭ
            nil #22 Код уровня проф. образования
        )
      end
    end
end