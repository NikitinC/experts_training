namespace :oge_2017 do
  task :participants_exam_soft_ware, [:exam_type_ids, :exam_codes] => :prepare do
    def transform_software language_code
      @config[:software_codes][language_code.to_i]
    end

    #def transform_programming_language language_code
    #  @config[:programming_language_codes][language_code.to_i]
    #end

    def transform_spreadsheet_editor editor_code
      @config[:spreadsheet_editor_codes][editor_code.to_i]
    end

    ExamStudent.find_each do |es|
      exam_code = federal_exam_code es.exam
      if exam_code && es.student && es.programming_language_code.present?
        line(
            es.student.guid, # 1. GUID участника
            exam_code, # 2. Код дня экзамена
            transform_software(es.programming_language_code), # 3. Код ПО
            #transform_programming_language(es.programming_language_code) # 4. Код Языка программирования
        )
        line(
            es.student.guid, # 1. GUID участника
            exam_code, # 2. Код дня экзамена
            transform_spreadsheet_editor(es.spreadsheet_editor_code), # 3. Код ПО
        )
      end
    end
  end
end