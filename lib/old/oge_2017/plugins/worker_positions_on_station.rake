# coding: utf-8

namespace :oge_2017 do
  task :worker_positions_on_station, [:exam_type_ids, :exam_codes] => :prepare do
    ExamStationEmployee.find_each do |ese|
      # puts ese.station_post.fiscode


      exam = ese.exam_station.try :exam

      role = ese.station_post.role
      if role == 0 then
        role = nil
      end

      next if exam.nil?
      next if exam.exam_type_id.nil?

      exam_code = 0
      exam_code = exam.code if exam.exam_type_id.in? [22, 23, 25, 26]


      # по данным из столбца ExamFormatCode таблицы rbd_ParticipantsExams выявил, что
      # 1 - устная форма; 2 - диктант; 3 - изложение; 4 - сочинение
      # в РБД коды ФЦТ данных экзаменов увеличил на 6000 - устная форма
      # 5000 - диктант; 4000 - изложение; 3000 - сочинение

      # puts es.station.station_additional_feature.fiscode

      if exam_code > 6000 then #устная часть
        exam_code = exam_code - 6000
      end
      if exam_code > 5000 then #диктант
        exam_code = exam_code - 5000
      end
      if exam_code > 4000 then #изложение
        exam_code = exam_code - 4000
      end
      if exam_code > 3000 then #сочинение
        exam_code = exam_code - 3000
      end




      station_post_code = ese.station_post.fiscode

      if exam && station_post_code && ese.exam_station && ese.employee && (exam_code > 0)
        line(
            ese.employee.guid, # 1. GUID работника ППЭ
            exam_code, # 2. Код дня экзамена
            ese.exam_station.station.guid, # 3. GUID ППЭ
            station_post_code, # 4. Код должности в ППЭ
            transform_time(ese.created_at), # 5. Дата-время создания
            role # 6. Код роли организатора
        )
      end
    end

  #   ExamStationPublicObserver.find_each do |espo|
  #     exam_code = federal_exam_code(espo.exam_station.try :exam)
  #     station_post_code = 6
  #     if exam_code
  #       line(
  #           espo.public_observer.guid, # 1. GUID работника ППЭ
  #           exam_code, # 2. Код дня экзамена
  #           espo.exam_station.station.guid, # 3. GUID ППЭ
  #           station_post_code, # 4. Код должности в ППЭ
  #           transform_time(espo.created_at), # 5. Дата-время создания
  #           nil # 6. Код роли организатора
  #       )
  #     end
  #   end
  # end

  # def transform_station_post_code(code)
  #   return {
  #       1 => 1,
  #       2 => 2,
  #       3 => 3,
  #       4 => 4,
  #       5 => 5,
  #       10 => 7,
  #       11 => 7,
  #       14 => 2,
  #       15 => 2,
  #       17 => 2,
  #       16 => 2,
  #       9 => 3,
  #       7 => 3,
  #       8 => 3
  #   }[code.to_i]
  end
end