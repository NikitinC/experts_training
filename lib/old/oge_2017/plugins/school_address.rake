namespace :oge_2017 do
  task :school_address, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do

      Ou.find_each do |item|
        line(
            item.legal_school_address_guid, # 1. GUID записи
            item.guid, # 2. GUID ОО
            item.address_guid, # 3. GUID адреса
            2, # 4. Код Типа адреса объекта
            item.created_at, # 5. Дата-время создания
            item.updated_at, # 6. Дата-время изменения
        )

        line(
            item.school_address_guid, # 1. GUID записи
            item.guid, # 2. GUID ОО
            item.address_guid, # 3. GUID адреса
            1, # 4. Код Типа адреса объекта
            item.created_at, # 5. Дата-время создания
            item.updated_at, # 6. Дата-время изменения
        )

      end
    end
  end
end