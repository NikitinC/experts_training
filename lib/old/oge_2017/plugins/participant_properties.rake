# coding: utf-8

namespace :oge_2017 do
  task :participant_properties => :prepare do
    puts 'use erbd_gia_reg_17_66'
    puts 'delete from rbd_ParticipantProperties where Property in (5, 12, 6, 8, 13, 7)'

    ActiveRecord::Base.uncached do

      Student \
          .includes('attestation_form_students')
          .where({attestation_form_students: {attestation_form_code: [AttestationForm::OGE, AttestationForm::GVE9], hidden: nil}})
          .find_each do |s|

            # next if s.id != 764015

            hidden = s.hidden.present?

            propertyID = 0
            pvalue = 0


            if s.foreign_middle_education then
                propertyID = 5
                pvalue = 1
                puts ("insert into rbd_ParticipantProperties values ('%s'" % SecureRandom.uuid() + ", '%s'" % s.guid + ", %d" % propertyID + ", %d" % pvalue + ", 66, NULL);")
            end

            if s.has_composition then
                propertyID = 12
                pvalue = 1
                puts ("insert into rbd_ParticipantProperties values ('%s'" % SecureRandom.uuid() + ", '%s'" % s.guid + ", %d" % propertyID + ", %d" % pvalue + ", 66, NULL);")
            end

            if s.closed_institution then
                propertyID = 6
                pvalue = 1
                puts ("insert into rbd_ParticipantProperties values ('%s'" % SecureRandom.uuid() + ", '%s'" % s.guid + ", %d" % propertyID + ", %d" % pvalue + ", 66, NULL);")
            end

            if s.migrant then
                propertyID = 8
                pvalue = 1
                puts ("insert into rbd_ParticipantProperties values ('%s'" % SecureRandom.uuid() + ", '%s'" % s.guid + ", %d" % propertyID + ", %d" % pvalue + ", 66, NULL);")
            end

            if s.comment != nil and s.comment != ''  then
                propertyID = 13
                pvalue = s.comment
                puts ("insert into rbd_ParticipantProperties values ('%s'" % SecureRandom.uuid() + ", '%s'" % s.guid + ", %d" % propertyID + ", '%s'" % pvalue + ", 66, NULL);")
            end


            if s.limited_facilities_group_code > 1 || s.on_home then
                propertyID = 7
                pvalue = 1
                puts ("insert into rbd_ParticipantProperties values ('%s'" % SecureRandom.uuid() + ", '%s'" % s.guid + ", %d" % propertyID + ", %d" % pvalue + ", 66, NULL);")
            end


      end
    end
  end
end