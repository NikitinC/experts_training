# coding: utf-8

namespace :oge_2017 do
  task :station_worker_on_station, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do
      Employee.find_each do |e|
        if !e.station_post_code_oge.nil? # сотрудники на ЕГЭ

          station = nil
          guid_worker = e.guid
          guid_station = ''

          next if e.on_station_oge.nil?
          # строка ниже выдает ошибку только при наличие сотрудников, должности которых ХЗ как определены

          station_post_fiscode = StationPost.where(code: e.station_post_code_oge).first!.fiscode
          # station_post_role = StationPost.where(code: e.station_post_code).first!.role


          # Перебор тех ППЭ, к которым может быть прикреплен работник и убеждаемся, что это ЕГЭ

          Station.where(:code => e.on_station).joins('INNER JOIN "exam_types_stations" ON "exam_types_stations"."station_id" = "stations"."id" AND "exam_types_stations"."exam_type_id" in (22, 23, 25, 26)').find_each do |st|
            station = st.exam_types if !e.on_station_oge.nil? && !(e.on_station_oge == '') && station.nil?
            guid_station = st.guid
          end


          if (guid_station != '') and (station_post_fiscode > 0) then
            line(
                  guid_worker, # 1. GUID работника ППЭ
                  guid_station, # 2. GUID ППЭ
                  0,            # 3. Признак прикрепления
                  station_post_fiscode,# 4. Код должности в ППЭ
                  transform_time(e.created_at), # 16. Дата-время создания
                  transform_time(e.updated_at), # 17. Дата-время изменения
            )
          end

        end
      end

    end
  end
end