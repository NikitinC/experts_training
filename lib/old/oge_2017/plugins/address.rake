namespace :oge_2017 do
  task :address, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do

      Ou.find_each do |item|
        fail if item.address_guid.nil?
        fail if item.legal_address_guid.nil?
        fail "Address is blank id  - #{item.id}" if blank_address? item

        line(
            item.address_guid, # 1. GUID адреса
            item.postcode, # 2. Индекс
            item.human_settlement_type_code, # 3. Код типа населенного пункта
            item.settlement_name, # 4. Наименование населенного пункта
            item.street_type_code, # 5. Код типа улицы
            item.street_name, # 6. Наименование улицы
            item.building_type_code, # 7. Код типа строения
            item.building_number, # 8. Номер здания
            item.ate.try(:okato), # 9. Код ОКАТО района
            item.created_at, # 10. Дата-время создания
            item.updated_at, # 11. Дата-время изменения
        )

        line(
            item.legal_address_guid, # 1. GUID адреса
            item.legal_postcode, # 2. Индекс
            item.legal_human_settlement_type_code, # 3. Код типа населенного пункта
            item.legal_settlement_name, # 4. Наименование населенного пункта
            item.legal_street_type_code, # 5. Код типа улицы
            item.legal_street_name, # 6. Наименование улицы
            item.legal_building_type_code, # 7. Код типа строения
            item.legal_building_number, # 8. Номер здания
            item.ate.try(:okato), # 9. Код ОКАТО района
            item.created_at, # 10. Дата-время создания
            item.updated_at, # 11. Дата-время изменения
        )
      end

      Station.in_home.find_each do |item|
        next if (item.exam_types.map(&:id) & [5, 19, 24]).empty?

        line(
            item.address_guid, # 1. GUID адреса
            item.postcode, # 2. Индекс
            item.human_settlement_type_code, # 3. Код типа населенного пункта
            item.settlement_name, # 4. Наименование населенного пункта
            item.street_type_code, # 5. Код типа улицы
            item.street_name, # 6. Наименование улицы
            item.building_type_code, # 7. Код типа строения
            item.building_number, # 8. Номер здания
            item.ou.try(:ate).try(:okato), # 9. Код ОКАТО района
            item.created_at, # 10. Дата-время создания
            item.updated_at, # 11. Дата-время изменения
        )
      end

    end
  end
end