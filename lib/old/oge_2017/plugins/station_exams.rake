namespace :oge_2017 do
  task :station_exams, [:exam_type_ids, :exam_codes] => :prepare do

    ExamStation.find_each do |es|
      exam_code = 0
      exam_code = es.exam.code if es.exam.exam_type_id.in? [22, 23, 25, 26]
      gve_code = nil

      next if es.exam.exam_type_id.nil?
      next if es.exam.code.nil?
      next if exam_code == 0

      # по данным из столбца ExamFormatCode таблицы rbd_ParticipantsExams выявил, что
      # 1 - устная форма; 2 - диктант; 3 - изложение; 4 - сочинение
      # в РБД коды ФЦТ данных экзаменов увеличил на 6000 - устная форма
      # 5000 - диктант; 4000 - изложение; 3000 - сочинение

      # puts es.station.station_additional_feature.fiscode

      if exam_code > 6000 then #устная часть
        exam_code = exam_code - 6000
        gve_code = 1
      end
      if exam_code > 5000 then #диктант
        exam_code = exam_code - 5000
        gve_code = 2
      end
      if exam_code > 4000 then #изложение
        exam_code = exam_code - 4000
        gve_code = 3
      end
      if exam_code > 3000 then #сочинение
        exam_code = exam_code - 3000
        gve_code = 4
      end

      next if es.station.nil? || exam_code.nil?

      tech = 0

      tech = 2 if es.station.station_additional_feature.fiscode.in? [260, 264, 268, 292, 300, 328, 356, 364, 772, 776, 780, 840, 868, 876]

      tech = tech + 1 if es.station.station_additional_feature.fiscode.in? [36, 44, 292, 300, 328, 356, 364, 868]

      if tech == 0 then
        tech = 4
      end

      line(
        es.station.guid,
        exam_code,
        es.capacity,
        transform_time(es.created_at),
        tech # Технологии ППЭ
      )

    end
  end
end


