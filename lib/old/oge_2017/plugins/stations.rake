namespace :oge_2017 do
  task :stations, [:exam_type_ids, :exam_codes] => :prepare do
    ActiveRecord::Base.uncached do
      Station \
          .includes('station_additional_feature')
              .find_each do |s|

        next if [4314].include? s.code.to_i

        fail "No ou for ppe #{s.id}" if s.ou.nil?

        next if (s.exam_types.map(&:id) & [22, 23, 25, 26]).empty?


        # нужно проработать №4, проверить формирование №15
        sql = <<SQL
SELECT s.code as code, coalesce(c1.cnt, 0) as podg, coalesce(c2.cnt, 0) as proved, coalesce(c3.cnt, 0) as gve
	FROM stations s
    LEFT JOIN (SELECT s.id as station_id, count (c1.id) as cnt
               from stations s
               JOIN classrooms c1 ON c1.station_id = s.id
               where c1.classroom_verbal_part_sing_code = 4 and c1.hidden is NULL
               group by s.id
              ) c1 ON c1.station_id = s.id
   LEFT JOIN (SELECT s.id as station_id, count (c2.id) as cnt
               from stations s
               JOIN classrooms c2 ON c2.station_id = s.id
               where c2.classroom_verbal_part_sing_code = 2 and c2.hidden is NULL
               group by s.id
              ) c2 ON c2.station_id = s.id
    LEFT JOIN (SELECT s.id as station_id, count (c.id) as cnt
               from stations s
               JOIN classrooms c ON c.station_id = s.id
               JOIN attestation_form_classrooms afc ON afc.classroom_id = c.id and afc.attestation_form_code = 9
               where c.hidden is NULL
               group by s.id
              ) c3 ON c3.station_id = s.id             
	where s.guid = (s.guid)
SQL
        station_exam_form_code = proved = podg = gve = 0

        ActiveRecord::Base.connection.execute(sql).each do |form|
          podg = form['podg'].to_f
          proved = form['proved'].to_f
          gve = form['gve'].to_f
        end

        if proved > 0 then
          station_exam_form_code = station_exam_form_code + 2
        end

        if podg > 0 then
          station_exam_form_code = station_exam_form_code + 4
        end

        if gve > 0 then
          station_exam_form_code = station_exam_form_code + 8
        end

        line(
            s.guid, # 1. GUID
            (s.hidden.present? ? 1 : 0), # 2. Признак удаленной строки
            s.code, # 3. Код ППЭ
            s.fbd_name, # 4. Наименование ППЭ
            s.in_home? ? '' : s.ou.guid, # 5. GUID ОУ, на территории которого находится ППЭ
            nil, #TODO 6. GUID ППОИ, на территории которого обрабатываются бланки ППЭ
            prepare_phone(s.phones), # 7. Телефон(ы) ППЭ
            s.email, # 8. Адрес(а) электронной почты ППЭ
            transform_bool(s.tom), # 9. Признак расположения ППЭ в ТОМ
            s.ou.mouo.guid, # 10. GUID МОУО, к которому относится ППЭ
            s.ou.ate.guid, # 11. GUID АТЕ, на территории которой расположен ППЭ
            transform_time(s.created_at), # 12. Дата-время создания
            transform_time(s.updated_at), # 13. Дата-время обновления
            station_cctv(s), # 14. Наличие видеонаблюдения
            station_exam_form_code, # todo WTF # 15. Форма экзамена
            s.station_additional_feature.fiscode,  #16 Доп. Признаки ППЭ
            station_address(s) # 17. Адрес ППЭ
        )
      end
    end
  end


  def station_address(station)
    station.ou.try(:address_guid) # нужно что-то сделать для ППЭ на дому
  end

  def station_cctv station
    station.hq_cctv.present? || station.corridor_cctv.present? || station.entrance_cctv.present? ? 1 : 0
  end
end
