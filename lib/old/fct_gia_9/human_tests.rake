namespace :fct_gia_9 do

  task :human_tests => :prepare do

    @xml.ArrayOfHumanTestsDto do |arrayOfHumanTestsDto|
      ExamStudent.where(exam_id: @config[:exam_ids]).find_each do |exam_student|
        arrayOfHumanTestsDto.HumanTestsDto do |humanTestsDto|
          humanTestsDto.UID exam_student.guid
          humanTestsDto.Region @config[:region][:code]
          humanTestsDto.ParticipantUID exam_student.student.try(:guid)
          humanTestsDto.ExamDate transform_date(exam_student.exam.try(:date))
          humanTestsDto.SubjectCode exam_student.exam.try(:subject_code)
          humanTestsDto.DepartmentCode "#{@config[:region][:code]}00"
          humanTestsDto.StationCode exam_student.exam_station.try(:station).try(:code)
          humanTestsDto.AuditoriumCode ''
          humanTestsDto.VariantCode ''
          humanTestsDto.ProcessCondition 6
        end
      end
    end
  end

end