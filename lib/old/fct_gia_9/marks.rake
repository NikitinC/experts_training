namespace :fct_gia_9 do
  task :marks => :prepare do
    filenames_subjects = {
        1 => 'rus.csv',
        9 => 'ang.csv',
        6 => 'bio.csv',
        3 => 'fiz.csv',
        11 => 'fran.csv',
        7 => 'ist.csv',
        2 => 'math.csv',
        10 => 'nem.csv'
    }
    time = Time.now.to_s.gsub(*@config[:time_transliterate_rules])
    folder = File.join(Rails.root, 'tmp', 'fct_gia_9_errors', 'marks')
    `mkdir -p #{folder}`

    ActiveRecord::Base.uncached do

      @xml.ArrayOfMarksDto do |arrayOfMarksDto|
        filenames_subjects.each do |subject_code, filename|
          conflicts = File.new File.join(folder, "marks_#{subject_code}_conflicts_#{time}.log"), 'w'

          file = File.open File.join(Rails.root, 'tmp', 'marks', filename), 'r'

          #line_count = 0
          file.each_line do |line|
            #line_count += 1
            #puts line_count
            second_name, first_name, middle_name = line.gsub('\n', '').split(';')[0].split(/\s+/)
            items = ExamStudent.includes(:student, :exam)
            .where(:students => {:first_name => first_name, :second_name => second_name, :middle_name => middle_name})
            .where(:exam_id => @config[:exam_ids], :exams => {:subject_code => subject_code})
            item = items.first
            student = item.try(:student)
            if item.nil?
              conflicts.puts "Can't found in DB: #{second_name} #{first_name} #{middle_name}"
            elsif items.count != 1
              conflicts.puts "Ambiguous name: #{student.full_name}"
            else
              data = line.gsub('\n', '').split(';')

              arrayOfMarksDto.MarksDto do |marksDto|
                marksDto.UID item.guid
                marksDto.Region @config[:region][:code]
                marksDto.PrimaryMark data[2]
                marksDto.PercentMark data[3]
                marksDto.Mark5 data[4]
                marksDto.PrimaryMarkA data[5]
                marksDto.TestResultA data[6]
                marksDto.PrimaryMarkB data[7]
                marksDto.TestResultB data[8]
                marksDto.PrimaryMarkC data[9]
                marksDto.TestResultC data[10]
                marksDto.PrimaryMarkD data[11] if data[11]
                marksDto.TestResultD data[12] if data[12]
              end
            end
          end

        end
      end
    end
  end
end