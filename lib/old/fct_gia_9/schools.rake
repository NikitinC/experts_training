namespace :fct_gia_9 do

  task :schools => :prepare do
    @xml.ArrayOfSchoolsDto do |arrayOfSchoolsDto|
      Ou.unscoped.where("valid_cache = true or code like '%0000'").find_each do |ou|
        next if ou.code.blank?

        #todo think
        #next if ou.students.includes(:exam_students).where(exam_students: {exam_id: @config[:exam_ids], hidden: nil}).count.zero?
        next if ou.students
          .where(valid_cache: true, groups: {valid_cache: true, :number => [9, 10]})
          .where(attestation_form_code: [2, 3])
          .count.zero?


        arrayOfSchoolsDto.SchoolsDto do |schoolsDto|
          schoolsDto.UID ou.guid
          schoolsDto.Region @config[:region][:code]
          schoolsDto.GovernmentUID ou.mouo.try(:guid)
          schoolsDto.SchoolCode ou.code
          schoolsDto.SchoolName ou.full_name
          schoolsDto.SchoolKind transform_school_kind_code(ou.ou_kind_code)
          schoolsDto.SchoolProperty ou.business_entity_type_code #Организационно-правовая форма
          schoolsDto.AreaUID ou.ate.try(:guid)
          schoolsDto.TownType transform_human_settlement_type(ou.human_settlement_type_code)
          schoolsDto.IsTOM transform_bool(ou.tom)
          schoolsDto.LawAddress ou.legal_address
          schoolsDto.Address ou.real_address
          schoolsDto.ShortName ou.short_name
          schoolsDto.Township transform_ate_okato(ou.ate.try(:okato))
        end
      end
    end
  end

end