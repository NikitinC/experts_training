namespace :fct_gia_9 do

  task :governments => :prepare do
    @xml.ArrayOfGovernmentsDto do |arrayOfGovernmentsDto|
      Mouo.find_each do |mouo|
        arrayOfGovernmentsDto.GovernmentsDto do |governmentsDto|
          governmentsDto.UID mouo.guid
          governmentsDto.Region @config[:region][:code]
          governmentsDto.GovernmentCode mouo.code
          governmentsDto.GovernmentName mouo.name
          governmentsDto.LawAddress mouo.legal_address
          governmentsDto.Address mouo.real_address
        end
      end
    end
  end

end