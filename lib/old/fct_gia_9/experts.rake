namespace :fct_gia_9 do
  task :experts => :prepare do
    filename = 'experts.txt'

    time = Time.now.to_s.gsub(*@config[:time_transliterate_rules])
    folder = File.join(Rails.root, 'tmp', 'fct_gia_9_errors')
    `mkdir -p #{folder}`
    conflicts = nil

    @xml.ArrayOfExpertsDto do |arrayOfExpertsDto|
      file = File.open File.join(Rails.root, 'tmp', filename), 'r'

      file.each_line do |line|
        second_name, first_name, middle_name = line.gsub('\n', '').split(/\s+/)
        employees = Employee.where(:first_name => first_name, :second_name => second_name, :middle_name => middle_name)
        next unless employees.first

        if employees.count != 1
          conflicts ||= File.new File.join(folder, 'experts_conflicts_' + time + '.log'), 'w'
          conflicts.puts "Ambiguous name: #{employees.first.full_name}"
        else
          employee = employees.first
          arrayOfExpertsDto.ExpertsDto do |expertsDto|
            expertsDto.UID employee.guid
            expertsDto.Region @config[:region][:code]
            expertsDto.ExpertCode ''
            expertsDto.Surname employee.second_name
            expertsDto.Name employee.first_name
            expertsDto.SecondName employee.middle_name
            expertsDto.DocumentSeries employee.document_series
            expertsDto.DocumentNumber employee.document_number
            expertsDto.DocumentType transform_document_type_code(employee.document_type_code)
          end
        end
      end
    end
  end
end