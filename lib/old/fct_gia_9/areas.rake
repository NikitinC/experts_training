namespace :fct_gia_9 do

  task :areas => :prepare do
    @xml.ArrayOfAreasDto do |arrayOfAreasDto|
      Ate.where(valid_cache: true).find_each do |ate|
        arrayOfAreasDto.AreasDto do |areasDto|
          areasDto.UID ate.guid
          areasDto.Region @config[:region][:code]
          areasDto.AreaCode ate.code
          areasDto.AreaName ate.name
        end
      end
    end
  end

end