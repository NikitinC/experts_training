namespace :fct_gia_9 do
  task :certificates => :prepare do
    filename = 'certificates.csv'

    time = Time.now.to_s.gsub(*@config[:time_transliterate_rules])
    folder = File.join(Rails.root, 'tmp', 'fct_gia_9_errors')
    `mkdir -p #{folder}`
    conflicts = File.new File.join(folder, 'certificates_conflicts_' + time + '.log'), 'w'

    @xml.ArrayOfPrnfCertificatePrintMainDto do |arrayOfPrnfCertificatePrintMainDto|
      file = File.open File.join(Rails.root, 'tmp', filename), 'r'

      file.each_line do |line|
        second_name, first_name, middle_name = line.gsub('\n', '').split(';')[1].split(/\s+/)
        items = Student.where(:first_name => first_name, :second_name => second_name, :middle_name => middle_name)
        student = items.first

        if student.nil?
          conflicts.puts "Can't found in DB: #{second_name} #{first_name} #{middle_name}"
        elsif items.count != 1
          conflicts.puts "Ambiguous name: #{student.full_name}"
        else
          data = line.gsub('\n', '').split(';')

          arrayOfPrnfCertificatePrintMainDto.PrnfCertificatePrintMainDto do |prnfCertificatePrintMainDto|
            prnfCertificatePrintMainDto.UID (student.guid + 'certificate')
            prnfCertificatePrintMainDto.Region @config[:region][:code]
            prnfCertificatePrintMainDto.ParticipantUID student.guid
            prnfCertificatePrintMainDto.LicenseNumber data[2]
            prnfCertificatePrintMainDto.PrintTime data[3]
          end
        end
      end
    end
  end
end