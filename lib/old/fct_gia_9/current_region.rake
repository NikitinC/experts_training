namespace :fct_gia_9 do

  task :current_region => :prepare do
    @xml.ArrayOfCurrentRegionDto do |arrayOfCurrentRegionDto|
      arrayOfCurrentRegionDto.CurrentRegionDto do |currentRegionDto|
        currentRegionDto.Region @config[:region][:code]
        currentRegionDto.Name @config[:region][:name]
      end
    end
  end

end