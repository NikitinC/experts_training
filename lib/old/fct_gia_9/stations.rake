namespace :fct_gia_9 do

  task :stations => :prepare do
    @xml.ArrayOfStationsDto do |arrayOfStationsDto|
      Station.joins(:exam_stations).where(exam_stations: {exam_id: @config[:exam_ids], hidden: nil})
      .uniq.valid.find_each do |station|
        arrayOfStationsDto.StationsDto do |stationsDto|
          stationsDto.UID station.guid
          stationsDto.Region @config[:region][:code]
          stationsDto.AreaUID station.ou.try(:ate).try(:guid)
          stationsDto.StationCode station.code
          stationsDto.StationName station.to_s
          stationsDto.StationAddress station.address
          stationsDto.SchoolUID station.ou.try(:guid)
          stationsDto.GovernmentUID station.ou.try(:mouo).try(:guid)
          stationsDto.sVolume station.capacity
          stationsDto.AuditoriumsCountNeeded station.classrooms.count
          stationsDto.IsTOM transform_bool(station.tom)
        end
      end
    end
  end

end