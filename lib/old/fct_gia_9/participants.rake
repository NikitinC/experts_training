namespace :fct_gia_9 do

  task :participants => :prepare do
    @xml.ArrayOfParticipantsDto do |arrayOfParticipantsDto|
      Student.includes(:group => :ou)
      .where(valid_cache: true, groups: {valid_cache: true, :number => [9, 10]})
      .where(attestation_form_code: [2, 3])
      .where("ous.valid_cache = true or ous.code like '%0000'").uniq.find_each do |student|
        arrayOfParticipantsDto.ParticipantsDto do |participantsDto|
          participantsDto.UID student.guid
          participantsDto.Region @config[:region][:code]
          participantsDto.ParticipantCode ''
          #TODO add stripping while saving to db; only cyrrilic characters are valid
          participantsDto.Name student.first_name.strip
          participantsDto.Surname student.second_name.strip
          participantsDto.SecondName student.middle_name.strip
          participantsDto.BirthDay transform_date(student.birthday)
          participantsDto.DocumentTypeCode transform_document_type_code(student.document_type_code)
          participantsDto.DocumentSeries student.document_series
          participantsDto.DocumentNumber student.document_number
          participantsDto.Sex transform_gender(student.gender)
          participantsDto.PClass transform_group_name(student.group)
          participantsDto.LimitPotencial transform_bool(student.special_seating)
          participantsDto.SchoolRegistrationUID student.group.try(:ou).try(:guid)
          participantsDto.Study student.study_form_code
          participantsDto.ParticipantCategory transform_participant_category_code(student.study_form_code)
        end
      end
    end
  end

end