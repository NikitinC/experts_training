#coding: utf-8
namespace :export do
  task :prepare => :export_tools do
  end

  task :practice_exam_ict => :prepare do
    @exam_type_id = 3

    line('Код ОУ',
         'Наименование ОУ',
         'ФИО',
         'Паспорт',
         'Тип экзамена',
         'Экзамен - Дата',
         'ППЭ',
         'Среда программирования',
         'Редактор электронных таблиц')

    ActiveRecord::Base.uncached do
      ExamStudent.includes(:exam).where(:exams => {:exam_type_id => @exam_type_id}).uniq.find_each do |exam_student|
        student = exam_student.student
        exam = exam_student.exam
        ou = student.try(:group).try(:ou)
        station = exam_student.exam_station.try(:station)

        line(ou.try(:code),
             ou.try(:full_name),
             student.try(:full_name),
             "#{student.try(:document_series)} #{student.try(:document_number)}",
             exam.exam_type.name,
             exam,
             station,
             exam_student.programming_language,
             exam_student.spreadsheet_editor,
        )
      end
    end
  end

end
