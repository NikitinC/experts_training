#coding: utf-8
namespace :export do
  task :ou_ips => :export_tools do

    line 'Код МОУО',
         'Код УО',
         'IP адрес',
         'Дата изменения в РБД IP адреса'

    ActiveRecord::Base.uncached do
      Mouo.find_each do |mouo|
        mouo.ous.find_each do |ou|
          users = User.where(:target_id => ou.id, :target_type => 'Ou')
          line ou.mouo.try(:code),
               ou.code,
               users.map { |u| u.ips }.join(' || '),
               users.map { |u|
                 RlogItem.where(:record_id => u.id, :record_type => 'User').map {|r| r.created_at if r.record_changes['ips']}.compact.last
               }.join(' || ')
        end
      end
    end
  end
end
