namespace :export do
  task :employees => :export_tools do
    app_helper = Class.new.extend ApplicationHelper
    relation = Employee

    line [
             I18n.t('activerecord.attributes.employee.second_name'),
             I18n.t('activerecord.attributes.employee.first_name'),
             I18n.t('activerecord.attributes.employee.middle_name'),
             I18n.t('activerecord.attributes.employee.document_series'),
             I18n.t('activerecord.attributes.employee.document_number'),
             I18n.t('activerecord.attributes.employee.document_date'),
             I18n.t('activerecord.attributes.employee.document_type_code'),
             I18n.t('activerecord.attributes.employee.document_given'),
             I18n.t('activerecord.attributes.employee.gender'),
             I18n.t('activerecord.attributes.employee.birthday'),
             I18n.t('activerecord.attributes.employee.pension_insurance'),
             I18n.t('activerecord.attributes.employee.inn'),
             I18n.t('activerecord.attributes.employee.post_index'),
             I18n.t('activerecord.attributes.employee.registration_address'),
             I18n.t('activerecord.attributes.employee.real_address'),
             I18n.t('activerecord.attributes.employee.home_phone'),
             I18n.t('activerecord.attributes.employee.office_phone'),
             I18n.t('activerecord.attributes.employee.email'),
             I18n.t('activerecord.attributes.employee.diploma_speciality'),
             I18n.t('activerecord.attributes.employee.education_code'),
             I18n.t('activerecord.attributes.employee.category_code'),
             I18n.t('activerecord.attributes.employee.category_year'),
             I18n.t('activerecord.attributes.employee.scientific_degree_code'),
             I18n.t('activerecord.attributes.employee.academic_rank_code'),
             I18n.t('activerecord.attributes.employee.academic_rank_diploma_number'),
             I18n.t('activerecord.attributes.employee.scientific_speciality_code'),
             I18n.t('activerecord.attributes.employee.teaching_experience'),
             I18n.t('activerecord.attributes.employee.full_experience'),
             I18n.t('activerecord.attributes.employee.tutor_subject_code'),
             I18n.t('activerecord.attributes.employee.group'),
             I18n.t('activerecord.attributes.exam_station_employee.exam_station'),
             I18n.t('activerecord.attributes.exam_station_employee.station_post'),
         ]

    ActiveRecord::Base.uncached do
      relation.find_each do |item|

        ese = item.exam_station_employees
        ese = [nil] unless ese.any?

        ese.each do |es|
          line [
                   item.second_name,
                   item.first_name,
                   item.middle_name,
                   item.document_series,
                   item.document_number,
                   item.document_date,
                   item.document_type,
                   item.document_given,
                   app_helper.bool_to_word(item.gender, :gender),
                   item.birthday,
                   item.employee_ous.map(&:ou).join('; '),
                   item.employee_ous.map(&:employee_post).join('; '),
                   item.pension_insurance,
                   item.inn,
                   item.post_index,
                   item.registration_address,
                   item.real_address,
                   item.home_phone,
                   item.office_phone,
                   item.email,
                   item.diploma_speciality,
                   item.education,
                   item.employee_category,
                   item.category_year,
                   item.scientific_degree,
                   item.academic_rank,
                   item.academic_rank_diploma_number,
                   item.scientific_speciality,
                   item.teaching_experience,
                   item.full_experience,
                   item.tutor,
                   item.group,
                   es.try(:exam_station),
                   es.try(:station_post)
               ]
        end
      end
    end
  end

end
