#coding: utf-8
namespace :export do
  task :short_ou => :export_tools do
    line "КОД МОУО",
         "Наименование МОУО",
         "Код ОУ",
         "Наименование ОУ",
         "ФИО руководителя ОУ",
         "Телефон руководителя ОУ",
         "E-mail руководителя ОУ",
         "ФИО ответственного за информационный обмен в ОУ",
         "Телефон ответственного за информационный обмен в ОУ",
         "E-mail ответственного за информационный обмен в ОУ",
         "Логин для сбора РБД"
    ActiveRecord::Base.uncached do
      Ou.all.find_each do |ou|
        line ou.mouo.try(:code),
             ou.mouo.try(:name),
             ou.code,
             ou.full_name,
             ou.chief_full_name,
             ou.chief_phone,
             ou.chief_email,
             ou.information_exchange_full_name,
             ou.information_exchange_phone,
             ou.information_exchange_email,
             ou.user.try(:login)
      end
    end
  end
end
