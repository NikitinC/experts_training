namespace :ikt do
  task :participants => :environment do
    exam_ids = ARGV
    exams = Exam.where :id => exam_ids
    programming_languages = ProgrammingLanguage.all
    exams.each do |exam|
      puts "==========#{exam}=========="
      ous = Ou.joins(groups: {students: :exam_students}).where(exam_students: {exam_id: exam.id}).uniq
      puts ["Code", "Short name", "Participans count", programming_languages.map(&:name)].flatten.join(";")
      ous.each do |ou|
        line = [ou.code, ou.short_name]
        exams = ExamStudent.includes(:student => :group).reorder("students.second_name").where(:groups => {:ou_id => ou.id}).where(:exam_id => exam.id)
        line.push exams.size
        programming_languages.each do |pl|
          line.push exams.with_custom_values(:programming_language => pl.id).size
        end
        puts line.join(";")
      end
    end
  end

  task :create_stations => :environment do
    exam_ids = ARGV
    ous = Ou.joins(groups: {students: :exam_students}).where(exam_students: {exam_id: exam_ids}).uniq.select { |ou| ou.stations.blank? }
    ous.each do |ou|
      s = ou.build_station address: ou.real_address, chief_fio: ou.chief_full_name, email: ou.chief_email, tom: false, phones: ou.chief_phone
      s.save validate: false
    end
  end

  task :assign_stations => :environment do
    exam_ids = ARGV
    Station.all.each do |s|
      s.exams = ExamStudent.includes(:student => :group).where(:groups => {:ou_id => s.ou.id}).where(:exam_id => exam_ids).map(&:exam).uniq
      s.save validate: false
    end
  end

  task :assign_students => :environment do
    exam_ids = ARGV
    exam_students = ExamStudent.where(:exam_id => exam_ids, :exam_station_id => nil)
    exam_students.each do |exam_student|
      exam_student.exam_station = exam_student.student.group.ou.stations.first.exam_stations(:exam_id => exam_student.exam_id).first
      if exam_student.exam_station && exam_student.save(:validate => false)
        puts "#{exam_student.id}: ok".green
      else
        puts "#{exam_student.id}: fail".red
      end
    end
  end

  task :create_classrooms => :environment do
    exam_ids = ARGV
    stations = Station.all
    stations.each do |station|
      exam_station_ids = station.exam_stations.map(&:id)
      es_count = ExamStudent.where(:exam_station_id => exam_station_ids).group(:exam_id).size.values.max || 0
      if es_count > 0
        need_classrooms = (es_count - station.capacity + 5) / 12
        need_classrooms.times do |i|
          c = station.classrooms.create name: i, number: i, organizer_order: true, rows_count: 2, seats_per_row: 6, short_name: i, special_sit: false
          if c.errors.empty?
            puts "#{station.id} (#{i}/#{need_classrooms}): ok".green
          else
            puts "#{station.id} (#{i}/#{need_classrooms}): fail".red
          end
        end
      end
    end
  end

  task :assign_employees => :environment do
    exam_ids = ARGV
    exams = Exam.where :id => exam_ids
    exams.each do |exam|
      exam.exam_stations.each do |exam_station|
        station = exam_station.station
        puts "#{station.code} | Create chief: " + assign_employee_if_no_exists(exam_station, 0, 1).present?.to_s
        puts "#{station.code} | Create PC operator: " + assign_employee_if_no_exists(exam_station, 12, 10).present?.to_s
        puts "#{station.code} | Create chief help: " + assign_employee_if_no_exists(exam_station, 1, 9).present?.to_s
        puts "#{station.code} | Create technical specialist: " + assign_employee_if_no_exists(exam_station, 12, 11).present?.to_s
        exam_station.station.classrooms.size.times do |i|
          puts "#{station.code} | Create employee in room #{i*2}: " + assign_employee_if_no_exists(exam_station, 1..50, 2).present?.to_s
          puts "#{station.code} | Create employee in room #{i*2+1}: " + assign_employee_if_no_exists(exam_station, 1..50, 2).present?.to_s
        end
        puts "#{station.code} | Create employee in not room: " + assign_employee_if_no_exists(exam_station, 1..50, 3).present?.to_s
        puts "#{station.code} | Create employee in not room: " + assign_employee_if_no_exists(exam_station, 1..50, 3).present?.to_s
      end
    end
  end

  def assign_employee_if_no_exists exam_station, ou_post_code, station_post_code
    ou = exam_station.station.ou
    employees = ou.employees.where(:employee_post_code => ou_post_code)
    already_employees = exam_station.employees.where(:id => employees.map(&:id))
    free_employees = employees - already_employees
    employee = free_employees.first
    ExamStationEmployee.create employee_id: employee.id, exam_station_id: exam_station.id, station_post_code: station_post_code if employee
  end

  task :export => :environment do
    exam_ids = ARGV
    exams = Exam.where :id => exam_ids
    stations = Station.joins(:exam_stations).where(exam_stations: {exam_id: exams.map(&:id)}).to_a.uniq
    stations.each do |station|
      puts "#{station.code}, #{station.ou.code}, #{station.ou.short_name}"
      exams.each do |exam|
        puts [nil, exam].join(";")
        exam_station = station.exam_stations.where(exam_id: exam.id).first
        if exam_station
          puts [nil, nil, "Station workers"].join(";")
          exam_station_employees = ExamStationEmployee.where(exam_station_id: exam_station.id)
          exam_station_employees.each do |exam_station_employee|
            puts [nil, nil, nil, exam_station_employee.employee.full_name, exam_station_employee.station_post].join(";")
          end
          puts [nil, nil, "Participants"].join(";")
          exam_students = ExamStudent.includes(:student).reorder("students.second_name").where(exam_station_id: exam_station.id)
          exam_students.each do |exam_student|
            puts [nil, nil, nil, exam_student.student, ProgrammingLanguage.where(:code => exam_student.custom_values[:programming_language])].join(";")
          end
          puts [nil, nil, "Participants count"].join(";")
          puts [nil, nil, nil, exam_students.size].join(";")
        else
          puts [nil, nil, "Station is not assign for this exam"].join(";")
        end
      end
    end
  end

  task :exam_station_employees => :environment do
    exam_station_employees =  ExamStationEmployee.joins(:employee => :employee_subjects).where(:employee_subjects => {:subject_code => 5})

    exam_station_employees.each do |ep|
      arr = []
      date = ep.try(:exam_station).try(:exam) && I18n.l(ep.try(:exam_station).try(:exam).try(:date))
      arr << date
      arr << ep.try(:exam_station).try(:station).try(:code)
      arr << ep.try(:employee).try(:full_name)
      arr << ep.try(:station_post)
      puts arr.join(";")
    end
  end
end
