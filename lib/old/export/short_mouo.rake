#coding: utf-8
namespace :export do
  task :short_mouo => :export_tools do
    line "КОД МОУО",
         "Наименование МОУО",
         "ФИО руководителя МОУО",
         "телефон руководителя МОУО",
         "e-mail руководителя МОУО",
         "ФИО ответственного за информационный обмен в МОУО",
         "телефон ответственного за информационный обмен в МОУО",
         "e-mail ответственного за информационный обмен в МОУО",
         "логин для сбора РБД"
    Mouo.all.each do |mouo|
      line mouo.code,
           mouo.name,
           mouo.chief_full_name,
           mouo.chief_phone,
           mouo.chief_email,
           mouo.information_exchange_specialist_full_name,
           mouo.information_exchange_specialist_phone,
           mouo.information_exchange_specialist_email,
           mouo.user.try(:login)
    end
  end
end
