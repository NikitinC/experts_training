#coding: utf-8
namespace :export do
  task :employees_in_station => :export_tools do
    line(
        "Код ППЭ",
        "Ф.И.О сотрудника",
        "паспортные данные сотрудника в аудитоии",
        "код ОУ, в котором работает сотрудника",
        "специализация сотрудника",
        "дата экз, за кототорй  закреплен сотрудник",
        "должность в ппэ (по этой колонке можно найти организаторов)"
    )

    #ege_exam_type_ids = APP_CONFIG['ege_exam_type_ids'].map(&:to_i)

    ActiveRecord::Base.uncached do

      ExamStationEmployee.includes(exam_station: :station).order('stations.code').find_each do |ese|
        employee = ese.employee
        next if employee.nil?

        line(
            ese.exam_station.try(:station).try(:code),
            employee.full_name,
            employee.document_full_number,
            employee.employee_ous.map(&:ou).map(&:code).join(', '),
            employee.employee_ous.map{|eo| {eo.ou.try(:code) => eo.employee_post.try(:to_s)}}.join(', '),
            ese.exam_station.try(:exam),
            ese.station_post
        )
      end

    end
  end
end
