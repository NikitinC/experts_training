#coding: utf-8
namespace :export do
  task :crowded_stations => :export_tools do

    line I18n.t("activerecord.models.station"),
         "Вместительность ППЭ",
         "Зарегистрированных учеников"

    ActiveRecord::Base.uncached do
      Station.find_each do |station|
        station.exam_students.includes(:exam).where(:exams => {:exam_type_id => [4,5,6]}).group(:date).size.each do |date, count|
          line [
                   station.to_s,
                   station.capacity,
                   count
               ] if station.capacity < count || station.capacity == 0
        end
      end
    end

  end
end
