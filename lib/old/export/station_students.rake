#coding: utf-8
namespace :export do
  task :station_students => :export_tools do
    line(
        "Код ППЭ",
        "Кол-во аудиторий",
        "Количество участников ЕГЭ, закрепленных за данным ППЭ",
        "Вместимость ППЭ",
        "Количество закрепленных участников по датам",
        "Количество участников ЕГЭ в данном ППЭ со спецсрассадкой",
        "Количество аудиторий со спецрассадкой",
        "Ф.И.О руководителя ППЭ",
        "Количество организаторов в аудитории",
        "Количество организаторов вне аудитории",
        "Количество технических специалистов в ППЭ"
    )

    ege_exam_type_ids = APP_CONFIG['ege_exam_type_ids'].map(&:to_i)

    ActiveRecord::Base.uncached do

      Station.find_each do |s|
        students_relation = Student.filtering(station_id: s.id, exam_type_id: ege_exam_type_ids)
        employees_relation = Employee.filtering(station_id: s.id)

        group_by_date = ExamStudent.includes(:exam_station, :exam)
        .where(exam_stations: {station_id: s.id}, exams: {exam_type_id: ege_exam_type_ids})
        .uniq('students.student_id')
        .group('exams.date').count

        line(
            s.code,
            s.classrooms.count,
            students_relation.count,
            s.capacity,
            group_by_date.inspect,
            students_relation.where(special_seating: true).count,
            s.classrooms.where(special_sit: true).count,
            s.chief_fio,
            employees_relation.filtering(station_post_code: '2').count,
            employees_relation.filtering(station_post_code: '3').count,
            employees_relation.filtering(station_post_code: '11').count
        )
      end

    end
  end
end
