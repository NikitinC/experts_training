# coding: utf-8

namespace :export do
  task :occupancy => :export_tools do
    Mouo.all.each do |mouo|
      line mouo
      mouo.ous.each do |ou|
        line nil, ou
        groups_occupancy = {}
        ou.groups.each do |group|
          groups_occupancy[group.number] ||= {all: 0, valid: 0}
          groups_occupancy[group.number][:all] += group.students.size
          groups_occupancy[group.number][:valid] += group.students.where(valid_cache: true).size
        end
        groups_occupancy.each do |number, occupancy|
          o = occupancy[:all].zero? ? "Нет учеников" : ((occupancy[:valid] * 100 / occupancy[:all]).to_s + "%")
          line nil, nil, number, o
        end

      end
    end
  end
end
