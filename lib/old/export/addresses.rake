#coding: utf-8
namespace :export do
  task addresses: :export_tools do

    line [
             'Код ОУ',
             'GUID',
             'Тип',
             I18n.t("activerecord.attributes.ou.legal_postcode"),
             I18n.t("activerecord.attributes.ou.legal_human_settlement_type") + ", (Код)",
             I18n.t("activerecord.attributes.ou.legal_human_settlement_type"),
             I18n.t("activerecord.attributes.ou.legal_settlement_name"),
             I18n.t("activerecord.attributes.ou.legal_street_type_code"),
             I18n.t("activerecord.attributes.ou.legal_street_name"),
             I18n.t("activerecord.attributes.ou.legal_building_type_code"),
             I18n.t("activerecord.attributes.ou.legal_building_number"),
         ]

    ActiveRecord::Base.uncached do
      Ou.find_each do |ou|
        line [
                 ou.code,
                 ou.address_guid,
                 'Фактический',
                 ou.postcode,
                 ou.human_settlement_type_code,
                 ou.human_settlement_type,
                 ou.settlement_name,
                 Ou::STREET_TYPES[ou.street_type_code],
                 ou.street_name,
                 Ou::BUILDING_TYPES[ou.building_type_code],
                 ou.building_number
             ]

        line [
                 ou.code,
                 ou.legal_address_guid,
                 'Юридический',
                 ou.legal_postcode,
                 ou.legal_human_settlement_type_code,
                 ou.legal_human_settlement_type,
                 ou.legal_settlement_name,
                 Ou::STREET_TYPES[ou.legal_street_type_code],
                 ou.legal_street_name,
                 Ou::BUILDING_TYPES[ou.legal_building_type_code],
                 ou.legal_building_number
             ]
      end
    end

  end
end
