#coding: utf-8
namespace :export_2014 do
  task :users => :export_tools do

    line 'login', 'email', 'password'

    ActiveRecord::Base.uncached do
      User.where(:target_type => %w(Ou Mouo)).find_each do |item|
        line [
                 item.login,
                 item.email,
                 item.encrypted_password
             ]
      end
    end

  end
end
