#coding: utf-8
namespace :export do
  task :rlog_items, [:begin_date, :model_name] => :export_tools do |t, args|
    include RlogItemsHelper

    # Arguments

    # begin_date
    #   format: dd-mm-yyyy
    #   example: '31-12-2010'

    where_params = {}
    where_params[:record_type] = args[:model_name].camelize if args[:model_name]
    created_at_condition = args[:begin_date] ? "created_at > ?" : nil

    line(
        'ID',
        I18n.t('activerecord.attributes.rlog_item.ip'),
        I18n.t('activerecord.attributes.rlog_item.user'),
        I18n.t('activerecord.attributes.rlog_item.record'),
        I18n.t('activerecord.attributes.rlog_item.record_changes'),
        I18n.t('activerecord.attributes.rlog_item.created_at')
    )

    ActiveRecord::Base.uncached do
      relation = RlogItem.where(where_params)
      relation = relation.where(created_at_condition, Date.parse(args[:begin_date])) if created_at_condition

      relation.find_each do |item|

        changes = ''

        if item.record_changes.is_a? Hash
          item.record_changes.each do |k, v|
            changes += I18n.t("activerecord.attributes.#{item.record_type.underscore}.#{k}") + ': from '
            changes += (value_conversion k, v[0], item).to_s
            changes += ' to '
            changes += (value_conversion k, v[1], item).to_s
          end
        end
        changes = changes.gsub(/\(/, '[').gsub(/\)/, ']').gsub('=', '').gsub('>', '')

        line(
            item.id,
            item.ip,
            item.user,
            item.record,
            changes,
            item.created_at
        )
      end
    end

  end
end
