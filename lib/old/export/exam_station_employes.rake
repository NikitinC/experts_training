#coding: utf-8
namespace :export do
  task exam_station_employees: :export_tools do

    line "Тип экзамена",
         "Дата экзамена",
         "Предмет",
         "Код МОУО (ППЭ)",
         "Нименование МОУО (ППЭ)",
         "Код ОУ (Основное место работы)",
         "Наименование ОУ (Основное место работы)",
         "Код ППЭ",
         "Фамилия",
         "Имя",
         "Отчество",
         "Паспортные данные",
         "Должность в ОУ (Основное место работы)",
         "Должность в ППЭ"

    ActiveRecord::Base.uncached do
      ExamStationEmployee.find_each do |ese|
        next if ese.exam_station.blank? || ese.employee.blank?

        line(
            ese.exam_station.exam.try(:exam_type),
            ese.exam_station.exam.try(:date),
            ese.exam_station.exam.try(:subject),
            ese.exam_station.station.try(:ou).try(:mouo).try(:code),
            ese.exam_station.station.try(:ou).try(:mouo).try(:name),
            ese.employee.main_ou.try(:code),
            ese.employee.main_ou.try(:short_name),
            ese.exam_station.station.try(:code),
            ese.employee.second_name,
            ese.employee.first_name,
            ese.employee.middle_name,
            "#{ese.employee.document_series} #{ese.employee.document_number}",
            ese.employee.main_employee_ou.try(:employee_post),
            ese.station_post
        )
      end
    end
  end
end