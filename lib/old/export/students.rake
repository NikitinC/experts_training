#coding: utf-8
namespace :export do

  # Arguments
  #   min_group
  #     - minimal group for a getting students from the database
  task :students, [:min_group] => :export_tools do |t, args|

    group_condition = args[:min_group] ? "groups.number > #{args[:min_group].to_i}" : nil

    app_helper = Class.new.extend ApplicationHelper

    line(
        I18n.t("students.tables_headers.is_valid"),
        I18n.t("students.tables_headers.ou_code"),
        I18n.t("students.tables_headers.ou_name"),
        I18n.t("students.tables_headers.group"),
        I18n.t("students.tables_headers.fio"),
        I18n.t("students.tables_headers.document"),
        I18n.t("activerecord.attributes.student.document_where"),
        I18n.t("activerecord.attributes.student.document_type"),
        I18n.t("students.tables_headers.document_scan_present"),
        I18n.t("activerecord.attributes.student.self_registration"),
        I18n.t("activerecord.attributes.student.phone"),
        I18n.t("activerecord.attributes.student.gender"),
        I18n.t("activerecord.attributes.student.citizenship"),
        I18n.t("activerecord.attributes.student.birthday"),
        I18n.t("activerecord.attributes.student.ege_participant_category"),
        I18n.t("activerecord.attributes.student.attestation_form"),
        I18n.t("activerecord.attributes.student.study_form"),
        I18n.t("activerecord.attributes.student.education_document_number"),
        I18n.t("students.tables_headers.education_scan_present"),
        I18n.t("activerecord.attributes.student.special_seating"),
        I18n.t("activerecord.attributes.student.main_period_obstacle_code"),
        I18n.t("activerecord.attributes.student.limited_facilities_group_code"),
        I18n.t("students.tables_headers.disabilities_scan_present"),
        I18n.t("activerecord.attributes.student.closed_institution"),
        I18n.t("activerecord.attributes.student.migrant"),
        I18n.t("activerecord.attributes.student.foreign_middle_education"),
        I18n.t("activerecord.attributes.student.graduate_of_other_ou"),
        I18n.t("activerecord.attributes.student.region_code"),
        I18n.t("activerecord.attributes.student.gia_active_results_math"),
        I18n.t("activerecord.attributes.student.gia_active_results_rus"),
        I18n.t("activerecord.attributes.student.gia_admission"),
        I18n.t("students.tables_headers.exam"),
        I18n.t("activerecord.models.exam_type"),
        I18n.t("students.tables_headers.station")
    )

    ActiveRecord::Base.uncached do
      relation = Student
      relation = relation.includes(:group).where group_condition if group_condition

      relation.find_each do |student|
        exam_students = student.exam_students.to_a
        exam_students = [nil] if exam_students.blank?
        exam_students.each do |exam_student|
          line(
              app_helper.bool_to_word(student.valid_cache),
              student.group.try(:ou).try(:code),
              student.group.try(:ou).try(:to_s),
              student.group,
              "#{student.second_name} #{student.first_name} #{student.middle_name}",
              "#{student.document_series} #{student.document_number}",
              student.document_where,
              student.document_type,
              app_helper.bool_to_word(student.document_scan.present?),
              app_helper.bool_to_word(student.self_registration),
              student.phone,
              app_helper.bool_to_word(student.gender, :gender),
              student.citizenship,
              student.birthday && Russian::l(student.birthday),
              student.ege_participant_category,
              student.attestation_form,
              student.study_form,
              student.education_document_number,
              app_helper.bool_to_word(student.education_scan.present?),
              app_helper.bool_to_word(student.special_seating),
              student.limited_facilities_group,
              app_helper.bool_to_word(student.disabilities_scan.present?),
              app_helper.bool_to_word(student.closed_institution),
              app_helper.bool_to_word(student.migrant),
              app_helper.bool_to_word(student.foreign_middle_education),
              app_helper.bool_to_word(student.graduate_of_other_ou),
              student.region,
              app_helper.bool_to_word(student.gia_active_results_math),
              app_helper.bool_to_word(student.gia_active_results_rus),
              app_helper.bool_to_word(student.gia_admission),
              exam_student.try(:exam),
              exam_student.try(:exam).try(:exam_type),
              exam_student.try(:exam_station).try(:station)
          )
        end
      end

    end
  end

end
