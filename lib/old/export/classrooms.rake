#coding: utf-8
namespace :export do
  task classrooms: :export_tools do
    app_helper = Class.new.extend ApplicationHelper

    line(
        'Код ППЭ',
        'Название ППЭ',
        'Адрес ППЭ',
        'Название',
        'Короткое название',
        'Номер',
        'Количество рядов',
        'Количество мест в ряду',
        'Доступных мест',
        'Вместимость для устной части',
        'Видеонаблюдение',
        'Расположение парт в аудитории',
        'Признак аудитории для специализированной рассадки',
        'RID ПАК',
        'ПАК',
        'СКУП',
        'IP',
        'Скан ПАК'
    )

    ActiveRecord::Base.uncached do
      Station.find_each do |station|
        line(
            station.code,
            station.name,
            station.address_to_s,
            '',
            '',
            'Штаб',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            station.rid_pak,
            station.pak,
            station.skup,
            station.ip,
            station.pak_scan ? "https://ege3.irro.ru/#{station.pak_scan.url}" : '-'
        )

        station.classrooms.find_each do |classroom|
          next if classroom.pak.nil?

          line(
              station.code,
              station.name,
              station.address_to_s,
              classroom.name,
              classroom.short_name,
              classroom.number,
              classroom.rows_count,
              classroom.seats_per_row,
              classroom.available_seats,
              classroom.capacity_for_verbal_part,
              classroom.cctv,
              app_helper.bool_to_word(classroom.organizer_order, :organizer),
              app_helper.bool_to_word(classroom.special_sit, :sit),
              classroom.rid_pak,
              classroom.pak,
              classroom.skup,
              classroom.ip,
              classroom.pak_scan ? "https://ege3.irro.ru/#{classroom.pak_scan.url}" : '-'
          )

        end
      end
    end

  end
end
