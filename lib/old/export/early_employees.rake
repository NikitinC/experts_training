#coding: utf-8
namespace :export do
  task :early_employees => :export_tools do

    line "ФИО",
         "Паспортные данные",
         "ППЭ",
         "Экзамен",
         "Должность в ППЭ"

    ActiveRecord::Base.uncached do
      ExamStationEmployee.includes(:exam_station => :exam).where(:exams => {:exam_type_id => 4}).find_each do |ese|
        line ese.employee.try(:full_name),
             "#{ese.employee.try(:document_series)} #{ese.employee.try(:document_number)}",
             ese.exam_station.try(:station),
             ese.exam_station.try(:exam),
             ese.station_post
      end
    end

  end
end