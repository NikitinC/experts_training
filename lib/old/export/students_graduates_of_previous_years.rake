#coding: utf-8
namespace :export do
  task students_graduates_of_previous_years: :export_tools do

    EXAM_TYPE_ID = 6

    line(
        'Категория выпускника',
        'Фамилия',
        'Имя',
        'Отчество',
        'Серия документа',
        'Номер документа',
        'Предмет',
        'Дата экзамена',
    )

    ActiveRecord::Base.uncached do
      ExamStudent.includes(:student).includes(:exam).where(exams: {exam_type_id: EXAM_TYPE_ID}).find_each do |es|
        student = es.student

        line(
            student.ege_participant_category,
            student.second_name,
            student.first_name,
            student.middle_name,
            student.document_series,
            student.document_number,
            es.exam.try(:subject),
            es.exam.try(:date),
        )

      end
    end
  end

end
