#coding: utf-8
namespace :export do
  task :invalid_ou => :export_tools do

    line "Код МОУО",
         "Название МОУО",
         "Код ОУ",
         "ОУ",
         "Неверно заполнен сам объект",
         "Сотрудники",
         "Неверно заполненных классов",
         (1..99).to_a.join(";")

    ActiveRecord::Base.uncached do

      Mouo.all.each do |mouo|
        line "", "", "", mouo
        mouo.ous.each do |ou|
          groups = ou.groups.where(:valid_cache => false).size
          all_groups = ou.groups.size
          invalid_student_sizes = ou.students.reorder("groups.number").where(:valid_cache => false).includes(:group).group("groups.number").size
          all_student_sizes = ou.students.reorder("groups.number").includes(:group).group("groups.number").size
          invalid_students = (1..99).map do |i|
            if all_student_sizes[i.to_s].nil? && ou.groups.where(:number => i).size == 0
              "-"
            else
              "#{invalid_student_sizes[i.to_s] || 0} из #{all_student_sizes[i.to_s] || 0}"
            end
          end
          employees = ou.employees.where(:valid_cache => false).size
          all_employees = ou.employees.size

          line mouo.code,
               mouo.name,
               ou.code,
               ou,
               ou.valid_cache ? 0 : 1,
               "#{employees} из #{all_employees}",
               "#{groups} из #{all_groups}",
               invalid_students.join(";")
        end
      end
    end
  end
end
