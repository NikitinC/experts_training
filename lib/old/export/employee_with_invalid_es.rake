#coding: utf-8
namespace :export do
  task :employee_with_invalid_es => :export_tools do

    line "ФИО",
         "Данные документа",
         "ОУ",
         "Телефон ответственного за информационный обмен в ОУ",
         "МОУО"
         "Телефон ответственного за информационный обмен в МОУО"
    ActiveRecord::Base.uncached do
      ready_employee = []
      ExamStationEmployee.find_each do |ese|
        unless ese.valid?
          employee = ese.try(:employee)
          if employee && !ready_employee.include?(employee.id)
            ready_employee << employee.id
            line "#{employee.second_name} #{employee.first_name} #{employee.middle_name}",
                 "#{employee.document_series} #{employee.document_number}",
                 employee.main_ou,
                 employee.main_ou.try(:information_exchange_phone),
                 employee.main_ou.try(:mouo),
                 employee.main_ou.try(:mouo).try(:information_exchange_specialist_phone)
          end
        end
      end
    end
  end
end