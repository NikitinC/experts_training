#coding: utf-8

namespace :export do
  task :invalid_exam_students => :export_tools do

    line "ученик",
         "паспортные данные",
         "примечания"

    ActiveRecord::Base.uncached do
      size = Student.count
      count = 0
      Student.find_each do |student|
        count += 1
        #puts "progress #{count}/#{size}"
        next if student.exam_students.count < 2

        student.exam_students.each do |exam_student|
          unless exam_student.exam
            line student.full_name,
                 "#{student.document_series} #{student.document_number}",
                 "Ученик назначен на экзамен, но экзамен не выбран (невалидная связка)"
            next
          end

          es_count = ExamStudent.includes(:exam).where(:student_id => student.id,
                                                       :exams => {:date => exam_student.exam.date}).count
          line [
                   student.full_name,
                   "#{student.document_series} #{student.document_number}",
                   "Ученик назначен на 2 экзамена в один день"
               ] if es_count > 1


        end


      end
    end

  end
end