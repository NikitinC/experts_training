#coding: utf-8
namespace :export do
  task :prepare => :export_tools do
  end

  task :practice_exam_ict_stations => :prepare do
    @exam_type_id = 3
    Rake::Task['export:export_station_by_exam'].execute
  end

  task :practice_exam_english_stations => :prepare do
    @exam_type_id = 9
    Rake::Task['export:export_station_by_exam'].execute
  end

  task :practice_exam_ict_employees => :prepare do
    @exam_type_id = 3
    Rake::Task['export:export_employee_by_exam'].execute
  end

  task :practice_exam_english_employees => :prepare do
    @exam_type_id = 9
    Rake::Task['export:export_employee_by_exam'].execute
  end

  task :export_station_by_exam do
    line('№ п/п',
         'Код МОУО',
         'Название МОУО',
         'Код ОУ',
         'Наименование ОУ',
         'Код ППЭ',
         'ФИО руководитель ППЭ')

    ActiveRecord::Base.uncached do
      i = 0
      Station.joins(:exams).where(:exams => {:exam_type_id => @exam_type_id}).uniq.find_each do |station|
        i += 1
        line(i,
             station.ou.try(:mouo).try(:code),
             station.ou.try(:mouo).try(:name),
             station.ou.try(:code),
             station.ou.try(:full_name),
             station.code,
             station.chief_fio)
      end
    end
  end

  task :export_employee_by_exam do
    line('№ п/п', 'Код МОУО', 'Наименование МОУО', 'Код ППЭ', 'Должность в ППЭ', 'ФИО')

    ActiveRecord::Base.uncached do
      i = 0
      ExamStationEmployee.joins(:exam_station => :exam).where(:exams => {:exam_type_id => @exam_type_id}).find_each do |ese|
        i += 1
        line(i,
             ese.exam_station.try(:station).try(:ou).try(:mouo).try(:code),
             ese.exam_station.try(:station).try(:ou).try(:mouo).try(:name),
             ese.exam_station.try(:station).try(:code),
             ese.station_post,
             ese.employee.try(:full_name))
      end
    end
  end
end