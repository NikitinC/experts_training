namespace :export do
  task :export_tools => :environment do
    include ExportHelper
  end
end

module ExportHelper
  def line *args
    puts args.join(";")
  end
end