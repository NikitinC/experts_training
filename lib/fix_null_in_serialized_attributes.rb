module ActiveRecord
  module Coders
    class YAMLColumn

      def dump(obj)
        YAML.dump(obj) unless obj.blank?
      end
    end
  end
end