module DistinctPaginate
  def self.included base
    base.class_exec do
      extend ClassMethods
    end
  end

  module ClassMethods
    def distinct_paginate(page, per_page)
      self.paginate page: page, per_page: per_page, count: {select: "distinct #{self.table_name}.id"}
    end
  end

end