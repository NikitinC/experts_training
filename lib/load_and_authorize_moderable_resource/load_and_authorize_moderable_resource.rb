module  LoadAndAuthorizeModerableResource
  def self.included base
    base.send :extend, ClassMethods


  end

  module ClassMethods
    def load_and_authorize_moderable_resource(options = {})
      load_and_authorize_resource find_by: :find_on_moderable_by_id, only: :update
      load_and_authorize_resource except: [:update].push(options[:except]).flatten()
    end
  end
end