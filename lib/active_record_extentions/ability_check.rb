module AbilityCheck
  def self.included base
    base.class_exec do
      after :save, :ability_check
      extend ClassMethods
    end
  end

  module ClassMethods
    def ability_check
      a = Ability.new(current_user)
      self.destroy a.can? :update, self
    end
  end

end