module ActiveRecordExtentions
  def self.included base
    base.class_exec do
      extend ClassMethods
    end
  end

  module ClassMethods
    def like_search str, field_name
      field_name = [*field_name].join(" || ' ' || ")
      arr = str.split("\s").compact
      perms = arr.permutation.to_a
      query = perms.map do |perm|
        "(#{field_name} ILIKE '%" + perm.join("% %") + "%')"
      end.join("OR")
      self.where(query)
    end

    def sample
      count = self.count
      self.offset(rand(count)).limit(1).first
    end
  end

end